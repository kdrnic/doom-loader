OBJDIR=obj
CFLAGS2=$(CFLAGS)

ifeq (,$(findstring djgpp,$(CC)))
	CFLAGS2+=-Wl,--stack,10485760
else
	OBJDIR:=$(OBJDIR)dos
endif

ifneq ($(filter fast,$(MAKECMDGOALS)),fast)
	CFLAGS2+=-ffp-contract=off -DDBG_FACE_SPLIT
endif

ifeq ($(filter debug,$(MAKECMDGOALS)),debug)
	OBJDIR:=$(OBJDIR)dbg
	CFLAGS2+=-g -DDEBUG
	BINNAME:=$(BINNAME)_dbg
endif

ifeq ($(filter profile,$(MAKECMDGOALS)),profile)
	CFLAGS2+=-gdwarf-2 -fno-omit-frame-pointer -O2
endif

ifeq ($(filter regular,$(MAKECMDGOALS)),regular)
	CFLAGS2+=-O3
endif

ifeq ($(filter fast,$(MAKECMDGOALS)),fast)
	CFLAGS2+=-gdwarf-2 -fno-omit-frame-pointer -O3 -march=sandybridge
endif

CFLAGS2+=-Wall -Wuninitialized -Werror=implicit-function-declaration -Wno-unused -fplan9-extensions
CPPFLAGS2=$(CFLAGS2) -Wno-reorder

BINNAME=game$(BINSUFFIX)

ifeq ($(origin CC),default)
	ifeq ($(OS),Windows_NT)
		CC = gcc
		CXX = g++
	endif
endif

ifeq ($(CC),clang)
	ifeq ($(OS),Windows_NT)
		CFLAGS2+=-target i686-pc-windows-gnu
	endif
endif

C_FILES=$(wildcard src/*.c)
C_OBJECTS=$(patsubst src/%,$(OBJDIR)/%,$(patsubst %.c,%.o,$(C_FILES)))
OBJECTS=$(C_OBJECTS)

HAVE_LIBS=

INCLUDE_PATHS=
LINK_PATHS=

ifeq ($(OS),Windows_NT)
	INCLUDE_PATHS+=-I$(ALLEGRO_PATH)include
	LINK_PATHS+=-L$(ALLEGRO_PATH)lib
endif

HEADER_FILES=$(wildcard src/*.h)

$(OBJDIR)/%.o: src/%.c $(HEADER_FILES)
	$(CC) $(INCLUDE_PATHS) $(CFLAGS2) $< -c -o $@

regular: $(BINNAME).exe

clean:
	rm -f $(OBJDIR)/*.o
	rm -f $(BINDIR)*.exe

$(OBJDIR):
	mkdir $(OBJDIR)

$(BINNAME).exe: $(OBJDIR) $(OBJECTS)
	$(CC) $(LINK_PATHS) $(LINK_FLAGS) $(CPPFLAGS2) $(OBJECTS) -o $(BINNAME).exe $(HAVE_LIBS) -lalleg -lm

debug: $(BINNAME).exe
profile: regular
fast: regular
