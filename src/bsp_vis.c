//BSP visibility set compilation functions
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "bsp.h"
#include "rdebug.h"
#include "arr.h"

//If included, the WAD_H define will trigger certain codepaths
#include "wad.h"

//Provides MATRIX_f
#include <allegro.h>

typedef struct bspportal
{
	bspvert verts[RVFACE_MAX_VERTS];
	int num_verts;
	
	int leafs[2];
	int num_leafs;
	
	int plane;
} bspportal;

typedef arr_t(bspportal *) arr_portalp;

//Only these 2 functions create new portals
static bspportal *bspnode2portal(bspdata *bsp, const bspnode *node)
{
	bspportal *port = calloc(1, sizeof(*port));
	mplane *plane = &bsp->planes[node->plane];
	
	port->plane = node->plane;
	port->num_leafs = 0;
	
	rvface_boxslice(plane, &node->min, &node->max, (rvface *) port);
	
	return port;
}

static void split_portal(const mplane *plane, const bspportal * const portal, bspportal *front, bspportal *back)
{
	bspportal tmp;
	
	if(!front) front = &tmp;
	if(!back) back = &tmp;
	
	*front = *portal;
	*back = *portal;
	
	rvface_split(plane, (const rvface *) portal, (rvface *) front, (rvface *) back);
}

/*
NOTE: previously spurious portals were generated in the "simple" dataset, etc.
Those mostly divided a single face of a volumeless leaf, not very useful
They were found to have two causes:
-Too tight limits in mplane_equal
-Blender breaking face coplanarity with some operations
*/
static arr_portalp clip_portal(bspdata *bsp, int node_i, bspportal *portal)
{
	const bspnode *node = &bsp->nodes[node_i];
	const int front_child = node->children[BSPNODE_CHILD_FRONT];
	const int back_child = node->children[BSPNODE_CHILD_BACK];
	const int is_front_leaf = BSPNODE_ISLEAF(front_child);
	const int is_back_leaf = BSPNODE_ISLEAF(back_child);
	const int node_plane_i = node->plane;
	const mplane *node_plane = &bsp->planes[node_plane_i];
	const int sort = rvface_sort(node_plane, (rvface *) portal);
	
	#define arr_portalp_of(item)({\
		arr_portalp _tmp;\
		\
		_tmp.is_static = 0;\
		_tmp.len = 1;\
		_tmp.siz = 1;\
		_tmp.data = malloc(sizeof(_tmp.data[0]));\
		_tmp.data[0] = item;\
		\
		_tmp;\
	})
	
	if(sort == RVFACE_FRONT){
		if(!is_front_leaf){
			return clip_portal(bsp, front_child, portal);
		}
		else{
			assert(portal->num_leafs < 2);
			portal->leafs[portal->num_leafs++] = BSPNODE_TO_LEAF(front_child);
			
			//return *((arr_portalp *) arr_of(portal));
			return arr_portalp_of(portal);
		}
	}
	else if(sort == RVFACE_BACK){
		if(!is_back_leaf){
			return clip_portal(bsp, back_child, portal);
		}
		else{
			//This change needed introducing to fix visibility in Doom maps
			#ifdef WAD_H
			assert(portal->num_leafs < 2);
			portal->leafs[portal->num_leafs++] = BSPNODE_TO_LEAF(back_child);
			
			//return *((arr_portalp *) arr_of(portal));
			return arr_portalp_of(portal);
			#else
			free(portal);
			arr_portalp tmp = arr_empty;
			return tmp;
			#endif
		}
	}
	else if(sort == RVFACE_COPLANAR){
		int i;
		arr_portalp back_list = arr_empty, front_list = arr_empty;
		
		if(!is_front_leaf){
			front_list = clip_portal(bsp, front_child, portal);
		}
		else{
			assert(portal->num_leafs < 2);
			portal->leafs[portal->num_leafs++] = BSPNODE_TO_LEAF(front_child);
			//front_list = *((arr_portalp *) arr_of(portal));
			front_list = arr_portalp_of(portal);
		}
		if(!front_list.len) return front_list;
		if(is_back_leaf){
			//This change also needed introducing to fix visibility in Doom maps
			#ifdef WAD_H
			arr_portalp nleaf_overflow_list = arr_empty;
			for(i = 0; i < front_list.len; i++){
				bspportal *portal = front_list.data[i];
				if(portal->num_leafs == 0 || portal->leafs[0] != BSPNODE_TO_LEAF(back_child)){
					//assert(portal->num_leafs < 2);
					if(portal->num_leafs == 2){
						bspportal *portal_cpy = malloc(sizeof(*portal));
						*portal_cpy = *portal;
						portal_cpy->leafs[0] = BSPNODE_TO_LEAF(back_child);
						arr_push(&nleaf_overflow_list, portal_cpy);
						portal->num_leafs--;
						
						#if 1
						rdebug_rvface((rvface *) portal);
						rvert midp;
						vec3 leafmidp[3];
						rvface_midpoint_area((rvface *) portal, &midp, 0);
						int leafis[3] = {portal->leafs[0], portal->leafs[1], BSPNODE_TO_LEAF(back_child)};
						for(int j = 0; j < 3; j++){
							vec3 leafmidp;
							bsp_leafmidpoint(bsp, leafis[j], &leafmidp);
							rdebug_arrow(&midp.pos, &leafmidp);
							rdebug_printf(&leafmidp, "%d %d", j, leafis[j]);
						}
						#endif
					}
					portal->leafs[portal->num_leafs++] = BSPNODE_TO_LEAF(back_child);
				}
			}
			arr_cat(&back_list, &nleaf_overflow_list);
			#endif
			return front_list;
		}
		
		for(i = 0; i < front_list.len; i++){
			arr_portalp tmp = clip_portal(bsp, back_child, front_list.data[i]);
			arr_cat(&back_list, &tmp);
			arr_free(&tmp);
		}
		
		arr_free(&front_list);
		
		return back_list;
	}
	else if(sort == RVFACE_SPANNING){
		bspportal *back_portal = malloc(sizeof(*back_portal));
		bspportal *front_portal = malloc(sizeof(*front_portal));
		arr_portalp back_list = arr_empty, front_list = arr_empty, join_list = arr_empty;
		
		split_portal(node_plane, portal, front_portal, back_portal);
		free(portal);
		
		back_list = clip_portal(bsp, node_i, back_portal);
		front_list = clip_portal(bsp, node_i, front_portal);
		
		arr_join(&back_list, &front_list, &join_list);
		arr_free(&back_list);
		arr_free(&front_list);
		
		return join_list;
	}
	arr_portalp tmp = arr_empty;
	return tmp;
}

//Just a collection of planes
typedef struct antipenumbra
{
	mplane planes[RVFACE_MAX_VERTS * RVFACE_MAX_VERTS * 2 + 2];
	int num;
} antipenumbra;

//Function to create an antipenumbra
//Move into vector lib?
static void generate_ap(antipenumbra *ap, const rvface * const src, const rvface * const dest)
{
	const rvface *face_a = src, *face_b = dest;
	int i, j, pass;
	
	for(pass = 0; pass < 2; pass++){
		for(i = 0; i < face_a->num_verts; i++){
			int o = (i + 1) % face_a->num_verts;
			const vec3 *e1 = &face_a->verts[i].pos;
			const vec3 *e2 = &face_a->verts[o].pos;
			
			for(j = 0; j < face_b->num_verts; j++){
				const vec3 *p = &face_b->verts[j].pos;
				mplane pl;
				int sortsrc, sortdest;
				
				vec3_verts2plane(e1, e2, p, &pl);
				
				sortsrc = rvface_sort(&pl, src);
				sortdest = rvface_sort(&pl, dest);
				
				if(sortsrc == sortdest) continue;
				//TODO: Investigate this test
				//< RVFACE_FRONT tends to cause crashes
				//< RVFACE_COPLANAR tends to fix the issue in gameerr2.sav
				if(sortsrc < RVFACE_COPLANAR) continue;
				
				//There are cases where the generated plane is spanning
				//Occurs often with two perpendicular portals
				//TODO: investigate whether RVFACE_COPLANAR should be allowed
				//Apparently it should not, see gameerr1.sav, which errs with it
				if(sortdest < RVFACE_FRONT) continue;
				
				if(sortdest == RVFACE_BACK){
					mplane_flip(&pl);
				}
				
				//Found to improve performance only modestly            
				#if 1
				int k;
				//Skip planes already in
				for(k = 0; k < ap->num; k++){
					if(mplane_equal(&pl, ap->planes + k)) break;
				}
				if(k < ap->num) continue;
				#endif
				
				assert(ap->num < sizeof(ap->planes) / sizeof(ap->planes[0]) - 1);
				ap->planes[ap->num++] = pl;
			}
		}
		
		face_a = dest;
		face_b = src;
	}
}

static int ap_to_mdl(const antipenumbra *ap, bspmdl *m)
{
	int i, j;
	const vec3 box_verts[8] = VEC3_BOXVERTS_MM(m->min, m->max);
	
	//Check all 8 verts
	//If at least 1 vert is inside the antipenumbra, then the box is visible
	for(j = 0; j < 8; j++){
		//Ignore first 2 planes:
		//They are the source and dest portal planes
		for(i = 2; i < ap->num; i++){
			if(!vec3_planecmp(ap->planes + i, box_verts + j)) break;
		}
		if(i == ap->num) return 1;
	}
	
	return 0;
}

static int rpdbg = 0;

/*
TODO: improve this?
-make non-recursive
-arguments in struct
*/
static void recurse_pvs(
	const arr_portalp * const leaf_portals,
	const bspdata * const bsp,
	const bspportal * const srcportal,
	const int destleaf_i,
	bspportal *destportal,
	unsigned char * const pvs
)
{
	int i, j, k;
	antipenumbra ap;
	const arr_portalp destleaf_portals = leaf_portals[destleaf_i];
	mplane srcplane = bsp->planes[srcportal->plane], destplane = bsp->planes[destportal->plane];
	
	if(rvface_sort(&destplane, (rvface *) srcportal) == RVFACE_FRONT){
		mplane_flip(&destplane);
	}
	
	if(rvface_sort(&destplane, (rvface *) srcportal) == RVFACE_SPANNING){
		bspportal front, back;
		split_portal(&destplane, srcportal, &front, &back);
		recurse_pvs(leaf_portals, bsp, &front, destleaf_i, destportal, pvs);
		recurse_pvs(leaf_portals, bsp, &back, destleaf_i, destportal, pvs);
		return;
	}
	
	if(rvface_sort(&srcplane, (rvface *) destportal) == RVFACE_BACK){
		mplane_flip(&srcplane);
	}
	
	if(rvface_sort(&srcplane, (rvface *) destportal) == RVFACE_SPANNING){
		bspportal front, back;
		split_portal(&srcplane, destportal, &front, &back);
		recurse_pvs(leaf_portals, bsp, srcportal, destleaf_i, &front, pvs);
		recurse_pvs(leaf_portals, bsp, srcportal, destleaf_i, &back, pvs);
		return;
	}
	
	ap.num = 0;
	ap.planes[ap.num++] = destplane;
	ap.planes[ap.num++] = srcplane;
	generate_ap(&ap, (const rvface *) srcportal, (const rvface *) destportal);
	
	//Add leaf to PVS
	pvs[destleaf_i] = 1;
	
	//Test all mdls for pvs inclusion
	for(i = 0; i < bsp->num_mdls; i++){
		bspmdl *m = bsp->mdls + i;
		antipenumbra ap2 = ap;
		
		//Check if either the source or dest portal leads to one of the model's leafs
		for(j = 0; m->leafs[j] >= 0; j++){
			const int l = m->leafs[j], *sl = srcportal->leafs, *dl = destportal->leafs;
			if(l == sl[0] || l == sl[1]){
				//Since antipenumbra is guaranteed to contain destportal and not srcportal,
				//if the model is in a srcportal leaf we must flip the antipenumbra's planes
				for(k = 2; k < ap2.num; k++){
					mplane_flip(ap2.planes + k);
				}
				break;
			}
			if(l == dl[0] || l == dl[1]) break;
		}
		if(m->leafs[j] < 0) continue;
		
		//Performs the model BB <-> antipenumbra check
		if(ap_to_mdl(&ap2, m)){
			pvs[bsp->num_leafs + i] = 1;
		}
	}
	
	for(i = 0; i < destleaf_portals.len; i++){
		bspportal *p_ = (bspportal *) destleaf_portals.data[i];
		bspportal p = *p_;
		int new_destleaf = p.leafs[p.leafs[0] == destleaf_i];
		
		//Ignore coplanar portals
		if(p.plane == destportal->plane) continue;
		//Ignore the same portal that went in
		if(p_ == destportal) continue;
		if(p.leafs[0] == destportal->leafs[0] && p.leafs[1] == destportal->leafs[1]) continue;
		
		//Clip portal by ap
		for(j = 0; j < ap.num; j++){
			const mplane *pl = &ap.planes[j];
			int s = rvface_sort(pl, (rvface *) &p);
			
			if(s == RVFACE_BACK){
				break;
			}
			//TODO: verify validity of break below
			if(s == RVFACE_COPLANAR){
				break;
			}
			if(s == RVFACE_SPANNING){
				bspportal clipped;
				split_portal(pl, &p, &clipped, 0);
				p = clipped;
			}
		}
		
		if(j < ap.num){
			//portal was not visible
			continue;
		}
		
		//then recurse
		recurse_pvs(leaf_portals, bsp, srcportal, new_destleaf, &p, pvs);
	}
}

static void build_pvs(bspdata *bsp, arr_portalp *leaf_portals)
{
	int leafi, spi, dpi, i, j;
	unsigned char *pvs;
	const int pvs_bytes = sizeof(*pvs) * (bsp->num_leafs + bsp->num_mdls);
	arr_int bsp_pvs = arr_empty;
	pvs = malloc(pvs_bytes);
	
	for(leafi = 0; leafi < bsp->num_leafs; leafi++){
		#ifndef __DJGPP__
		printf("%d\t", leafi);
		#endif
		
		int curr;
		arr_portalp srcleaf_portals = leaf_portals[leafi];
		
		memset(pvs, 0, pvs_bytes);
		
		rpdbg = (leafi == 132);
		
		for(spi = 0; spi < srcleaf_portals.len; spi++){
			bspportal *srcportal = srcleaf_portals.data[spi];
			int destleaf_i = srcportal->leafs[srcportal->leafs[0] == leafi];
			arr_portalp destleaf_portals = leaf_portals[destleaf_i];
			
			pvs[destleaf_i] = 1;
			
			for(i = 0; i < bsp->num_mdls; i++){
				bspmdl *m = bsp->mdls + i;
				
				for(j = 0; m->leafs[j] >= 0; j++){
					if(m->leafs[j] == leafi || m->leafs[j] == destleaf_i) break;
				}
				if(m->leafs[j] < 0) continue;
				
				pvs[bsp->num_leafs + i] = 1; 
			}
			
			for(dpi = 0; dpi < destleaf_portals.len; dpi++){
				bspportal *destportal = destleaf_portals.data[dpi];
				int genleaf_i = destportal->leafs[destportal->leafs[0] == destleaf_i];
				
				if(destportal == srcportal) continue;
				
				recurse_pvs(
					leaf_portals,
					bsp,
					(bspportal *) srcportal,
					genleaf_i,
					(bspportal *) destportal,
					pvs
				);
			}
		}
		
		//Write RLE PVS
		bsp->leafs[leafi].pvs = bsp_pvs.len;
		curr = 0;
		for(i = 0; i < pvs_bytes / sizeof(*pvs); i++){
			if(pvs[i]){
				arr_push(&bsp_pvs, i - curr);
				curr = i;
			}
		}
		arr_push(&bsp_pvs, -1);
	}
	
	arr_unreserve(&bsp_pvs);
	bsp->pvs = bsp_pvs.data;
	bsp->pvs_length = bsp_pvs.len;
	
	free(pvs);
}

int build_bsp_vis(bspdata *bsp)
{
	int i, j, k, l;
	int bad;
	arr_portalp big_pl = arr_empty;
	arr_portalp *leaf_portals = malloc(sizeof(*leaf_portals) * bsp->num_leafs);
	int error = 0;
	
	assert(!bsp->pvs);
	
	//Build portals
	for(i = 0; i < bsp->num_nodes; i++){
		const bspnode *node = &bsp->nodes[i];
		bspportal *portal = bspnode2portal(bsp, node);
		arr_portalp pl;
		
		//Skip orphaned nodes
		if(i){
			for(j = 0; j < bsp->num_nodes; j++){
				const bspnode *parent = bsp->nodes + j;
				for(k = 0; k < 2; k++){
					if(parent->children[k] == i){
						break;
					}
				}
				if(k < 2) break;
			}
			if(j == bsp->num_nodes){
				printf("Skipping orphaned node %d, j %d k %d\n", i, j, k);
				continue;
			}
		}
		
		/*
		NOTE: Previously, here was uncovered an issue with BSP generation.
		Nodes dividing a single face from solidness were seem to be generated.
		This was solved after increasing robustness of same-planeness-comparisons.
		*/
		if((bad = rvface_isbad((rvface *) portal))){
			printf("Bad portal generated type %d\n", bad);
			rdebug_set_colour(0xFF0000);
			rdebug_rvface((rvface *) portal);
			continue;
		}
		
		//Clip portals, also adds leaf information
		pl = clip_portal(bsp, 0, portal);
		if(!pl.len) continue;
		
		//Filter out portals not connecting leafs
		for(j = 0; j < pl.len; j++){
			bspportal *p = pl.data[j];
			if(p->num_leafs < 2){
				free(p);
				pl.data[j] = 0;
				continue;
			}
			
			//Allows for quicker comparisons
			if(p->leafs[1] > p->leafs[0]){
				int tmp = p->leafs[0];
				p->leafs[0] = p->leafs[1];
				p->leafs[1] = tmp;
			}
		}
		
		arr_sieve(&pl, el, idx, el);
		
		arr_cat(&big_pl, &pl);
		arr_free(&pl);
	}
	
	//Debug info for portal planes
	#if 0
	for(i = 0; i < big_pl.len; i++){
		rvert midp;
		const bspportal *portal = big_pl.data[i];
		rvface_midpoint_area((rvface *) portal, &midp, 0);
		rdebug_set_colour(makecol(255, 255, 255));
		rdebug_printf(&midp.pos, "%d", portal->plane);
		rdebug_rvface((rvface *) portal);
	}
	#endif
	
	//Clip portals by leaf faces
	//A hack necessary for Doom levels, due to not having the BSP_SOLID leafs
	#ifdef WAD_H
	for(j = 0; j < big_pl.len; j++){
		bspportal **a_ = (bspportal **) big_pl.data + j;
		if(!(*a_)) continue;
		bspportal *a = *a_;
		
		for(k = 0; *a_ && k < 2; k++){
			int leaf_i = a->leafs[k];
			bspleaf *leaf = &bsp->leafs[leaf_i];
			mplane planes[RVFACE_MAX_VERTS];
			int num_planes = 0;
			for(i = leaf->first_face; i <= leaf->last_face; i++){
				bspface *f = bsp->faces + i;
				
				if(f->type > 1) continue;
				vec3_verts2plane(
					&f->verts[0].pos,
					&f->verts[1].pos,
					&f->verts[2].pos,
					&planes[num_planes++]
				);
				if(f->type > 0) continue;
				
				vec3 normal;
				vec3_normal(
						&f->verts[0].pos,
						&f->verts[1].pos,
						&f->verts[2].pos,
						&normal
				);
				
				for(l = 0; l < f->num_verts; l++){
					mplane plane;
					vec3 c = VEC3_ADD(f->verts[(l + 1) % f->num_verts].pos, normal);
					
					vec3_verts2plane(
						&f->verts[l].pos,
						&f->verts[(l + 1) % f->num_verts].pos,
						&c,
						&plane
					);
					mplane_flip(&plane);
					
					planes[num_planes++] = plane;
				}
			}
			for(i = 0; i < num_planes; i++){
				mplane plane = planes[i];
				int s = rvface_sort(&plane, (rvface *) a);
				if(s == RVFACE_BACK){
					free(a);
					*a_ = 0;
					break;
				}
				if(s == RVFACE_COPLANAR) continue;
				if(s == RVFACE_SPANNING){
					bspportal clipped, deleted;
					split_portal(&plane, a, &clipped, &deleted);
					*a = clipped;
				}
			}
		}
	}
	#endif
	arr_sieve(&big_pl, el, idx, el);
	
	//Filter out redundant smaller portals
	for(j = 0; j < big_pl.len; j++){
		bspportal *a = big_pl.data[j];
		bspportal **ap = &big_pl.data[j];
		if(!a) continue;
		
		for(k = j + 1; k < big_pl.len; k++){
			bspportal *b = big_pl.data[k];
			bspportal **bp = &big_pl.data[k];
			if(!b) continue;
			
			if((a->leafs[0] == b->leafs[0]) && (a->leafs[1] == b->leafs[1])){
				//Build 2D faces from portals projected into their shared plane
				rvface a2d = *((rvface *) a), b2d = *((rvface *) b);
				MATRIX_f mat;
				matrix_f_fromrvface((rvface *) a, &mat);
				matrix_f_invert(&mat);
				rvface_apply_matrix_f(&a2d, &mat);
				rvface_apply_matrix_f(&b2d, &mat);
				
				//Calculate 2D bounds
				vec3 mina, maxa, minb, maxb;
				rvface_bounds(&a2d, &mina, &maxa);
				rvface_bounds(&b2d, &minb, &maxb);
				
				//Calculate size and set bound pointers (greater and lesser)
				vec3 siza = VEC3_SUB(maxa, mina), sizb = VEC3_SUB(maxb, minb);
				vecc_t lena = vec3_sqlen(&siza), lenb = vec3_sqlen(&sizb);
				vec3
					*min_gtr = (lena < lenb) ? &minb : &mina,
					*max_gtr = (lena < lenb) ? &maxa : &maxb, 
					*min_lss = (lena < lenb) ? &minb : &mina, 
					*max_lss = (lena < lenb) ? &maxa : &maxb;
				
				//Greater portal doesn't completely cover the lesser one
				//Expected to never happen, included as a correctness check
				assert
				//if(!
				(
					min_lss->x >= min_gtr->x && max_lss->x <= max_gtr->x &&
					min_lss->y >= min_gtr->y && max_lss->y <= max_gtr->y
				)
				;
				/*
				){
					rdebug_set_rvfscl(1.0);
					rdebug_set_colour(makecol(255, 255, 0));
					rdebug_rvface((rvface *) a);
					rdebug_set_rvfscl(0.9);
					rdebug_set_colour(makecol(0, 0, 255));
					rdebug_rvface((rvface *) b);
					error = 1;
				}
				*/
				
				if(lena < lenb){
					free(a);
					*ap = 0;
					break;
				}
				else{
					free(b);
					*bp = 0;
				}
			}
		}
	}
	arr_sieve(&big_pl, el, idx, el);
	
	//Debug info for portal leafs
	#if 0
	for(i = 0; i < big_pl.len; i++){
		unsigned int colour1, colour2;
		rvert midp;
		const bspportal *portal = big_pl.data[i];
		
		BSPNODE_CALC_LEAF_COLOUR(j, portal->leafs[0], colour1)
		BSPNODE_CALC_LEAF_COLOUR(j, portal->leafs[1], colour2)
		
		rvface_midpoint_area((rvface *) portal, &midp, 0);
		rdebug_set_colour(colour1);
		rdebug_printf(&midp.pos, "%d %d", portal->leafs[0], portal->leafs[1]);
		rdebug_set_colour(colour2);
		rdebug_rvface((rvface *) portal);
	}
	#endif
	
	//Build leaf-keyed portals array
	for(i = 0; i < bsp->num_leafs; i++){
		leaf_portals[i] = (arr_portalp) arr_empty;
		
		for(j = 0; j < big_pl.len; j++){
			bspportal *a = big_pl.data[j];
			
			if((a->leafs[0] != i) && (a->leafs[1] != i)) continue;
			
			arr_push(leaf_portals + i, a);
		}
	}
	
	//Build PVS
	build_pvs(bsp, leaf_portals);
	
	//Free big_pl and build portals
	//Also making a new flat big_pl2 array of portals
	arr_t(bspportal2) big_pl2 = {};
	arr_reserve(&big_pl2, big_pl.len);
	arr_t(vec3) portal_verts = {};
	arr_reserve(&portal_verts, big_pl.len * 3);
	for(j = 0; j < big_pl.len; j++){
		bspportal *p = big_pl.data[j];
		bspportal2 p2 = {
			.first_vert = portal_verts.len,
			.num_verts = p->num_verts,
			.leafs = {p->leafs[0], p->leafs[1]},
			.plane = p->plane,
		};
		
		for(i = 0; i < p->num_verts; i++){
			arr_push(&portal_verts, p->verts[i].pos);
		}
		
		arr_push(&big_pl2, p2);
		free(p);
	}
	bsp->portals = big_pl2.data;
	bsp->num_portals = big_pl2.len;
	bsp->portal_verts = portal_verts.data;
	bsp->num_portal_verts = portal_verts.len;
	
	//Builds a flat array of leaf portal indices
	arr_t(int) leaf_portals2 = {};
	for(i = 0; i < bsp->num_leafs; i++){
		arr_reserve(&leaf_portals2, leaf_portals2.len + leaf_portals[i].len);
		for(j = 0; j < leaf_portals[i].len; j++){
			arr_push(&leaf_portals2, arr_idxof(&big_pl, leaf_portals[i].data[j]));
		}
	}
	arr_unreserve(&leaf_portals2);
	//Transfer the flat array to bsp, freeing leaf_portals
	for(i = 0; i < bsp->num_leafs; i++){
		if(i){
			bsp->leafs[i].portals = bsp->leafs[i - 1].portals + bsp->leafs[i - 1].num_portals;
		}
		else{
			bsp->leafs[i].portals = leaf_portals2.data;
		}
		bsp->leafs[i].num_portals = leaf_portals[i].len;
		
		arr_free(leaf_portals + i);
	}
	free(leaf_portals);
	bsp->num_portal_idx = leaf_portals2.len;
	
	arr_free(&big_pl);
	
	return error;
}
