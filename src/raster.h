#ifndef RASTER_H
#define RASTER_H

//#include "compat.h"

#define KDR_NEAR 0.1

struct BITMAP;
struct V3D_f;

extern int raster_triangle_func;
extern int raster_mipmap_levels;
extern int kdr_bf_cull;

enum {
	rf_triangle3d_f = 0,
	rf_kdr_triangle3d_f_null,
	rf_kdr_triangle3d_f_generic,
	rf_kdr_triangle3d_f_persp, 
	rf_kdr_triangle3d_f_persp32bpp,
	rf_kdr_triangle3d_f_persp8bpp,
	rf_kdr_triangle3d_f_affinezb32bpp,
	rf_kdr_triangle3d_f_affinezb8bpp,
	rf_kdr_triangle3d_f_persp_bilinear,
	rf_kdr_triangle3d_f_affinetransp32bpp,
	rf_kdr_triangle3d_f_affinetransp8bpp,
	rf_kdr_triangle3d_f_vischeck,
};

typedef struct kdr_zbuf
{
	int w, h;
	int *data;
	int *line[];
} kdr_zbuf;

extern kdr_zbuf *kdr_active_zbuf;

kdr_zbuf *kdr_create_zbuf(int w, int h);
void kdr_destroy_zbuf(kdr_zbuf *zb);
void kdr_create_zbuf_static(kdr_zbuf *zb, int w, int h, int *data);
void kdr_clear_zbuf(const kdr_zbuf *zb);
void kdr_drawzb(BITMAP *to);
void kdr_polygon3d_f(struct BITMAP *a, int b, struct BITMAP **c, int d, struct V3D_f *e[]);

void kdr_affinelit(BITMAP *bmp, V3D_f *v1, V3D_f *v2, V3D_f *v3);

void raster_push();
void raster_pop();

void kdr_particle3d_f(struct BITMAP *a, float x, float y, float z, int c);
void kdr_particle3d_lit(struct BITMAP *a, int c);

struct sprsheet;
void kdr_sprite3d_f(struct BITMAP *bmp, struct BITMAP *lit, struct sprsheet *spr, int frame, V3D_f *verts, int flip_h);

extern int kdr_vischeck_hit;

void kdr_triangle3d_f_persp_offsuv(BITMAP *bmp, int kdr_piuv_offs, int kdr_piuv_w, V3D_f *_v1, V3D_f *_v2, V3D_f *_v3);

#endif
