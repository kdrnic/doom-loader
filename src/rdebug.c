#include <allegro.h>

#include <stdio.h>
#include <stdarg.h>
#include <math.h>

#include "rdebug.h"
#include "vector.h"
#include "mesh.h"
#include "compat.h"
#include "rvf_draw.h"
#include "arr.h"
#include "crc32.h"

typedef struct rdebug_info
{
	union
	{
		struct
		{
			char string[256];
			vec3 string_pos;
		};
		vec3 point;
		struct
		{
			rvface rvf;
		};
		struct
		{
			vec3 p1, p2;
		};
	};
	int type;
	int colour;
	int mask;
	int notransform;
	//TODO: implement solid drawing
	int solid;
	int mat;
	float scale2d;
	
	unsigned int hash;
} rdebug_info;

enum
{
	RDBG_STRING,
	RDBG_ARROW,
	RDBG_LINE,
	RDBG_FACE,
	RDBG_POINT,
	RDBG_BOX2D,
	RDBG_FACE2D,
	RDBG_STRING2D,
	RDBG_PARTICLE,
};

static int rdebug_mask = -1, rdebug_colour = 0xFF, rdebug_facesolid = 0, rdebug_shader = 0;
static arr_t(rdebug_info) rdis = arr_empty;
static int num_rdis_sorted = 0;

#define RDEBUG_STACK_LEN 5*10
static int stack[RDEBUG_STACK_LEN];
static int stack_i = 0;

#define RDEBUG_MATRIX_POOL_LEN 100

static MATRIX_f mat_pool[RDEBUG_MATRIX_POOL_LEN];
static int mat_last_used[RDEBUG_MATRIX_POOL_LEN] = {0};
static int curr_mat = -1, mat_uses = 0;

static float scale2d = 32.0;

static float rvfscl = 1.0;

void rdebug_set_rvfscl(float s)
{
	rvfscl = s;
}

int rdebug_get_n()
{
	return rdis.len;
}

void new_mat_idx(const MATRIX_f *m)
{
	int i;
	if(curr_mat >= 0 && matrix_f_equal(m, mat_pool + curr_mat)) return;
	int lowest_i = RDEBUG_MATRIX_POOL_LEN, lowest_uses = -1;
	
	for(i = 0; i < RDEBUG_MATRIX_POOL_LEN; i++){
		if(matrix_f_equal(m, mat_pool + i)){
			lowest_i = i;
			goto after;
		}
	}
	for(i = 0; i < RDEBUG_MATRIX_POOL_LEN; i++){
		if(lowest_uses < 0 || mat_last_used[i] < lowest_uses){
			lowest_uses = mat_last_used[i];
			lowest_i = i;
		}
	}
	after:
	mat_uses++;
	mat_last_used[lowest_i] = mat_uses;
	curr_mat = lowest_i;
	mat_pool[curr_mat] = *m;
}

void rdebug_set_scale2d(float f)
{
	scale2d = f;
}

void rdebug_push()
{
	stack[stack_i++] = rdebug_mask;
	stack[stack_i++] = rdebug_colour;
	stack[stack_i++] = rdebug_facesolid;
	stack[stack_i++] = rdebug_shader;
	stack[stack_i++] = curr_mat;
	stack[stack_i++] = floor(scale2d * 1000);
	stack[stack_i++] = floor(log(rvfscl) * 10000.0);
}

void rdebug_pop()
{
	rvfscl = pow(2.7182818, stack[--stack_i] * 10000.0);
	scale2d =			((float)(stack[--stack_i])) / 1000.0f;
	curr_mat = 			stack[--stack_i];
	rdebug_shader = 	stack[--stack_i];
	rdebug_facesolid = 	stack[--stack_i];
	rdebug_colour =		stack[--stack_i];
	rdebug_mask =		stack[--stack_i];
}

static int notransform = 0;

void rdebug_notransform()
{
	notransform = 1;
}

void push_rdi(rdebug_info r)
{
	const arr_t(rdebug_info) rdis_sorted = {
		.data = rdis.data,
		.len = num_rdis_sorted,
		.siz = rdis.siz,
		.is_static = 1
	};
	crc32_state_t crc = CRC32_STATE0;
	
	r.mask = rdebug_mask;
	r.colour = rdebug_colour;
	r.solid = rdebug_facesolid;
	r.mat = curr_mat;
	r.scale2d = scale2d;
	
	r.notransform = notransform;
	notransform = 0;
	
	r.hash = 0;
	r.hash = crc32_buffer((char *) &r, sizeof(r), crc);
	
	int i = arr_bsearch_ex(&rdis, offsetof(rdebug_info, hash), r.hash);
	
	for(; i < rdis_sorted.len && rdis_sorted.data[i].hash == r.hash; i++){
		if(!memcmp(rdis_sorted.data + i, &r, sizeof(r))) return;
	}
	
	arr_push(&rdis, r);
}

void rdebug_set_zb(int z)
{
	rdebug_shader = z ? POLYTYPE_ZBUF : 0;
}

void rdebug_set_solid(int s)
{
	rdebug_facesolid = s;
}

void rdebug_set_mask(int m)
{
	rdebug_mask = m;
}

void rdebug_set_colour(int c)
{
	rdebug_colour = c;
}

void rdebug_set_matrix(const struct MATRIX_f *m)
{
	new_mat_idx(m);
}

void rdebug_string(const vec3 *pos, const char *s)
{
	rdebug_info r = {};
	r.type = RDBG_STRING;
	strncpy(r.string, s, sizeof(r.string) - 1);
	r.string[sizeof(r.string) - 1] = 0;
	r.string_pos = *pos;
	
	push_rdi(r);
}

void rdebug_printf(const vec3 *pos, const char *fmt, ...)
{
	va_list argptr;
	rdebug_info r = {};
	
	r.type = RDBG_STRING;
	r.string_pos = *pos;
	r.string[0] = 0;
	
	va_start(argptr, fmt);
	
	#ifndef MSDOS_BUILD
	vsnprintf(r.string, sizeof(r.string), fmt, argptr);
	#endif
	
	va_end(argptr);
	
	push_rdi(r);
}

void rdebug_point(const vec3 *p)
{
	rdebug_info r = {};
	r.type = RDBG_POINT;
	r.point = *p;
	
	push_rdi(r);
}

void rdebug_particle(const vec3 *p)
{
	rdebug_info r = {};
	r.type = RDBG_PARTICLE;
	r.point = *p;
	
	push_rdi(r);
}

void rdebug_line(const vec3 *p1, const vec3 *p2)
{
	rdebug_info r = {};
	r.type = RDBG_LINE;
	r.p1 = *p1;
	r.p2 = *p2;
	
	push_rdi(r);
}

void rdebug_arrow(const vec3 *p1, const vec3 *p2)
{
	rdebug_info r = {};
	r.type = RDBG_ARROW;
	r.p1 = *p1;
	r.p2 = *p2;
	
	push_rdi(r);
}

void rdebug_face(const vec3 *verts, int num_verts)
{
	int i;
	rdebug_info r = {};
	r.type = RDBG_FACE;
	for(i = 0; i < num_verts; i++){
		r.rvf.verts[i].pos = verts[i];
	}
	r.rvf.num_verts = num_verts;
	
	push_rdi(r);
}

void rdebug_rvface(const rvface *face)
{
	rdebug_info r = {};
	r.type = RDBG_FACE;
	r.rvf = *face;
	
	rvface_scale(&r.rvf, rvfscl);
	
	push_rdi(r);
}

void rdebug_box_sc(const struct vec3 *size, const struct vec3 *centre)
{
	const vec3 verts[8] = VEC3_BOXVERTS_SC(*size, *centre);
	int i;
	
	for(i = 0; i < 12; i++){
		rdebug_line(verts + idx_boxedges[i][0], verts + idx_boxedges[i][1]);
	}
}

void rdebug_ruler(const struct vec3 *pos_)
{
	const vec3 pos = *pos_;
	double i;
	for(i = 0; i < 3.0001; i += 0.25){
		const vec3 x = {pos.x + i, pos.y, pos.z}, y = {pos.x, pos.y + i, pos.z}, z = {pos.x, pos.y, pos.z + i};
		rdebug_set_colour(0xFF0000);
		rdebug_printf(&x, "%f", i);
		rdebug_set_colour(0xFF00);
		rdebug_printf(&y, "%f", i);
		rdebug_set_colour(0xFF);
		rdebug_printf(&z, "%f", i);
		if(fabs(3.0 - i) < 0.00001){
			rdebug_set_colour(0xFF0000);
			rdebug_arrow(pos_, &x);
			rdebug_set_colour(0xFF00);
			rdebug_arrow(pos_, &y);
			rdebug_set_colour(0xFF);
			rdebug_arrow(pos_, &z);
		}
	}
}

void rdebug_box2d(const struct vec3 *a, const vec3 *b)
{
	rdebug_info r = {};
	r.type = RDBG_BOX2D;
	r.p1 = *a;
	r.p2 = *b;
	
	push_rdi(r);
}

void rdebug_face2d(const struct rvface *f)
{
	rdebug_info r = {};
	r.type = RDBG_FACE2D;
	r.rvf = *f;
	
	push_rdi(r);
}

void rdebug_printf2d(const struct vec3 *p, const char *fmt, ...)
{
	va_list argptr;
	rdebug_info r = {};
	
	r.type = RDBG_STRING2D;
	r.string_pos = *p;
	r.string[0] = 0;
	
	va_start(argptr, fmt);
	
	#ifndef MSDOS_BUILD
	vsnprintf(r.string, sizeof(r.string), fmt, argptr);
	#endif
	
	va_end(argptr);
	
	push_rdi(r);
}

void rdebug_hemicube(struct MATRIX_f *hemicube_mat)
{
	double depth = 0.6;
	vec3
		topleft =		{-0.5, -0.5,	depth},
		topright =		{0.5, -0.5,		depth},
		bottomleft = 	{-0.5, 0.5,		depth},
		bottomright =	{0.5, 0.5,		depth},
		origin = 		{0.0, 0.0,		depth - 0.5};
	vec3 topleft_m, topright_m, bottomleft_m, bottomright_m, origin_m;
	
	for(int i = 0; i < 5; i++){
		MATRIX_f hemicube_mat_inv = hemicube_mat[i];
		matrix_f_invert(&hemicube_mat_inv);
		
		topleft_m =		topleft;
		topright_m =	topright;
		bottomleft_m = 	bottomleft;
		bottomright_m =	bottomright;
		origin_m = origin;
		vec3_apply_matrix_f(&topleft_m,		&hemicube_mat_inv);
		vec3_apply_matrix_f(&topright_m,	&hemicube_mat_inv);
		vec3_apply_matrix_f(&bottomleft_m,	&hemicube_mat_inv);
		vec3_apply_matrix_f(&bottomright_m,	&hemicube_mat_inv);
		vec3_apply_matrix_f(&origin_m,		&hemicube_mat_inv);
		rdebug_line(&topleft_m,		&origin_m);
		rdebug_line(&topright_m,	&origin_m);
		rdebug_line(&bottomleft_m,	&origin_m);
		rdebug_line(&bottomright_m,	&origin_m);
		rdebug_line(&topleft_m,		&topright_m);
		rdebug_line(&topright_m,	&bottomright_m);
		rdebug_line(&bottomright_m,	&bottomleft_m);
		rdebug_line(&bottomleft_m,	&topleft_m);
		
		if(!i){
			vec3
				newbottomleft = 	{-0.5, 0.0,	depth},
				newbottomright =	{0.5, 0.0,	depth};
			bottomleft = newbottomleft;
			bottomright = newbottomright;
		}
	}
}

static void arrow(BITMAP *b, float x1, float y1, float x2, float y2, float c)
{
	float angle = atan2(y1 - y2, x1 - x2);
	line(b, x1, y1, x2, y2, c);
	line(b, x2, y2, x2 + cos(angle - M_PI * 0.25) * 5.0, y2 + sin(angle - M_PI * 0.25) * 5.0, c);
	line(b, x2, y2, x2 + cos(angle + M_PI * 0.25) * 5.0, y2 + sin(angle + M_PI * 0.25) * 5.0, c);
}

//Note very stupid bughunt for v->z < near plane z
#define vec3_transform(c, v)\
({\
	vec3_apply_matrix_f(v, c);\
	vec3_persp_project_f(v);\
	(v)->z > 0.1;\
})

static int rdi_compare(const rdebug_info *a, const rdebug_info *b)
{
	if(a->hash < b->hash) return -1;
	if(a->hash > b->hash) return 1;
	return 0;
}

void rdebug_draw(MATRIX_f *c_, BITMAP *bmp)
{
	int i, j;
	rvface tmp;
	MATRIX_f c__, *c = &c__;
	#if 0
	textprintf_ex(
		bmp,
		font,
		0,
		8,
		0xFFFFFF,
		-1,
		"%d %d",
		rdis.len, num_rdis_sorted
	);
	#endif
	vec3 p1, p2;
	
	for(i = 0; i < rdis.len; i++){
		rdebug_info r = rdis.data[i];
		if(r.mat >= 0) matrix_mul_f(mat_pool + r.mat, r.notransform ? &identity_matrix_f : c_, c);
		else *c = *(r.notransform ? &identity_matrix_f : c_);
		switch(r.type){
			case RDBG_ARROW:
				p1 = r.p1;
				p2 = r.p2;
				#define vec3_persp_project_f(...)
				if(vec3_transform(c, &p1) && vec3_transform(c, &p2) && vec3_clipline(&p1, &p2)){
					#undef vec3_persp_project_f
					vec3_persp_project_f(&p1);
					vec3_persp_project_f(&p2);
					arrow(bmp, p1.x, p1.y, p2.x, p2.y, r.colour);
				}
				break;
			case RDBG_FACE:
				tmp = r.rvf;
				rvface_al_matrix(&tmp, c);
				rvface_al_clip(&tmp, &tmp, POLYTYPE_FLAT, 0.1, 0.1);
				rvface_al_persp(&tmp);
				for(j = 0; j < tmp.num_verts; j++){
					int o = (j + 1) % tmp.num_verts;
					line(
						bmp,
						tmp.verts[j].pos.x,
						tmp.verts[j].pos.y,
						tmp.verts[o].pos.x,
						tmp.verts[o].pos.y,
						r.colour
					);
				}
				break;
			case RDBG_LINE:
				p1 = r.p1;
				p2 = r.p2;
				#define vec3_persp_project_f(...)
				if(vec3_transform(c, &p1) && vec3_transform(c, &p2) && vec3_clipline(&p1, &p2)){
					#undef vec3_persp_project_f
					vec3_persp_project_f(&p1);
					vec3_persp_project_f(&p2);
					line(bmp, p1.x, p1.y, p2.x, p2.y, r.colour);
				}
				break;
			case RDBG_POINT:
				if(vec3_transform(c, &r.point)){
					circle(bmp, r.point.x, r.point.y, 5, r.colour);
				}
				break;
			case RDBG_PARTICLE:
				if(vec3_transform(c, &r.point)){
					putpixel(bmp, r.point.x, r.point.y, r.colour);
				}
				break;
			case RDBG_STRING:
				if(vec3_transform(c, &r.string_pos)){
					textprintf_centre_ex(
						bmp,
						font,
						r.string_pos.x,
						r.string_pos.y,
						r.colour,
						-1,
						"%s",
						r.string
					);
				}
				break;
			case RDBG_BOX2D:
				r.p1.x *= r.scale2d;
				r.p1.y *= r.scale2d;
				r.p2.x *= r.scale2d;
				r.p2.y *= r.scale2d;
				r.p1.x += SCREEN_W * 0.5f;
				r.p1.y += SCREEN_H * 0.5f;
				r.p2.x += SCREEN_W * 0.5f;
				r.p2.y += SCREEN_H * 0.5f;
				vec3_print(&r.p1);vec3_print(&r.p2);
				rect(bmp, r.p1.x, r.p1.y, r.p2.x, r.p2.y, r.colour);
				break;
			case RDBG_FACE2D:
				for(j = 0; j < r.rvf.num_verts; j++){
					int o = (j + 1) % r.rvf.num_verts;
					line(
						bmp,
						r.rvf.verts[j].pos.x * r.scale2d + SCREEN_W * 0.5f,
						r.rvf.verts[j].pos.y * r.scale2d + SCREEN_H * 0.5f,
						r.rvf.verts[o].pos.x * r.scale2d + SCREEN_W * 0.5f,
						r.rvf.verts[o].pos.y * r.scale2d + SCREEN_H * 0.5f,
						r.colour
					);
				}
				break;
			case RDBG_STRING2D:
				textprintf_ex(
					bmp,
					font,
					r.string_pos.x * r.scale2d + SCREEN_W * 0.5f,
					r.string_pos.y * r.scale2d + SCREEN_H * 0.5f,
					r.colour,
					-1,
					"%s",
					r.string
				);
				break;
		}
	}
	
	arr_sort(&rdis, rdi_compare);
	num_rdis_sorted = rdis.len;
}

void rdebug_clear()
{
	rdis.len = 0;
	rdebug_mask = -1;
	rdebug_colour = 0xFF;
	curr_mat = -1;
	memset(mat_last_used, 0, sizeof(mat_last_used));
	stack_i = 0;
}
