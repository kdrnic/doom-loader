#include <allegro.h>

#include <math.h>
#include <stdio.h>
#include <limits.h>
#include <stdint.h>

#include "raster.h"
#include "sprsheet.h"

/*
Illustration of the two subtriangles

             -verts[0]                  -vs[0]=ve[0]                             
            / \                        / \                                       
           /   \-                     /   \-                                     
          /      \                   /      \                                    
         /        \                 /        \                                   
        /          \-              /          \-                                 
       /             \            /             \                           ve[0]
      /             /-\verts[1]  -----------------ve[1]    -----------------     
     /          /---                                      /          /---        
    /       /---                                         /       /---            
   /    /---                                            /    /---                
  / /---                                               / /---                    
 ---                                                  ---                        
 verts[2]                                             vs[1]=ve[1]              
 
*/

static void kdr_triangle3d_f_generic(BITMAP *bmp, int shader, BITMAP *tex, V3D_f *_v1, V3D_f *_v2, V3D_f *_v3)
{
	//Holds ends of triangle scanlines
	//'p' stands for perspective correction
	//and the z is actually inverse
	struct {
		float x, u, v, up, vp, zp;
	} scan_s, scan_e, *scan_l, *scan_r;
	
	//Local copy, since may be order swapped
	V3D_f *verts[] = {_v1, _v2, _v3};
	//Used for swaps
	V3D_f *tmp_vp;
	//Triangle is cut into two triangles, by the horizontal line that goes through verts[1]
	//vs is the segment that intersects this line
	V3D_f *ve0, *ve1;
	const V3D_f *vs0, *vs1;
	//Final values for each pixel
	int x_i, y_i, u_i, v_i, up_i, vp_i;
	float z_i;
	//Calculated colour
	int c = 0;
	//One pass for each of the subtriangles
	int pass;
	//Enable Unreal-style bilinear dithering
	#if 1
	#define UNREAL_DITHER
	unsigned int udu = 0, udv = 0, udt = 0;
	#define UV_FACTOR 256.0f
	#else
	#define UV_FACTOR 1.0f
	#endif
	
	#define SWAP_V(a, b) ({ tmp_vp = (a); (a) = (b); (b) = tmp_vp; (void)0;})
	//Linearly interpolate between a and b by c amount
	#define LINTERP(a, b, c) ((a) + ((b) - (a)) * c)
	//A ratio of how far along is c, in the segment a to b
	#define ALONG(a, b, c) (((b) - (a)) ? ((c) - (a)) / ((b) - (a)) : 0.0)
	
	//Order verts
	if(verts[1]->y < verts[0]->y) SWAP_V(verts[1], verts[0]);
	if(verts[2]->y < verts[0]->y) SWAP_V(verts[2], verts[0]);
	if(verts[1]->y > verts[2]->y) SWAP_V(verts[1], verts[2]);
	
	vs0 = verts[0];
	vs1 = verts[2];
	
	//Verts for upper subtriangle
	ve0 = verts[0];
	ve1 = verts[1];
	//Two passes as discussed above
	for(pass = 0; pass < 2; pass++){
		//One iteration per horizontal scanline
		for(y_i = ceil(ve0->y); y_i < ceil(ve1->y); y_i++){
			//Calculate interpolated values for both ends of the scanline
			//Of note, both those and the interpolations down below can have calculations
			//simplified since they obviously move by constant steps
			const float along_s = ALONG(vs0->y, vs1->y, (float) y_i);
			const float along_e = ALONG(ve0->y, ve1->y, (float) y_i);
			
			scan_s.x = LINTERP(vs0->x, vs1->x, along_s);
			scan_s.u = LINTERP(vs0->u, vs1->u, along_s);
			scan_s.v = LINTERP(vs0->v, vs1->v, along_s);
			scan_s.up = LINTERP(vs0->u / vs0->z, vs1->u / vs1->z, along_s);
			scan_s.vp = LINTERP(vs0->v / vs0->z, vs1->v / vs1->z, along_s);
			scan_s.zp = LINTERP(1.0 / vs0->z, 1.0 / vs1->z, along_s);
			
			scan_e.x = LINTERP(ve0->x, ve1->x, along_e);
			scan_e.u = LINTERP(ve0->u, ve1->u, along_e);
			scan_e.v = LINTERP(ve0->v, ve1->v, along_e);
			scan_e.up = LINTERP(ve0->u / ve0->z, ve1->u / ve1->z, along_e);
			scan_e.vp = LINTERP(ve0->v / ve0->z, ve1->v / ve1->z, along_e);
			scan_e.zp = LINTERP(1.0 / ve0->z, 1.0 / ve1->z, along_e);
			
			//This test could actually be done only once
			//scan_l to scan_r is strictly left to right
			if(scan_e.x > scan_s.x){
				scan_l = &scan_s;
				scan_r = &scan_e;
			}
			else{
				scan_l = &scan_e;
				scan_r = &scan_s;
			}
			
			//Iterate through scanline
			for(x_i = ceil(scan_l->x); x_i < ceil(scan_r->x); x_i++){
				//Final interpolation for each pixel
				const float along_x = ALONG(scan_l->x, scan_r->x, (float) x_i);
				u_i = floor(UV_FACTOR *		LINTERP(scan_l->u,	scan_r->u,	along_x));
				v_i = floor(UV_FACTOR *		LINTERP(scan_l->v,	scan_r->v,	along_x));
				//Some games only calculate the inverse of z_i every few pixels
				//and add a further linear interpolation inbetween
				z_i =			LINTERP(scan_l->zp,	scan_r->zp,	along_x);
				up_i = floor(UV_FACTOR *	LINTERP(scan_l->up,	scan_r->up,	along_x) / z_i);
				vp_i = floor(UV_FACTOR *	LINTERP(scan_l->vp,	scan_r->vp,	along_x) / z_i);
				
				//Pick colour according to shader
				switch(shader){
					case POLYTYPE_FLAT:
						c = verts[0]->c;
						break;

					case POLYTYPE_PTEX:
					case POLYTYPE_PTEX_MASK:
						//Handle negative UVs and wrapping
						udu = up_i & 0xFF;
						udv = vp_i & 0xFF;
						#ifdef  UNREAL_DITHER
						up_i >>= 8;
						vp_i >>= 8;
						#endif
						up_i += tex->w << 8;
						vp_i += tex->h << 8;
						#ifdef  UNREAL_DITHER
						//Essentially behaves as a random dither
						udu *= udu;
						udv *= udv;
						if((udt & 0xFFFF) < udu) up_i++;
						if((udt & 0xFFFF) < udv) vp_i++;
						//Update udt with a LCG
						udt = (22695477u * udt + 1u);
						#endif
						up_i %= tex->w;
						vp_i %= tex->h;
						
						c = getpixel(tex, up_i, vp_i);
						break;

					case POLYTYPE_ATEX:
					case POLYTYPE_ATEX_MASK:
						//Handle negative UVs and wrapping
						u_i += tex->w << 8;
						v_i += tex->h << 8;
						u_i %= tex->w;
						v_i %= tex->h;
						c = getpixel(tex, u_i, v_i);
						break;
				}

				putpixel(bmp, x_i, y_i, c);
			}
		}
		//Verts for lower subtriangle
		ve0 = verts[1];
		ve1 = verts[2];
	}
	
	#ifdef UNREAL_DITHER
	#undef UNREAL_DITHER
	#endif
	#undef SWAP_V
	#undef LINTERP
	#undef ALONG
}

/*
Persp-correct only, slightly faster version of the above
Instead of using the linterp formula directly, keep current pos and increase with fixed steps
*/
static void kdr_triangle3d_f_persp(BITMAP *bmp, int s, BITMAP *tex,  V3D_f *_v1, V3D_f *_v2, V3D_f *_v3)
{
	V3D_f scan_s, scan_e, *scan_l, *scan_r, scan_x;
	V3D_f scan_x_step, scan_s_step, scan_e_step;
	
	//Hard-copied so that u and v may be divided by z, and z inverted
	V3D_f __verts[3] = {*_v1, *_v2, *_v3};
	V3D_f *verts[] = {&__verts[0], &__verts[1], &__verts[2]};
	V3D_f *tmp_v;
	V3D_f *ve0, *ve1;
	const V3D_f *vs0, *vs1;
	int x_i, y_i, u_i, v_i, x_l, x_r;
	float tmp;
	int pass, c;
	
	#define SWAP_V(a, b) ({ tmp_v = (a); (a) = (b); (b) = tmp_v; (void)0;})
	#define STEPSIZE(a, b, c) (((b) - (a)) * (c)) 
	
	if(verts[1]->y < verts[0]->y) SWAP_V(verts[1], verts[0]);
	if(verts[2]->y < verts[0]->y) SWAP_V(verts[2], verts[0]);
	if(verts[1]->y > verts[2]->y) SWAP_V(verts[1], verts[2]);
	
	//Applies perspective projection
	verts[0]->z = 1.0f / verts[0]->z;
	verts[1]->z = 1.0f / verts[1]->z;
	verts[2]->z = 1.0f / verts[2]->z;
	verts[0]->u *= verts[0]->z;
	verts[1]->u *= verts[1]->z;
	verts[2]->u *= verts[2]->z;
	verts[0]->v *= verts[0]->z;
	verts[1]->v *= verts[1]->z;
	verts[2]->v *= verts[2]->z;
	
	vs0 = verts[0];
	vs1 = verts[2];
	
	ve0 = verts[0];
	ve1 = verts[1];
	
	//Step along Y
	tmp = 1.0f / ((vs1->y) - (vs0->y));
	scan_s_step.z = STEPSIZE(vs0->z, vs1->z, tmp);
	scan_s_step.u = STEPSIZE(vs0->u, vs1->u, tmp);
	scan_s_step.v = STEPSIZE(vs0->v, vs1->v, tmp);
	scan_s_step.x = STEPSIZE(vs0->x, vs1->x, tmp);
	
	scan_s = *vs0;
	//Move to the height of the first line
	tmp = ceil(ve0->y) - scan_s.y;
	scan_s.z += scan_s_step.z * tmp;
	scan_s.u += scan_s_step.u * tmp;
	scan_s.v += scan_s_step.v * tmp;
	scan_s.x += scan_s_step.x * tmp;
	
	for(pass = 0; pass < 2; pass++){
		//Step along Y
		tmp = 1.0f / ((ve1->y) - (ve0->y));
		scan_e_step.z = STEPSIZE(ve0->z, ve1->z, tmp);
		scan_e_step.u = STEPSIZE(ve0->u, ve1->u, tmp);
		scan_e_step.v = STEPSIZE(ve0->v, ve1->v, tmp);
		scan_e_step.x = STEPSIZE(ve0->x, ve1->x, tmp);
		
		scan_e = *ve0;
		//Move to the height of the first line
		tmp = ceil(ve0->y) - scan_e.y;
		scan_e.z += scan_e_step.z * tmp;
		scan_e.u += scan_e_step.u * tmp;
		scan_e.v += scan_e_step.v * tmp;
		scan_e.x += scan_e_step.x * tmp;
		
		//This check stays relevant throughout a pass
		if((scan_e_step.x > scan_s_step.x) ^ (pass)){
			scan_l = &scan_s;
			scan_r = &scan_e;
		}
		else{
			scan_l = &scan_e;
			scan_r = &scan_s;
		}
		
		for(y_i = ceil(ve0->y); y_i < ceil(ve1->y); y_i++){
			//Step along X
			tmp = 1.0f / (ceil(scan_r->x) - ceil(scan_l->x));
			scan_x_step.z = STEPSIZE(scan_l->z, scan_r->z, tmp);
			scan_x_step.u = STEPSIZE(scan_l->u, scan_r->u, tmp);
			scan_x_step.v = STEPSIZE(scan_l->v, scan_r->v, tmp);
			
			scan_x = *scan_l;
			
			x_l = ceil(scan_l->x);
			x_r = ceil(scan_r->x);
			
			for(x_i = x_l; x_i < x_r; x_i++){
				tmp =  1.0f / scan_x.z;
				u_i = floor(scan_x.u * tmp);
				v_i = floor(scan_x.v * tmp);
				
				//Negative and wrapping UVs
				u_i = (u_i + 0x10000) & (tex->w - 1);
				v_i = (v_i + 0x10000) & (tex->h - 1);
				
				c = getpixel(tex, u_i, v_i);
				putpixel(bmp, x_i, y_i, c);
				
				//Step along the scanline
				scan_x.z += scan_x_step.z;
				scan_x.u += scan_x_step.u;
				scan_x.v += scan_x_step.v;
			}
			
			//Step along Y for both the start and end of the scanline
			scan_s.x += scan_s_step.x;
			scan_s.z += scan_s_step.z;
			scan_s.u += scan_s_step.u;
			scan_s.v += scan_s_step.v;
			
			scan_e.x += scan_e_step.x;
			scan_e.z += scan_e_step.z;
			scan_e.u += scan_e_step.u;
			scan_e.v += scan_e_step.v;
		}
		ve0 = verts[1];
		ve1 = verts[2];
	}
	
	#undef SWAP_V
	#undef STEPSIZE
}

/*
Faster version of the above
Only perspective correct support in 32bpp
Tries to calculate perspective correct UVs each 16 pixels, and linterp inbetween
An enormous, ugly hack
*/
static void kdr_triangle3d_f_persp32bpp(BITMAP *bmp, int s, BITMAP *tex,  V3D_f *_v1, V3D_f *_v2, V3D_f *_v3)
{
	V3D_f scan_s, scan_e, *scan_l, *scan_r, scan_x;
	V3D_f scan_x_step, scan_s_step, scan_e_step;
	
	V3D_f __verts[3] = {*_v1, *_v2, *_v3};
	V3D_f *verts[] = {&__verts[0], &__verts[1], &__verts[2]};
	V3D_f *tmp_v;
	V3D_f *ve0, *ve1;
	const V3D_f *vs0, *vs1;
	int x_i, y_i, u_i, v_i, u_i_n, v_i_n, x_i_n, x_l, x_r;
	float z_i, z_i_n;
	int pass;
	float tmp;
	int *pixel;
	const int w_1 = tex->w - 1;
	
	#define SWAP_V(a, b) ({ tmp_v = (a); (a) = (b); (b) = tmp_v; (void)0;})
	#define STEPSIZE(a, b, c) (((b) - (a)) * (c)) 
	
	if(verts[1]->y < verts[0]->y) SWAP_V(verts[1], verts[0]);
	if(verts[2]->y < verts[0]->y) SWAP_V(verts[2], verts[0]);
	if(verts[1]->y > verts[2]->y) SWAP_V(verts[1], verts[2]);
	
	verts[0]->z = 1.0f / verts[0]->z;
	verts[1]->z = 1.0f / verts[1]->z;
	verts[2]->z = 1.0f / verts[2]->z;
	verts[0]->u *= verts[0]->z;
	verts[1]->u *= verts[1]->z;
	verts[2]->u *= verts[2]->z;
	verts[0]->v *= verts[0]->z;
	verts[1]->v *= verts[1]->z;
	verts[2]->v *= verts[2]->z;
	
	vs0 = verts[0];
	vs1 = verts[2];
	
	ve0 = verts[0];
	ve1 = verts[1];
	
	tmp = 1.0f / ((vs1->y) - (vs0->y));
	scan_s_step.z = STEPSIZE(vs0->z, vs1->z, tmp);
	scan_s_step.u = STEPSIZE(vs0->u, vs1->u, tmp);
	scan_s_step.v = STEPSIZE(vs0->v, vs1->v, tmp);
	scan_s_step.x = STEPSIZE(vs0->x, vs1->x, tmp);
	
	scan_s = *vs0;
	tmp = ceil(ve0->y) - scan_s.y;
	scan_s.z += scan_s_step.z * tmp;
	scan_s.u += scan_s_step.u * tmp;
	scan_s.v += scan_s_step.v * tmp;
	scan_s.x += scan_s_step.x * tmp;
	for(pass = 0; pass < 2; pass++){
		tmp = 1.0f / ((ve1->y) - (ve0->y));
		scan_e_step.z = STEPSIZE(ve0->z, ve1->z, tmp);
		scan_e_step.u = STEPSIZE(ve0->u, ve1->u, tmp);
		scan_e_step.v = STEPSIZE(ve0->v, ve1->v, tmp);
		scan_e_step.x = STEPSIZE(ve0->x, ve1->x, tmp);
		
		scan_e = *ve0;
		tmp = ceil(ve0->y) - scan_e.y;
		scan_e.z += scan_e_step.z * tmp;
		scan_e.u += scan_e_step.u * tmp;
		scan_e.v += scan_e_step.v * tmp;
		scan_e.x += scan_e_step.x * tmp;
		
		if((scan_e_step.x > scan_s_step.x) ^ (pass)){
			scan_l = &scan_s;
			scan_r = &scan_e;
		}
		else{
			scan_l = &scan_e;
			scan_r = &scan_s;
		}
		
		#define STEPLEN 16
		#define STEPLENF 16.0f
		
		for(y_i = ceil(ve0->y); y_i < ceil(ve1->y); y_i++){
			tmp = 1.0f / ((scan_r->x) - (scan_l->x));
			scan_x_step.z = STEPSIZE(scan_l->z, scan_r->z, tmp);
			scan_x_step.u = STEPSIZE(scan_l->u, scan_r->u, tmp);
			scan_x_step.v = STEPSIZE(scan_l->v, scan_r->v, tmp);
			
			scan_x = *scan_l;
			x_l = (scan_l->x);
			x_r = (scan_r->x);
			
			scan_s.x += scan_s_step.x;
			scan_s.z += scan_s_step.z;
			scan_s.u += scan_s_step.u;
			scan_s.v += scan_s_step.v;
			
			scan_e.x += scan_e_step.x;
			scan_e.z += scan_e_step.z;
			scan_e.u += scan_e_step.u;
			scan_e.v += scan_e_step.v;
			
			//Avoid writes out of vertical bounds
			if(y_i < 0) continue;
			if(y_i >= bmp->h) continue;
			
			//Avoid writes out of horizontal bounds
			if(scan_x.x < 0.0f){
				tmp = -scan_x.x;
				scan_x.z += scan_x_step.z * tmp;
				scan_x.u += scan_x_step.u * tmp;
				scan_x.v += scan_x_step.v * tmp;
				scan_x.x = 0.0f;
				x_l = 0;
			}
			if(x_r >= bmp->w) x_r = bmp->w;
			
			scan_x_step.z *= STEPLENF;
			scan_x_step.u *= STEPLENF;
			scan_x_step.v *= STEPLENF;
			
			#define FIXCOEFF 256.0f
			#define FIXSHIFT 8
			
			z_i =  1.0 / scan_x.z;
			u_i = (FIXCOEFF * scan_x.u * z_i);
			v_i = (FIXCOEFF * scan_x.v * z_i);
			
			scan_x.z += scan_x_step.z;
			scan_x.u += scan_x_step.u;
			scan_x.v += scan_x_step.v;
			
			z_i_n =  1.0f / scan_x.z;
			
			pixel = (__typeof__(pixel)) bmp->line[y_i];
			pixel += x_l;
			
			for(x_i = x_l; x_i < x_r; x_i = x_i_n){
				x_i_n = x_i + STEPLEN;
				
				u_i_n = (FIXCOEFF * scan_x.u * z_i_n);
				v_i_n = (FIXCOEFF * scan_x.v * z_i_n);
				u_i_n -= u_i;
				v_i_n -= v_i;
				
				//TODO: Check whether this branch may be expensive
				if(x_i_n < x_r){
					scan_x.z += scan_x_step.z;
					scan_x.u += scan_x_step.u;
					scan_x.v += scan_x_step.v;
					z_i_n =  1.0f / scan_x.z;
				}
				//Note: seems replacing the wrapping with & 63 increases speed,
				//likely also by simplifications to the linterp expression
				#define UP_I(n) (((u_i + (u_i_n * (n))) >> FIXSHIFT) & (w_1))
				#define VP_I(n) (((v_i + (v_i_n * (n))) >> FIXSHIFT) & (w_1))
				#define X_I(n) (x_i + (n))
				//Direct memory access provides a significant boost,
				//as it avoids bmp_* function calls
				//Note: Accessing int ** by [y][x] is faster than int * by [y + x * w]
				//unless w is one of {1,2,4,8}
				#define C(n) (((__typeof__(pixel)) tex->line[VP_I(n)])[UP_I(n)])
				#define PUTPIXEL(i, c) *(pixel + (i)) = (c); (void)0;
				#define TEXLINE(l) ((__typeof__(pixel)) tex->line[l])
				//This unrolling provides a quite radical performance boost
				#if 1
				u_i_n >>= 4;
				v_i_n >>= 4;
				switch(x_r - x_i){
					default:
					#if STEPLEN >= 16
					case 16: PUTPIXEL(15, C(15)); case 15: PUTPIXEL(14, C(14));
					case 14: PUTPIXEL(13, C(13)); case 13: PUTPIXEL(12, C(12));
					case 12: PUTPIXEL(11, C(11)); case 11: PUTPIXEL(10, C(10));
					case 10: PUTPIXEL(9, C(9));	case 9: PUTPIXEL(8, C(8));
					#endif
					#if STEPLEN >= 8
					case 8: PUTPIXEL(7, C(7)); case 7: PUTPIXEL(6, C(6));
					case 6:	PUTPIXEL(5, C(5)); case 5: PUTPIXEL(4, C(4));
					#endif
					#if STEPLEN >= 4
					case 4: PUTPIXEL(3, C(3)); case 3: PUTPIXEL(2, C(2));
					#endif
					#if STEPLEN >= 2
					case 2: PUTPIXEL(1, C(1));
					#endif        
					case 1: PUTPIXEL(0, C(0));
				}
				pixel += STEPLEN;
				u_i += u_i_n << 4;
				v_i += v_i_n << 4;
				#else
				const int ix = x_i_n - x_i;
				for(;
					x_i < x_i_n && x_i < x_r;
					x_i++, u_i += u_i_n / ix, v_i += v_i_n / ix, pixel++
				){
					*pixel = TEXLINE((v_i >> FIXSHIFT) & w_1)[(u_i >> FIXSHIFT) & w_1];
				}
				#endif
			}
		}
		ve0 = verts[1];
		ve1 = verts[2];
	}
	
	#undef FIXCOEFF
	#undef FIXSHIFT
	#undef STEPLEN
	#undef STEPLENF
	#undef UP_I
	#undef VP_I
	#undef X_I
	#undef C
	#undef TEXLINE
	#undef PUTPIXEL
	#undef SWAP_V
	#undef STEPSIZE
}

static void kdr_triangle3d_f_persp8bpp(BITMAP *bmp, int s, BITMAP *tex,  V3D_f *_v1, V3D_f *_v2, V3D_f *_v3)
{
	V3D_f scan_s, scan_e, *scan_l, *scan_r, scan_x;
	V3D_f scan_x_step, scan_s_step, scan_e_step;
	
	V3D_f __verts[3] = {*_v1, *_v2, *_v3};
	V3D_f *verts[] = {&__verts[0], &__verts[1], &__verts[2]};
	V3D_f *tmp_v;
	V3D_f *ve0, *ve1;
	const V3D_f *vs0, *vs1;
	int x_i, y_i, u_i, v_i, u_i_n, v_i_n, x_i_n, x_l, x_r;
	float z_i, z_i_n;
	int pass;
	float tmp;
	char *pixel;
	const int w_1 = tex->w - 1;
	
	#define SWAP_V(a, b) ({ tmp_v = (a); (a) = (b); (b) = tmp_v; (void)0;})
	#define STEPSIZE(a, b, c) (((b) - (a)) * (c)) 
	
	if(verts[1]->y < verts[0]->y) SWAP_V(verts[1], verts[0]);
	if(verts[2]->y < verts[0]->y) SWAP_V(verts[2], verts[0]);
	if(verts[1]->y > verts[2]->y) SWAP_V(verts[1], verts[2]);
	
	verts[0]->z = 1.0f / verts[0]->z;
	verts[1]->z = 1.0f / verts[1]->z;
	verts[2]->z = 1.0f / verts[2]->z;
	verts[0]->u *= verts[0]->z;
	verts[1]->u *= verts[1]->z;
	verts[2]->u *= verts[2]->z;
	verts[0]->v *= verts[0]->z;
	verts[1]->v *= verts[1]->z;
	verts[2]->v *= verts[2]->z;
	
	vs0 = verts[0];
	vs1 = verts[2];
	
	ve0 = verts[0];
	ve1 = verts[1];
	
	tmp = 1.0f / ((vs1->y) - (vs0->y));
	scan_s_step.z = STEPSIZE(vs0->z, vs1->z, tmp);
	scan_s_step.u = STEPSIZE(vs0->u, vs1->u, tmp);
	scan_s_step.v = STEPSIZE(vs0->v, vs1->v, tmp);
	scan_s_step.x = STEPSIZE(vs0->x, vs1->x, tmp);
	
	scan_s = *vs0;
	tmp = ceil(ve0->y) - scan_s.y;
	scan_s.z += scan_s_step.z * tmp;
	scan_s.u += scan_s_step.u * tmp;
	scan_s.v += scan_s_step.v * tmp;
	scan_s.x += scan_s_step.x * tmp;
	for(pass = 0; pass < 2; pass++){
		tmp = 1.0f / ((ve1->y) - (ve0->y));
		scan_e_step.z = STEPSIZE(ve0->z, ve1->z, tmp);
		scan_e_step.u = STEPSIZE(ve0->u, ve1->u, tmp);
		scan_e_step.v = STEPSIZE(ve0->v, ve1->v, tmp);
		scan_e_step.x = STEPSIZE(ve0->x, ve1->x, tmp);
		
		scan_e = *ve0;
		tmp = ceil(ve0->y) - scan_e.y;
		scan_e.z += scan_e_step.z * tmp;
		scan_e.u += scan_e_step.u * tmp;
		scan_e.v += scan_e_step.v * tmp;
		scan_e.x += scan_e_step.x * tmp;
		
		if((scan_e_step.x > scan_s_step.x) ^ (pass)){
			scan_l = &scan_s;
			scan_r = &scan_e;
		}
		else{
			scan_l = &scan_e;
			scan_r = &scan_s;
		}
		
		#define STEPLEN 16
		#define STEPLENF 16.0f
		
		for(y_i = ceil(ve0->y); y_i < ceil(ve1->y); y_i++){
			tmp = 1.0f / ((scan_r->x) - (scan_l->x));
			scan_x_step.z = STEPSIZE(scan_l->z, scan_r->z, tmp);
			scan_x_step.u = STEPSIZE(scan_l->u, scan_r->u, tmp);
			scan_x_step.v = STEPSIZE(scan_l->v, scan_r->v, tmp);
			
			scan_x = *scan_l;
			x_l = (scan_l->x);
			x_r = (scan_r->x);
			
			scan_s.x += scan_s_step.x;
			scan_s.z += scan_s_step.z;
			scan_s.u += scan_s_step.u;
			scan_s.v += scan_s_step.v;
			
			scan_e.x += scan_e_step.x;
			scan_e.z += scan_e_step.z;
			scan_e.u += scan_e_step.u;
			scan_e.v += scan_e_step.v;
			
			//Avoid writes out of vertical bounds
			if(y_i < 0) continue;
			if(y_i >= bmp->h) continue;
			
			//Avoid writes out of horizontal bounds
			if(scan_x.x < 0.0f){
				tmp = -scan_x.x;
				scan_x.z += scan_x_step.z * tmp;
				scan_x.u += scan_x_step.u * tmp;
				scan_x.v += scan_x_step.v * tmp;
				scan_x.x = 0.0f;
				x_l = 0;
			}
			if(x_r >= bmp->w) x_r = bmp->w;
			
			scan_x_step.z *= STEPLENF;
			scan_x_step.u *= STEPLENF;
			scan_x_step.v *= STEPLENF;
			
			#define FIXCOEFF 256.0f
			#define FIXSHIFT 8
			
			z_i =  1.0 / scan_x.z;
			u_i = (FIXCOEFF * scan_x.u * z_i);
			v_i = (FIXCOEFF * scan_x.v * z_i);
			
			scan_x.z += scan_x_step.z;
			scan_x.u += scan_x_step.u;
			scan_x.v += scan_x_step.v;
			
			z_i_n =  1.0f / scan_x.z;
			
			pixel = (__typeof__(pixel)) bmp->line[y_i];
			pixel += x_l;
			
			for(x_i = x_l; x_i < x_r; x_i = x_i_n){
				x_i_n = x_i + STEPLEN;
				
				u_i_n = (FIXCOEFF * scan_x.u * z_i_n);
				v_i_n = (FIXCOEFF * scan_x.v * z_i_n);
				u_i_n -= u_i;
				v_i_n -= v_i;
				
				//TODO: Check whether this branch may be expensive
				if(x_i_n < x_r){
					scan_x.z += scan_x_step.z;
					scan_x.u += scan_x_step.u;
					scan_x.v += scan_x_step.v;
					z_i_n =  1.0f / scan_x.z;
				}
				//Note: seems replacing the wrapping with & 63 increases speed,
				//likely also by simplifications to the linterp expression
				#define UP_I(n) (((u_i + (u_i_n * (n))) >> FIXSHIFT) & (w_1))
				#define VP_I(n) (((v_i + (v_i_n * (n))) >> FIXSHIFT) & (w_1))
				#define X_I(n) (x_i + (n))
				//Direct memory access provides a significant boost,
				//as it avoids bmp_* function calls
				//Note: Accessing int ** by [y][x] is faster than int * by [y + x * w]
				//unless w is one of {1,2,4,8}
				#define C(n) (((__typeof__(pixel)) tex->line[VP_I(n)])[UP_I(n)])
				#define PUTPIXEL(i, c) ({*(pixel + (i)) = (c); (void)0;})
				#define TEXLINE(l) ((__typeof__(pixel)) tex->line[l])
				//This unrolling provides a quite radical performance boost
				#if 1
				u_i_n >>= 4;
				v_i_n >>= 4;
				switch(x_r - x_i){
					default:
					#if STEPLEN >= 16
					case 16: PUTPIXEL(15, C(15)); case 15: PUTPIXEL(14, C(14));
					case 14: PUTPIXEL(13, C(13)); case 13: PUTPIXEL(12, C(12));
					case 12: PUTPIXEL(11, C(11)); case 11: PUTPIXEL(10, C(10));
					case 10: PUTPIXEL(9, C(9));	case 9: PUTPIXEL(8, C(8));
					#endif
					#if STEPLEN >= 8
					case 8: PUTPIXEL(7, C(7)); case 7: PUTPIXEL(6, C(6));
					case 6:	PUTPIXEL(5, C(5)); case 5: PUTPIXEL(4, C(4));
					#endif
					#if STEPLEN >= 4
					case 4: PUTPIXEL(3, C(3)); case 3: PUTPIXEL(2, C(2));
					#endif
					#if STEPLEN >= 2
					case 2: PUTPIXEL(1, C(1));
					#endif        
					case 1: PUTPIXEL(0, C(0));
				}
				pixel += STEPLEN;
				u_i += u_i_n << 4;
				v_i += v_i_n << 4;
				#else
				const int ix = x_i_n - x_i;
				for(;
					x_i < x_i_n && x_i < x_r;
					x_i++, u_i += u_i_n / ix, v_i += v_i_n / ix, pixel++
				){
					*pixel = TEXLINE((v_i >> FIXSHIFT) & w_1)[(u_i >> FIXSHIFT) & w_1];
				}
				#endif
			}
		}
		ve0 = verts[1];
		ve1 = verts[2];
	}
	
	#undef FIXCOEFF
	#undef FIXSHIFT
	#undef STEPLEN
	#undef STEPLENF
	#undef UP_I
	#undef VP_I
	#undef X_I
	#undef C
	#undef TEXLINE
	#undef PUTPIXEL
	#undef SWAP_V
	#undef STEPSIZE
}

//Bilinear filtered no-wrap 1 channel rasteriser, assumes 8bpp bitmaps
static void kdr_triangle3d_f_persp_bilinear(BITMAP *bmp, int s, BITMAP *tex,  V3D_f *_v1, V3D_f *_v2, V3D_f *_v3)
{
	V3D_f scan_s, scan_e, *scan_l, *scan_r, scan_x;
	V3D_f scan_x_step, scan_s_step, scan_e_step;
	
	V3D_f __verts[3] = {*_v1, *_v2, *_v3};
	V3D_f *verts[] = {&__verts[0], &__verts[1], &__verts[2]};
	V3D_f *tmp_v;
	V3D_f *ve0, *ve1;
	const V3D_f *vs0, *vs1;
	int x_i, y_i, u_i, v_i, u_i_n, v_i_n, x_i_n, x_l, x_r;
	float z_i, z_i_n;
	int pass;
	float tmp;
	unsigned char *pixel;
	const int w_1 = tex->w - 1;
	
	#define SWAP_V(a, b) ({ tmp_v = (a); (a) = (b); (b) = tmp_v; (void)0;})
	#define STEPSIZE(a, b, c) (((b) - (a)) * (c)) 
	
	if(verts[1]->y < verts[0]->y) SWAP_V(verts[1], verts[0]);
	if(verts[2]->y < verts[0]->y) SWAP_V(verts[2], verts[0]);
	if(verts[1]->y > verts[2]->y) SWAP_V(verts[1], verts[2]);
	
	verts[0]->z = 1.0f / verts[0]->z;
	verts[1]->z = 1.0f / verts[1]->z;
	verts[2]->z = 1.0f / verts[2]->z;
	verts[0]->u *= verts[0]->z;
	verts[1]->u *= verts[1]->z;
	verts[2]->u *= verts[2]->z;
	verts[0]->v *= verts[0]->z;
	verts[1]->v *= verts[1]->z;
	verts[2]->v *= verts[2]->z;
	
	vs0 = verts[0];
	vs1 = verts[2];
	
	ve0 = verts[0];
	ve1 = verts[1];
	
	tmp = 1.0f / ((vs1->y) - (vs0->y));
	scan_s_step.z = STEPSIZE(vs0->z, vs1->z, tmp);
	scan_s_step.u = STEPSIZE(vs0->u, vs1->u, tmp);
	scan_s_step.v = STEPSIZE(vs0->v, vs1->v, tmp);
	scan_s_step.x = STEPSIZE(vs0->x, vs1->x, tmp);
	
	scan_s = *vs0;
	tmp = ceil(ve0->y) - scan_s.y;
	scan_s.z += scan_s_step.z * tmp;
	scan_s.u += scan_s_step.u * tmp;
	scan_s.v += scan_s_step.v * tmp;
	scan_s.x += scan_s_step.x * tmp;
	for(pass = 0; pass < 2; pass++){
		tmp = 1.0f / ((ve1->y) - (ve0->y));
		scan_e_step.z = STEPSIZE(ve0->z, ve1->z, tmp);
		scan_e_step.u = STEPSIZE(ve0->u, ve1->u, tmp);
		scan_e_step.v = STEPSIZE(ve0->v, ve1->v, tmp);
		scan_e_step.x = STEPSIZE(ve0->x, ve1->x, tmp);
		
		scan_e = *ve0;
		tmp = ceil(ve0->y) - scan_e.y;
		scan_e.z += scan_e_step.z * tmp;
		scan_e.u += scan_e_step.u * tmp;
		scan_e.v += scan_e_step.v * tmp;
		scan_e.x += scan_e_step.x * tmp;
		
		if((scan_e_step.x > scan_s_step.x) ^ (pass)){
			scan_l = &scan_s;
			scan_r = &scan_e;
		}
		else{
			scan_l = &scan_e;
			scan_r = &scan_s;
		}
		
		#define STEPLEN 16
		#define STEPLENF 16.0f
		
		for(y_i = ceil(ve0->y); y_i < ceil(ve1->y); y_i++){
			tmp = 1.0f / ((scan_r->x) - (scan_l->x));
			scan_x_step.z = STEPSIZE(scan_l->z, scan_r->z, tmp);
			scan_x_step.u = STEPSIZE(scan_l->u, scan_r->u, tmp);
			scan_x_step.v = STEPSIZE(scan_l->v, scan_r->v, tmp);
			
			scan_x = *scan_l;
			x_l = (scan_l->x);
			x_r = (scan_r->x);
			
			scan_s.x += scan_s_step.x;
			scan_s.z += scan_s_step.z;
			scan_s.u += scan_s_step.u;
			scan_s.v += scan_s_step.v;
			
			scan_e.x += scan_e_step.x;
			scan_e.z += scan_e_step.z;
			scan_e.u += scan_e_step.u;
			scan_e.v += scan_e_step.v;
			
			if(y_i < 0) continue;
			if(y_i >= bmp->h) continue;
			
			if(scan_x.x < 0.0f){
				tmp = -scan_x.x;
				scan_x.z += scan_x_step.z * tmp;
				scan_x.u += scan_x_step.u * tmp;
				scan_x.v += scan_x_step.v * tmp;
				scan_x.x = 0.0f;
				x_l = 0;
			}
			if(x_r >= bmp->w) x_r = bmp->w;
			
			scan_x_step.z *= STEPLENF;
			scan_x_step.u *= STEPLENF;
			scan_x_step.v *= STEPLENF;
			
			#define FIXCOEFF 65536.0f
			#define FIXSHIFT 16
			
			z_i =  1.0 / scan_x.z;
			u_i = (FIXCOEFF * scan_x.u * z_i);
			v_i = (FIXCOEFF * scan_x.v * z_i);
			
			scan_x.z += scan_x_step.z;
			scan_x.u += scan_x_step.u;
			scan_x.v += scan_x_step.v;
			
			z_i_n =  1.0f / scan_x.z;
			
			pixel = (unsigned char *) bmp->line[y_i];
			pixel += x_l;
			
			for(x_i = x_l; x_i < x_r; x_i = x_i_n){
				x_i_n = x_i + STEPLEN;
				
				u_i_n = (FIXCOEFF * scan_x.u * z_i_n);
				v_i_n = (FIXCOEFF * scan_x.v * z_i_n);
				u_i_n -= u_i;
				v_i_n -= v_i;
				
				if(x_i_n < x_r){
					scan_x.z += scan_x_step.z;
					scan_x.u += scan_x_step.u;
					scan_x.v += scan_x_step.v;
					z_i_n =  1.0f / scan_x.z;
				}
				
				#define TEXLINE(l) ((__typeof__(pixel)) tex->line[l])
				#define LINTERP(a, b, c) ((a) + ((((b) - (a)) * (c)) >> FIXSHIFT))
				#define U_I(n) (u_i + (u_i_n * (n)))
				#define V_I(n) (v_i + (v_i_n * (n)))
				#define UP_I(n) (U_I(n) >> FIXSHIFT)
				#define VP_I(n) (V_I(n) >> FIXSHIFT)
				#define UF_I(n) (U_I(n) & ((1 << FIXSHIFT) - 1))
				#define VF_I(n) (V_I(n) & ((1 << FIXSHIFT) - 1))
				#define C11(n) (TEXLINE((VP_I(n)+0) & w_1)[(UP_I(n)+0) & w_1])
				#define C12(n) (TEXLINE((VP_I(n)+1) & w_1)[(UP_I(n)+0) & w_1])
				#define C21(n) (TEXLINE((VP_I(n)+0) & w_1)[(UP_I(n)+1) & w_1])
				#define C22(n) (TEXLINE((VP_I(n)+1) & w_1)[(UP_I(n)+1) & w_1])
				#define C(n) (LINTERP(LINTERP(C11(n), C12(n), VF_I(n)), LINTERP(C21(n), C22(n), VF_I(n)), UF_I(n)))
				#define PUTPIXEL(i, c) ({*(pixel + (i)) = (c); (void)0;})
				#define PUTPIXELS(i, c4, c3, c2, c1) ({*(((unsigned int *)pixel) + (i >> 2)) = (c1) | (c2) << 8u | (c3) << 16u | (c4) << 24u;(void) 0;})
				#if 1
				u_i_n >>= 4;
				v_i_n >>= 4;
				switch(x_r - x_i){
					#if 1
					default:
					#if STEPLEN >= 16
					case 16: PUTPIXEL(15, C(15)); case 15: PUTPIXEL(14, C(14));
					case 14: PUTPIXEL(13, C(13)); case 13: PUTPIXEL(12, C(12));
					case 12: PUTPIXEL(11, C(11)); case 11: PUTPIXEL(10, C(10));
					case 10: PUTPIXEL(9, C(9));	case 9: PUTPIXEL(8, C(8));
					#endif
					#if STEPLEN >= 8
					case 8: PUTPIXEL(7, C(7)); case 7: PUTPIXEL(6, C(6));
					case 6:	PUTPIXEL(5, C(5)); case 5: PUTPIXEL(4, C(4));
					#endif
					#if STEPLEN >= 4
					case 4: PUTPIXEL(3, C(3)); case 3: PUTPIXEL(2, C(2));
					#endif
					#if STEPLEN >= 2
					case 2: PUTPIXEL(1, C(1));
					#endif        
					case 1: PUTPIXEL(0, C(0));
					#else
					//This may case unalligned acesses - bad
					default:
					case 16: PUTPIXELS(15, C(15), C(14), C(13), C(12));
					case12:
					case 12: PUTPIXELS(11, C(11), C(10), C(9), C(8));
					case8:
					case 8: PUTPIXELS(7, C(7), C(6), C(5), C(4));
					case4:
					case 4:	PUTPIXELS(3, C(3), C(2), C(1), C(0));
					break;
					case 15: PUTPIXEL(14, C(14));
					case 14: PUTPIXEL(13, C(13));
					case 13: PUTPIXEL(12, C(12));
					goto case12;
					case 11: PUTPIXEL(10, C(10));
					case 10: PUTPIXEL(9, C(9));
					case 9: PUTPIXEL(8, C(8));
					goto case8;
					case 7: PUTPIXEL(6, C(6));
					case 6: PUTPIXEL(5, C(5));
					case 5: PUTPIXEL(4, C(4));
					goto case4;
					case 3: PUTPIXEL(2, C(2));
					case 2: PUTPIXEL(1, C(1));
					case 1: PUTPIXEL(0, C(0));
					#endif
				}
				u_i += u_i_n << 4;
				v_i += v_i_n << 4;
				pixel += STEPLEN;
				#else
				const int ix = x_i_n - x_i;
				for(;
					x_i < x_i_n && x_i < x_r;
					x_i++, u_i += u_i_n / ix, v_i += v_i_n / ix, pixel++
				){
					const int up_i = (u_i >> FIXSHIFT);
					const int vp_i = (v_i >> FIXSHIFT);
					const int uf_i = (u_i & ((1 << FIXSHIFT) - 1));
					const int vf_i = (v_i & ((1 << FIXSHIFT) - 1));
					const int c11 = (TEXLINE((vp_i+0) & w_1)[(up_i+0) & w_1]);
					const int c12 = (TEXLINE((vp_i+1) & w_1)[(up_i+0) & w_1]);
					const int c21 = (TEXLINE((vp_i+0) & w_1)[(up_i+1) & w_1]);
					const int c22 = (TEXLINE((vp_i+1) & w_1)[(up_i+1) & w_1]);
					const int c1 = LINTERP(c11, c12, vf_i);
					const int c2 = LINTERP(c21, c22, vf_i);
					const unsigned char c = LINTERP(c1, c2, uf_i);
					*pixel = c;
				}
				#endif
			}
		}
		ve0 = verts[1];
		ve1 = verts[2];
	}
	
	#undef FIXCOEFF
	#undef FIXSHIFT
	#undef STEPLEN
	#undef STEPLENF
	#undef UP_I
	#undef VP_I
	#undef UF_I
	#undef VF_I
	#undef C11
	#undef C12
	#undef C21
	#undef C22
	#undef C
	#undef PUTPIXEL
	#undef SWAP_V
	#undef STEPSIZE
}

kdr_zbuf *kdr_active_zbuf = 0;

kdr_zbuf *kdr_create_zbuf(int w, int h)
{
	int i;
	kdr_zbuf *zb = malloc(sizeof(*zb) + sizeof(*zb->line[0]) * h);
	
	zb->w = w;
	zb->h = h;
	
	zb->data = malloc(sizeof(*zb->data) * w * h);
	zb->line[0] = zb->data;
	for(i = 1; i < h; i++){
		zb->line[i] = zb->line[i - 1] + w;
	}
	
	return zb;
}

void kdr_destroy_zbuf(kdr_zbuf *zb)
{
	free(zb->data);
	free(zb);
}

void kdr_create_zbuf_static(kdr_zbuf *zb, int w, int h, int *data)
{
	int i;
	
	zb->w = w;
	zb->h = h;
	
	zb->data = data;
	zb->line[0] = zb->data;
	for(i = 1; i < h; i++){
		zb->line[i] = zb->line[i - 1] + w;
	}
}

void kdr_clear_zbuf(const kdr_zbuf *zb)
{
	memset(zb->data, 0, zb->w * zb->h * sizeof(*zb->data));
}

/*
zbuffer writing function, based on the above
Doesn't do masking
TODO: remove use of V3D_f, replace some pointers with copies, etc.
*/
static void kdr_triangle3d_f_perspzb(BITMAP *bmp, int s, BITMAP *tex, V3D_f *_v1, V3D_f *_v2, V3D_f *_v3)
{
	V3D_f scan_s, scan_e, *scan_l, *scan_r, scan_x;
	V3D_f scan_x_step, scan_s_step, scan_e_step;
	
	V3D_f __verts[3] = {*_v1, *_v2, *_v3};
	V3D_f *verts[] = {&__verts[0], &__verts[1], &__verts[2]};
	V3D_f *tmp_v;
	V3D_f *ve0, *ve1;
	const V3D_f *vs0, *vs1;
	int x_i, y_i, x_l, x_r;
	int pass;
	float tmp;
	int *zb_pixel;
	
	int scan_x_z_i;
	int scan_x_step_z_i;
	
	#define SWAP_V(a, b) ({ tmp_v = (a); (a) = (b); (b) = tmp_v; (void)0;})
	#define STEPSIZE(a, b, c) (((b) - (a)) * (c)) 
	
	if(verts[1]->y < verts[0]->y) SWAP_V(verts[1], verts[0]);
	if(verts[2]->y < verts[0]->y) SWAP_V(verts[2], verts[0]);
	if(verts[1]->y > verts[2]->y) SWAP_V(verts[1], verts[2]);
	
	verts[0]->z = (((float) INT_MAX) * KDR_NEAR) / verts[0]->z;
	verts[1]->z = (((float) INT_MAX) * KDR_NEAR) / verts[1]->z;
	verts[2]->z = (((float) INT_MAX) * KDR_NEAR) / verts[2]->z;
	
	vs0 = verts[0];
	vs1 = verts[2];
	
	ve0 = verts[0];
	ve1 = verts[1];
	
	tmp = 1.0f / ((vs1->y) - (vs0->y));
	scan_s_step.z = STEPSIZE(vs0->z, vs1->z, tmp);
	scan_s_step.x = STEPSIZE(vs0->x, vs1->x, tmp);
	
	scan_s = *vs0;
	tmp = ceil(ve0->y) - scan_s.y;
	scan_s.z += scan_s_step.z * tmp;
	scan_s.x += scan_s_step.x * tmp;
	for(pass = 0; pass < 2; pass++){
		tmp = 1.0f / ((ve1->y) - (ve0->y));
		scan_e_step.z = STEPSIZE(ve0->z, ve1->z, tmp);
		scan_e_step.x = STEPSIZE(ve0->x, ve1->x, tmp);
		
		scan_e = *ve0;
		tmp = ceil(ve0->y) - scan_e.y;
		scan_e.z += scan_e_step.z * tmp;
		scan_e.x += scan_e_step.x * tmp;
		
		if((scan_e_step.x > scan_s_step.x) ^ (pass)){
			scan_l = &scan_s;
			scan_r = &scan_e;
		}
		else{
			scan_l = &scan_e;
			scan_r = &scan_s;
		}
		
		for(y_i = ceil(ve0->y); y_i < ceil(ve1->y); y_i++){
			tmp = 1.0f / ((scan_r->x) - (scan_l->x));
			scan_x_step.z = STEPSIZE(scan_l->z, scan_r->z, tmp);
			
			scan_x = *scan_l;
			x_l = (scan_l->x);
			x_r = (scan_r->x);
			
			scan_s.x += scan_s_step.x;
			scan_s.z += scan_s_step.z;
			
			scan_e.x += scan_e_step.x;
			scan_e.z += scan_e_step.z;
			
			if(y_i < 0) continue;
			if(y_i >= kdr_active_zbuf->h) continue;
			
			if(scan_x.x < 0.0f){
				tmp = -scan_x.x;
				scan_x.z += scan_x_step.z * tmp;
				scan_x.x = 0.0f;
				x_l = 0;
			}
			if(x_r >= kdr_active_zbuf->w) x_r = kdr_active_zbuf->w;
			
			zb_pixel = kdr_active_zbuf->line[y_i];
			zb_pixel += x_l;
			
			scan_x_z_i = scan_x.z;
			scan_x_step_z_i = scan_x_step.z;
			
			for(x_i = x_l; x_i < x_r; x_i++){
				*zb_pixel = scan_x_z_i;
				scan_x_z_i += scan_x_step_z_i;
				zb_pixel++;
			}
		}
		ve0 = verts[1];
		ve1 = verts[2];
	}
	
	#undef SWAP_V
	#undef STEPSIZE
}

#define N1 1000
#define N2 2000
static uint16_t ktaz_y[N1], ktaz_n[N2], ktaz_x[N2], ktaz_len[N2];
#undef N1
#undef N2
typedef struct ktaz_ptrs
{
	uint16_t *y, *n, *x, *l;
} ktaz_ptrs;
const static ktaz_ptrs ktaz_p0 = {ktaz_y, ktaz_n, ktaz_x, ktaz_len};
static ktaz_ptrs ktaz_ptr;

#define KTAZ_RESET \
	ktaz_ptr = ktaz_p0;
#define KTAZ_OPEN \
	*ktaz_ptr.y = y_i;\
	*ktaz_ptr.n = 0;\
	*ktaz_ptr.l = 0;
#define KTAZ_PIXEL	\
	if(!(*ktaz_ptr.l)) *ktaz_ptr.x = x_i;\
	*ktaz_ptr.l += 1;
#define KTAZ_EMPTY \
	if(*ktaz_ptr.l){\
		*ktaz_ptr.n += 1;\
		ktaz_ptr.l++;\
		*ktaz_ptr.l = 0;\
		ktaz_ptr.x++;\
	}
#define KTAZ_CLOSE \
	KTAZ_EMPTY \
	if(*ktaz_ptr.n){\
		ktaz_ptr.n++;\
		ktaz_ptr.y++;\
	}

/*
Random internet code to solve system of 3 lin eqs:

float a,b,c,d,l,m,n,k,p,D,q,r,s,x,y,z;
printf("PROGRAM TO SOLVE THREE VARIABLE LINEAR SIMULTANEOUS EQUATIONS");
printf("The equations are of the form:"
"ax+by+cz+d=0"
"lx+my+nz+k=0"
"px+qy+rz+s=0");
printf("Enter the coefficients in the order a,b,c,d,l,m,n,k,p,q,r,s");
scanf("%f%f%f%f%f%f%f%f%f%f%f%f",&a,&b,&c,&d,&l,&m,&n,&k,&p,&q,&r,&s);
printf("The equations you have input are:");
printf("  %.2f*x + %.2f*y + %.2f*z + %.2f = 0",a,b,c,d);
printf("  %.2f*x + %.2f*y + %.2f*z + %.2f = 0",l,m,n,k);
printf("  %.2f*x + %.2f*y + %.2f*z + %.2f = 0",p,q,r,s);
D = (a*m*r+b*p*n+c*l*q)-(a*n*q+b*l*r+c*m*p);
x = ((b*r*k+c*m*s+d*n*q)-(b*n*s+c*q*k+d*m*r))/D;
y = ((a*n*s+c*p*k+d*l*r)-(a*r*k+c*l*s+d*n*p))/D;
z = ((a*q*k+b*l*s+d*m*p)-(a*m*s+b*p*k+d*l*q))/D;

printf("The solutions to the above three equations are :");
printf("  x = %5.2f  y = %5.2f  z = %5.2f",x,y,z);
getch();
return 0;
*/

void kdr_affinelit(BITMAP *bmp, V3D_f *v1, V3D_f *v2, V3D_f *v3)
{
	const int v1x = v1->x;
	const int v2x = v2->x;
	const int v3x = v3->x;
	
	const int v1y = v1->y;
	const int v2y = v2->y;
	const int v3y = v3->y;
	
	const int v1c = v1->c;
	const int v2c = v2->c;
	const int v3c = v3->c;
	
	#if 1
	#define KDR_AFFINELIT_LINTERP
	//Linear interpolation:
	//
	//v1x * a + v1y * b + c = v1c
	//v2x * a + v2y * b + c = v2c
	//v3x * a + v3y * b + c = v3c
	//
	//The above transformed:
	//
	//v1x * a + v1y * b + c - v1c = 0
	//v2x * a + v2y * b + c - v2c = 0
	//v3x * a + v3y * b + c - v3c = 0
	
	int D, a = 0, b = 0, c = 0;
	
	//Thus plugging the linear interpolation eqs into the solver code above:
	#define Ex a
	#define Ey b
	#define Ez c
	#define ED D
	#define Ea ((__typeof__(D))(v1x   ))
	#define Eb ((__typeof__(D))(v1y   ))
	#define Ec ((__typeof__(D))(1     ))
	#define Ed ((__typeof__(D))((-v1c)))
	#define El ((__typeof__(D))(v2x   ))
	#define Em ((__typeof__(D))(v2y   ))
	#define En ((__typeof__(D))(1     ))
	#define Ek ((__typeof__(D))((-v2c)))
	#define Ep ((__typeof__(D))(v3x   ))
	#define Eq ((__typeof__(D))(v3y   ))
	#define Er ((__typeof__(D))(1     ))
	#define Es ((__typeof__(D))((-v3c)))
	
	#define ERes 256
	
	ED = (Ea * Em * Er + Eb * Ep * En + Ec * El * Eq) - (Ea * En * Eq + Eb * El * Er + Ec * Em * Ep);
	if(ED){
		Ex = (((Eb * Er * Ek + Ec * Em * Es + Ed * En * Eq) - (Eb * En * Es + Ec * Eq * Ek + Ed * Em * Er)) * ERes) / ED;
		Ey = (((Ea * En * Es + Ec * Ep * Ek + Ed * El * Er) - (Ea * Er * Ek + Ec * El * Es + Ed * En * Ep)) * ERes) / ED;
		//Constant term needs no fixed point factor
		Ez = (((Ea * Eq * Ek + Eb * El * Es + Ed * Em * Ep) - (Ea * Em * Es + Eb * Ep * Ek + Ed * El * Eq))       ) / ED;
	}
	
	#undef Ex
	#undef Ey
	#undef Ez
	#undef Ea
	#undef Eb
	#undef Ec
	#undef Ed
	#undef El
	#undef Em
	#undef En
	#undef Ek
	#undef Ep
	#undef Eq
	#undef Er
	#undef Es
	#undef ED
	
	#endif
	
	const uint16_t *max = ktaz_ptr.y;
	for(ktaz_ptr = ktaz_p0; ktaz_ptr.y < max; ktaz_ptr.y++, ktaz_ptr.n++){
		for(; *ktaz_ptr.n; *ktaz_ptr.n -= 1, ktaz_ptr.l++, ktaz_ptr.x++){
			for(; *ktaz_ptr.l; *ktaz_ptr.l -= 1){
				const int x = *ktaz_ptr.x + *ktaz_ptr.l - 1;
				const int y = *ktaz_ptr.y;
				_putpixel(bmp, x, y,
					#ifdef KDR_AFFINELIT_LINTERP
					MIN(MAX(0, (x * a + y * b) / ERes + c), 255)
					#undef ERes
					#undef KDR_AFFINELIT_LINTERP
					#else
					(v3c + v2c + v1c) / 3
					#endif
				);
			}
		}
	}
}

//Affine textured triangle drawing function, with depth checks
//Doesn't do masking
static void kdr_triangle3d_f_affinezb32bpp(BITMAP *bmp, int s, BITMAP *tex,  V3D_f *_v1, V3D_f *_v2, V3D_f *_v3)
{
	V3D_f scan_s, scan_e, *scan_l, *scan_r, scan_x, scan_x_step;
	V3D_f scan_s_step, scan_e_step;
	
	V3D_f __verts[3] = {*_v1, *_v2, *_v3};
	V3D_f *verts[] = {&__verts[0], &__verts[1], &__verts[2]};
	V3D_f *tmp_v;
	V3D_f *ve0, *ve1;
	const V3D_f *vs0, *vs1;
	int x_i, y_i, x_l, x_r;
	float tmp;
	int pass;
	int scan_x_z_i, scan_x_u_i, scan_x_v_i;
	int scan_x_step_z_i, scan_x_step_u_i, scan_x_step_v_i;
	int *zb_pixel, *pixel;
	const int w_1 = tex->w - 1;
	
	#define SWAP_V(a, b) ({ tmp_v = (a); (a) = (b); (b) = tmp_v; (void)0;})
	#define STEPSIZE(a, b, c) (((b) - (a)) * (c)) 
	
	if(verts[1]->y < verts[0]->y) SWAP_V(verts[1], verts[0]);
	if(verts[2]->y < verts[0]->y) SWAP_V(verts[2], verts[0]);
	if(verts[1]->y > verts[2]->y) SWAP_V(verts[1], verts[2]);
	
	verts[0]->z = (((float) INT_MAX) * KDR_NEAR) / verts[0]->z;
	verts[1]->z = (((float) INT_MAX) * KDR_NEAR) / verts[1]->z;
	verts[2]->z = (((float) INT_MAX) * KDR_NEAR) / verts[2]->z;
	
	#define FIXCOEFF 256.0f
	#define FIXSHIFT 8
	
	verts[0]->u *= FIXCOEFF;
	verts[1]->u *= FIXCOEFF;
	verts[2]->u *= FIXCOEFF;
	verts[0]->v *= FIXCOEFF;
	verts[1]->v *= FIXCOEFF;
	verts[2]->v *= FIXCOEFF;
	
	vs0 = verts[0];
	vs1 = verts[2];
	
	ve0 = verts[0];
	ve1 = verts[1];
	
	tmp = 1.0f / ((vs1->y) - (vs0->y));
	scan_s_step.z = STEPSIZE(vs0->z, vs1->z, tmp);
	scan_s_step.u = STEPSIZE(vs0->u, vs1->u, tmp);
	scan_s_step.v = STEPSIZE(vs0->v, vs1->v, tmp);
	scan_s_step.x = STEPSIZE(vs0->x, vs1->x, tmp);
	
	scan_s = *vs0;
	tmp = ceil(ve0->y) - scan_s.y;
	scan_s.z += scan_s_step.z * tmp;
	scan_s.u += scan_s_step.u * tmp;
	scan_s.v += scan_s_step.v * tmp;
	scan_s.x += scan_s_step.x * tmp;
	
	for(pass = 0; pass < 2; pass++){
		tmp = 1.0f / ((ve1->y) - (ve0->y));
		scan_e_step.z = STEPSIZE(ve0->z, ve1->z, tmp);
		scan_e_step.u = STEPSIZE(ve0->u, ve1->u, tmp);
		scan_e_step.v = STEPSIZE(ve0->v, ve1->v, tmp);
		scan_e_step.x = STEPSIZE(ve0->x, ve1->x, tmp);
		
		scan_e = *ve0;
		tmp = ceil(ve0->y) - scan_e.y;
		scan_e.z += scan_e_step.z * tmp;
		scan_e.u += scan_e_step.u * tmp;
		scan_e.v += scan_e_step.v * tmp;
		scan_e.x += scan_e_step.x * tmp;
		
		if((scan_e_step.x > scan_s_step.x) ^ (pass)){
			scan_l = &scan_s;
			scan_r = &scan_e;
		}
		else{
			scan_l = &scan_e;
			scan_r = &scan_s;
		}
		
		for(y_i = ceil(ve0->y); y_i < ceil(ve1->y); y_i++){
			tmp = 1.0f / (scan_r->x - scan_l->x);
			scan_x_step.z = STEPSIZE(scan_l->z, scan_r->z, tmp);
			scan_x_step.u = STEPSIZE(scan_l->u, scan_r->u, tmp);
			scan_x_step.v = STEPSIZE(scan_l->v, scan_r->v, tmp);
			
			scan_x = *scan_l;
			
			x_l = scan_l->x;
			x_r = scan_r->x;
			
			if(y_i < 0) continue;
			if(y_i >= bmp->h) continue;
			
			if(scan_x.x < 0.0f){
				tmp = -scan_x.x;
				scan_x.z += scan_x_step.z * tmp;
				scan_x.u += scan_x_step.u * tmp;
				scan_x.v += scan_x_step.v * tmp;
				scan_x.x = 0.0f;
				x_l = 0;
			}
			if(x_r >= bmp->w) x_r = bmp->w;
			
			scan_x_z_i = scan_x.z;
			scan_x_u_i = scan_x.u;
			scan_x_v_i = scan_x.v;
			scan_x_step_z_i = scan_x_step.z;
			scan_x_step_u_i = scan_x_step.u;
			scan_x_step_v_i = scan_x_step.v;
			
			zb_pixel = kdr_active_zbuf->line[y_i];
			zb_pixel += x_l;
			
			pixel = (int *) bmp->line[y_i];
			pixel += x_l;
			
			KTAZ_OPEN
			
			for(x_i = x_l; x_i < x_r; x_i++){
				if(*zb_pixel < scan_x_z_i){
					KTAZ_PIXEL
					
					*zb_pixel = scan_x_z_i;
					*pixel = ((int *) tex->line[(scan_x_v_i >> FIXSHIFT) & w_1])[(scan_x_u_i >> FIXSHIFT) & w_1];
				}
				else
					KTAZ_EMPTY
				
				scan_x_z_i += scan_x_step_z_i;
				scan_x_u_i += scan_x_step_u_i;
				scan_x_v_i += scan_x_step_v_i;
				zb_pixel++;
				pixel++;
			}
			
			KTAZ_CLOSE
			
			scan_s.x += scan_s_step.x;
			scan_s.z += scan_s_step.z;
			scan_s.u += scan_s_step.u;
			scan_s.v += scan_s_step.v;
			
			scan_e.x += scan_e_step.x;
			scan_e.z += scan_e_step.z;
			scan_e.u += scan_e_step.u;
			scan_e.v += scan_e_step.v;
		}
		ve0 = verts[1];
		ve1 = verts[2];
	}
	
	#undef SWAP_V
	#undef STEPSIZE
	#undef FIXCOEFF
	#undef FIXSHIFT
}

static void kdr_triangle3d_f_affinezb8bpp(BITMAP *bmp, int s, BITMAP *tex,  V3D_f *_v1, V3D_f *_v2, V3D_f *_v3)
{
	V3D_f scan_s, scan_e, *scan_l, *scan_r, scan_x, scan_x_step;
	V3D_f scan_s_step, scan_e_step;
	
	V3D_f __verts[3] = {*_v1, *_v2, *_v3};
	V3D_f *verts[] = {&__verts[0], &__verts[1], &__verts[2]};
	V3D_f *tmp_v;
	V3D_f *ve0, *ve1;
	const V3D_f *vs0, *vs1;
	int x_i, y_i, x_l, x_r;
	float tmp;
	int pass;
	int scan_x_z_i, scan_x_u_i, scan_x_v_i;
	int scan_x_step_z_i, scan_x_step_u_i, scan_x_step_v_i;
	int *zb_pixel;
	char *pixel;
	const int w_1 = tex->w - 1;
	
	#define SWAP_V(a, b) ({ tmp_v = (a); (a) = (b); (b) = tmp_v; (void)0;})
	#define STEPSIZE(a, b, c) (((b) - (a)) * (c)) 
	
	if(verts[1]->y < verts[0]->y) SWAP_V(verts[1], verts[0]);
	if(verts[2]->y < verts[0]->y) SWAP_V(verts[2], verts[0]);
	if(verts[1]->y > verts[2]->y) SWAP_V(verts[1], verts[2]);
	
	verts[0]->z = (((float) INT_MAX) * KDR_NEAR) / verts[0]->z;
	verts[1]->z = (((float) INT_MAX) * KDR_NEAR) / verts[1]->z;
	verts[2]->z = (((float) INT_MAX) * KDR_NEAR) / verts[2]->z;
	
	#define FIXCOEFF 256.0f
	#define FIXSHIFT 8
	
	verts[0]->u *= FIXCOEFF;
	verts[1]->u *= FIXCOEFF;
	verts[2]->u *= FIXCOEFF;
	verts[0]->v *= FIXCOEFF;
	verts[1]->v *= FIXCOEFF;
	verts[2]->v *= FIXCOEFF;
	
	vs0 = verts[0];
	vs1 = verts[2];
	
	ve0 = verts[0];
	ve1 = verts[1];
	
	tmp = 1.0f / ((vs1->y) - (vs0->y));
	scan_s_step.z = STEPSIZE(vs0->z, vs1->z, tmp);
	scan_s_step.u = STEPSIZE(vs0->u, vs1->u, tmp);
	scan_s_step.v = STEPSIZE(vs0->v, vs1->v, tmp);
	scan_s_step.x = STEPSIZE(vs0->x, vs1->x, tmp);
	
	scan_s = *vs0;
	tmp = ceil(ve0->y) - scan_s.y;
	scan_s.z += scan_s_step.z * tmp;
	scan_s.u += scan_s_step.u * tmp;
	scan_s.v += scan_s_step.v * tmp;
	scan_s.x += scan_s_step.x * tmp;
	
	for(pass = 0; pass < 2; pass++){
		tmp = 1.0f / ((ve1->y) - (ve0->y));
		scan_e_step.z = STEPSIZE(ve0->z, ve1->z, tmp);
		scan_e_step.u = STEPSIZE(ve0->u, ve1->u, tmp);
		scan_e_step.v = STEPSIZE(ve0->v, ve1->v, tmp);
		scan_e_step.x = STEPSIZE(ve0->x, ve1->x, tmp);
		
		scan_e = *ve0;
		tmp = ceil(ve0->y) - scan_e.y;
		scan_e.z += scan_e_step.z * tmp;
		scan_e.u += scan_e_step.u * tmp;
		scan_e.v += scan_e_step.v * tmp;
		scan_e.x += scan_e_step.x * tmp;
		
		if((scan_e_step.x > scan_s_step.x) ^ (pass)){
			scan_l = &scan_s;
			scan_r = &scan_e;
		}
		else{
			scan_l = &scan_e;
			scan_r = &scan_s;
		}
		
		for(y_i = ceil(ve0->y); y_i < ceil(ve1->y); y_i++){
			tmp = 1.0f / (scan_r->x - scan_l->x);
			scan_x_step.z = STEPSIZE(scan_l->z, scan_r->z, tmp);
			scan_x_step.u = STEPSIZE(scan_l->u, scan_r->u, tmp);
			scan_x_step.v = STEPSIZE(scan_l->v, scan_r->v, tmp);
			
			scan_x = *scan_l;
			
			x_l = scan_l->x;
			x_r = scan_r->x;
			
			if(y_i < 0) continue;
			if(y_i >= bmp->h) continue;
			
			if(scan_x.x < 0.0f){
				tmp = -scan_x.x;
				scan_x.z += scan_x_step.z * tmp;
				scan_x.u += scan_x_step.u * tmp;
				scan_x.v += scan_x_step.v * tmp;
				scan_x.x = 0.0f;
				x_l = 0;
			}
			if(x_r >= bmp->w) x_r = bmp->w;
			
			scan_x_z_i = scan_x.z;
			scan_x_u_i = scan_x.u;
			scan_x_v_i = scan_x.v;
			scan_x_step_z_i = scan_x_step.z;
			scan_x_step_u_i = scan_x_step.u;
			scan_x_step_v_i = scan_x_step.v;
			
			zb_pixel = kdr_active_zbuf->line[y_i];
			zb_pixel += x_l;
			
			pixel = (__typeof__(pixel)) bmp->line[y_i];
			pixel += x_l;
			
			KTAZ_OPEN
			
			for(x_i = x_l; x_i < x_r; x_i++){
				if(*zb_pixel < scan_x_z_i){
					KTAZ_PIXEL
					
					*zb_pixel = scan_x_z_i;
					*pixel = ((__typeof__(pixel)) tex->line[(scan_x_v_i >> FIXSHIFT) & w_1])[(scan_x_u_i >> FIXSHIFT) & w_1];
				}
				else
					KTAZ_EMPTY
				
				scan_x_z_i += scan_x_step_z_i;
				scan_x_u_i += scan_x_step_u_i;
				scan_x_v_i += scan_x_step_v_i;
				zb_pixel++;
				pixel++;
			}
			
			KTAZ_CLOSE
			
			scan_s.x += scan_s_step.x;
			scan_s.z += scan_s_step.z;
			scan_s.u += scan_s_step.u;
			scan_s.v += scan_s_step.v;
			
			scan_e.x += scan_e_step.x;
			scan_e.z += scan_e_step.z;
			scan_e.u += scan_e_step.u;
			scan_e.v += scan_e_step.v;
		}
		ve0 = verts[1];
		ve1 = verts[2];
	}
	
	#undef SWAP_V
	#undef STEPSIZE
	#undef FIXCOEFF
	#undef FIXSHIFT
}

static int kpf_x, kpf_y;

void kdr_particle3d_f(struct BITMAP *a, float x, float y, float z, int c)
{
	if(z < KDR_NEAR) return;
	
	kpf_x = (int)x;
	kpf_y = (int)y;
	
	if(kpf_x < 0 || kpf_x >= a->w || kpf_y < 0 || kpf_y >= a->h) return;
	
	int *zb_pixel;
	int iz = (((float) INT_MAX) * KDR_NEAR) / z;
	zb_pixel = kdr_active_zbuf->line[kpf_y];
	zb_pixel += kpf_x;
	if(*zb_pixel < iz){
		*zb_pixel = iz;
		putpixel(a, kpf_x, kpf_y, c);
	}
	else kpf_x = -1;
}

void kdr_particle3d_lit(struct BITMAP *a, int c)
{
	if(kpf_x >= 0 && kpf_x < a->w && kpf_y >= 0 && kpf_y < a->h) _putpixel(a, kpf_x, kpf_y, c);
}

void kdr_sprite3d_f(struct BITMAP *bmp, struct BITMAP *lit, struct sprsheet *spr, int frame, V3D_f *verts, int flip_h)
{
	int l = verts[0].c;
	int xa = floor(verts[0].x), xb = ceil(verts[1].x);
	int ya = floor(verts[0].y), yb = ceil(verts[1].y);
	
	//TODO: into fixed point,
	//	filog2(i)				->	31u-__builtin_clz((int)i)
	//	floor(log2((int)i))		==	filog2(i)
	//	-floor(0.5 * log2(ds))	==	(log2(ws + hs) - log2(dx * dx + dy * dy)) * 0.5
	//*/
	//const double
	//	ws = (*mips)->w * (*mips)->w,
	//	hs = (*mips)->h * (*mips)->h,
	double
		dx = xb - xa,
		dy = yb - ya;
	//	ds = (dx * dx + dy * dy) / (ws + hs),
	//	hlds = -floor(0.5 * log2(ds));
	//const int dtex = MIN(MAX(((int)(hlds)), 0), MAX(raster_mipmap_levels - 1, 0));
	//mips += dtex;
	//BITMAP *tex = *mips;
	
	BITMAP *tex = spr->src;
	
	int iz = (((float) INT_MAX) * KDR_NEAR) / verts[0].z;
	
	#define FIXSHIFT 12
	#define FIXCOEFF (1<<FIXSHIFT)
	int 
		tex_dx = (spr->frames[frame].w * FIXCOEFF) / dx,
		tex_dy = (spr->frames[frame].h * FIXCOEFF) / dy,
		tex_x = spr->frames[frame].x * FIXCOEFF,
		tex_y = spr->frames[frame].y * FIXCOEFF;
	
	if(flip_h){
		tex_dx *= -1;
		tex_x += spr->frames[frame].w * FIXCOEFF;
	}
	
	if(xa < 0){
		tex_x += tex_dx * (-xa);
		xa = 0;
	}
	if(ya < 0){
		tex_y += tex_dy * (-ya);
		ya = 0;
	}
	if(xb > bmp->w) xb = bmp->w;
	if(yb > bmp->h) yb = bmp->h;
	
	int *zb_pixel;
	int x, y, tx, ty;
	if(get_color_depth() == 8){
		for(y = ya, ty = tex_y; y < yb; y++, ty += tex_dy){
			zb_pixel = kdr_active_zbuf->line[y];
			for(x = xa, tx = tex_x, zb_pixel += xa; x < xb; x++, tx += tex_dx, zb_pixel++){
				if(*zb_pixel < iz){
					const int c = _getpixel(tex, tx / FIXCOEFF, ty / FIXCOEFF);
					if(c != MASK_COLOR_8){
						_putpixel(bmp, x, y, c);
						if(lit) _putpixel(lit, x, y, l);
						*zb_pixel = iz;
					}
				}
			}
		}
	}
	else {
		for(y = ya, ty = tex_y; y < yb; y++, ty += tex_dy){
			zb_pixel = kdr_active_zbuf->line[y];
			for(x = xa, tx = tex_x, zb_pixel += xa; x < xb; x++, tx += tex_dx, zb_pixel++){
				if(*zb_pixel < iz){
					const int c = _getpixel32(tex, tx / FIXCOEFF, ty / FIXCOEFF);
					if(c != MASK_COLOR_32){
						_putpixel32(bmp, x, y, c);
						if(lit) _putpixel(lit, x, y, l);
						*zb_pixel = iz;
					}
				}
			}
		}
	}
	
	#undef FIXCOEFF
	#undef FIXSHIFT
}

//Affine textured triangle drawing function, with depth checks
//Doesn't do masking
static void kdr_triangle3d_f_affinetransp32bpp(BITMAP *bmp, int s, BITMAP *tex,  V3D_f *_v1, V3D_f *_v2, V3D_f *_v3)
{
	V3D_f scan_s, scan_e, *scan_l, *scan_r, scan_x, scan_x_step;
	V3D_f scan_s_step, scan_e_step;
	
	V3D_f __verts[3] = {*_v1, *_v2, *_v3};
	V3D_f *verts[] = {&__verts[0], &__verts[1], &__verts[2]};
	V3D_f *tmp_v;
	V3D_f *ve0, *ve1;
	const V3D_f *vs0, *vs1;
	int x_i, y_i, x_l, x_r;
	float tmp;
	int pass;
	int scan_x_u_i, scan_x_v_i;
	int scan_x_step_u_i, scan_x_step_v_i;
	int *pixel;
	const int w_1 = tex->w - 1;
	
	#define SWAP_V(a, b) ({ tmp_v = (a); (a) = (b); (b) = tmp_v; (void)0;})
	#define STEPSIZE(a, b, c) (((b) - (a)) * (c)) 
	
	if(verts[1]->y < verts[0]->y) SWAP_V(verts[1], verts[0]);
	if(verts[2]->y < verts[0]->y) SWAP_V(verts[2], verts[0]);
	if(verts[1]->y > verts[2]->y) SWAP_V(verts[1], verts[2]);
	
	#define FIXCOEFF 256.0f
	#define FIXSHIFT 8
	
	verts[0]->u *= FIXCOEFF;
	verts[1]->u *= FIXCOEFF;
	verts[2]->u *= FIXCOEFF;
	verts[0]->v *= FIXCOEFF;
	verts[1]->v *= FIXCOEFF;
	verts[2]->v *= FIXCOEFF;
	
	vs0 = verts[0];
	vs1 = verts[2];
	
	ve0 = verts[0];
	ve1 = verts[1];
	
	tmp = 1.0f / ((vs1->y) - (vs0->y));
	scan_s_step.u = STEPSIZE(vs0->u, vs1->u, tmp);
	scan_s_step.v = STEPSIZE(vs0->v, vs1->v, tmp);
	scan_s_step.x = STEPSIZE(vs0->x, vs1->x, tmp);
	
	scan_s = *vs0;
	tmp = ceil(ve0->y) - scan_s.y;
	scan_s.u += scan_s_step.u * tmp;
	scan_s.v += scan_s_step.v * tmp;
	scan_s.x += scan_s_step.x * tmp;
	
	for(pass = 0; pass < 2; pass++){
		tmp = 1.0f / ((ve1->y) - (ve0->y));
		scan_e_step.u = STEPSIZE(ve0->u, ve1->u, tmp);
		scan_e_step.v = STEPSIZE(ve0->v, ve1->v, tmp);
		scan_e_step.x = STEPSIZE(ve0->x, ve1->x, tmp);
		
		scan_e = *ve0;
		tmp = ceil(ve0->y) - scan_e.y;
		scan_e.u += scan_e_step.u * tmp;
		scan_e.v += scan_e_step.v * tmp;
		scan_e.x += scan_e_step.x * tmp;
		
		if((scan_e_step.x > scan_s_step.x) ^ (pass)){
			scan_l = &scan_s;
			scan_r = &scan_e;
		}
		else{
			scan_l = &scan_e;
			scan_r = &scan_s;
		}
		
		for(y_i = ceil(ve0->y); y_i < ceil(ve1->y); y_i++){
			tmp = 1.0f / (scan_r->x - scan_l->x);
			scan_x_step.u = STEPSIZE(scan_l->u, scan_r->u, tmp);
			scan_x_step.v = STEPSIZE(scan_l->v, scan_r->v, tmp);
			
			scan_x = *scan_l;
			
			x_l = scan_l->x;
			x_r = scan_r->x;
			
			if(y_i < 0) continue;
			if(y_i >= bmp->h) continue;
			
			if(scan_x.x < 0.0f){
				tmp = -scan_x.x;
				scan_x.u += scan_x_step.u * tmp;
				scan_x.v += scan_x_step.v * tmp;
				scan_x.x = 0.0f;
				x_l = 0;
			}
			if(x_r >= bmp->w) x_r = bmp->w;
			
			scan_x_u_i = scan_x.u;
			scan_x_v_i = scan_x.v;
			scan_x_step_u_i = scan_x_step.u;
			scan_x_step_v_i = scan_x_step.v;
			
			pixel = (int *) bmp->line[y_i];
			pixel += x_l;
			
			for(x_i = x_l; x_i < x_r; x_i++){
				const int c = ((int *) tex->line[(scan_x_v_i >> FIXSHIFT) & w_1])[(scan_x_u_i >> FIXSHIFT) & w_1];
				if((c & 0xFFFFFF) != 0xFF00FF){
					*pixel = c;
				}
				
				scan_x_u_i += scan_x_step_u_i;
				scan_x_v_i += scan_x_step_v_i;
				pixel++;
			}
			
			scan_s.x += scan_s_step.x;
			scan_s.u += scan_s_step.u;
			scan_s.v += scan_s_step.v;
			
			scan_e.x += scan_e_step.x;
			scan_e.u += scan_e_step.u;
			scan_e.v += scan_e_step.v;
		}
		ve0 = verts[1];
		ve1 = verts[2];
	}
	
	#undef SWAP_V
	#undef STEPSIZE
	#undef FIXCOEFF
	#undef FIXSHIFT
}

static void kdr_triangle3d_f_affinetransp8bpp(BITMAP *bmp, int s, BITMAP *tex,  V3D_f *_v1, V3D_f *_v2, V3D_f *_v3)
{
	V3D_f scan_s, scan_e, *scan_l, *scan_r, scan_x, scan_x_step;
	V3D_f scan_s_step, scan_e_step;
	
	V3D_f __verts[3] = {*_v1, *_v2, *_v3};
	V3D_f *verts[] = {&__verts[0], &__verts[1], &__verts[2]};
	V3D_f *tmp_v;
	V3D_f *ve0, *ve1;
	const V3D_f *vs0, *vs1;
	int x_i, y_i, x_l, x_r;
	float tmp;
	int pass;
	int scan_x_u_i, scan_x_v_i;
	int scan_x_step_u_i, scan_x_step_v_i;
	char *pixel;
	const int w_1 = tex->w - 1;
	
	#define SWAP_V(a, b) ({ tmp_v = (a); (a) = (b); (b) = tmp_v; (void)0;})
	#define STEPSIZE(a, b, c) (((b) - (a)) * (c)) 
	
	if(verts[1]->y < verts[0]->y) SWAP_V(verts[1], verts[0]);
	if(verts[2]->y < verts[0]->y) SWAP_V(verts[2], verts[0]);
	if(verts[1]->y > verts[2]->y) SWAP_V(verts[1], verts[2]);
	
	#define FIXCOEFF 256.0f
	#define FIXSHIFT 8
	
	verts[0]->u *= FIXCOEFF;
	verts[1]->u *= FIXCOEFF;
	verts[2]->u *= FIXCOEFF;
	verts[0]->v *= FIXCOEFF;
	verts[1]->v *= FIXCOEFF;
	verts[2]->v *= FIXCOEFF;
	
	vs0 = verts[0];
	vs1 = verts[2];
	
	ve0 = verts[0];
	ve1 = verts[1];
	
	tmp = 1.0f / ((vs1->y) - (vs0->y));
	scan_s_step.u = STEPSIZE(vs0->u, vs1->u, tmp);
	scan_s_step.v = STEPSIZE(vs0->v, vs1->v, tmp);
	scan_s_step.x = STEPSIZE(vs0->x, vs1->x, tmp);
	
	scan_s = *vs0;
	tmp = ceil(ve0->y) - scan_s.y;
	scan_s.u += scan_s_step.u * tmp;
	scan_s.v += scan_s_step.v * tmp;
	scan_s.x += scan_s_step.x * tmp;
	
	for(pass = 0; pass < 2; pass++){
		tmp = 1.0f / ((ve1->y) - (ve0->y));
		scan_e_step.u = STEPSIZE(ve0->u, ve1->u, tmp);
		scan_e_step.v = STEPSIZE(ve0->v, ve1->v, tmp);
		scan_e_step.x = STEPSIZE(ve0->x, ve1->x, tmp);
		
		scan_e = *ve0;
		tmp = ceil(ve0->y) - scan_e.y;
		scan_e.u += scan_e_step.u * tmp;
		scan_e.v += scan_e_step.v * tmp;
		scan_e.x += scan_e_step.x * tmp;
		
		if((scan_e_step.x > scan_s_step.x) ^ (pass)){
			scan_l = &scan_s;
			scan_r = &scan_e;
		}
		else{
			scan_l = &scan_e;
			scan_r = &scan_s;
		}
		
		for(y_i = ceil(ve0->y); y_i < ceil(ve1->y); y_i++){
			tmp = 1.0f / (scan_r->x - scan_l->x);
			scan_x_step.u = STEPSIZE(scan_l->u, scan_r->u, tmp);
			scan_x_step.v = STEPSIZE(scan_l->v, scan_r->v, tmp);
			
			scan_x = *scan_l;
			
			x_l = scan_l->x;
			x_r = scan_r->x;
			
			if(y_i < 0) continue;
			if(y_i >= bmp->h) continue;
			
			if(scan_x.x < 0.0f){
				tmp = -scan_x.x;
				scan_x.u += scan_x_step.u * tmp;
				scan_x.v += scan_x_step.v * tmp;
				scan_x.x = 0.0f;
				x_l = 0;
			}
			if(x_r >= bmp->w) x_r = bmp->w;
			
			scan_x_u_i = scan_x.u;
			scan_x_v_i = scan_x.v;
			scan_x_step_u_i = scan_x_step.u;
			scan_x_step_v_i = scan_x_step.v;
			
			pixel = (char *) bmp->line[y_i];
			pixel += x_l;
			
			for(x_i = x_l; x_i < x_r; x_i++){
				const int c = ((char *) tex->line[(scan_x_v_i >> FIXSHIFT) & w_1])[(scan_x_u_i >> FIXSHIFT) & w_1];
				if(c){
					*pixel = c;
				}
				
				scan_x_u_i += scan_x_step_u_i;
				scan_x_v_i += scan_x_step_v_i;
				pixel++;
			}
			
			scan_s.x += scan_s_step.x;
			scan_s.u += scan_s_step.u;
			scan_s.v += scan_s_step.v;
			
			scan_e.x += scan_e_step.x;
			scan_e.u += scan_e_step.u;
			scan_e.v += scan_e_step.v;
		}
		ve0 = verts[1];
		ve1 = verts[2];
	}
	
	#undef SWAP_V
	#undef STEPSIZE
	#undef FIXCOEFF
	#undef FIXSHIFT
}

static void kdr_triangle3d_f_null(BITMAP *bmp, int s, BITMAP *tex,  V3D_f *_v1, V3D_f *_v2, V3D_f *_v3)
{
}

int kdr_vischeck_hit = 0;

static void kdr_triangle3d_f_vischeck(BITMAP *bmp, int s, BITMAP *tex,  V3D_f *_v1, V3D_f *_v2, V3D_f *_v3)
{
	V3D_f scan_s, scan_e, *scan_l, *scan_r, scan_x, scan_x_step;
	V3D_f scan_s_step, scan_e_step;
	
	V3D_f __verts[3] = {*_v1, *_v2, *_v3};
	V3D_f *verts[] = {&__verts[0], &__verts[1], &__verts[2]};
	V3D_f *tmp_v;
	V3D_f *ve0, *ve1;
	const V3D_f *vs0, *vs1;
	int x_i, y_i, x_l, x_r;
	float tmp;
	int pass;
	int scan_x_z_i;
	int scan_x_step_z_i;
	int *zb_pixel;
	
	#define SWAP_V(a, b) ({ tmp_v = (a); (a) = (b); (b) = tmp_v; (void)0;})
	#define STEPSIZE(a, b, c) (((b) - (a)) * (c)) 
	
	if(verts[1]->y < verts[0]->y) SWAP_V(verts[1], verts[0]);
	if(verts[2]->y < verts[0]->y) SWAP_V(verts[2], verts[0]);
	if(verts[1]->y > verts[2]->y) SWAP_V(verts[1], verts[2]);
	
	verts[0]->z = (((float) INT_MAX) * KDR_NEAR) / verts[0]->z;
	verts[1]->z = (((float) INT_MAX) * KDR_NEAR) / verts[1]->z;
	verts[2]->z = (((float) INT_MAX) * KDR_NEAR) / verts[2]->z;
	
	vs0 = verts[0];
	vs1 = verts[2];
	
	ve0 = verts[0];
	ve1 = verts[1];
	
	tmp = 1.0f / ((vs1->y) - (vs0->y));
	scan_s_step.z = STEPSIZE(vs0->z, vs1->z, tmp);
	scan_s_step.x = STEPSIZE(vs0->x, vs1->x, tmp);
	
	scan_s = *vs0;
	tmp = ceil(ve0->y) - scan_s.y;
	scan_s.z += scan_s_step.z * tmp;
	scan_s.x += scan_s_step.x * tmp;
	
	for(pass = 0; pass < 2; pass++){
		tmp = 1.0f / ((ve1->y) - (ve0->y));
		scan_e_step.z = STEPSIZE(ve0->z, ve1->z, tmp);
		scan_e_step.x = STEPSIZE(ve0->x, ve1->x, tmp);
		
		scan_e = *ve0;
		tmp = ceil(ve0->y) - scan_e.y;
		scan_e.z += scan_e_step.z * tmp;
		scan_e.x += scan_e_step.x * tmp;
		
		if((scan_e_step.x > scan_s_step.x) ^ (pass)){
			scan_l = &scan_s;
			scan_r = &scan_e;
		}
		else{
			scan_l = &scan_e;
			scan_r = &scan_s;
		}
		
		for(y_i = ceil(ve0->y); y_i < ceil(ve1->y); y_i++){
			tmp = 1.0f / (scan_r->x - scan_l->x);
			scan_x_step.z = STEPSIZE(scan_l->z, scan_r->z, tmp);
			
			scan_x = *scan_l;
			
			x_l = scan_l->x;
			x_r = scan_r->x;
			
			if(y_i < 0) continue;
			if(y_i >= bmp->h) continue;
			
			if(scan_x.x < 0.0f){
				tmp = -scan_x.x;
				scan_x.z += scan_x_step.z * tmp;
				scan_x.x = 0.0f;
				x_l = 0;
			}
			if(x_r >= bmp->w) x_r = bmp->w;
			
			scan_x_z_i = scan_x.z;
			scan_x_step_z_i = scan_x_step.z;
			
			zb_pixel = kdr_active_zbuf->line[y_i];
			zb_pixel += x_l;
			
			for(x_i = x_l; x_i < x_r; x_i++){
				if(*zb_pixel < scan_x_z_i){
					kdr_vischeck_hit = 1;
					return;
				}
				scan_x_z_i += scan_x_step_z_i;
				zb_pixel++;
			}
			
			scan_s.x += scan_s_step.x;
			scan_s.z += scan_s_step.z;
			
			scan_e.x += scan_e_step.x;
			scan_e.z += scan_e_step.z;
		}
		ve0 = verts[1];
		ve1 = verts[2];
	}
	
	#undef SWAP_V
	#undef STEPSIZE
}

/*
Perspective-correct perfect rasterizer of UV coordinates
Rasterizes to bmp 32bpp pixels in the format kdr_piuv_offs+u+(v*kdr_piuv_w)
Purpose is radiosity rasterizer
*/
void kdr_triangle3d_f_persp_offsuv(BITMAP *bmp, int kdr_piuv_offs, int kdr_piuv_w, V3D_f *_v1, V3D_f *_v2, V3D_f *_v3)
{
	V3D_f scan_s, scan_e, *scan_l, *scan_r, scan_x;
	V3D_f scan_x_step, scan_s_step, scan_e_step;
	
	V3D_f __verts[3] = {*_v1, *_v2, *_v3};
	V3D_f *verts[] = {&__verts[0], &__verts[1], &__verts[2]};
	V3D_f *tmp_v;
	V3D_f *ve0, *ve1;
	const V3D_f *vs0, *vs1;
	int x_i, y_i, u_i, v_i, x_l, x_r;
	float tmp;
	int pass, c;
	
	#define SWAP_V(a, b) ({ tmp_v = (a); (a) = (b); (b) = tmp_v; (void)0;})
	#define STEPSIZE(a, b, c) (((b) - (a)) * (c)) 
	
	//triangle(bmp, verts[0]->x, verts[0]->y, verts[1]->x, verts[1]->y, verts[2]->x, verts[2]->y, 1 << 29);
	
	if(verts[1]->y < verts[0]->y) SWAP_V(verts[1], verts[0]);
	if(verts[2]->y < verts[0]->y) SWAP_V(verts[2], verts[0]);
	if(verts[1]->y > verts[2]->y) SWAP_V(verts[1], verts[2]);
	
	verts[0]->z = 1.0f / verts[0]->z;
	verts[1]->z = 1.0f / verts[1]->z;
	verts[2]->z = 1.0f / verts[2]->z;
	verts[0]->u *= verts[0]->z;
	verts[1]->u *= verts[1]->z;
	verts[2]->u *= verts[2]->z;
	verts[0]->v *= verts[0]->z;
	verts[1]->v *= verts[1]->z;
	verts[2]->v *= verts[2]->z;
	
	vs0 = verts[0];
	vs1 = verts[2];
	
	ve0 = verts[0];
	ve1 = verts[1];
	
	tmp = 1.0f / ((vs1->y) - (vs0->y));
	scan_s_step.z = STEPSIZE(vs0->z, vs1->z, tmp);
	scan_s_step.u = STEPSIZE(vs0->u, vs1->u, tmp);
	scan_s_step.v = STEPSIZE(vs0->v, vs1->v, tmp);
	scan_s_step.x = STEPSIZE(vs0->x, vs1->x, tmp);
	
	scan_s = *vs0;
	tmp = ceil(ve0->y) - scan_s.y;
	scan_s.z += scan_s_step.z * tmp;
	scan_s.u += scan_s_step.u * tmp;
	scan_s.v += scan_s_step.v * tmp;
	scan_s.x += scan_s_step.x * tmp;
	
	for(pass = 0; pass < 2; pass++){
		tmp = 1.0f / ((ve1->y) - (ve0->y));
		scan_e_step.z = STEPSIZE(ve0->z, ve1->z, tmp);
		scan_e_step.u = STEPSIZE(ve0->u, ve1->u, tmp);
		scan_e_step.v = STEPSIZE(ve0->v, ve1->v, tmp);
		scan_e_step.x = STEPSIZE(ve0->x, ve1->x, tmp);
		
		scan_e = *ve0;
		tmp = ceil(ve0->y) - scan_e.y;
		scan_e.z += scan_e_step.z * tmp;
		scan_e.u += scan_e_step.u * tmp;
		scan_e.v += scan_e_step.v * tmp;
		scan_e.x += scan_e_step.x * tmp;
		
		if((scan_e_step.x > scan_s_step.x) ^ (pass)){
			scan_l = &scan_s;
			scan_r = &scan_e;
		}
		else{
			scan_l = &scan_e;
			scan_r = &scan_s;
		}
		
		for(y_i = ceil(ve0->y); y_i < ceil(ve1->y); y_i++){
			tmp = 1.0f / (ceil(scan_r->x) - ceil(scan_l->x));
			scan_x_step.z = STEPSIZE(scan_l->z, scan_r->z, tmp);
			scan_x_step.u = STEPSIZE(scan_l->u, scan_r->u, tmp);
			scan_x_step.v = STEPSIZE(scan_l->v, scan_r->v, tmp);
			
			scan_x = *scan_l;
			
			x_l = ceil(scan_l->x);
			x_r = ceil(scan_r->x);
			
			if(y_i < 0) continue;
			if(y_i >= bmp->h) continue;
			if(scan_x.x < 0.0f){
				tmp = -scan_x.x;
				scan_x.z += scan_x_step.z * tmp;
				scan_x.u += scan_x_step.u * tmp;
				scan_x.v += scan_x_step.v * tmp;
				scan_x.x = 0.0f;
				x_l = 0;
			}
			if(x_r >= bmp->w) x_r = bmp->w;
			
			for(x_i = x_l; x_i < x_r; x_i++){
				tmp =  1.0f / scan_x.z;
				u_i = floor(scan_x.u * tmp);
				v_i = floor(scan_x.v * tmp);
				
				_putpixel32(bmp, x_i, y_i, kdr_piuv_offs + u_i + (v_i * kdr_piuv_w));
				
				scan_x.z += scan_x_step.z;
				scan_x.u += scan_x_step.u;
				scan_x.v += scan_x_step.v;
			}
			
			scan_s.x += scan_s_step.x;
			scan_s.z += scan_s_step.z;
			scan_s.u += scan_s_step.u;
			scan_s.v += scan_s_step.v;
			
			scan_e.x += scan_e_step.x;
			scan_e.z += scan_e_step.z;
			scan_e.u += scan_e_step.u;
			scan_e.v += scan_e_step.v;
		}
		ve0 = verts[1];
		ve1 = verts[2];
	}
	
	#undef SWAP_V
	#undef STEPSIZE
}

void kdr_drawzb(BITMAP *to)
{
	int x, y, i;
	float f;
	for(y = 0; y < kdr_active_zbuf->h && y < to->h; y++){
		for(x = 0; x < kdr_active_zbuf->w && x < to->w; x++){
			f = 1.0 / ((float) kdr_active_zbuf->line[y][x]);
			f *= (float) INT_MAX;
			f *= KDR_NEAR;
			f = 255.0f / (f + 1.0 - KDR_NEAR);
			i = (int) f;
			putpixel(to, x, y, makecol(i, i, i));
		}
	}
}

int kdr_bf_cull = 1;
int raster_triangle_func = 0;
int raster_mipmap_levels = 0;
typedef void (*t3d_f_func_ptr)(BITMAP *, int, BITMAP *, V3D_f *, V3D_f *, V3D_f *);

static inline void kdr_polygon3d_f_(BITMAP *bmp, int shader, BITMAP **tex, V3D_f *verts0, V3D_f *verts1, V3D_f *verts2)
{
	const t3d_f_func_ptr funcs[] = {
		triangle3d_f,
		kdr_triangle3d_f_null,
		kdr_triangle3d_f_generic,
		kdr_triangle3d_f_persp, 
		kdr_triangle3d_f_persp32bpp,
		kdr_triangle3d_f_persp8bpp,
		kdr_triangle3d_f_affinezb32bpp,
		kdr_triangle3d_f_affinezb8bpp,
		kdr_triangle3d_f_persp_bilinear,
		kdr_triangle3d_f_affinetransp32bpp,
		kdr_triangle3d_f_affinetransp8bpp,
		kdr_triangle3d_f_vischeck,
	};
	
	if(raster_triangle_func >= sizeof(funcs) / sizeof(funcs[0])) return;
	
	if(!tex) goto aftermips;
	/*
	TODO: into fixed point,
		filog2(i)				->	31u-__builtin_clz((int)i)
		floor(log2((int)i))		==	filog2(i)
		-floor(0.5 * log2(ds))	==	(log2(ws + hs) - log2(dx * dx + dy * dy)) * 0.5
	*/
	const double
		ws = (*tex)->w * (*tex)->w,
		hs = (*tex)->h * (*tex)->h,
		du1 = verts0->u - verts1->u,
		du2 = verts0->u - verts2->u,
		du3 = verts1->u - verts2->u,
		dv1 = verts0->v - verts1->v,
		dv2 = verts0->v - verts2->v,
		dv3 = verts1->v - verts2->v,
		dx1 = verts0->x - verts1->x,
		dx2 = verts0->x - verts2->x,
		dx3 = verts1->x - verts2->x,
		dy1 = verts0->y - verts1->y,
		dy2 = verts0->y - verts2->y,
		dy3 = verts1->y - verts2->y,
	/*
		ds1 = sqrt(dx1 * dx1 + dy1 * dy1) / sqrt(du1 * du1 * ws + dv1 * dv1 * hs),
		ds2 = sqrt(dx2 * dx2 + dy2 * dy2) / sqrt(du2 * du2 * ws + dv2 * dv2 * hs),
		ds3 = sqrt(dx3 * dx3 + dy3 * dy3) / sqrt(du3 * du3 * ws + dv3 * dv3 * hs),
		l1 = -floor(log2(ds1)),
		l2 = -floor(log2(ds2)),
		l3 = -floor(log2(ds3));
	*/
	// log2 sqrt x = log2 pow x 0.5 = 0.5 log2 x
		ds1 = (dx1 * dx1 + dy1 * dy1) / (du1 * du1 * ws + dv1 * dv1 * hs),
		ds2 = (dx2 * dx2 + dy2 * dy2) / (du2 * du2 * ws + dv2 * dv2 * hs),
		ds3 = (dx3 * dx3 + dy3 * dy3) / (du3 * du3 * ws + dv3 * dv3 * hs),
		l1 = -floor(0.5 * log2(ds1)),
		l2 = -floor(0.5 * log2(ds2)),
		l3 = -floor(0.5 * log2(ds3));
	const int l = MAX(MAX(l1, l2), l3);
	const int dtex = MIN(MAX(l, 0), MAX(raster_mipmap_levels - 1, 0));
	tex += dtex;
	
	verts0->u *= (*tex)->w;
	verts1->u *= (*tex)->w;
	verts2->u *= (*tex)->w;
	verts0->v *= (*tex)->h;
	verts1->v *= (*tex)->h;
	verts2->v *= (*tex)->h;
	
	aftermips:
	
	if(raster_triangle_func < rf_kdr_triangle3d_f_affinezb32bpp && kdr_active_zbuf) kdr_triangle3d_f_perspzb(bmp, shader, 0, verts0, verts1, verts2);
	funcs[raster_triangle_func](bmp, shader, tex ? *tex : 0, verts0, verts1, verts2);
	
	if(!tex) return;
	
	verts0->u /= (*tex)->w;
	verts1->u /= (*tex)->w;
	verts2->u /= (*tex)->w;
	verts0->v /= (*tex)->h;
	verts1->v /= (*tex)->h;
	verts2->v /= (*tex)->h;
}

void kdr_polygon3d_f(BITMAP *bmp, int shader, BITMAP **tex, int num_verts, V3D_f **verts)
{
	if(num_verts <= 2) return;
	
	#define ax ((verts[0])->x - (verts[1])->x)
	#define bx ((verts[0])->x - (verts[2])->x)
	#define ay ((verts[0])->y - (verts[1])->y)
	#define by ((verts[0])->y - (verts[2])->y)
	if(kdr_bf_cull > 0 && ax * by - ay * bx > 0) return;
	if(kdr_bf_cull < 0 && ax * by - ay * bx < 0) return;
	
	if(raster_triangle_func == rf_kdr_triangle3d_f_affinezb32bpp || raster_triangle_func == rf_kdr_triangle3d_f_affinezb8bpp){
		KTAZ_RESET
	}
	
	if(num_verts > 3){
		int i;
		for(i = 1; i < num_verts - 1; i++){
			kdr_polygon3d_f_(bmp, shader, tex, verts[0], verts[i], verts[i + 1]);
		}
		return;
	}
	kdr_polygon3d_f_(bmp, shader, tex, verts[0], verts[1], verts[2]);
}

static int raster_stack[3 * 16];
static int *raster_stack_ptr = raster_stack;
static kdr_zbuf *raster_stack_zb[3 * 16];
static kdr_zbuf **raster_stack_zb_ptr = raster_stack_zb;

void raster_push()
{
	*raster_stack_ptr++ = raster_triangle_func;
	*raster_stack_ptr++ = raster_mipmap_levels;
	*raster_stack_ptr++ = kdr_bf_cull;
	
	*raster_stack_zb_ptr++ = kdr_active_zbuf;
}

void raster_pop()
{
	kdr_bf_cull = *--raster_stack_ptr;
	raster_mipmap_levels = *--raster_stack_ptr;
	raster_triangle_func = *--raster_stack_ptr;
	
	kdr_active_zbuf = *--raster_stack_zb_ptr;
}
