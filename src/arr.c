#include <stdlib.h>
#include <assert.h>

#include "arr.h"

void arr_cleanup_func(void *v)
{
	arr_void *v_ = v;
	arr_free(v_);
}

const int arr_grow_factor = 3;
