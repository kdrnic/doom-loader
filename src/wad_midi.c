#include <string.h>
#include <stdint.h>
#include <stdio.h>

#include <allegro.h>

#include "wad.h"
#include "arr.h"

enum{
	MIDI_SYSEX		= 0xF0,		 // SysEx begin
	MIDI_SYSEXEND	= 0xF7,		 // SysEx end
	MIDI_META		= 0xFF,		 // Meta event begin
	MIDI_META_TEMPO = 0x51,
	MIDI_META_EOT	= 0x2F,		 // End-of-track
	MIDI_META_SSPEC	= 0x7F,		 // System-specific event
	MIDI_NOTEOFF	= 0x80,		 // + note + velocity
	MIDI_NOTEON 	= 0x90,		 // + note + velocity
	MIDI_POLYPRESS	= 0xA0,		 // + pressure (2 bytes)
	MIDI_CTRLCHANGE = 0xB0,		 // + ctrlr + value
	MIDI_PRGMCHANGE = 0xC0,		 // + new patch
	MIDI_CHANPRESS	= 0xD0,		 // + pressure (1 byte)
	MIDI_PITCHBEND	= 0xE0,		 // + pitch bend (2 bytes)
	MUS_NOTEOFF 	= 0x00,
	MUS_NOTEON		= 0x10,
	MUS_PITCHBEND	= 0x20,
	MUS_SYSEVENT	= 0x30,
	MUS_CTRLCHANGE	= 0x40,
	MUS_SCOREEND	= 0x60,
};

typedef struct
{
	union{
		uint32_t dword;
		char str[4];
	} magic;
	uint16_t song_len;
	uint16_t song_start;
	uint16_t num_chans;
	uint16_t num_secondarychans;
	uint16_t num_instruments;
	uint16_t pad;
} mus_header;

/*
struct headerStruct {
	//0
    char chunktype[4]; //MThd
	//4
    long length; //32bit length
	//8
    int format;//16bit format
	//10
    int ntraks;//16bit number of tracks
	//12
    int division;//16bit division
};
*/

static const uint8_t midi_head[] =
{
//'M','T','h','d',
// 0, 0, 0, 6, //32bit length
//0, 0, // format 0: only one track
//0, 1, // yes, there is really only one track
//0, 70, // 70 divisions
//
//14
//'M','T','r','k',//track header
//18
 //0, 0, 0, 0,//track length
 //22
// The first event sets the tempo to 500,000 microsec/quarter note
0,//delta time 0
255,//metaevent
 81,//tempo change
 3, 
 0x07,//the 500000
 0xa1,
 0x20
};

static const uint8_t ctrl_tbl[15] =
{
	0,	// program change
	0,	// bank select
	1,	// modulation pot
	7,	// volume
	10, // pan pot
	11, // expression pot
	91, // reverb depth
	93, // chorus depth
	64, // sustain pedal
	67, // soft pedal
	120, // all sounds off
	123, // all notes off
	126, // mono
	127, // poly
	121, // reset all controllers
};

typedef arr_t(uint8_t) arr_uint8;

enum {
	PROMID_MAGIC = 1,
	PROMID_ISMIDI,
	PROMID_NCHANS,
	PROMID_OVERRUN,
	PROMID_UNKNOWN
};

static const char *PROMID_str[] = {
	"PROMID_MAGIC",
	"PROMID_ISMIDI",
	"PROMID_NCHANS",
	"PROMID_OVERRUN",
	"PROMID_UNKNOWN"
};

static int ProduceMIDI(const uint8_t *musBuf, arr_uint8 *outFile, int mus_buf_len)
{
	uint8_t midStatus, midArgs, mid1, mid2;
	size_t mus_p, maxmus_p;
	uint8_t event;
	int deltaTime;
	union {
		uint8_t musHeadBytes[sizeof(mus_header)];
		const mus_header musHead;
	} musHeadUnion;
	const mus_header *musHead = &musHeadUnion.musHead;
	uint8_t status;
	uint8_t chanUsed[16];
	uint8_t lastVel[16];
	long trackLen;
	int no_op;
	
	memcpy(musHeadUnion.musHeadBytes, musBuf, sizeof(mus_header));

	// Do some validation of the MUS file
	if(memcmp("MUS\x1a", musHead->magic.str, 4)){
		if(!memcmp("MThd", musHead->magic.str, 4)){
			return PROMID_ISMIDI;
		}
		return PROMID_MAGIC;
	}
	
	if(musHead->num_chans > 15)
		return PROMID_NCHANS;
	
	// Prep for conversion
	arr_cat_s(outFile, midi_head);

	musBuf += musHead->song_start;
	maxmus_p = musHead->song_len;
	mus_p = 0;
	mus_buf_len -= musHead->song_start;
	
	memset(lastVel, 100, 16);
	memset(chanUsed, 0, 16);
	event = 0;
	deltaTime = 0;
	status = 0;
	
	while(mus_p < maxmus_p && (event & 0x70) != MUS_SCOREEND){
		int channel;
		uint8_t t = 0;
		
		#define mus_buf_getc() ({\
			uint8_t c;\
			if(mus_buf_len){\
				c = musBuf[mus_p++];\
				mus_buf_len--;\
			}\
			else{\
				goto overrun;\
			}\
			c;\
		})
		
		event = mus_buf_getc();
		if((event & 0x70) != MUS_SCOREEND){
			t = mus_buf_getc();
		}
		channel = event & 15;
		if(channel == 15){
			channel = 9;
		}
		else if(channel >= 9){
			channel++;
		}
		
		if(!chanUsed[channel]){
			// This is the first time this channel has been used,
			// so sets its volume to 127.
			chanUsed[channel] = 1;
			arr_push(outFile, (uint8_t)(0));//deltat = 0
			arr_push(outFile, (uint8_t)(0xB0 | channel)); //control change
			arr_push(outFile, (uint8_t)(7));//controller = volume (coarse)
			arr_push(outFile, (uint8_t)(127));//127
		}
		
		midStatus = channel;
		midArgs = 0;		// Most events have two args (0 means 2, 1 means 1)
		no_op = 0;

		switch(event & 0x70){
		case MUS_NOTEOFF:
			midStatus |= MIDI_NOTEOFF;
			mid1 = t & 127; //pitch
			mid2 = 64; //velocity
			break;
			
		case MUS_NOTEON:
			midStatus |= MIDI_NOTEON;
			mid1 = t & 127; //pitch
			if(t & 128){
				lastVel[channel] = mus_buf_getc() & 127;
			}
			mid2 = lastVel[channel]; //velocity
			break;
			
		case MUS_PITCHBEND:
			//MUS:
			//Bend all notes on the channel by the given amount. 0 is one tone down, 255 is one tone up.
			//64 is a half-tone down, 192 is a half-tone up, and 128 is normal (no bend).
			//MIDI:
			//The two bytes of the pitch bend message form a 14 bit number, 0 to 16383.
			//The value 8192 (sent, LSB first, as 0x00 0x40), is centered, or "no pitch bend."
			//The value 0 (0x00 0x00) means, "bend as low as possible," and, similarly, 16383 (0x7F 0x7F) is to
			//"bend as high as possible." The exact range of the pitch bend is specific to the synthesizer. 
			midStatus |= MIDI_PITCHBEND;
			mid1 = (t & 1) << 6;
			mid2 = (t >> 1) & 127;
			break;
			
		case MUS_SYSEVENT:
			if(t < 10 || t > 14){
				no_op = 1;
			}
			else{
				midStatus |= MIDI_CTRLCHANGE;
				mid1 = ctrl_tbl[t];
				mid2 = t == 12 /* Mono */ ? (musHead->num_chans) : 0;
			}
			break;
			
		case MUS_CTRLCHANGE:
			if(t == 0){
				// program change
				midArgs = 1;
				midStatus |= MIDI_PRGMCHANGE;
				mid1 = mus_buf_getc() & 127;
				mid2 = 0;	// Assign mid2 just to make GCC happy
			}
			else if(t > 0 && t < 10){
				midStatus |= MIDI_CTRLCHANGE;
				mid1 = ctrl_tbl[t];
				mid2 = mus_buf_getc();
			}
			else{
				no_op = 1;
			}
			break;
			
		case MUS_SCOREEND:
			midStatus = MIDI_META;
			mid1 = MIDI_META_EOT;
			mid2 = 0;
			break;
			
		default:
			return PROMID_UNKNOWN;
		}

		if(no_op){
			// A system-specific event with no data is a no-op.
			midStatus = MIDI_META;
			mid1 = MIDI_META_SSPEC;
			mid2 = 0;
		}

		({
			unsigned long time = deltaTime;
			unsigned long buffer;
			
			buffer = time & 0x7f;
			while(time >>= 7){
				buffer = (buffer << 8) | 0x80 | (time & 0x7f);
			}
			while(1){
				arr_push(outFile, (uint8_t) (buffer & 0xff));
				if(buffer & 0x80){
					buffer >>= 8;
				}
				else{
					break;
				}
			}
			(void) 0;
		});
		
		if(midStatus != status){
			status = midStatus;
			arr_push(outFile, (uint8_t) status);
		}
		arr_push(outFile, mid1);
		if(midArgs == 0){
			arr_push(outFile, mid2);
		}
		if(event & 128){
			unsigned long time = 0;
			uint8_t t;
			
			if(((time = mus_buf_getc()) & 0x80)){
				time &= 0x7F;
				do{
					time = (time << 7) + ((t = mus_buf_getc()) & 0x7F);
				}while(t & 0x80);
			}
			deltaTime = time;
		}
		else{
			deltaTime = 0;
		}
	}
	
	return 0;
	
	overrun:
	return PROMID_OVERRUN;
}

struct musbuf_pf
{
	uint8_t *ptr, *ptr_max;
};

static int	pf_fclose(void *userdata)
{
	struct musbuf_pf *pf = userdata;
	pf->ptr = pf->ptr_max;
	return 0;
}

static int	pf_getc(void *userdata)
{
	struct musbuf_pf *pf = userdata;
	if(pf->ptr == pf->ptr_max){
		return EOF;
	}
	return *(pf->ptr++);
}

static int	pf_ungetc(int c, void *userdata)
{
	abort();
	return 0;
}

static long	pf_fread(void *p, long n, void *userdata)
{
	struct musbuf_pf *pf = userdata;
	long num_read = MIN(n, pf->ptr_max - pf->ptr);
	memcpy(p, pf->ptr, num_read);
	pf->ptr += num_read;
	return num_read;
}

static int	pf_putc(int c, void *userdata)
{
	abort();
	return 0;
}

static long	pf_fwrite(AL_CONST void *p, long n, void *userdata)
{
	abort();
	return 0;
}

static int	pf_fseek(void *userdata, int offset)
{
	struct musbuf_pf *pf = userdata;
	if(offset <= pf->ptr_max - pf->ptr){
		pf->ptr += offset;
		return 0;
	}
	return 1;
}

static int	pf_feof(void *userdata)
{
	struct musbuf_pf *pf = userdata;
	return (pf->ptr == pf->ptr_max);
}

static int	pf_ferror(void *userdata)
{
	return 0;
}

static MIDI *load_midi2(PACKFILE *fp)
{
   int c;
   char buf[4];
   long data;
   MIDI *midi;
   int num_tracks;

   midi = malloc(sizeof(MIDI));              /* get some memory */
   if (!midi) {
      pack_fclose(fp);
      return NULL;
   }

   for (c=0; c<MIDI_TRACKS; c++) {
      midi->track[c].data = NULL;
      midi->track[c].len = 0;
   }

   pack_fread(buf, 4, fp); /* read midi header */

   /* Is the midi inside a .rmi file? */
   if (memcmp(buf, "RIFF", 4) == 0) { /* check for RIFF header */
      pack_mgetl(fp);

      while (!pack_feof(fp)) {
         pack_fread(buf, 4, fp); /* RMID chunk? */
         if (memcmp(buf, "RMID", 4) == 0) break;

         pack_fseek(fp, pack_igetl(fp)); /* skip to next chunk */
      }

      if (pack_feof(fp)) goto err;

      pack_mgetl(fp);
      pack_mgetl(fp);
      pack_fread(buf, 4, fp); /* read midi header */
   }

   if (memcmp(buf, "MThd", 4))
      goto err;

   pack_mgetl(fp);                           /* skip header chunk length */

   data = pack_mgetw(fp);                    /* MIDI file type */
   if ((data != 0) && (data != 1))
      goto err;

   num_tracks = pack_mgetw(fp);              /* number of tracks */
   if ((num_tracks < 1) || (num_tracks > MIDI_TRACKS))
      goto err;

   data = pack_mgetw(fp);                    /* beat divisions */
   midi->divisions = ABS(data);

   for (c=0; c<num_tracks; c++) {            /* read each track */
      pack_fread(buf, 4, fp);                /* read track header */
      if (memcmp(buf, "MTrk", 4))
	 goto err;

      data = pack_mgetl(fp);                 /* length of track chunk */
      midi->track[c].len = data;

      midi->track[c].data = malloc(data); /* allocate memory */
      if (!midi->track[c].data)
	 goto err;
					     /* finally, read track data */
      if (pack_fread(midi->track[c].data, data, fp) != data)
	 goto err;
   }

   pack_fclose(fp);
   lock_midi(midi);
   return midi;

   /* oh dear... */
   err:
   pack_fclose(fp);
   destroy_midi(midi);
   return NULL;
}

MIDI *wad_read_midi(FILE *file_wad, struct wad_lump *lump)
{
	int i, j;
	MIDI *midi = 0;
	uint8_t *mus_buf = malloc(lump->size);
	arr_uint8 track0 = arr_empty;
	int error;
	
	fseek(file_wad, lump->offs, SEEK_SET);
	fread(mus_buf, 1, lump->size, file_wad);
	
	if(!(error = ProduceMIDI(mus_buf, &track0, lump->size))){
		midi = malloc(sizeof(*midi));
		for(i = 0; i < MIDI_TRACKS; i++){
			midi->track[i].data = 0;
			midi->track[i].len = 0;
		}
		midi->divisions = 70;
		midi->track[0].len = track0.len;
		midi->track[0].data = track0.data;
	}
	else{
		if(error == PROMID_ISMIDI){
			const PACKFILE_VTABLE vt = {
				pf_fclose,
				pf_getc,
				pf_ungetc,
				pf_fread,
				pf_putc,
				pf_fwrite,
				pf_fseek,
				pf_feof,
				pf_ferror,
			};
			struct musbuf_pf userdata = {
				.ptr = mus_buf,
				.ptr_max = mus_buf + lump->size,
			};
			PACKFILE *pf = pack_fopen_vtable(&vt, &userdata);
			midi = load_midi2(pf);
		}
		else{
			fprintf(stderr, "ProduceMIDI error %s\n", PROMID_str[error]);
		}
	}
	
	free(mus_buf);
	
	return midi;
}
