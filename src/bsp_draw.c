//BSP drawing functions
#include <allegro.h>

#include <stdio.h>
#include <math.h>

#include "bsp.h"
#include "mesh.h"
#include "vector.h"
#include "compat.h"

#include "raster.h"

int draw_bsp_leafs = 0;
int draw_bsp_current_leaf = 0;

static V3D_f dbspf_tmp[MESH_MAX_FACE_VERTS], dbspf_vout[MESH_MAX_FACE_VERTS * 12], dbspf_tmp2[MESH_MAX_FACE_VERTS * 12], dbspf_tmp3[MESH_MAX_FACE_VERTS * 12];
static int dbspf_bout[MESH_MAX_FACE_VERTS * 12];
static V3D_f *dbspf_ts[MESH_MAX_FACE_VERTS];
static V3D_f *dbspf_vs[MESH_MAX_FACE_VERTS * 12];
static V3D_f *dbspf_t2s[MESH_MAX_FACE_VERTS * 12];

void draw_bsp_init()
{
	int i;
	
	for(i = 0; i < MESH_MAX_FACE_VERTS; i++){
		dbspf_ts[i] = &dbspf_tmp[i];
	}
	
	for(i = 0; i < MESH_MAX_FACE_VERTS * 12; i++){
		dbspf_vs[i] = &dbspf_vout[i];
		dbspf_t2s[i] = &dbspf_tmp2[i];
	}
}

static const MATRIX_f *cm;
static BITMAP *bmp;
static BITMAP ***textures;
static int shader;
static vec3 cpos_;
static const vec3 *cpos = &cpos_;
static const bspdata *b;
static BITMAP *litbmp = 0;
static BITMAP *lightmap = 0;

static int draw_bsp_node_hack = 0;

static int col_counter = 0;

static inline void draw_bspface(int face_i)
{
	int i, k, vc;
	unsigned int leaf_color = 0u;
	int shader_ = shader;
	const bspface *face = b->faces + face_i;
	
	if(draw_bsp_leafs || draw_bsp_node_hack){
		shader_ = POLYTYPE_FLAT;
	}
	
	BSPNODE_CALC_LEAF_COLOUR(i, draw_bsp_current_leaf, leaf_color)
	
	leaf_color = (draw_bsp_current_leaf >> 4) | ((draw_bsp_current_leaf << 4) & 0xFF);
	
	for(k = 0; k < face->num_verts; k++){
		V3D_f tmp_ = RVERT_TO_V3DF(face->verts[k]);
		
		if(draw_bsp_leafs){
			tmp_.c = (leaf_color & 0xFFFFFFu);
		}
		if(draw_bsp_node_hack){
			tmp_.c = leaf_color & 0xFF;
		}
		#if 0
		if(face->material == b->sky_material){
			tmp_.u = (fixtof(fixmul(fixatan2(ftofix(cpos_.x - tmp_.x), ftofix(cpos_.y - tmp_.y)), itofix(4))) + 512.0f) / 256.0f;
		}
		#endif
		
		dbspf_tmp[k] = tmp_;
		apply_matrix_f(cm, dbspf_tmp[k].x, dbspf_tmp[k].y, dbspf_tmp[k].z, &dbspf_tmp[k].x, &dbspf_tmp[k].y, &dbspf_tmp[k].z);
		
		#if 0
		if(face->material == b->sky_material){
			 dbspf_tmp[k].v = (fixtof(fixatan2(ftofix(dbspf_tmp[k].y), ftofix(dbspf_tmp[k].z))) + 32) / 128.0f;
		}
		#endif
	}
	
	vc = clip3d_f(
		shader_,
		1.0,
		1.0,
		face->num_verts,
		(AL_CLIP3D_CONST V3D_f **) dbspf_ts,
		dbspf_vs,
		dbspf_t2s,
		dbspf_bout
	);
	
	float xadd = 0;
	if(face->material == b->sky_material){
		MATRIX_f mat = *cm;
		matrix_f_invert(&mat);
		
		float x, y, z;
		apply_matrix_f(&mat, 1.0, 1.0, 1.0, &x, &y, &z);
		xadd = (fixtof(fixmul(fixatan2(ftofix(cpos_.x - x), ftofix(cpos_.y - y)), itofix(4))) + 512.0f);
		//xadd = atan2((cpos_.x - x), (cpos_.y - y)) * (4.0f * 128.0f / M_PI) + 512.0f;
		
		#if 0
		for(k = 0; k < vc; k++){
			apply_matrix_f(&mat, dbspf_vout[k].x, dbspf_vout[k].y, dbspf_vout[k].z, &x, &y, &z);
			dbspf_vout[k].u = (fixtof(fixmul(fixatan2(ftofix(cpos_.x - x), ftofix(cpos_.y - y)), itofix(4))) + 512.0f) / 256.0f;
		}
		#endif
	}
	
	for(k = 0; k < vc; k++){
		persp_project_f(dbspf_vout[k].x, dbspf_vout[k].y, dbspf_vout[k].z, &dbspf_vout[k].x, &dbspf_vout[k].y);
	}
	if(face->material == b->sky_material){
		float z0 = dbspf_vout[0].z;
		for(k = 0; k < vc; k++){
			dbspf_vout[k].u = (dbspf_vout[k].x + xadd * 2.0) / (128.0 * (bmp->h / 400.0) * 4.0);
			dbspf_vout[k].v = dbspf_vout[k].y / (128.0 * (bmp->h / 400.0) * 2.0);
			dbspf_vout[k].z = z0;
		}
	}
	kdr_polygon3d_f(bmp, shader_, textures ? textures[face->material] : 0, vc, dbspf_vs);
	
	if(litbmp){
		BITMAP *lightmap0[1] = {lightmap};
		
		//Hack to keep lightmap UVs in place
		//Lightmaps map each triangle individually
		
		memcpy(dbspf_tmp3, dbspf_tmp, face->num_verts * sizeof(V3D_f));
		for(k = 0; k < face->num_verts; k++){
			dbspf_tmp3[k].u = b->lm_uvs[b->lm_faces[face_i] + k][0];
			dbspf_tmp3[k].v = b->lm_uvs[b->lm_faces[face_i] + k][1];
		}
		
		for(i = 1; i < face->num_verts - 1; i++){
			dbspf_tmp[0] = dbspf_tmp3[0];
			dbspf_tmp[1] = dbspf_tmp3[i];
			dbspf_tmp[2] = dbspf_tmp3[i + 1];
			
			vc = clip3d_f(
				shader_,
				KDR_NEAR,
				KDR_NEAR,
				3,
				(AL_CLIP3D_CONST V3D_f **) dbspf_ts,
				dbspf_vs,
				dbspf_t2s,
				dbspf_bout
			);
			for(k = 0; k < vc; k++){
				persp_project_f(dbspf_vout[k].x, dbspf_vout[k].y, dbspf_vout[k].z, &dbspf_vout[k].x, &dbspf_vout[k].y);
			}
			raster_push();
			raster_mipmap_levels = 0;
			raster_triangle_func = rf_kdr_triangle3d_f_persp_bilinear;
			kdr_polygon3d_f(litbmp, shader_, lightmap0, vc, dbspf_vs);
			raster_pop();
		}
	}
}

static int old_camera_leaf = 0;
static unsigned int *pvs = 0;
static int pvs_len = 0;
//TODO: rename
unsigned int pvs_counter = 0;

unsigned int *bsp_pointpvs(const bspdata *b, const vec3 *cpos)
{
	int camera_node = 0, camera_leaf;
	int curr = 0;
	int j;
	
	//Allocate PVS
	if(!pvs || pvs_len < b->num_leafs + b->num_mdls){
		pvs_len = b->num_leafs + b->num_mdls;
		pvs = realloc(pvs, sizeof(*pvs) * pvs_len);
		memset(pvs, 0, sizeof(*pvs) * pvs_len);
	}
	
	//Find camera leaf
	while(!BSPNODE_ISLEAF(camera_node)){
		bspnode *node = &b->nodes[camera_node];
		camera_node = node->children[!vec3_planecmp(&b->planes[node->plane], cpos)];
	}
	if(camera_node == BSPLEAF_SOLID || key[KEY_C]) camera_leaf = old_camera_leaf;
	else camera_leaf = BSPNODE_TO_LEAF(camera_node);
	
	old_camera_leaf = camera_leaf;
	
	//Comparison with counter avoids need to always memset
	pvs_counter++;
	//Take care of counter overflow
	if(pvs_counter == 0){
		memset(pvs, 0, sizeof(*pvs) * pvs_len);
		pvs_counter = 1;
	}
	
	//Find PVS
	pvs[camera_leaf] = pvs_counter;
	for(j = b->leafs[camera_leaf].pvs; b->pvs[j] >= 0; j++){
		curr += b->pvs[j];
		pvs[curr] = pvs_counter;
	}
	
	return pvs;
}

void (*draw_bsp_onleaf)(
	const bspdata *bsp,
	const struct MATRIX_f *cm,
	struct BITMAP *bmp,
	struct BITMAP *litbmp,
	int leaf_i,
	void *data
) = 0;
void *draw_bsp_onleaf_data = 0;

static void draw_vis_bsp_node(const int node_i, int foo)
{
	int j;
	
	if(node_i == BSPLEAF_SOLID) return;
	
	if(node_i == draw_bsp_node_hl){
		foo = 1;
	}
	
	if(!BSPNODE_ISLEAF(node_i)){
		const bspnode *node = &b->nodes[node_i];
		const int pc = vec3_planecmp(&b->planes[node->plane], cpos);
		
		draw_vis_bsp_node(node->children[pc], foo);
		draw_vis_bsp_node(node->children[!pc], foo);
	}
	else{
		const int leaf_i = BSPNODE_TO_LEAF(node_i);
		const bspleaf *leaf = &b->leafs[leaf_i];
		
		draw_bsp_node_hack = foo;
		
		if(leaf_i < pvs_len && pvs[leaf_i] != pvs_counter) return;
		
		draw_bsp_current_leaf = leaf_i;
		for(j = leaf->first_face; j <= leaf->last_face; j++){
			draw_bspface(j);
		}
		
		if(draw_bsp_onleaf){
			draw_bsp_onleaf(b, cm, bmp, litbmp, leaf_i, draw_bsp_onleaf_data);
		}
	}
}

int draw_bsp_node_hl = 0;

static void draw_bsp_node(const int node_i, int foo)
{
	if(node_i == BSPLEAF_SOLID) return;
	
	if(node_i == draw_bsp_node_hl){
		foo = 1;
	}
	
	if(!BSPNODE_ISLEAF(node_i)){
		const bspnode *node = &b->nodes[node_i];
		const int pc = vec3_planecmp(&b->planes[node->plane], cpos);
		
		draw_bsp_node(node->children[pc], foo);
		draw_bsp_node(node->children[!pc], foo);
	}
	else{
		const int leaf_i = BSPNODE_TO_LEAF(node_i);
		const bspleaf *leaf = &b->leafs[leaf_i];
		int i;
		
		draw_bsp_node_hack = foo;
		
		draw_bsp_current_leaf = leaf_i;
		for(i = leaf->first_face; i <= leaf->last_face; i++){
			draw_bspface(i);
		}
		
		if(draw_bsp_onleaf){
			draw_bsp_onleaf(b, cm, bmp, litbmp, leaf_i, draw_bsp_onleaf_data);
		}
	}
}

unsigned int *draw_bsp(
	const bspdata *b_,
	const struct MATRIX_f *cm_,
	struct BITMAP *bmp_,
	struct BITMAP ***textures_,
	const int shader_,
	BITMAP *litbmp_,
	BITMAP *lightmap_
)
{
	//Fine programming
	b = b_;
	
	matrix_f_cpos(cm_, &cpos_);
	
	cm = cm_;
	bmp = bmp_;
	textures = textures_;
	shader = shader_;
	if(litbmp_ && b->lm_uvs && lightmap_){
		litbmp = litbmp_;
		lightmap = lightmap_;
	}
	else litbmp = 0;
	
	col_counter++;
	
	if(b->pvs){
		bsp_pointpvs(b, cpos);
		draw_vis_bsp_node(0, 0);
		return pvs;
	}
	else draw_bsp_node(0, 0);
	return 0;
}
