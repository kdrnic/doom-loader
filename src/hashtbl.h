#ifndef HASHTBL_H
#define HASHTBL_H

#include <stdint.h>

//Values in percent
#define HASHTABLE_LOAD_HI 70
#define HASHTABLE_LOAD_LO 10

typedef struct hashtable
{
	struct bucket
	{
		//Storing the hash allows for fast reinsertion
		uint32_t hash;
		//Currently stored as pointers to individual strings
		//If pointing to the very table buffer, means a previously deleted bucket
		char *key;
		void *val;
	} *table;
	//Hash func ptr
	uint32_t (*hash)(const char *);
	//Num of used buckets, total capacity
	int len, siz;
} hashtable;

#define ht_cleanup __attribute__ ((__cleanup__(ht_destroy)))

#define ht_foreach(ht, keyname, valname, func)\
({\
	__typeof__(ht) _ht = (ht);\
	int _i;\
	for(_i = 0; _i < _ht->siz; _i++){\
		if(!_ht->table[_i].key) continue;\
		if(_ht->table[_i].key == (char *) _ht->table) continue;\
		({\
			char *keyname = _ht->table[_i].key;\
			void *valname = _ht->table[_i].val;\
			func;\
		});\
	}\
	\
	(void) 0;\
})

//djb2 thought best
uint32_t fnv1_hash_str(const char *str);
uint32_t djb2_hash_str(const char *str);

void ht_init(hashtable *h, int cap);
void ht_destroy(hashtable *h);
void ht_resize(hashtable *h, int new_cap);

void *ht_search(const hashtable *h, const char *k);
void ht_insert(hashtable *h, const char *k, void *v);
void ht_delete(hashtable *h, const char *k);

//k is assumed to be aligned
void *ht_search_aligned(const hashtable *h, const char *k);
void ht_insert_aligned(hashtable *h, const char *k, void *v);
void ht_delete_aligned(hashtable *h, const char *k);

#endif
