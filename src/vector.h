//Vector and assorted maths

#ifndef VECTOR_H
#define VECTOR_H

#ifndef M_PI
#define M_PI 3.141592653
#endif

#define DEG_TO_RAD(x) fmod(((x)/360.0)*M_PI*2.0,M_PI*1.9999)
#define RAD_TO_DEG(x) fmod(((x)/(M_PI*2.0))*360.0,359.99999)
#define BIN_TO_RAD(x) fmod(((x)/256.0)*M_PI*2.0,M_PI*1.9999)
#define RAD_TO_BIN(x) fmod(((x)/(M_PI*2.0))*256.0,255.99999)

//VECtor Component Type
//May use double if extra precision is necessary
//Compiling with gcc -ffp-contract=off will improve precision, specially of cross product,
//an operation that is often turned into a fused multiply add instruction, which has bad rounding
//i.e. (x,y,z)x(-x,-y,-z) will often have a non-zero Z component
//In older gcc it is called -mno-fused-madd
//Disabling it allows correct bsp building with only single precision vecct, which is wonderful
#ifndef VECCT_SINGLE
#ifndef VECCT_DOUBLE
#define VECCT_DOUBLE
#endif
#endif
#ifdef VECCT_DOUBLE
#define VECCT_FORMAT "%lf"
typedef double vecc_t;
#else
#define VECCT_FORMAT "%f"
typedef float vecc_t;
#endif

//FLT_EPSILON - works well even with VECCT_DOUBLE
#define VECCT_EPSILON 1.19209290E-07F
#define VECCT_SIGN(a) ((a) < -VECCT_EPSILON ? -1 : (a) > VECCT_EPSILON ? 1 : 0)

//In the future, may replace with x86 fsincos instruction
#define DBL_SINCOS(x, s, c)\
({\
	const double _x = (x);\
	const double _sin = sin(_x);\
	const double _cos = cos(_x);\
	s = _sin;\
	c = _cos;\
});

typedef struct vec3
{
	vecc_t x, y, z;
	#ifdef __SSE__
	vecc_t w;
	#endif
} vec3;

typedef union{
	vec3 v;
	vecc_t c[3];
} vec3u;

//'rot' are Tait-Bryan angles with ZYX order, as used by Allegro
struct obb
{
	vec3 pos, rot, size;
};

//Useful constants
extern const vec3 vec3c_origin, vec3c_min, vec3c_max, vec3c_nan, vec3c_x, vec3c_y, vec3c_z, vec3c_unit;

//The "m" is for "maths"
typedef struct mplane
{
	vec3 normal;
	vecc_t dist;
} mplane;

//A literal of 0.000003814697265625000000206795153139, with the last digit rounded up,
//was found the minimal allowing compilation of BOBf with single floating point precision vecc_t
//This value is between 0x1p-18 and 0x1.000000000049dp-18
//(i.e. 0.000003814697265625 and 0.000003814697265626)
//The next adjacent (single) float to 0x1p-18 is 0x1.000002p-18 (decimal 0.0000038146977203723509)
//The next adjacent double is 0x1.0000000000001p-18             (decimal 0.0000038146972656250008)
//The value is however set to an arbitrary 10 micrometres
#define MPLANE_TOLERANCE 0.00001

//Not using V3D_f allows easily changing precision to double if needed
//The "r" is for "render"
typedef struct rvert
{
	vec3 pos;
	float u, v;
} rvert;

#define RVFACE_MAX_VERTS 30

//TODO: change to use vec3s for verts and keep UVs in separate array
typedef struct rvface
{
	rvert verts[RVFACE_MAX_VERTS];
	int num_verts;
} rvface;

#define RVERT_TO_V3DF(r) {\
	(r).pos.x,\
	(r).pos.y,\
	(r).pos.z,\
	(r).u,\
	(r).v\
}

#define V3DF_TO_RVERT(r) {{\
		(r).x,\
		(r).y,\
		(r).z,\
	},\
	(r).u,\
	(r).v\
}

#define VEC3_RECOMPOSE(v) {\
	(v).x,\
	(v).y,\
	(v).z,\
}

//Macros only for initialisers
#define VEC3_ADD(a, b)		{(a).x + (b).x,		(a).y + (b).y,		(a).z + (b).z	}
#define VEC3_MADD(a, b, m)	{(a).x + (b).x * m,	(a).y + (b).y  * m,	(a).z + (b).z * m	}
#define VEC3_MID(a, b)		{\
	((a).x + (b).x) * 0.5,\
	((a).y + (b).y) * 0.5,\
	((a).z + (b).z) * 0.5\
}
#define VEC3_SUB(a, b)		{(a).x - (b).x,		(a).y - (b).y,		(a).z - (b).z	}
#define VEC3_MULT(a, m)		{(a).x * (m),		(a).y * (m),		(a).z * (m)		}
#define VEC3_CROSS(a, b){\
	(a).y * (b).z - (a).z * (b).y,\
	(a).z * (b).x - (a).x * (b).z,\
	(a).x * (b).y - (a).y * (b).x\
}
#define VEC3_RAND(mag) ({\
	vec3 _tmp = {kdr_randdnormal(), kdr_randdnormal(), kdr_randdnormal()};\
	vec3_fnormalize(&_tmp);\
	vec3_mult(&_tmp, (mag));\
	_tmp;\
})

//The famous
float q_fisqrt(float number);

//TODO: pick some and add inline specifier
void vec3_add(const vec3 *a, const vec3 *b, vec3 *out);
void vec3_sub(const vec3 *a, const vec3 *b, vec3 *out);
void vec3_addto(vec3 *a, const vec3 *b);
void vec3_subfrom(vec3 *a, const vec3 *b);
vecc_t vec3_dot(const vec3 *a, const vec3 *b);
void vec3_cross(const vec3 *a, const vec3 *b, vec3 *out);
vecc_t vec3_crossz(const vec3 *a, const vec3 *b);
vecc_t vec3_sqlen(const vec3 *a);
vecc_t vec3_len(const vec3 *a);
vecc_t vec3_filen(const vec3 *a);
#define VEC3_DIST(a,b) ({\
	const vec3 _a = (a), _b = (b), _tmp = VEC3_SUB(_a, _b);\
	vec3_len(&_tmp);\
})
#define VEC3_SQDIST(a,b) ({\
	const vec3 _a = (a), _b = (b), _tmp = VEC3_SUB(_a, _b);\
	vec3_sqlen(&_tmp);\
})
#define VEC3_FIDIST(a,b) ({\
	const vec3 _a = (a), _b = (b), _tmp = VEC3_SUB(_a, _b);\
	vec3_filen(&_tmp);\
})
void vec3_normalize(vec3 *a);
void vec3_fnormalize(vec3 *a);
void vec3_mult(vec3 *a, const vecc_t m);
void vec3_maddto(vec3 *a, const vec3 *b, const vecc_t m);
void vec3_madd(const vec3 *a, const vec3 *b, const vecc_t m, vec3 *out);
void vec3_normal(const vec3 *a, const vec3 *b, const vec3 *c, vec3 *out);
void vec3_fnormal(const vec3 *a, const vec3 *b, const vec3 *c, vec3 *out);
void vec3_sqnormal(const vec3 *a, const vec3 *b, const vec3 *c, vec3 *out);
void vec3_setmax(const vec3 *a, vec3 *out);
void vec3_setmin(const vec3 *a, vec3 *out);
void vec3_print(const vec3 *in);
void vec3_swap(vec3 *a, vec3 *b);
int vec3_equal(const vec3 *a, const vec3 *b);
void vec3_bounds(const vec3 *verts, const int nverts, vec3 *min, vec3 *max);
void vec3_interp(const vec3 *a, const vec3 *b, vecc_t i, vec3 *out);
void vec3_slerp(const vec3 *a, const vec3 *b, vecc_t i, vec3 *out);
void vec3_fslerp(const vec3 *a, const vec3 *b, vecc_t i, vec3 *out);
void vec3_lineclosest(const vec3 *a, const vec3 *b, const vec3 *p, vec3 *out);

void vec3_verts2plane(const vec3 *a, const vec3 *b, const vec3 *c, mplane *out);
void vec3_point2plane(const vec3 *p, const vec3 *norm, mplane *out);
int vec3_planecmp(const mplane *pl, const vec3 *p);
vecc_t vec3_dist2plane(const mplane *pl, const vec3 *p);
vecc_t vec3_ray2plane(const mplane *p, const vec3 *start, const vec3 *dir, vec3 *res);
int vec3_segment2plane(const mplane *p, const vec3 *p1, const vec3 *p2, vec3 *res, vecc_t *dist);
int mplane_equal(const mplane *p1, const mplane *p2);
int mplane_intersect3(const mplane *p1, const mplane *p2, const mplane *p3, vec3 *out);
void mplane_flip(mplane *pl);
int mplane_cutbox_mm(const mplane *p, const vec3 *box_min, const vec3 *box_max, vec3 *new_box_min, vec3 *new_box_max);
int mplane_cutbox_sc(const mplane *p, const vec3 *box_min, const vec3 *box_max, vec3 *new_box_min, vec3 *new_box_max);
void mplane_closest(const mplane *pl, const vec3 *in, vec3 *out);

int vec3_intersectrayaabb_mm(const vec3 *p, const vec3 *d, const vec3 *amin, const vec3 *amax, double *tmin, vec3 *q);
int vec3_testsegmentaabb_mm(const vec3 *p0, const vec3 *p1, const vec3 *bmin, const vec3 *bmax);
vecc_t vec3_ray2box_s(const vec3 *start, const vec3 *dir, const vec3 *box_s);
//int vec3_seg2box_mm(const vec3 *start_, const vec3 *end_, const vec3 *box_min, const vec3 *box_max, vec3 *new_start, vec3 *new_end);
int vec3_box2box_sc(const vec3 *siza, const vec3 *posa, const vec3 *sizb, const vec3 *posb, vec3 *push);
int vec3_box2box_mm(const vec3 *mina, const vec3 *maxa, const vec3 *minb, const vec3 *maxb, vec3 *push);

#define VEC3_MM2SC(min, max, siz, ctr) siz = {\
		((max).x - (min).x) * 0.5,\
		((max).y - (min).y) * 0.5,\
		((max).z - (min).z) * 0.5\
	},\
	ctr = {\
		((max).x + (min).x) * 0.5,\
		((max).y + (min).y) * 0.5,\
		((max).z + (min).z) * 0.5\
	}
#define VEC3_SC2MM(siz, ctr, min, max) min = {\
		(ctr).x - (siz).x,\
		(ctr).y - (siz).y,\
		(ctr).z - (siz).z\
	},\
	max = {\
		(ctr).x + (siz).x,\
		(ctr).y + (siz).y,\
		(ctr).z + (siz).z\
	}

#define VEC3_BOXVERTS_MM(min, max) {\
	{(min).x, (min).y, (min).z},\
	{(max).x, (min).y, (min).z},\
	{(min).x, (max).y, (min).z},\
	{(max).x, (max).y, (min).z},\
	{(min).x, (min).y, (max).z},\
	{(max).x, (min).y, (max).z},\
	{(min).x, (max).y, (max).z},\
	{(max).x, (max).y, (max).z}\
}

#define VEC3_BOXVERTS_SC(siz, ctr) {\
	{(ctr).x - (siz).x, (ctr).y - (siz).y, (ctr).z - (siz).z},\
	{(ctr).x + (siz).x, (ctr).y - (siz).y, (ctr).z - (siz).z},\
	{(ctr).x - (siz).x, (ctr).y + (siz).y, (ctr).z - (siz).z},\
	{(ctr).x + (siz).x, (ctr).y + (siz).y, (ctr).z - (siz).z},\
	{(ctr).x - (siz).x, (ctr).y - (siz).y, (ctr).z + (siz).z},\
	{(ctr).x + (siz).x, (ctr).y - (siz).y, (ctr).z + (siz).z},\
	{(ctr).x - (siz).x, (ctr).y + (siz).y, (ctr).z + (siz).z},\
	{(ctr).x + (siz).x, (ctr).y + (siz).y, (ctr).z + (siz).z}\
}

extern const int idx_boxedges[12][2];

void rvert_interp(const rvert *v1, const rvert *v2, rvert *out, const vecc_t val);
int rvface_interp2(const rvface *f, const rvface *f2, const vec3 *pos, rvert *res);

#define BARYC_TOLERANCE MPLANE_TOLERANCE
#define VEC3_BARYC_INSIDE(baryc) (\
((baryc).x > -BARYC_TOLERANCE && (baryc).x < 1.0 + BARYC_TOLERANCE) &&\
((baryc).y > -BARYC_TOLERANCE && (baryc).y < 1.0 + BARYC_TOLERANCE) &&\
((baryc).z > -BARYC_TOLERANCE && (baryc).z < 1.0 + BARYC_TOLERANCE))

struct MATRIX_f;

void rvface_flip(rvface *face);
void rvface_scale(rvface *face, float s);
void rvface_midpoint(const rvface *face, vec3 *out);
vecc_t rvface_area(const rvface *f);
int rvface2d_zcheck(const rvface *f);
void rvface2d_midpoint_area(const rvface *f, vec3 *midp, vecc_t *area);
void rvface_midpoint_area(const rvface *f, rvert *midp, vecc_t *area);
int rvface_isconvex(const rvface *f);
int rvface_isplanar(const rvface *f);
int rvface_isbad(const rvface *f);
void rvface_print(const rvface *f);
void rvface_split(const mplane *p, const rvface *face, rvface *front_face, rvface *back_face);
int rvface_sort(const mplane *p, const rvface *face);
void rvface_sortverts(rvface *face);
void rvface_boxslice(const mplane *p, const vec3 *min, const vec3 *max, rvface *out);
void rvface_planeproj(const vec3 *normal, const rvface *in, rvface *out);
void rvface_bounds(const rvface *f, vec3 *min, vec3 *max);
int rvface_pick(const rvface *f, const vec3 *start, const vec3 *dir, vec3 *res, vecc_t *dist);
void rvface_planecircle(const mplane *pl, rvface *f, vecc_t radius, int segs);
void rvface_tri_baryc(const rvface *tri, const vec3 *in, vec3 *out);
int rvface_interp(const rvface *f, const vec3 *pos, rvert *res);
void rvface_fitminrect(const rvface *face, rvface *minface2d, float *w, float *h, struct MATRIX_f *bestmat);
int rvfaces_to_obb(rvface *faces, int num_faces, struct vec3 (*boxes)[5]);
void rvface_closest(const rvface *face, const vec3 *in, vec3 *out);

#define RVFACE_SPANNING 0
#define RVFACE_COPLANAR 1
#define RVFACE_FRONT 2
#define RVFACE_BACK 3

/*
A note on matrices
------------------
From allegro\src\c\cmisc.c:
*/
#if 0
void apply_matrix_f(AL_CONST MATRIX_f *m, float x, float y, float z,
		    float *xout, float *yout, float *zout)
{
#define CALC_ROW(n) (x * m->v[(n)][0] + y * m->v[(n)][1] + z * m->v[(n)][2] + m->t[(n)])
   *xout = CALC_ROW(0);
   *yout = CALC_ROW(1);
   *zout = CALC_ROW(2);
#undef CALC_ROW
}
#endif
/*
And from Allegro docs,

"Given the 4x4 matrix: 

   ( a, b, c, d )
   ( e, f, g, h )
   ( i, j, k, l )
   ( m, n, o, p )
a pattern can be observed in which parts of it do what. The top left 3x3 grid
implements rotation and scaling. The three values in the top right column (d,
h, and l) implement translation, and as long as the matrix is only used for 
affine transformations, m, n and o will always be zero and p will always be 1.
If you don't know what affine means, read Foley & Van Damme: basically it
covers scaling, translation, and rotation, but not projection. Since Allegro 
uses a separate function for projection, the matrix functions only need to 
support affine transformations, which means that there is no need to store the 
bottom row of the matrix. Allegro implicitly assumes that it contains (0,0,0,
1), and optimises the matrix manipulation functions accordingly. Read chapter
"Structures and types defined by Allegro" for an internal view of the MATRIX_f 
structures."

Furthermore, from
https://seanmiddleditch.com/matrices-handedness-pre-and-post-multiplication-row-vs-column-major-and-notations/
(archive: https://web.archive.org/web/20190717031038/http://seanmiddleditch.com/matrices-handedness-pre-and-post-multiplication-row-vs-column-major-and-notations/)

"In mathematics texts, post-multiplication is the norm. This in turn means that 
the matrix product is computed like so: each component of the result, at a 
given row R and column C, is computed by taking the dot product of the left 
matrix's row R by the right matrix's column C. [...] What matters is whether 
pre-multiplication or post-multiplication are used. The D3DX math library uses 
pre-multiplication and OpenGL's standard matrix stack uses post-multiplication.
 That in general would mean that the matrices must be transposed."

So Allegro uses post-multiplication and column vectors.
*/

void vec3_apply_matrix_f(vec3 *a, const struct MATRIX_f *m);
void rvface_apply_matrix_f(rvface *a, const struct MATRIX_f *m);
void vec3_apply_euler(vec3 *a, const vec3 *euler);
vec3 vec3_euler_dir(const vec3 *euler);
void vec3_dir2pitchyaw(const vec3 dir, float *pitch, float *yaw);
void vec3_persp_project_f(vec3 *a);
int matrix_f_invert(struct MATRIX_f *m);
int matrix_f_invert2(struct MATRIX_f *m);
void matrix_f_print(struct MATRIX_f *m);
int matrix_f_frustum2box_mm(const struct MATRIX_f *m, const vec3 *min_, const vec3 *max_);
void matrix_f_toeuler(const struct MATRIX_f *m, vec3 *euler);
int matrix_f_equal(const struct MATRIX_f *m1, const struct MATRIX_f *m2);
void matrix_f_transp(struct MATRIX_f *m);
void matrix_f_cpos(const struct MATRIX_f *m, vec3 *pos);
void matrix_f_trs(const vec3 *pos, const vec3 *rot, const vec3 *scl, struct MATRIX_f *res);
void matrix_f_to_trs(vec3 *pos, vec3 *rot, vec3 *scl, const struct MATRIX_f *src_);
void matrix_f_from3points(const vec3 *a, const vec3 *b, const vec3 *c, struct MATRIX_f *out);
void matrix_f_fromrvface(const rvface *face, struct MATRIX_f *out);
void matrix_f_interp(const struct MATRIX_f *a, const struct MATRIX_f *b, double i, struct MATRIX_f *out);
#define matrix_f_to_obb(mat, obb_)\
({\
	struct obb *_obb = (obb_);\
	\matrix_f_to_trs(&_obb->pos, &_obb->rot, &_obb->size, (mat));\
})
#define matrix_f_mul(a,b,c) matrix_mul_f(a,b,c)
#define matrix_f_mulr(a,b,c) matrix_mul_f(b,a,c)
void matrix_f_box_sc(const struct MATRIX_f *m, vec3 *siz, vec3 *pos);
void matrix_f_hemicube(const struct MATRIX_f *patch_mat, const vec3 *pos, struct MATRIX_f *hemicube_mats);

int vec3_clipline(vec3 *a, vec3 *b);

#endif
