/*
Translated to C from potpack library by MapBox
https://github.com/mapbox/potpack
(archive: https://web.archive.org/web/20200509133554/https://github.com/mapbox/potpack)

Original license follows:

ISC License

Copyright (c) 2018, Mapbox

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted, provided that the above copyright notice
and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.
*/

#include <math.h>
#include <stdlib.h>
#include <limits.h>

#include "rectpack.h"
#include "arr.h"

#define MAX(a,b)\
({\
	__typeof__(a) _a = (a);\
	__typeof__(b) _b = (b);\
	_a >= _b ? _a : _b;\
})

static int box_h_cmp(const struct rp_rect *a, const struct rp_rect *b)
{
	return b->h - a->h;
}

void rectpack(struct rp_rect *boxes_in, struct rp_rect *boxes_out, int num_boxes, int *w, int *h)
{
	arr_t(struct rp_rect) boxes = {
		.data = boxes_in,
		.len = num_boxes,
		.siz = num_boxes,
		.is_static = 1
	};
	// calculate total box area and maximum box width
	int area = 0;
	int maxWidth = 0;
	arr_foreach(&boxes, box, idx, ({
		area += box.w * box.h;
		maxWidth = MAX(maxWidth, box.w);
	}));
	
	// sort the boxes for insertion by height, descending
	arr_sort(&boxes, box_h_cmp);
	
	// aim for a squarish resulting container,
	// slightly adjusted for sub-100% space utilization
	const int startWidth = MAX(ceil(sqrt(area / 0.95)), maxWidth);
	
	// start with a single empty space, unbounded at the bottom
	#define RP_RECT(...) ({ struct rp_rect _tmp = { __VA_ARGS__ }; _tmp; })
	arr_t(struct rp_rect) spaces arr_cleanup = {};
	arr_push(&spaces, RP_RECT(.x = 0, .y = 0, .w = startWidth, .h = INT_MAX));
	arr_t(struct rp_rect) packed = {
		.data = boxes_out,
		.len = 0,
		.siz = num_boxes,
		.is_static = 1
	};
	
	arr_foreach(&boxes, box, idx, ({
		// look through spaces backwards so that we check smaller spaces first
		for(int i = spaces.len - 1; i >= 0; i--){
			struct rp_rect *space = spaces.data + i;
			
			// look for empty spaces that can accommodate the current box
			if(box.w > space->w || box.h > space->h) continue;
			
			// found the space; add the box to its top-left corner
			// |-------|-------|
			// |	box	|			 |
			// |_______|			 |
			// |				 space |
			// |_______________|
			struct rp_rect tmp = box;
			tmp.x = space->x;
			tmp.y = space->y;
			arr_push(&packed, tmp);
			
			if(box.w == space->w && box.h == space->h){
				// space matches the box exactly; remove it
				const struct rp_rect last = arr_last(&spaces);
				arr_pop(&spaces);
				if(i < spaces.len){
					spaces.data[i] = last;
				}
			}
			else if(box.h == space->h){
				// space matches the box height; update it accordingly
				// |-------|---------------|
				// |	box	| updated space |
				// |_______|_______________|
				space->x += box.w;
				space->w -= box.w;

			}
			else if(box.w == space->w){
				// space matches the box width; update it accordingly
				// |---------------|
				// |			box			|
				// |_______________|
				// | updated space |
				// |_______________|
				space->y += box.h;
				space->h -= box.h;
			}
			else{
				// otherwise the box splits the space into two spaces
				// |-------|-----------|
				// |	box	| new space |
				// |_______|___________|
				// | updated space		 |
				// |___________________|
				arr_push(&spaces, RP_RECT(
					.x = space->x + box.w,
					.y = space->y,
					.w = space->w - box.w,
					.h = box.h
				));
				space = spaces.data + i;
				space->y += box.h;
				space->h -= box.h;
			}
			break;
		}
	}));
	
	*w = 0;
	*h = 0;
	arr_foreach(&packed, box, idx, ({
		if(*w < box.x + box.w) *w = box.x + box.w;
		if(*h < box.y + box.h) *h = box.y + box.h;
	}));
}
