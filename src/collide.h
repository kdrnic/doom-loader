#ifndef COLLIDE_H
#define COLLIDE_H

struct bspdata;
struct vec3;
struct rvface;

typedef struct bsp_tracebox_q
{
	const struct bspdata *bsp;
	const vec3 box_s, box_pos, move;
	vec3 hit_normal;
	int face_id;
	double time;
	
	double (*on_leaf)(struct bsp_tracebox_q *, int);
} bsp_tracebox_q;

typedef struct bsp_pick_q
{
	const struct bspdata *bsp;
	const vec3 start, end;
	vec3 res;
	vec3 dir;
	int face_id;
	double dist;
	
	double (*on_leaf)(struct bsp_pick_q *, int);
} bsp_pick_q;

#define BSP_BOXLEAFS_MAX 16

double box_tracebox(const struct vec3 *other_s, const struct vec3 *other_pos, const struct vec3 *box_s, const struct vec3 *move, struct vec3 *hit_normal);

int poly_testbox(const struct rvface *tri, const struct vec3 *box_s);
double poly_tracebox(const struct rvface *tri, const struct vec3 *box_s, const struct vec3 *move, struct vec3 *hit_normal);
int poly_pushbox(const struct rvface *tri, const struct vec3 *box_s, struct vec3 *push);

double bsp_tracebox(bsp_tracebox_q *q);
int bsp_pick(bsp_pick_q *q);

int bsp_testbox(const struct bspdata *bsp, const struct vec3 *box_s, const struct vec3 *box_pos);
int bsp_pushbox(const struct bspdata *bsp, const struct vec3 *box_s, struct vec3 *box_pos, struct vec3 *push);
int bsp_boxleafs(const struct bspdata *bsp, const struct vec3 *box_s, const struct vec3 *box_pos, int *leafs);

#endif
