#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <math.h>
#include <locale.h>
#include <errno.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <ctype.h>

#include <allegro.h>

#include "arr.h"
#include "vector.h"
#include "bsp.h"
#include "wad.h"
#include "info.h"
#include "raster.h"
#include "rdebug.h"
#include "sprsheet.h"
#include "bmputil.h"

//Increase stack length under MS-DOS
#ifdef __DJGPP__
unsigned _stklen = 10485760;
#endif

static int list_wad_callback(const char *filename, int attrib, void *param)
{
	char **list = param;
	strcpy(*list, filename);
	*list += strlen(*list) + 1;
	
	return 0;
}

typedef struct str256
{
	char val[256];
	char val2[256];
} str256;

static arr_t(str256) str256list = {};

#define WHITE -1
#define BLUE -2
#define GREEN -3

static char *str256list_getter(int index, int *list_size)
{
	if(index < 0){
		*list_size = str256list.len;
	}
	else{
		return str256list.data[index].val;
	}
	return 0;
}

static char *get_menu_option(int idx2, const char *def)
{
	static const DIALOG the_dialog_[] =
	{
	//	(dialog proc)		(x)		(y)		(w)			(h)			(fg)	(bg)	(key)	(flags)		(d1)					(d2)	(dp)				(dp2)	(dp3)
		{d_clear_proc,		0,		0,		0,			0,			WHITE,	0,		0,		0,			0,						0,		NULL,				NULL,	NULL	},//0
	//	{d_rtext_proc,		96,		32,		0,			0,			WHITE,	0,		0,		0,			0,						0,		"Address:",			0,		0		},//1
	//	{d_edit_proc,		96,		32,		224,		8,			WHITE,	0,		0,		0,			sizeof(address)-1,		0,		address,			NULL,	NULL	},//2
	//	{d_radio_proc,		320,	32,		160,		16,			WHITE,	0,		0,		D_SELECTED,	0,						0,		"Server",			0,		0		},//3
	//	{d_radio_proc,		480,	32,		160,		16,			WHITE,	0,		0,		0,			0,						0,		"Client",			0,		0		},//4
	//	{d_button_proc,		80,		64,		160,		16,			WHITE,	0,		0,		D_EXIT,		0,						0,		"Connect/Listen",	NULL,	NULL	},//5
	//	{d_ctext_proc,		320,	8,		0,			0,			WHITE,	0,		0,		0,			0,						0,		status,				0,		0		},//6
		{d_list_proc,		16,		16,		320 - 32,	200 - 32,	WHITE,	0,		0,		D_EXIT,		0,						0,		str256list_getter,	0,		0		},//7
		{NULL,				0,		0,		0,			0,			0,		0,		0,		0,			0,						0,		NULL,				NULL,	NULL	}
	};
	static DIALOG the_dialog[sizeof(the_dialog_) / sizeof(DIALOG)];
	
	int i;
	memcpy(the_dialog, the_dialog_, sizeof(the_dialog_));
	const int colours[3] = {makecol(255, 255, 255), makecol(0, 0, 255), makecol(0, 255, 0)};
	int idx_list = 0;
	for(i = 0; i < sizeof(the_dialog_) / sizeof(DIALOG); i++){
		if(the_dialog[i].fg < 0){
			the_dialog[i].fg = colours[-1 - the_dialog[i].fg];
		}
		if(the_dialog[i].bg < 0){
			the_dialog[i].bg = colours[-1 - the_dialog[i].bg];
		}
		if(the_dialog[i].proc == d_list_proc){
			idx_list = i;
			the_dialog[i].d1 = arr_findidx(&str256list, el, idx, ({!strcmp(def, el.val);}));
		}
		
		the_dialog[i].x = (the_dialog[i].x * SCREEN_W) / 320;
		the_dialog[i].y = (the_dialog[i].y * SCREEN_W) / 320;
		the_dialog[i].w = (the_dialog[i].w * SCREEN_W) / 320;
		the_dialog[i].h = (the_dialog[i].h * SCREEN_W) / 320;
	}
	
	int ret = do_dialog(the_dialog, -1);
	
	if(ret == idx_list){
		int idx = the_dialog[idx_list].d1;
		if(idx >= 0) return idx2 ? str256list.data[idx].val2 : str256list.data[idx].val;
	}
	return 0;
}

static int flatface_comp(const void *a, const void *b)
{
	return ((struct wad_flatface *) a)->face.verts[0].pos.z - ((struct wad_flatface *) b)->face.verts[0].pos.z;
}

static vec3 linface_cmp_z;
static int linface_cmp(const void *a, const void *b)
{
	int i;
	const struct wad_wallface *lfa = a;
	const struct wad_wallface *lfb = b;
	const vec3 *z = &linface_cmp_z;
	float mina = FLT_MAX, minb = FLT_MAX;
	float maxa = -FLT_MAX, maxb = -FLT_MAX;
	for(i = 0; i < lfa->face.num_verts; i++){
		float dot = vec3_dot(&lfa->face.verts[i].pos, z);
		if(dot < mina) mina = dot;
		if(dot > maxa) mina = dot;
	}
	for(i = 0; i < lfb->face.num_verts; i++){
		float dot = vec3_dot(&lfb->face.verts[i].pos, z);
		if(dot < minb) minb = dot;
		if(dot < maxb) maxb = dot;
	}
	#define QSCMP(a,b) (a<b)-(a>b)
	return QSCMP(mina,minb);
}

struct str8
{
	char val[8];
	BITMAP *bmp;
};

static volatile unsigned int tics = 0;
static void count_tics()
{
	tics++;
}
END_OF_FUNCTION(count_tics);

static void add_wads(const char *wad_dir)
{
	if(!strlen(wad_dir)) return;
	int i;
	char *wad_list_ptr, *wad_list_it;
	char wad_list[100000] = {0};
	char wad_str[256];
	strcpy(wad_str, wad_dir);
	strcat(wad_str, "\\*.wad");
	wad_list_ptr = wad_list;
	for_each_file_ex(wad_str, FA_NONE, 0, &list_wad_callback, &wad_list_ptr);
	for(i = 1, wad_list_it = wad_list; wad_list_it < wad_list_ptr; wad_list_it += strlen(wad_list_it) + 1, i++){
		str256 mapname256;
		strcpy(mapname256.val, get_filename(wad_list_it));
		strcpy(mapname256.val2, wad_list_it);
		arr_push(&str256list, mapname256);
	}
}

int main(int argc, char **argv)
{
	char *wad_filename = 0;
	int i, j, k;
	
	str256 mapname256;
	
	//-------------------- Allegro setup ---------------------------------
	
	allegro_init();
	
	#if __DJGPP__
	set_config_file("doomdos.cfg");
	#else
	set_config_file("doom.cfg");
	#endif
	
	set_color_depth(8);
	
	#if !__DJGPP__
	if(set_gfx_mode(
		get_config_int("gfx", "fullscreen", 0) ?
			GFX_AUTODETECT_FULLSCREEN :
			GFX_AUTODETECT_WINDOWED,
		get_config_int("gfx", "screenw", 640),
		get_config_int("gfx", "screenh", 480),
		0,
		0
	))
	#else
	if(set_gfx_mode(
		GFX_AUTODETECT,
		get_config_int("gfx", "screenw", 640),
		get_config_int("gfx", "screenh", 480),
		0,
		0
	))
	#endif
	{
		allegro_exit();
		printf("Couldn't set graphics mode: \"%s\"\n", allegro_error);
		return 1;
	}
	
	install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, 0);
	install_keyboard();
	install_mouse();
	
	LOCK_FUNCTION(count_tics);
	LOCK_VARIABLE(tics);
	install_int_ex(count_tics, BPS_TO_TIMER(35));
	
	//-------------------- Allegro setup end ---------------------------------
	
	//Let player pick WAD from those found in waddir
	add_wads(get_config_string("wad", "waddir", "."));
	add_wads(get_config_string("wad", "waddir2", ""));
	
	wad_filename = (argc < 2) ? get_menu_option(1, "DOOM.WAD") : ({
		arr_find_p(&str256list, el, idx, ({!strcmp(el.val, argv[1]);}))->val2;
	});
	wad_filename = strdup(wad_filename);
	
	FILE *file_wad = fopen(wad_filename, "rb");
	if(!file_wad){
		printf("Not found WAD: %s\n", wad_filename);
		exit(1);
	}
	
	//Read header palette title pic etc.
	
	struct wad_header h;
	fread(&h, sizeof(h), 1, file_wad);
	
	struct wad_lump lumps[h.num_lumps], *playpal_lump, *title_lump, *map_lump;
	wad_read_dir(file_wad, &h, lumps);
	for(i = 0; i < h.num_lumps; i++){
		//puts(lumps[i].name);
	}
	
	playpal_lump = wad_dir_lump(&h, lumps, "PLAYPAL");
	
	PALETTE pals[14];
	
	wad_read_palettes(file_wad, playpal_lump, pals);
	
	set_palette(pals[0]);
	
	title_lump = wad_dir_lump(&h, lumps, "TITLEPIC");
	if(title_lump && (argc < 2)){
		BITMAP *titlepic = wad_read_pic(file_wad, title_lump);
		stretch_blit(titlepic, screen, 0, 0, 320, 200, 0, 0, SCREEN_W, SCREEN_H);
		while(!((mouse_b & 1) || key[KEY_Z])){}
	}
	
	// Map select menu ----------------------------------------
	void *ret;
	struct wad_lump *midi_lump = 0;
	char *selected_map;
	str256list.len = 0;
	for(i = 1, k = 0; i <= 3; i++){
		for(j = 1; j <= 19; j++){
			sprintf(mapname256.val, "E%dM%d", i, j);
			ret = &&next1; goto addmap;
			
			next1:
			continue;
		}
	}
	for(i = 0; i <= 32; i++){
		sprintf(mapname256.val, "MAP%02d", i);
		ret = &&next2; goto addmap;
		
		next2:
		continue;
	}
	goto after;
	
	addmap:
	map_lump = wad_dir_lump(&h, lumps, mapname256.val);
	if(map_lump){
		arr_push(&str256list, mapname256);
	}
	goto *ret;
	
	after:
	while(1){
		selected_map = (argc < 3) ? get_menu_option(0, "E1M1") : argv[2];
		if(selected_map){
			map_lump = wad_dir_lump(&h, lumps, selected_map);
			if(!map_lump){
				printf("Not found MAP: %s", selected_map);
				exit(1);
			}
			
			char midi_name[256];
			strcpy(midi_name, "D_");
			strcat(midi_name, selected_map);
			midi_lump = wad_dir_lump(&h, lumps, midi_name);
			
			break;
		}
	}
	
	// Map select menu end ----------------------------------------
	
	if(midi_lump){
		MIDI *midi = wad_read_midi(file_wad, midi_lump);
		if(midi){
			int length = get_midi_length(midi);
			int min = length / 60;
			int hours = min / 60;
			//printf("MIDI %dh%dm%ds (%d), %d bytes\n", hours, min % 60, length % 60, length, midi->track[0].len);
			if(length) play_midi(midi, 1);
		}
		//while(1) rest(20);
	}
	else{
		puts("MIDI not found");
	}
	
	// Load map, pnames, colormap, sprites, etc. ----------------------------------------
	
	struct wad_lump *map_lumps[WAD_MAP_LUMPS_LEN] = {0};
	
	wad_read_map_lumps(file_wad, map_lump, map_lumps);
	
	struct wad_flatface *ffit;
	struct wad_flat *flattexes;
	
	int num_flats = wad_read_flats(file_wad, lumps, &flattexes);
	
	COLOR_MAP colormap;
	struct wad_lump *colormap_lump = wad_dir_lump(&h, lumps, "COLORMAP");
	wad_read_colormap(file_wad, colormap_lump, &colormap);
	color_map = &colormap;
	
	struct wad_lump *pnames_lump = wad_dir_lump(&h, lumps, "PNAMES");
	fseek(file_wad, pnames_lump->offs, SEEK_SET);
	int num_pnames;
	fread(&num_pnames, 4, 1, file_wad);
	char *pnames = malloc(8 * num_pnames);
	fread(pnames, 1, num_pnames * 8, file_wad);
	BITMAP **wall_patches = malloc(num_pnames * sizeof(*wall_patches));
	for(i = 0; i < num_pnames; i++){
		char *pname = pnames + i * 8;
		for(j = 0; j < 8; j++) pname[j] = toupper(pname[j]);
		struct wad_lump *lump;
		if((lump = wad_dir_lump(&h, lumps, pname))) wall_patches[i] = wad_read_pic(file_wad, lump);
		else{
			wall_patches[i] = 0;
			//printf("Patch \"%.8s\" not found\n", pname);
		}
	}
	
	struct wad_texture **textures = 0;
	int num_textures = 0;
	wad_read_textures(file_wad, wad_dir_lump(&h, lumps, "TEXTURE2"), &textures, &num_textures);
	wad_read_textures(file_wad, wad_dir_lump(&h, lumps, "TEXTURE1"), &textures, &num_textures);
	
	BITMAP *ftex = create_bitmap_ex(8, 64, 64);
	int texres = get_config_int("gfx", "texres", 128);
	BITMAP *bmp256 = create_bitmap_ex(8, texres, texres);
	
	BITMAP ***bsp_textures = malloc(sizeof(*bsp_textures) * (num_textures + num_flats));
	for(i = 0; i < num_textures; i++){
		BITMAP *tex = wad_texture_to_bitmap(textures, num_textures, wall_patches, textures[i]->name);
		if(tex){
			stretch_blit(tex, bmp256, 0, 0, tex->w, tex->h, 0, 0, bmp256->w, bmp256->h);
			destroy_bitmap(tex);
		}
		else{
			printf("NF %d %.8s\n", i, textures[i]->name);
		}
		bsp_textures[i] = malloc(sizeof(*bsp_textures[i]));
		bsp_textures[i][0] = create_bitmap_ex(8, bmp256->w, bmp256->h);
		clear_to_color(bsp_textures[i][0], 0);
		draw_sprite(bsp_textures[i][0], bmp256, 0, 0);
	}
	for(i = 0; i < num_flats; i++){
		bsp_textures[i + num_textures] = malloc(sizeof(*bsp_textures[i]));
		bsp_textures[i + num_textures][0] = create_bitmap_ex(8, 64, 64);
		clear_to_color(bsp_textures[i + num_textures][0], 0);
		
		ftex->dat = flattexes[i].data;
		for(j = 1, ftex->line[0] = ftex->dat; j < 64; j++){
			ftex->line[j] = ftex->line[j - 1] + 64;
		}
		draw_sprite(bsp_textures[i + num_textures][0], ftex, 0, 0);
	}
	
	struct wad_map_build_faces_t level;
	wad_read_map(file_wad, map_lumps, &level);
	
	BITMAP *db = create_bitmap_ex(8, get_config_int("gfx", "renderw", 320), get_config_int("gfx", "renderh", 200));
	
	//Calculates scale to fit whole level -------------------------------------
	float scale, scale_, dx, dy;
	int minx = INT_MAX, miny = INT_MAX, maxx = INT_MIN, maxy = INT_MIN;
	for(i = 0; i < level.num_vertices; i++){
		minx = MIN(minx, level.vertices[i].x);
		maxx = MAX(maxx, level.vertices[i].x);
		miny = MIN(miny, level.vertices[i].y);
		maxy = MAX(maxy, level.vertices[i].y);
	}
	scale = scale_ = ((float) db->w) / ((float) MAX(maxx - minx, maxy - miny));
	dx = -minx;
	dy = -miny;
	
	//Builds polygon geometry ------------------------------------------------
	wad_map_build_faces(&level);
	
	struct wad_wallface *lfit;
	
	//Parse cmd arguments ----------------------------------------------------
	int show_sectors = 0, exp_obj = 0, nopvs = 0;
	for(i = 0; i < argc; i++){
		if(!strcmp(argv[i], "-ss")){
			show_sectors = 1;
		}
		if(!strcmp(argv[i], "-obj")){
			exp_obj = 1;
		}
		if(!strcmp(argv[i], "-nopvs")){
			nopvs = 1;
		}
	}
	if(!exp_obj) goto afterobj;
	
	//-------------------- Export .OBJ and .MTL files, and textures -----------------------
	puts("Exporting obj");
	char filename[256] = "";
	char filedir[256] = "";
	strcpy(filedir, get_filename(wad_filename));
	*(strstr(filedir, ".")) = 0;
	strcat(filedir, "/");
	strcat(filename, filedir);
	strcat(filename, selected_map);
	strcat(filename, ".obj");
	
	wad_map_export_obj(filename, &level, textures, num_textures, 1.0/32.0);
	replace_extension(filename, filename, "mtl", 256);
	FILE *file_mtl = fopen(filename, "w");
	for(i = 0; i < num_textures; i++){
		fprintf(file_mtl,
			"newmtl %.8s\n"
			"Ka 1.0 1.0 1.0\n"
			"Ks 0.0 1.0 1.0\n"
			"map_Kd %s%.8s.pcx\n", textures[i]->name, filedir, textures[i]->name);
		
		memset(filename, 0, 256);
		strcat(filename, filedir);
		memcpy(filename + strlen(filename), textures[i]->name, 8);
		strcat(filename, ".pcx");
		if(!exists(filename)){
			save_bitmap(filename, bsp_textures[i][0], pals[0]);
		}
	}
	for(i = 0; i < num_flats; i++){
		fprintf(file_mtl,
			"newmtl %.8s\n"
			"Ka 1.0 1.0 1.0\n"
			"Ks 0.0 1.0 1.0\n"
			"map_Kd %s%.8s.pcx\n", flattexes[i].name, filedir, flattexes[i].name);
		
		memset(filename, 0, 256);
		strcat(filename, filedir);
		memcpy(filename + strlen(filename), flattexes[i].name, 8);
		strcat(filename, ".pcx");
		if(!exists(filename)){
			save_bitmap(filename, bsp_textures[i + num_textures][0], pals[0]);
		}
	}
	fclose(file_mtl);
	return 0;
	afterobj:
	// --------------------------------- Export OBJ end ----------------------------
	
	show_mouse(screen);
	
	clear_to_color(screen, 0);
	
	ZBUFFER *zb = create_zbuffer(db);
	set_zbuffer(zb);
	
	//Load sprite names ----------------------------------------------------------------
	arr_t(struct str8) spr_lump_names = {};
	struct wad_lump *sprlump;
	for(sprlump = lumps; strncmp(sprlump->name, "S_START", 8); sprlump++){}
	for(sprlump++; strncmp(sprlump->name, "S_END", 8); sprlump++){
		struct str8 n = {{0}, 0};
		memcpy(n.val, sprlump->name, 8);
		arr_push(&spr_lump_names, n);
		//printf("%.8s\n", n.val);
	}
	
	float zmult = 0.0, zadd = 0.0;
	int minz = INT_MAX, maxz = INT_MIN;
	for(i = 0; i < level.num_sectors; i++){
		if(level.sectors[i].h_floor < minz) minz = level.sectors[i].h_floor;
		if(level.sectors[i].h_ceil < minz) minz = level.sectors[i].h_ceil;
		if(level.sectors[i].h_floor > maxz) maxz = level.sectors[i].h_floor;
		if(level.sectors[i].h_ceil  > maxz) maxz = level.sectors[i].h_ceil;
	}
	//printf("%d %d\n", minz, maxz);
	zmult = -1;
	zadd = (maxz - minz);
	
	int mouse_b2 = 0;
	//goto secondloop;
	
	vec3 cpos;
	
	//Orthographic topdown view ----------------------------------------------------
	//Controls:
	//L mouse click - spawn at cursor position
	//R mouse click - spawn at cursor position, way up high, looking down, similarly to ortho view
	//M mouse click - spawn at singleplayer spawn
	//Mouse wheel   - zoom in/out
	//Dir keys      - move around
	//Esc           - quit
	firstloop:
	while(!key[KEY_ESC]){
		int mouse2_x = (mouse_x * db->w) / SCREEN_W;
		int mouse2_y = (mouse_y * db->h) / SCREEN_H;
		clear_to_color(db, 1);
		clear_zbuffer(zb, 0);
		vec3 zscl = {((db->w / 2) - mouse2_x) / ((float) db->w / 2), ((db->h / 2) - mouse2_y) / ((float) db->h / 2), 0};
		
		for(ffit = level.flatfaces.data; ffit < level.flatfaces.data + level.flatfaces.len; ffit++){
			float z = level.sectors[ffit->sector].h_floor;
			struct wad_sector sector = level.sectors[ffit->sector];
			BITMAP *bmp_tex = 0;
			for(j = 0; j < num_flats; j++){
				if(!strncmp(flattexes[j].name, sector.tex_floor, 8)){
					bmp_tex = bsp_textures[num_textures + j][0];
					break;
				}
			}
			char c = ffit->sector;
			c *= 31; c *= 31;
			c *= 31; c *= 31;
			c *= 31;
			
			for(i = 1; i < ffit->face.num_verts - 1; i++){
				V3D_f verts[3] = {
					{
						.x = (dx + ffit->face.verts[0].pos.x + z * zscl.x) * scale,
						.y = (dy + ffit->face.verts[0].pos.y + z * zscl.y) * scale,
						.u = ffit->face.verts[0].pos.x,
						.v = ffit->face.verts[0].pos.y,
						.z = z
					},{
						.x = (dx + ffit->face.verts[i].pos.x + z * zscl.x) * scale,
						.y = (dy + ffit->face.verts[i].pos.y + z * zscl.y) * scale,
						.u = ffit->face.verts[i].pos.x,
						.v = ffit->face.verts[i].pos.y,
						.z = z
					},{
						.x = (dx + ffit->face.verts[i + 1].pos.x + z * zscl.x) * scale,
						.y = (dy + ffit->face.verts[i + 1].pos.y + z * zscl.y) * scale,
						.u = ffit->face.verts[i + 1].pos.x,
						.v = ffit->face.verts[i + 1].pos.y,
						.z = z
					},
				};
				
				const float ax = verts[0].x - verts[1].x;
				const float bx = verts[0].x - verts[2].x;
				const float ay = verts[0].y - verts[1].y;
				const float by = verts[0].y - verts[2].y;
				//if(ax * by - ay * bx > 0) continue;
				//if(ax * by - ay * bx < 0) continue;
				
				for(j = 0; j < 3; j++){
					verts[j].c = 31 - (sector.light / 8);
					if(show_sectors) verts[j].c = c;
					
					verts[j].z *= zmult;
					verts[j].z += zadd;
				}
				
				if(bmp_tex){
					triangle3d_f(db, (!show_sectors ? POLYTYPE_ATEX_LIT : POLYTYPE_FLAT) | POLYTYPE_ZBUF, bmp_tex, verts, verts + 1, verts + 2);
				}
			}
		}
		linface_cmp_z = zscl;
		for(lfit = level.wallfaces.data; lfit < level.wallfaces.data + level.wallfaces.len; lfit++){
			rvface *face = &lfit->face;
			struct wad_sector sector = level.sectors[level.sidedefs[lfit->sidedef].sector];
			char c = level.sidedefs[lfit->sidedef].sector;
			c *= 31; c *= 31;
			c *= 31; c *= 31;
			c *= 31;
			
			for(i = 1; i < face->num_verts - 1; i++){
				V3D_f verts[3] = {
					{
						.x = (dx + face->verts[0].pos.x + face->verts[0].pos.z * zscl.x) * scale,
						.y = (dy + face->verts[0].pos.y + face->verts[0].pos.z * zscl.y) * scale,
						.u = face->verts[0].u,
						.v = face->verts[0].v,
						.c = 31 - (sector.light / 8),
						.z = face->verts[0].pos.z
					},{
						.x = (dx + face->verts[i].pos.x + face->verts[i].pos.z * zscl.x) * scale,
						.y = (dy + face->verts[i].pos.y + face->verts[i].pos.z * zscl.y) * scale,
						.u = face->verts[i].u,
						.v = face->verts[i].v,
						.c = 31 - (sector.light / 8),
						.z = face->verts[i].pos.z
					},{
						.x = (dx + face->verts[i + 1].pos.x + face->verts[i + 1].pos.z * zscl.x) * scale,
						.y = (dy + face->verts[i + 1].pos.y + face->verts[i + 1].pos.z * zscl.y) * scale,
						.u = face->verts[i + 1].u,
						.v = face->verts[i + 1].v,
						.c = 31 - (sector.light / 8),
						.z = face->verts[i + 1].pos.z
					},
				};
				
				const float ax = verts[0].x - verts[1].x;
				const float bx = verts[0].x - verts[2].x;
				const float ay = verts[0].y - verts[1].y;
				const float by = verts[0].y - verts[2].y;
				//if(ax * by - ay * bx > 0) continue;
				//if(ax * by - ay * bx < 0) continue;
				
				for(j = 0; j < 3; j++){
					if(show_sectors) verts[j].c = c;
					
					verts[j].z *= zmult;
					verts[j].z += zadd;
				}
				
				struct wad_texture *tex = 0;
				for(j = 0; j < num_textures; j++){
					if(!strncmp(lfit->tex, textures[j]->name, 8)){
						tex = textures[j];
						break;
					}
				}
				if(!tex) continue;
				BITMAP *bmp_tex = bsp_textures[j][0];
				for(j = 0; j < 3; j++){
					verts[j].u = (verts[j].u * bmp_tex->w) / tex->width;
					verts[j].v = (verts[j].v * bmp_tex->h) / tex->height;
				}
				triangle3d_f(db, POLYTYPE_ATEX_LIT | POLYTYPE_ZBUF, bmp_tex, verts, verts + 1, verts + 2);
			}
		}
		
		for(i = 0; i < level.num_things; i++){
			int tx = level.things[i].x;
			int ty = level.things[i].y;
			int x = (dx + tx) * scale;
			int y = (dy + ty) * scale;
			
			int sector_i = wad_map_sector_at(&level, level.things[i].x, level.things[i].y);
			int z = level.sectors[sector_i].h_floor;
			x += (z * zscl.x) * scale;
			y += (z * zscl.y) * scale;
			
			for(j = 0; j < NUMMOBJTYPES; j++){
				if(mobjinfo[j].doomednum == level.things[i].type) break;
			}
			
			int mobjn = j;
			int statenum = j < NUMMOBJTYPES ? mobjinfo[j].spawnstate : 0;
			int sprnum = states[statenum].sprite;
			char sprname[9];
			strcpy(sprname, sprnames[sprnum]);
			strcat(sprname, "A0");
			findlump2:
			for(j = 0; j < spr_lump_names.len; j++){
				if(!strncmp(spr_lump_names.data[j].val, sprname, 8)) break;
			}
			if(j == spr_lump_names.len){
				if(sprname[5] == '0'){
					sprname[5] = '1';
					goto findlump2;
				}
				textprintf_centre_ex(db, font, x, y - 4, makecol(255, 255, 255), -1, "%s", sprname);
				continue;
			}
			BITMAP *bmp;
			if(!spr_lump_names.data[j].bmp){
				spr_lump_names.data[j].bmp = wad_read_pic(file_wad, wad_dir_lump(&h, lumps, sprname));
			}
			bmp = spr_lump_names.data[j].bmp;
			
			int w = ceil(bmp->w * scale);
			int h = (mobjinfo[mobjn].height * scale) / (1<<16);
			//int h = ceil(bmp->h * scale);
			
			V3D_f verts[4] = {
				{ .x = x - w * 0.5, .y = y - h * 1.0, .u = 0, .v = 0, .z = z + h / scale},
				{ .x = x + w * 0.5, .y = y - h * 1.0, .u = 1, .v = 0, .z = z + h / scale},
				{ .x = x + w * 0.5, .y = y + h * 0.0, .u = 1, .v = 1, .z = z + h / scale},
				{ .x = x - w * 0.5, .y = y + h * 0.0, .u = 0, .v = 1, .z = z + h / scale},
			};
			V3D_f *vertsp[4] = {verts, verts + 1, verts + 2, verts + 3};
			
			char c = sector_i;
			c *= 31; c *= 31;
			c *= 31; c *= 31;
			c *= 31;
			
			for(j = 0; j < 4; j++){
				if(show_sectors) verts[j].c = c;
				verts[j].u *= bmp256->w;
				verts[j].v *= bmp256->h;
				
				verts[j].z *= zmult;
				verts[j].z += zadd;
			}
			
			clear_to_color(bmp256, 0);
			//stretch_sprite(bmp256, bmp, 0, 0, ceil(bmp->w * scale), ceil(bmp->h * scale));
			stretch_sprite(bmp256, bmp, 0, 0, bmp256->w, bmp256->h);
			
			//if(show_sectors){
			//	rectfill(bmp256, 0, 0, ceil(bmp->w * scale), ceil(bmp->h * scale), c);
			//	rect(bmp256, 0, 0, ceil(bmp->w * scale), ceil(bmp->h * scale), 1);
			//}
			
			//draw_sprite(db, bmp256, x - ceil(bmp->w * scale) / 2, y - ceil(bmp->h * scale) / 2);
			polygon3d_f(db, (!show_sectors ? POLYTYPE_ATEX_MASK : POLYTYPE_FLAT) | POLYTYPE_ZBUF, bmp256, 4, vertsp);
		}
		
		stretch_blit(db, screen, 0, 0, db->w, db->h, 0, 0, SCREEN_W, SCREEN_H);
		
		if(key[KEY_LEFT]) dx += 1.0/scale;
		if(key[KEY_RIGHT]) dx -= 1.0/scale;
		if(key[KEY_UP])   dy += 1.0/scale;
		if(key[KEY_DOWN]) dy -= 1.0/scale;
		
		if(scale_ * pow(1.1, mouse_z) != scale){
			dx = mouse2_x / (scale_ * pow(1.1, mouse_z)) - ((mouse2_x / scale) - dx);
			dy = mouse2_y / (scale_ * pow(1.1, mouse_z)) - ((mouse2_y / scale) - dy);
			scale = scale_ * pow(1.1, mouse_z);
		}
		
		if(mouse_b){
			//(dx + cpos.x) * scale = mouse2_x;
			//mouse2_x / scale = dx + cpos.x
			mouse_b2 = mouse_b;
			
			cpos.x = -dx + (mouse2_x / scale);
			cpos.y = -dy + (mouse2_y / scale);
			int sector_i = wad_map_sector_at(&level, cpos.x, cpos.y);
			cpos.z = (level.sectors[sector_i].h_floor + level.sectors[sector_i].h_ceil) * 0.5;
			goto secondloop;
		}
	}
	goto die;
	secondloop:
	
	show_mouse(0);
	
	//Build BSP ----------------------------------------------------------
	bspdata bsp;
	wad_create_bspdata(&level, &bsp, textures, num_textures, flattexes, num_flats);
	
	//Set up rendering options ---------------------------------------------
	kdr_active_zbuf = kdr_create_zbuf(db->w, db->h);
	draw_bsp_init();
	kdr_clear_zbuf(kdr_active_zbuf);
	raster_mipmap_levels = 0;
	raster_triangle_func = rf_kdr_triangle3d_f_persp8bpp;// + (get_color_depth() == 8);
	//raster_triangle_func = rf_triangle3d_f;
	draw_bsp_onleaf = 0;
	draw_bsp_onleaf_data = 0;
	set_projection_viewport(0, 0, db->w, db->h);
	kdr_bf_cull = 1;
	//draw_bsp_leafs = 1;
	
	//Build sprite sheet, optimised ----------------------------------------
	struct sprsheet sprite_sheet = {
		.w = 0,
		.h = 0,
		.num_frames = spr_lump_names.len,
		.frames = malloc(sizeof(struct sprsheet_frame) * spr_lump_names.len),
	};
	int srcw = 0, srch = 0;
	for(j = 0; j < spr_lump_names.len; j++){
		if(!spr_lump_names.data[j].bmp) spr_lump_names.data[j].bmp = wad_read_pic(file_wad, wad_dir_lump(&h, lumps, spr_lump_names.data[j].val));
		if(!spr_lump_names.data[j].bmp) continue;
		srcw += spr_lump_names.data[j].bmp->w;
		if(srch < spr_lump_names.data[j].bmp->h) srch = spr_lump_names.data[j].bmp->h;
	}
	sprite_sheet.src = create_bitmap_ex(8, srcw, srch);
	clear_to_color(sprite_sheet.src, 0);
	srcw = 0;
	for(j = 0; j < spr_lump_names.len; j++){
		struct wad_pic_header pic;
		wad_read_pic_header(file_wad, wad_dir_lump(&h, lumps, spr_lump_names.data[j].val), &pic);
		if(!spr_lump_names.data[j].bmp) continue;
		draw_sprite(sprite_sheet.src, spr_lump_names.data[j].bmp, srcw, 0);
		sprite_sheet.frames[j].x = srcw;
		sprite_sheet.frames[j].y = 0;
		sprite_sheet.frames[j].w = spr_lump_names.data[j].bmp->w;
		sprite_sheet.frames[j].h = spr_lump_names.data[j].bmp->h;
		sprite_sheet.frames[j].off_x = pic.offs_x;
		sprite_sheet.frames[j].off_y = pic.offs_y;
		srcw += spr_lump_names.data[j].bmp->w;
	}
	optimize_sprsheet(&sprite_sheet);
	if(!exists("sprites.pcx")) save_bitmap("sprites.pcx", bitmap_to32bpp(sprite_sheet.src, 0), 0);
	
	float sprscale = 1.0;
	float pitch = 0, yaw = 0;
	draw_bsp_node_hl = -1;
	
	//Position camera --------------------------------------------------
	if(mouse_b2 & 2){
		mouse_b2e2:
		pitch = -M_PI * 0.499;
		cpos.z = MAX((maxx - minx), (maxy-miny)) * 0.5;
	}
	else if(mouse_b2 & 4){
		for(i = 0; i < level.num_things; i++){
			if(level.things[i].type == 1) break;
		}
		if(i == level.num_things) goto mouse_b2e2;
		cpos.x = level.things[i].x;
		cpos.y = level.things[i].y;
		int sector_i = wad_map_sector_at(&level, cpos.x, cpos.y);
		cpos.z = level.sectors[sector_i].h_floor + 41;
		int a1 = 0, a2 = 0;
		get_mouse_mickeys(&a1, &a2);
		position_mouse(SCREEN_W / 2, SCREEN_H / 2);
	}
	
	//Build potential visibility set --------------------------------------
	if(!nopvs){
		//rdebug_set_rvfscl(0.9);
		time_t time0 = time(0);
		if(build_bsp_vis(&bsp)){}
		time_t time1 = time(0);
		printf("PVS built\nTook %ld seconds\n", time1 - time0);
		//bsp.pvs = 0;
	}
	
	#if 0
	for(i = 0; i < bsp.num_leafs; i++){
		vec3 midp;
		bsp_leafmidpoint(&bsp, i, &midp);
		rdebug_printf(&midp, "%d", i);
	}
	#endif
	
	#if 0
	for(i = 0; i < level.num_nodes; i++){
		struct wad_node *node = level.nodes + i;
		vec3 p1 = {node->x, node->y, (minz + maxz) / 2},
			p2 = {node->x + node->dx, node->y + node->dy, (minz + maxz) / 2};
		int sector_i;
		sector_i = wad_map_sector_at(&level, p1.x, p1.y);
		//p1.z = level.sectors[sector_i].h_floor;
		sector_i = wad_map_sector_at(&level, p2.x, p2.y);
		//p2.z = level.sectors[sector_i].h_floor;
		rdebug_arrow(&p1, &p2);
	}
	#endif
	
	float fov = 90.0 * (256.0 / 360.0);
	
	
	//First person 3D view ----------------------------------------------------
	//Controls:
	//Left/right    - turn
	//W/S           - move freely
	//Up/dow        - move with floor collision
	//A/D           - strafe
	//Mouse wheel   - change FOV
	//Dir keys      - move around
	//Esc           - quit
	//Shift         - run
	//N             - enter node number with +- 0123456789 backspace
	//+-            - change node number
	//0             - select child node 0
	//1             - select child node 1
	//P             - select parent node
	while(!key[KEY_ESC]){
		int old_draw_bsp_node_hl = draw_bsp_node_hl;
		if(key[KEY_N]){
			char str[5] = {'+', 0, 0, 0, 0};
			for(j = 1; j < 4; j++){
				for(i = 0; i < 10; i++){
					if(key[KEY_MINUS] || key[KEY_MINUS_PAD]) str[0] = '-';
					if(key[KEY_EQUALS] || key[KEY_PLUS_PAD]) str[0] = '+';
					if(key[KEY_BACKSPACE]) break;
					if(key[KEY_0 + i] || key[KEY_0_PAD + i]){
						str[j] = '0' + i;
						while(key[KEY_0 + i] || key[KEY_0_PAD + i]) {};
						break;
					}
				}
				if(key[KEY_BACKSPACE]) break;
				if(i == 10){
					j--;
				}
			}
			draw_bsp_node_hl = atoi(str);
		}
		
		if(key[KEY_MINUS] || key[KEY_MINUS_PAD]) draw_bsp_node_hl--;
		if(key[KEY_EQUALS] || key[KEY_PLUS_PAD]) draw_bsp_node_hl++;
		if(key[KEY_0] || key[KEY_0_PAD])
			if(draw_bsp_node_hl >= 0 && draw_bsp_node_hl < bsp.num_nodes)
				draw_bsp_node_hl = bsp.nodes[draw_bsp_node_hl].children[0];
		if(key[KEY_1] || key[KEY_1_PAD])
			if(draw_bsp_node_hl >= 0 && draw_bsp_node_hl < bsp.num_nodes)
				draw_bsp_node_hl = bsp.nodes[draw_bsp_node_hl].children[1];
		if(key[KEY_P]){
			for(i = 0; i < bsp.num_nodes; i++){
				if(bsp.nodes[i].children[0] == draw_bsp_node_hl ||
					bsp.nodes[i].children[1] == draw_bsp_node_hl) break;
			}
			if(i < bsp.num_nodes) draw_bsp_node_hl = i;
		}
		
		if(old_draw_bsp_node_hl != draw_bsp_node_hl){
			if(draw_bsp_node_hl >= 0 && draw_bsp_node_hl < bsp.num_nodes){
				rdebug_clear();
				
				const bspnode *node = &bsp.nodes[draw_bsp_node_hl];
				rvface f;
				mplane *plane = &bsp.planes[node->plane];
				
				rdebug_set_colour(makecol(0, 0, 255));
				
				rvface_boxslice(plane, &node->min, &node->max, &f);
				rdebug_rvface(&f);
				
				int node_i = draw_bsp_node_hl;
				if(node_i == 0){
					node_i = level.num_nodes - 1;
				}
				else node_i--;
				
				rvface_boxslice(&level.nodeplanes[node_i], &node->min, &node->max, &f);
				rdebug_rvface(&f);
				
				struct wad_node *wnode = &level.nodes[node_i];
				
				vec3
					lmin = {wnode->bbox_l[2], wnode->bbox_l[1], minz}, lmax = {wnode->bbox_l[3], wnode->bbox_l[0], maxz},
					rmin = {wnode->bbox_r[2], wnode->bbox_r[1], minz}, rmax = {wnode->bbox_r[3], wnode->bbox_r[0], maxz};
				
				rdebug_set_colour(makecol(255, 0, 0));
				RDEBUG_BOX_MM(lmin, lmax);
				RDEBUG_BOX_MM(rmin, rmax);
				
				//vec3 vr = get_nodei_point(&level, level.nodes[node_i].child_r);
				//vec3 vl = get_nodei_point(&level, level.nodes[node_i].child_l);
				//vec3_print(&vr);
				//vec3_print(&vl);
				//rdebug_string(&vr, "R");
				//rdebug_string(&vl, "L");
			}
			if(BSPNODE_ISLEAF(draw_bsp_node_hl)){
				const int leaf_i = BSPNODE_TO_LEAF(draw_bsp_node_hl);
				bspleaf *leaf = &bsp.leafs[leaf_i];
				//printf("%d %d %d\n", leaf_i, leaf->first_face, leaf->last_face);
				rdebug_clear();
				for(i = leaf->first_face; i <= leaf->last_face; i++){
					rdebug_rvface((rvface *)(bsp.faces + i));
				}
			}
		}
		
		while(key[KEY_MINUS] || key[KEY_MINUS_PAD] || key[KEY_EQUALS] || key[KEY_PLUS_PAD] || key[KEY_1] || key[KEY_1_PAD] || key[KEY_0] || key[KEY_0_PAD] || key[KEY_P]){};
		
		if(draw_bsp_node_hl == -1) raster_triangle_func = rf_kdr_triangle3d_f_persp8bpp;
		else raster_triangle_func = rf_triangle3d_f;
		
		kdr_clear_zbuf(kdr_active_zbuf);
		clear_to_color(db, 0);
		
		vec3 dir, dir2;
		dir.x = sin(yaw) * cos(pitch);
		dir.z = sin(pitch);
		dir.y = cos(yaw) * cos(pitch);
		
		dir2.x = -cos(yaw);
		dir2.z = 0;
		dir2.y = sin(yaw);
		
		MATRIX_f mat;
		
		int mdx, mdy;
		get_mouse_mickeys(&mdx, &mdy);
		position_mouse(SCREEN_W / 2, SCREEN_H / 2);
		
		pitch += -(mdy) * 0.01;
		yaw += (mdx) * 0.01;
		
		if(pitch < -M_PI * 0.499)	pitch = -M_PI * 0.499;
		if(pitch > M_PI * 0.499)	pitch = M_PI * 0.499;
		
		float spd = (key[KEY_LSHIFT] || key[KEY_RSHIFT]) ? 10.0 : 3.333;
		if(key[KEY_W]) vec3_maddto(&cpos, &dir, spd);
		if(key[KEY_S]) vec3_maddto(&cpos, &dir, -spd);
		if(key[KEY_A]) vec3_maddto(&cpos, &dir2, spd);
		if(key[KEY_D]) vec3_maddto(&cpos, &dir2, -spd);
		
		if(key[KEY_UP]){
			pitch = 0;
			vec3_maddto(&cpos, &dir, spd);
			int sector_i = wad_map_sector_at(&level, cpos.x, cpos.y);
			cpos.z = level.sectors[sector_i].h_floor + 41;
		}
		if(key[KEY_DOWN]){
			pitch = 0;
			vec3_maddto(&cpos, &dir, -spd);
			int sector_i = wad_map_sector_at(&level, cpos.x, cpos.y);
			cpos.z = level.sectors[sector_i].h_floor + 41;
		}
		if(key[KEY_LEFT]){
			pitch = 0;
			yaw -= 0.1;
		}
		if(key[KEY_RIGHT]){
			pitch = 0;
			yaw += 0.1;
		}
		
		fov = (90.0 * (256.0 / 360.0)) * pow(1.01, mouse_z);
		
		get_camera_matrix_f(
			&mat,
			cpos.x, cpos.y, cpos.z,
			dir.x, dir.y, dir.z,
			0, 0, 1, 
			fov,
			(1.0 * (float)db->w) / (1.0 * (float)db->h)
		);
		
		draw_bsp(
			&bsp,
			&mat,
			db,
			bsp_textures,
			POLYTYPE_PTEX_MASK,
			0,
			0
		);
		
		//Sprite rendering -----------------------------------------------------------
		for(i = 0; i < level.num_things; i++){
			if((level.things[i].flags & 4) == 0) continue;
			if((level.things[i].flags & 16) != 0) continue;
			
			int tx = level.things[i].x;
			int ty = level.things[i].y;
			
			fixed anglef = fixatan2(itofix(-ty + cpos.y), itofix(-tx + cpos.x));
			int anglei = fixtoi(anglef);
			int angle;
			
			switch(anglei){
				case -16	... 16:		angle = '1'; break;
				case 17		... 48:		angle = '2'; break;
				case 49		... 80:		angle = '3'; break;
				case 81		... 112:	angle = '4'; break;
				case 113	... 128:
				case -128	... -113:	angle = '5'; break;
				case -112	... -81:	angle = '6'; break;
				case -80	... -49:	angle = '7'; break;
				case -48	... -17:	angle = '8'; break;
				default:				angle = '0'; break;
			}
			
			int sector_i = wad_map_sector_at(&level, tx, ty);
			int tz = level.sectors[sector_i].h_floor;
			
			for(j = 0; j < NUMMOBJTYPES; j++){
				if(mobjinfo[j].doomednum == level.things[i].type) break;
			}
			int mobjn = j;
			int statenum = mobjn < NUMMOBJTYPES ? mobjinfo[mobjn].spawnstate : 0;
			int sprnum = states[statenum].sprite;
			char sprname[9];
			int frame = 'A';
			strcpy(sprname, sprnames[sprnum]);
			int flip = 0;
			for(j = 0; j < spr_lump_names.len; j++){
				if(strncmp(spr_lump_names.data[j].val, sprname, 4)) continue;
				if(spr_lump_names.data[j].val[4] == frame && spr_lump_names.data[j].val[5] == '0') break;
				if(spr_lump_names.data[j].val[4] == frame && spr_lump_names.data[j].val[5] == angle) break;
				if(!spr_lump_names.data[j].val[6]) continue;
				if(spr_lump_names.data[j].val[6] == frame && spr_lump_names.data[j].val[7] == angle){
					flip = 1;
					break;
				}
			}
			if(j == spr_lump_names.len) continue;
			int framenum = j;
			
			const MATRIX_f *camera = &mat;
			vec3 right =	{	camera->v[0][0],	camera->v[0][1],	camera->v[0][2]	};
			vec3 up =		{	camera->v[1][0],	camera->v[1][1],	camera->v[1][2]	};
			
			vec3_fnormalize(&right);
			vec3_fnormalize(&up);
			
			const int off_x = /*el.off_x */ sprite_sheet.frames[framenum].off_x;
			const int off_y = /*el.off_y */ sprite_sheet.frames[framenum].off_y;
			const int w = sprite_sheet.frames[framenum].w;
			const int h = sprite_sheet.frames[framenum].h;
			
			//tz += mobjinfo[mobjn].height;
			tz += 4;
			
			V3D_f verts[2] = {{
				.x = tx - (right.x * off_x + up.x * off_y) * sprscale,
				.y = ty - (right.y * off_x + up.y * off_y) * sprscale,
				.z = tz - (right.z * off_x + up.z * off_y) * sprscale,
			},{
				.x = tx + (right.x * (w - off_x) + up.x * (h - off_y)) * sprscale,
				.y = ty + (right.y * (w - off_x) + up.y * (h - off_y)) * sprscale,
				.z = tz + (right.z * (w - off_x) + up.z * (h - off_y)) * sprscale
			}};
			
			for(j = 0; j < 2; j++){
				apply_matrix_f(camera, verts[j].x, verts[j].y, verts[j].z, &verts[j].x, &verts[j].y, &verts[j].z);
				persp_project_f(verts[j].x, verts[j].y, verts[j].z, &verts[j].x, &verts[j].y);
			}
			
			kdr_sprite3d_f(db, 0, &sprite_sheet, framenum, verts, flip);
		}
		
		rdebug_draw(&mat, db);
		
		textprintf_ex(db, font, 0, 0, makecol(255, 255, 255), -1, "DBHL %+04d %+04d", draw_bsp_node_hl, BSPNODE_ISLEAF(draw_bsp_node_hl) ? BSPNODE_TO_LEAF(draw_bsp_node_hl) : -999);
		
		if(db->w == SCREEN_W && db->h == SCREEN_H){
			blit(db, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
		}
		else{
			stretch_blit(db, screen, 0, 0, db->w, db->h, 0, 0, SCREEN_W, SCREEN_H);
		}
		
		tics = 0;
		while(!tics){};
	};
	while(key[KEY_ESC]) continue;
	goto firstloop;
	
	die:
	allegro_exit();
	
	exit(1);
	
	return 0;
}
END_OF_MAIN()
