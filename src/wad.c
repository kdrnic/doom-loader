#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <math.h>
#include <locale.h>
#include <errno.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <assert.h>
#include <ctype.h>

#include <allegro.h>

#include "vector.h"
#include "wad.h"
#include "arr.h"

void wad_read_dir(FILE *f, const struct wad_header *h, struct wad_lump *lumps)
{
	int i;
	fseek(f, h->offs_lumps, SEEK_SET);
	for(i = 0; i < h->num_lumps; i++, lumps++){
		fread(lumps, 16, 1, f);
		lumps->name[8] = 0;
	}
}

struct wad_lump *wad_dir_lump(const struct wad_header *h, struct wad_lump *lumps, const char *name)
{
	int i;
	for(i = 0; i < h->num_lumps; i++, lumps++){
		if(!strncmp(name, lumps->name, 8)) return lumps;
	}
	char tmp[8];
	for(i = 0; i < h->num_lumps; i++, lumps++){
		memcpy(tmp, lumps->name, 8);
		for(char *c = tmp; *c && (c - tmp) < 8; c++){
			*c = toupper(*c);
		}
		if(!strncmp(name, tmp, 8)) return lumps;
	}
	return 0;
}

void wad_read_palettes(FILE *f, struct wad_lump *lump, PALETTE pals[14])
{
	char pal[256 * 3];
	int i, j;
	assert(lump->size == 256 * 3 * 14);
	fseek(f, lump->offs, SEEK_SET);
	for(i = 0; i < 14; i++){
		fread(pal, 1, 256 * 3, f);
		for(j = 0; j < 256; j++){
			pals[i][j].r = pal[j * 3] >> 2;
			pals[i][j].g = pal[j * 3 + 1] >> 2;
			pals[i][j].b = pal[j * 3 + 2] >> 2;
		}
	}
}

void wad_read_pic_header(FILE *f, struct wad_lump *lump, struct wad_pic_header *pic)
{
	fseek(f, lump->offs, SEEK_SET);
	
	uint8_t data[lump->size];
	
	fread(data, 1, lump->size, f);
	memcpy(pic, data, sizeof(*pic));
}

BITMAP *wad_read_pic(FILE *f, struct wad_lump *lump)
{
	BITMAP *bmp;
	
	fseek(f, lump->offs, SEEK_SET);
	
	uint8_t data[lump->size];
	struct wad_pic_header pic;
	
	//Replaces colour 0, as Doom has a different manner to handle transparency
	int col0 = makecol(getr(0), getg(0), getb(0));
	
	fread(data, 1, lump->size, f);
	memcpy(&pic, data, sizeof(pic));
	uint32_t offs_columns[pic.width];
	memcpy(offs_columns, data + 8, 4 * pic.width);
	
	bmp = create_bitmap_ex(8, pic.width, pic.height);
	clear_to_color(bmp, 0);
	
	int x, y;
	for(x = 0; x < pic.width; x++){
		uint8_t *post = data + offs_columns[x];
		while(1){
			int offs_y = *(post++);
			if(offs_y == 0xFF) break;
			int len = *(post++);
			
			post++;
			for(y = 0; y < len; y++){
				_putpixel(bmp, x, y + offs_y, *(post++) ?: col0);
			}
			
			post++;
		}
		
	}
	
	return bmp;
}

int wad_read_map_lumps(FILE *f, struct wad_lump *lump, struct wad_lump **maplumps)
{
	int i, j;
	lump++;
	for(i = 0; 1; i++, lump++){
		for(j = 0; j < WAD_MAP_LUMPS_LEN; j++){
			if(!strcmp(wad_map_lump_str[j], lump->name)) break;
		}
		if(j == WAD_MAP_LUMPS_LEN) break;
		maplumps[j] = lump;
	}
	return i;
}

int wad_read_flats(FILE *file_wad, struct wad_lump *lumps, struct wad_flat **flats_ptr)
{
	int i;
	struct wad_flat f;
	
	arr_t(struct wad_flat) flats = arr_empty;
	
	while(strncmp(lumps->name, "F_START", 8)) lumps++;
	for(; strncmp(lumps->name, "F_END", 8); lumps++){
		if(lumps->size != 4096) continue;
		memcpy(f.name, lumps->name, 8);
		fseek(file_wad, lumps->offs, SEEK_SET);
		fread(f.data, 1, 4096, file_wad);
		//flats[num_flats++] = f;
		arr_push(&flats, f);
	}
	*flats_ptr = flats.data;
	
	arr_unreserve(&flats);
	
	return flats.len;
}

void wad_read_colormap(FILE *file_wad, struct wad_lump *lump, COLOR_MAP *colormap)
{
	char data[34 * 256];
	fseek(file_wad, lump->offs, SEEK_SET);
	fread(data, 1, sizeof(data), file_wad);
	
	for(int i = 0; i < 34 * 256; i++){
		const int a = i % 256;
		const int b = i / 256;
		colormap->data[b][a] = data[i];
	}
}

void wad_read_textures(FILE *file_wad, struct wad_lump *lump, struct wad_texture ***textures_ptr, int *num_textures_ptr)
{
	if(!lump) return;
	
	int32_t num_textures;
	fseek(file_wad, lump->offs, SEEK_SET);
	fread(&num_textures, 4, 1, file_wad);
	int32_t *offsets = malloc(num_textures * sizeof(int32_t));
	fread(offsets, 4, num_textures, file_wad);
	
	struct wad_texture **textures = *textures_ptr;
	if(!textures || !*num_textures_ptr){
		textures = malloc(num_textures * sizeof(*textures));
		*textures_ptr = textures;
		*num_textures_ptr = num_textures;
	}
	else{
		textures = realloc(textures, ((*num_textures_ptr) + num_textures) * sizeof(*textures));
		*textures_ptr = textures;
		textures += *num_textures_ptr;
		*num_textures_ptr += num_textures;
	}
	int i;
	for(i = 0; i < num_textures; i++){
		textures[i] = malloc(0x16);
		fseek(file_wad, lump->offs, SEEK_SET);
		fseek(file_wad, offsets[i], SEEK_CUR);
		fread(textures[i], 0x16, 1, file_wad);
		int tex_size = 0x16 + 10 * textures[i]->num_patches;
		textures[i] = realloc(textures[i], tex_size);
		fseek(file_wad, lump->offs, SEEK_SET);
		fseek(file_wad, offsets[i], SEEK_CUR);
		fread(textures[i], tex_size, 1, file_wad);
		
		//printf("%03d %03d %.8s\n", i, &textures[i] - *textures_ptr, textures[i]->name);
	}
}

BITMAP *wad_texture_to_bitmap(struct wad_texture **textures, int num_textures, BITMAP **wall_patches, char *name)
{
	int i, j;
	BITMAP *bmp = 0;
	
	struct wad_texture *tex = 0;
	for(i = 0; i < num_textures; i++){
		if(!strncmp(name, textures[i]->name, 8)){
			tex = textures[i];
			break;
		}
	}
	if(!tex){
		return 0;
	}
	
	bmp = create_bitmap_ex(8, tex->width, tex->height);
	clear_to_color(bmp, 0);
	
	for(i = 0; i < tex->num_patches; i++){
		struct wad_patch *patch = tex->patches + i;
		BITMAP *patch_bmp;
		if((patch_bmp = wall_patches[patch->patch_num])){
			masked_blit(patch_bmp, bmp, 0, 0, patch->x, patch->y, patch_bmp->w, patch_bmp->h);
		}
	}
	return bmp;
}

const char *wad_map_lump_str[] =
{
    "THINGS",
    "LINEDEFS",
    "SIDEDEFS",
    "VERTEXES",
    "SEGS",
    "SSECTORS",
    "NODES",
    "SECTORS",
    "REJECT",
    "BLOCKMAP",
    "BEHAVIOR"
};

void wad_read_map(FILE *file_wad, struct wad_lump **maplumps, struct wad_map *map)
{
	int i = 0;
	
	#define RDLUMP(typ)\
	({\
		const struct wad_lump *lump = maplumps[i];\
		fseek(file_wad, lump->offs, SEEK_SET);\
		map->num_ ## typ = lump->size / sizeof(map->typ[0]);\
		map->typ = malloc(lump->size);\
		fread(map->typ, sizeof(map->typ[0]), map->num_ ## typ, file_wad);\
		i++;\
	})
	
    RDLUMP(things);
    RDLUMP(linedefs);
    RDLUMP(sidedefs);
    RDLUMP(vertices);
    RDLUMP(segs);
    RDLUMP(ssectors);
    RDLUMP(nodes);
    RDLUMP(sectors);
    //RDLUMP(reject);
    //RDLUMP(blockmap);
    //RDLUMP(behavior);
	
	#undef RDLUMP
}
