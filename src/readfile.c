#include <stdio.h>
#include <stdlib.h>

#include "readfile.h"

long read_file(const char *filename, char **ptxt, char binary)
{
	char *txt = 0;
	size_t size2 = -6;
	long size = -1;
	FILE *f = fopen(filename, binary ? "rb" : "r");
	if(f != 0){
		if(fseek(f, 0L, SEEK_END) == 0){
			size = ftell(f);
			if(size < 0) return -3;
			txt = malloc(size + 1);
			if(!txt) return -4;
			if(fseek(f, 0L, SEEK_SET) != 0) return -1;
			size2 = fread(txt, 1, size, f);
			if(!size2) return -5;
			txt[size2] = '\0';
			*ptxt = txt;
		}
		fclose(f);
	}
	else return -2;
	return size2;
}
