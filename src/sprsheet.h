struct sprsheet_frame
{
	unsigned short int
		x, y,
		w, h,
		off_x, off_y,
		duration;
};

struct sprsheet
{
	struct BITMAP *src;
	unsigned short int w, h;
	unsigned short int num_frames;
	struct sprsheet_frame *frames;
};

enum{
	LOAD_SPRSHEET_JSON_ERR_READFILE = 1,
	LOAD_SPRSHEET_JSON_ERR_BMPNOTFOUND,
	LOAD_SPRSHEET_JSON_ERR_LOADBITMAP,
	LOAD_SPRSHEET_JSON_ERR_JSONPARSE,
	LOAD_SPRSHEET_JSON_ERR_OBJEXPECTED,
	LOAD_SPRSHEET_JSON_ERR_PROPERTY,
	LOAD_SPRSHEET_JSON_ERR_ROTATED,
	LOAD_SPRSHEET_JSON_ERR_STREXPECTED,
	LOAD_SPRSHEET_JSON_ERR_PROPERTYFRAME,
	LOAD_SPRSHEET_JSON_ERR_PROPERTYSRCSIZE,
	LOAD_SPRSHEET_JSON_ERR_PROPERTYSPRSRCSIZE,
	LOAD_SPRSHEET_JSON_ERR_NOTFRAMES,
	LOAD_SPRSHEET_JSON_ERR_MULTIWIDTH,
	LOAD_SPRSHEET_JSON_ERR_JSONNOTFOUND,
};

int load_sprsheet_json_ex(struct sprsheet *sheet, const char *fn);
struct sprsheet *load_sprsheet_json(const char *fn);
void optimize_sprsheet(struct sprsheet *sheet);
void destroy_sprsheet(struct sprsheet *sheet);
