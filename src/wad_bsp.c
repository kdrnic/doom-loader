#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <math.h>
#include <locale.h>
#include <errno.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <assert.h>
#include <limits.h>

#include "vector.h"
#include "bsp.h"
#include "arr.h"
#include "wad.h"
#include "rdebug.h"

static vec3 get_nodei_point(struct wad_map_build_faces_t *level, int node_i)
{
	if(node_i < 0){
		int ssector_i = 32768 + node_i;
		//struct wad_ssector *ssector = level->ssectors + ssector_i;
		//struct wad_seg *seg = level->segs + ssector->first_seg;
		//struct wad_vertex *start = level->vertices + seg->start;
		//struct wad_vertex *end = level->vertices + seg->end;
		//p.x = (start->x + end->x) * 0.5;
		//p.y = (start->y + end->y) * 0.5;
		//p.z = 0;
		struct wad_flatface *ff;
		for(ff = level->flatfaces.data; ff < level->flatfaces.data + level->flatfaces.len; ff++){
			if(ff->ssector == ssector_i) break;
		}
		rvert pr;
		rvface_midpoint_area(&ff->face, &pr, 0);
		return pr.pos;
	}
	else return get_nodei_point(level, level->nodes[node_i].child_r);
}

void wad_create_bspdata(struct wad_map_build_faces_t *level, bspdata *bsp, struct wad_texture **textures, int num_textures, struct wad_flat *flats, int num_flats)
{
	memset(bsp, 0, sizeof(*bsp));
	
	bsp->sky_material = -1;
	
	int i, j, k;
	bsp->faces = malloc(
		sizeof(*bsp->faces) * (level->flatfaces.len + level->wallfaces.len)
	);
	bsp->num_faces = 0;
	bsp->num_leafs = level->num_ssectors;
	bsp->leafs = malloc(sizeof(*bsp->leafs) * level->num_ssectors);
	for(i = 0; i < level->num_ssectors; i++){
		bspface f;
		bspleaf leaf;
		
		leaf.first_face = bsp->num_faces;
		leaf.last_face = bsp->num_faces - 1;
		
		for(struct wad_wallface *wf = level->wallfaces.data; wf < level->wallfaces.data + level->wallfaces.len; wf++){
			if(wf->ssector != i) continue;
			
			memcpy(f.verts, wf->face.verts, sizeof(*f.verts) * wf->face.num_verts);
			f.num_verts = wf->face.num_verts;
			
			for(j = 0; j < num_textures; j++){
				if(!strncmp(textures[j]->name, wf->tex, 8)) break;
			}
			if(j == num_textures){
				printf("Not found texture: %.8s\n", wf->tex);
				rdebug_rvface(&wf->face);
				j = 0;
			}
			f.material = j;
			for(j = 0; j < f.num_verts; j++){
				f.verts[j].u *= 1.0 / textures[f.material]->width;
				f.verts[j].v *= 1.0 / textures[f.material]->height;
			}
			
			f.type = wf->type;
			
			leaf.last_face++;
			bsp->faces[bsp->num_faces++] = f;
		}
		for(struct wad_flatface *ff = level->flatfaces.data; ff < level->flatfaces.data + level->flatfaces.len; ff++){
			if(ff->ssector != i) continue;
			
			memcpy(f.verts, ff->face.verts, sizeof(*f.verts) * ff->face.num_verts);
			f.num_verts = ff->face.num_verts;
			
			for(j = 0; j < num_flats; j++){
				if(!strncmp(flats[j].name, ff->tex, 8)) break;
			}
			if(j == num_flats){
				printf("Not found flat %.8s\n", ff->tex);
				rdebug_rvface(&ff->face);
				j = 0;
			}
			f.material = j + num_textures;
			
			if(!strncmp(flats[j].name, "F_SKY", 5)){
				for(j = 0; j < num_textures; j++){
					if(!strncmp(textures[j]->name, ff->tex + 2, 6)) break;
				}
				f.material = j;
				if(j == num_textures){
					printf("Not found texture: %.8s\n", ff->tex);
					rdebug_rvface(&ff->face);
					j = 0;
				}
				for(j = 0; j < f.num_verts; j++){
					f.verts[j].u *= 1.0 / textures[f.material]->width;
					f.verts[j].v *= 1.0 / textures[f.material]->height;
				}
				
				bsp->sky_material = f.material;
			}
			else{
				for(j = 0; j < f.num_verts; j++){
					f.verts[j].u *= 1.0 / 64.0;
					f.verts[j].v *= 1.0 / 64.0;
				}
			}
			
			f.type = ff->type;
			
			leaf.last_face++;
			bsp->faces[bsp->num_faces++] = f;
		}
		
		//assert(leaf.last_face >= leaf.first_face);
		
		bsp->leafs[i] = leaf;
	}
	
	bsp->nodes = malloc(sizeof(*bsp->nodes) * level->num_nodes);
	bsp->planes = malloc(sizeof(*bsp->planes) * level->num_nodes);
	bsp->num_nodes = level->num_nodes;
	int minc = INT_MAX, maxc = INT_MIN;
	for(i = 0; i < level->num_nodes; i++){
		mplane p;
		bspnode n;
		
		struct wad_node *node = level->nodes + i;
		#ifndef USE_NODEPLANES
		const vec3 linev[3] = {
			{node->x, node->y, 0},
			{node->x + node->dx, node->y + node->dy, 0},
			{node->x, node->y, 100},
		};
		vec3_verts2plane(linev, linev + 1, linev + 2, &p);
		#else
		p = level->nodeplanes[i];
		#endif
		
		vec3 pr = get_nodei_point(level, node->child_r);
		vec3 pl = get_nodei_point(level, node->child_l);
		int cr = vec3_planecmp(&p, &pr);
		int cl = vec3_planecmp(&p, &pl);
		
		//assert(cr != cl);
		//assert(cr == 1);
		//assert(cl == 0);
		
		n.children[0] = node->child_r;
		n.children[1] = node->child_l;
		//printf("%d %d ", n.children[0], n.children[1]);
		for(j = 0; j < 2; j++){
			int *child = n.children + j;
			if(*child >= 0) (*child)++;
			else{
				*child = BSPNODE_TO_LEAF(32768 + *child);
				
				if(*child < minc) minc = *child;
				if(*child > maxc) maxc = *child;
			}
		}
		//printf("%d %d %d %d\n", n.children[0], n.children[1], node->child_r, node->child_l);
		
		//if((cr == cl) || (cr != 1) || (cr != 0)){
		//	printf("wad_bsp ass failure: cr %d cl %d child[0] %d child[1] %d\n", cr, cl, n.children[0], n.children[1]);
		//}
		
		const int idx = (i == level->num_nodes - 1) ? 0 : i + 1;
		n.plane = idx;
		bsp->planes[idx] = p;
		bsp->nodes[idx] = n;
	}
	
	//Collapse empty leafs
	for(i = 0; i < bsp->num_nodes; i++){
		bspnode *node = bsp->nodes + i;
		bspnode *parent = 0;
		int parent_child = -1;
		for(j = 0; j < bsp->num_nodes; j++){
			parent = bsp->nodes + j;
			for(k = 0; k < 2; k++){
				if(parent->children[k] == i){
					parent_child = k;
					break;
				}
			}
			if(k < 2) break;
		}
		if(j == bsp->num_nodes) continue;
		
		int empty_child = -1;
		for(j = 0; j < 2; j++){
			if(BSPNODE_ISLEAF(node->children[j])){
				const int leaf_i = BSPNODE_TO_LEAF(node->children[j]);
				const bspleaf *leaf = bsp->leafs + leaf_i;
				if(leaf->last_face < leaf->first_face){
					empty_child = j;
					break;
				}
			}
		}
		if(j == 2) continue;
		
		const int other_child = !empty_child;
		parent->children[parent_child] = node->children[other_child];
		
		printf("Node collapsed: %d, parent: %d, leaf: %d\n", i, parent - bsp->nodes, node->children[empty_child]);
	}
	
	//printf("minc %d maxc %d (%d %d)\n", minc, maxc, BSPNODE_TO_LEAF(minc), BSPNODE_TO_LEAF(maxc));
	
	bsp_calcbounds(bsp, 0, 0, 0);
}
