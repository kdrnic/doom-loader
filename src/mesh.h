//Static mesh stuff

#ifndef MESH_H
#define MESH_H

#include "vector.h"

#define MESH_MAX_FACE_VERTS 30

struct MATRIX_f;
struct BITMAP;

typedef unsigned int mesh_face[MESH_MAX_FACE_VERTS];
typedef char mesh_texture[256];

typedef struct dedup_verts_data
{
	mesh_face *uv_faces, *pos_faces;
	int num_uv, num_pos;
	int num_faces;
	int *face_numverts;
	struct rvert *uv_verts, *pos_verts;
} dedup_verts_data;

struct MATRIX_f;
struct hashtable;
typedef struct mesh
{
	char (*sub_names)[256];
	int (*sub_faces)[2];
	struct MATRIX_f *sub_matrices;
	struct rvert *verts;
	mesh_texture *textures;
	mesh_texture *materials;
	int *texids;
	int num_verts, num_faces, num_textures, num_subs;
	mesh_face *faces;
	int *face_numverts;
	struct hashtable *sub_props;
	unsigned int crc32;
} mesh;

unsigned int dedup_verts(dedup_verts_data d, mesh_face *out_faces, struct rvert **out_verts);
int mesh_pickface(mesh *m, struct vec3 *start, struct vec3 *dir, struct vec3 *res);
void destroy_mesh(mesh *m);

#endif
