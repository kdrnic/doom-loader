#include <allegro.h>

#include "rvf_draw.h"
#include "compat.h"

void rvface_al_clip(const rvface *in, rvface *out, int shader, float znear, float zfar)
{
	V3D_f tmp[RVFACE_MAX_VERTS], tmp2[RVFACE_MAX_VERTS * 2], tmp3[RVFACE_MAX_VERTS * 2];
	V3D_f *tmpp[RVFACE_MAX_VERTS], *tmp2p[RVFACE_MAX_VERTS * 2], *tmp3p[RVFACE_MAX_VERTS * 2];
	int itmp[RVFACE_MAX_VERTS * 2];
	int i, vc;
	
	for(i = 0; i < RVFACE_MAX_VERTS * 2; i++){
		tmp2p[i] = &tmp2[i];
		tmp3p[i] = &tmp3[i];
		itmp[i] = 0;
		if(i < RVFACE_MAX_VERTS){
			tmpp[i] = &tmp[i];
		}
	}
	
	for(i = 0; i < in->num_verts; i++){
		tmp[i].x = in->verts[i].pos.x;
		tmp[i].y = in->verts[i].pos.y;
		tmp[i].z = in->verts[i].pos.z;
		tmp[i].u = in->verts[i].u;
		tmp[i].v = in->verts[i].v;
	}
	
	vc = clip3d_f(
		shader,
		znear,
		zfar,
		in->num_verts,
		(AL_CLIP3D_CONST V3D_f **) tmpp,
		tmp2p,
		tmp3p,
		itmp
	);
	
	if(vc > RVFACE_MAX_VERTS){
		out->num_verts = 0;
		return;
	}
	out->num_verts = vc;
	
	for(i = 0; i < vc; i++){
		out->verts[i].pos.x = tmp2[i].x;
		out->verts[i].pos.y = tmp2[i].y;
		out->verts[i].pos.z = tmp2[i].z;
		out->verts[i].u = tmp2[i].u;
		out->verts[i].v = tmp2[i].v;
	}
}

void rvface_al_persp(rvface *f)
{
	int i;
	float x, y;
	
	for(i = 0; i < f->num_verts; i++){
		persp_project_f(
			f->verts[i].pos.x,
			f->verts[i].pos.y,
			f->verts[i].pos.z,
			&x,
			&y
		);
		f->verts[i].pos.x = x;
		f->verts[i].pos.y = y;
	}
}

void rvface_al_matrix(rvface *f, const struct MATRIX_f *m)
{
	int i;
	for(i = 0; i < f->num_verts; i++){
		float x = f->verts[i].pos.x;
		float y = f->verts[i].pos.y;
		float z = f->verts[i].pos.z;
		apply_matrix_f(m, x, y, z, &x, &y, &z);
		f->verts[i].pos.x = x;
		f->verts[i].pos.y = y;
		f->verts[i].pos.z = z;
	}
}

void rvface_al_draw(BITMAP *bmp, int shader, BITMAP *tex, rvface *face)
{
	rvface tmp;
	V3D_f tmpv[RVFACE_MAX_VERTS];
	V3D_f *tmpvp[RVFACE_MAX_VERTS];
	int i;
	
	rvface_al_clip(face, &tmp, shader, 0.1, 0.1);
	rvface_al_persp(&tmp);
	
	for(i = 0; i < face->num_verts; i++){
		tmpv[i].x = face->verts[i].pos.x;
		tmpv[i].y = face->verts[i].pos.y;
		tmpv[i].z = face->verts[i].pos.z;
		tmpv[i].u = face->verts[i].u;
		tmpv[i].v = face->verts[i].v;
		tmpvp[i] = &tmpv[i];
	}
	
	polygon3d_f(bmp, shader, tex, face->num_verts, tmpvp);
}