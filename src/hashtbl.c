#include <string.h>
#include <assert.h>
#include <stdlib.h>

#include "prime.h"
#include "hashtbl.h"
#include "kdrlib.h"

uint32_t fnv1_hash_str(const char *str)
{
	#define FNV_PRIME_32 16777619
	#define FNV_OFFSET_32 2166136261U
	
	uint32_t hash = FNV_OFFSET_32;
	int c;
	while((c = *str++)){
		hash ^= c;
		hash *= FNV_PRIME_32;
	}
	return hash;
	
	#undef FNV_OFFSET_32
	#undef FNV_PRIME_32
}

uint32_t djb2_hash_str(const char *str)
{
	uint32_t hash = 5381;
	int c;
	
	while((c = *str++)) hash = ((hash << 5) + hash) + c;
	
	return hash;
}

void ht_init(hashtable *h, int cap)
{
	h->len = 0;
	h->siz = get_prime_gt(cap);
	h->table = calloc(h->siz, sizeof(*h->table));
	h->hash = djb2_hash_str;
}

//Destroys a hash table
void ht_destroy(hashtable *h)
{
	int i;
	
	for(i = 0; i < h->siz; i++){
		if(h->table[i].key && h->table[i].key != (char *) h->table){
			free(h->table[i].key);
		}
	}
	
	free(h->table);
}

//Grows the hashtable to new capacity
void ht_resize(hashtable *h, int new_cap)
{
	int j;
	hashtable newh;
	ht_init(&newh, new_cap);
	
	for(j = 0; j < h->siz; j++){
		int i;
		const int i0 = h->table[j].hash % newh.siz;
		
		if(!h->table[j].key) continue;
		if(h->table[j].key == (char *) h->table) continue;
		
		i = i0;
		do {
			if(!newh.table[i].key) break;
			i = (i + 1) % newh.siz;
		} while(i != i0);
		newh.table[i] = h->table[j];
	}
	
	free(h->table);
	h->table = newh.table;
	h->siz = newh.siz;
}

//Searches for key, returns value or 0 if not found
void *ht_search(const hashtable *h, const char *k)
{
	assert(h->hash);
	assert(h->siz);
	
	int i;
	const int i0 = h->hash(k) % h->siz;
	
	i = i0;
	do {
		if(!h->table[i].key) break;
		if(h->table[i].key != (char *) h->table){
			if(!strcmp(h->table[i].key, k)) return h->table[i].val;
		}
		i = (i + 1) % h->siz;
	} while(i != i0);
	
	return 0;
}

//Insert key-val pair, grows table if necessary; if key already in, overwrites
void ht_insert(hashtable *h, const char *k, void *v)
{
	assert(h->hash);
	assert(h->siz);
	
	int i;
	const uint32_t hash = h->hash(k);
	const int i0 = hash % h->siz;
	
	i = i0;
	do {
		if(!h->table[i].key) break;
		if(h->table[i].key == (char *) h->table) break;
		if(!strcmp(h->table[i].key, k)){
			h->table[i].val = v;
			return;
		}
		
		i = (i + 1) % h->siz;
	} while(i != i0);
	
	if(h->table[i].key && h->table[i].key != (char *) h->table){
		ht_resize(h, h->siz * 2);
		ht_insert(h, k, v);
		return;
	}
	
	h->table[i].key = kdr_strdup(k);
	h->table[i].val = v;
	h->table[i].hash = hash;
	h->len++;
	
	if(h->len * 100 > h->siz * HASHTABLE_LOAD_HI){
		ht_resize(h, h->siz * 2);
	}
}

//Deletes a key; may resize table down
void ht_delete(hashtable *h, const char *k)
{
	assert(h->hash);
	assert(h->siz);
	
	int i;
	const int i0 = h->hash(k) % h->siz;
	
	i = i0;
	do {
		if(!h->table[i].key) return;
		if(h->table[i].key != (char *) h->table){
			if(!strcmp(h->table[i].key, k)){
				free(h->table[i].key);
				h->table[i].key = (char *) h->table;
				h->len--;
				return;
			}
		}
		i = (i + 1) % h->siz;
	} while(i != i0);
	
	if(h->siz > 53 && h->len * 100 < h->siz * HASHTABLE_LOAD_LO){
		ht_resize(h, h->siz / 2);
	}
}

//Searches for key, returns value or 0 if not found
//k must be aligned
void *ht_search_aligned(const hashtable *h, const char *k)
{
	assert(h->hash);
	assert(h->siz);
	
	int i;
	const int i0 = h->hash(k) % h->siz;
	
	i = i0;
	do {
		if(!h->table[i].key) break;
		if(h->table[i].key != (char *) h->table){
			if(!kdr_strcmp_aligned(h->table[i].key, k)) return h->table[i].val;
		}
		i = (i + 1) % h->siz;
	} while(i != i0);
	
	return 0;
}

//Insert key-val pair, grows table if necessary; if key already in, overwrites
//k must be aligned
void ht_insert_aligned(hashtable *h, const char *k, void *v)
{
	assert(h->hash);
	assert(h->siz);
	
	int i;
	const uint32_t hash = h->hash(k);
	const int i0 = hash % h->siz;
	
	i = i0;
	do {
		if(!h->table[i].key) break;
		if(h->table[i].key == (char *) h->table) break;
		if(!kdr_strcmp_aligned(h->table[i].key, k)){
			h->table[i].val = v;
			return;
		}
		
		i = (i + 1) % h->siz;
	} while(i != i0);
	
	if(h->table[i].key && h->table[i].key != (char *) h->table){
		ht_resize(h, h->siz * 2);
		ht_insert(h, k, v);
		return;
	}
	
	h->table[i].key = kdr_strdup_aligned(k);
	h->table[i].val = v;
	h->table[i].hash = hash;
	h->len++;
	
	if(h->len * 100 > h->siz * HASHTABLE_LOAD_HI){
		ht_resize(h, h->siz * 2);
	}
}

//Deletes a key; may resize table down
//k must be aligned
void ht_delete_aligned(hashtable *h, const char *k)
{
	assert(h->hash);
	assert(h->siz);
	
	int i;
	const int i0 = h->hash(k) % h->siz;
	
	i = i0;
	do {
		if(!h->table[i].key) return;
		if(h->table[i].key != (char *) h->table){
			if(!kdr_strcmp_aligned(h->table[i].key, k)){
				free(h->table[i].key);
				h->table[i].key = (char *) h->table;
				h->len--;
				return;
			}
		}
		i = (i + 1) % h->siz;
	} while(i != i0);
	
	if(h->siz > 53 && h->len * 100 < h->siz * HASHTABLE_LOAD_LO){
		ht_resize(h, h->siz / 2);
	}
}
