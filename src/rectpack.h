struct rp_rect
{
	int x, y, w, h;
	void *data;
};

void rectpack(struct rp_rect *boxes_in, struct rp_rect *boxes_out, int num_boxes, int *w, int *h);
