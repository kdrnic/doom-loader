#ifndef WAD_H
#define WAD_H

#include <inttypes.h>

struct wad_header
{
	char type[4];
	int32_t num_lumps;
	int32_t offs_lumps;
};

struct wad_lump
{
	int32_t offs;
	int32_t size;
	char name[9];
};

struct wad_pic_header
{
	uint16_t width;
	uint16_t height;
	int16_t offs_x;
	int16_t offs_y;
};

struct wad_thing
{
	int16_t x;
	int16_t y;
	int16_t angle;
	int16_t type;
	int16_t flags;
};

enum wad_map_lump
{
    WAD_MAP_LUMP_THINGS = 0,
    WAD_MAP_LUMP_LINEDEFS,
    WAD_MAP_LUMP_SIDEDEFS,
    WAD_MAP_LUMP_VERTEXES,
    WAD_MAP_LUMP_SEGS,
    WAD_MAP_LUMP_SSECTORS,
    WAD_MAP_LUMP_NODES,
    WAD_MAP_LUMP_SECTORS,
    WAD_MAP_LUMP_REJECT,
    WAD_MAP_LUMP_BLOCKMAP,
    WAD_MAP_LUMP_BEHAVIOR,
	WAD_MAP_LUMPS_LEN
};

extern const char *wad_map_lump_str[];

struct wad_vertex
{
	int16_t x, y;
};

struct wad_linedef
{
	int16_t
		start,
		end,
		flags, //Such as ML_BLOCKING
		type,
		tag,
		sidedef_r,
		sidedef_l;
};

struct wad_seg
{
	int16_t
		start,
		end,
		angle,
		linedef,
		dir,
		offs;
};

struct wad_node
{
	int16_t
		x, y, dx, dy,
		bbox_r[4], bbox_l[4],
		child_r, child_l;
};

struct wad_sector
{
	int16_t
		h_floor,
		h_ceil;
	char
		tex_floor[8],
		tex_ceil[8];
	int16_t
		light,
		type,
		tag;
};

struct wad_ssector
{
	int16_t
		num_segs,
		first_seg;
};

struct wad_sidedef
{
	int16_t
		offs_u, offs_v;
	char tex_upper[8], tex_lower[8], tex_mid[8];
	int16_t sector;
};

struct wad_map
{
	struct wad_node *nodes;
	struct wad_seg *segs;
	struct wad_sector *sectors;
	struct wad_ssector *ssectors;
	struct wad_vertex *vertices;
	struct wad_linedef *linedefs;
	struct wad_sidedef *sidedefs;
	struct wad_thing *things;
	int
		num_nodes,
		num_segs,
		num_sectors,
		num_ssectors,
		num_vertices,
		num_linedefs,
		num_sidedefs,
		num_things;
};

struct wad_patch
{
	int16_t x, y;
	int16_t patch_num;
	int16_t unused1, unused2;
};

struct wad_texture
{
	char name[8];
	int32_t masked;
	int16_t width, height;
	int32_t foo;
	int16_t num_patches;
	struct wad_patch patches[];
};

struct wad_flat
{
	char name[8];
	char data[4096];
};

#ifdef VECTOR_H

struct wad_flatface
{
	rvface face;
	int sector;
	int ssector;
	int type;
	char tex[8];
};

struct wad_wallface
{
	rvface face;
	int ssector;
	int sidedef;
	int type;
	char tex[8];
};

#ifdef ARR_H

struct wad_map_build_faces_t
{
	struct wad_map;
	arr_t(struct wad_flatface) flatfaces;
	arr_t(struct wad_wallface) wallfaces;
	mplane *nodeplanes;
	int error;
};

#endif

#define USE_NODEPLANES

#endif

#define WAD_BSPFACE_FLOOR 0
#define WAD_BSPFACE_CEIL 1
#define WAD_BSPFACE_UPPER 2
#define WAD_BSPFACE_LOWER 3
#define WAD_BSPFACE_MIDDLE 4

#ifdef EOF

#ifdef ALLEGRO_VERSION
void wad_read_palettes(FILE *f, struct wad_lump *lump, PALETTE pals[14]);
BITMAP *wad_read_pic(FILE *f, struct wad_lump *lump);
BITMAP *wad_texture_to_bitmap(struct wad_texture **textures, int num_textures, BITMAP **wall_patches, char *name);
void wad_read_colormap(FILE *file_wad, struct wad_lump *lump, COLOR_MAP *colormap);
MIDI *wad_read_midi(FILE *file_wad, struct wad_lump *lump);
#endif

void wad_read_pic_header(FILE *f, struct wad_lump *lump, struct wad_pic_header *pic);
void wad_read_dir(FILE *f, const struct wad_header *h, struct wad_lump *lumps);
int wad_read_map_lumps(FILE *f, struct wad_lump *lump, struct wad_lump **maplumps);
int wad_read_flats(FILE *file_wad, struct wad_lump *lumps, struct wad_flat **flats);
void wad_read_textures(FILE *file_wad, struct wad_lump *lump, struct wad_texture ***textures_ptr, int *num_textures_ptr);
void wad_read_map(FILE *f, struct wad_lump **maplumps, struct wad_map *map);

#endif

struct wad_lump *wad_dir_lump(const struct wad_header *h, struct wad_lump *lumps, const char *name);

#ifdef VECTOR_H
#ifdef ARR_H
void wad_map_build_faces(struct wad_map_build_faces_t *bsp);
void wad_map_export_obj(const char *filename, struct wad_map_build_faces_t *bsp, struct wad_texture **textures, int num_textures, const float objscale);

#ifdef BSP_H
void wad_create_bspdata(struct wad_map_build_faces_t *level, bspdata *bsp, struct wad_texture **textures, int num_textures, struct wad_flat *flats, int num_flats);
#endif

#endif
#endif

int wad_map_sector_at(struct wad_map *bsp, int x, int y);

#endif
