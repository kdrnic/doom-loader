//Stuff for crossplatform compatibility

#ifndef COMPAT_H
#define COMPAT_H

#if __GNUC__ < 3

#define AL_FILENAME_CAST(a) ((char *)(a))
#define AL_FILENAME_CONST
#define AL_CLIP3D_CONST
#define AL_END_OF_MAIN

#define POLYTYPE_ZBUF 0
#define KEY_MAX 127

typedef unsigned int uintptr_t;

#define textprintf_ex(b, f, x, y, c, bg, fmt, ARGS...)\
if(1){ text_mode(bg); textprintf(b, f, x, y, c, fmt, ##ARGS); }

#define FLEX_MEMBER_ARR_LEN 0

#else
	
#define AL_FILENAME_CAST(a) (a)
#define AL_FILENAME_CONST const
#define AL_CLIP3D_CONST const
#define AL_END_OF_MAIN END_OF_MAIN()
#define FLEX_MEMBER_ARR_LEN

#endif

/*
from DJGPP's math.h:
extern float       __dj_nan;
#define NAN        __dj_nan
*/
#ifdef __DJGPP__
#undef NAN
#define NAN 0.0/0.0
#endif

#endif
