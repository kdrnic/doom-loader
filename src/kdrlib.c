#include <string.h>
#include <assert.h>
#include <stdlib.h>

#include "kdrlib.h"

#define TOALIGNED(arg) __builtin_assume_aligned(arg,__BIGGEST_ALIGNMENT__)

int kdr_strlen_aligned(const char *c)
{
	const char * a = TOALIGNED(c);
	return strlen(a);
}

char *kdr_strdup(const char *c)
{
	const int len = strlen(c) + 1;
	char *out = malloc(len + (len % __BIGGEST_ALIGNMENT__));
	memcpy(TOALIGNED(out), c, len);
	return out;
}

char *kdr_strdup_aligned(const char *c)
{
	const char * a = TOALIGNED(c);
	const int len = strlen(a) + 1;
	char *out = malloc(len + (len % __BIGGEST_ALIGNMENT__));
	memcpy(TOALIGNED(out), a, len);
	return out;
}

int kdr_strcmp_aligned(const char *a_, const char *b_)
{
    const char * a = TOALIGNED(a_);
    const char * b = TOALIGNED(b_);
    while(1){
        if(memcmp(a, b, __BIGGEST_ALIGNMENT__)) return 1;
        a += __BIGGEST_ALIGNMENT__;
        b += __BIGGEST_ALIGNMENT__;
    }
    return 0;
}

#undef TOALIGNED
