#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <math.h>
#include <locale.h>
#include <errno.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <assert.h>
#include <ctype.h>
#include <limits.h>

#include "arr.h"
#include "vector.h"
#include "wad.h"
#include "doomdata.h"
#include "rdebug.h"

static struct wad_flatface bboxflatface(int16_t *bbox)
{
	struct wad_flatface f = {
		.face = {
			.verts = {
				{.pos = {bbox[2], bbox[0], 0},},
				{.pos = {bbox[3], bbox[0], 0},},
				{.pos = {bbox[3], bbox[1], 0},},
				{.pos = {bbox[2], bbox[1], 0},},
			},
			.num_verts = 4,
		},
	};
	return f;
}

static void wad_map_build_flat_faces(struct wad_map_build_faces_t *bsp, int node_i, rvface face)
{
	mplane linep = {};
	if(node_i < 0){
		int ssector_i = 32768 + node_i;
		struct wad_ssector *ss = bsp->ssectors + ssector_i;
		
		int i, j, sector;
		//Cut face by every subsector segment
		for(i = 0, j = ss->first_seg; i < ss->num_segs; i++, j++){
			struct wad_seg *seg = bsp->segs + j;
			//Using segment vertices directly causes accuracy problems
			//struct wad_vertex *start = bsp->vertices + seg->start;
			//struct wad_vertex *end = bsp->vertices + seg->end;
			struct wad_linedef *linedef = bsp->linedefs + seg->linedef;
			struct wad_vertex *start = bsp->vertices + linedef->start;
			struct wad_vertex *end = bsp->vertices + linedef->end;
			const vec3 linev[3] = {
				{start->x, start->y, 0},
				{end->x, end->y, 0},
				{start->x, start->y, seg->dir ? -100 : 100},
			};
			vec3_verts2plane(linev, linev + 1, linev + 2, &linep);
			rvface right, left;
			rvface_split(&linep, &face, &right, &left);
			face = right;
			
			if(!face.num_verts) return;
		}
		
		//Try and find sector face is in
		if(1){
			struct wad_linedef linedef = bsp->linedefs[bsp->segs[ss->first_seg].linedef];
			struct wad_vertex *start = bsp->vertices + linedef.start;
			struct wad_vertex *end = bsp->vertices + linedef.end;
			const vec3 linev[3] = {
				{start->x, start->y, 0},
				{end->x, end->y, 0},
				{start->x, start->y, 100},
			};
			vec3_verts2plane(linev, linev + 1, linev + 2, &linep);
			vec3 midp = {};
			vecc_t area = 0;
			rvface2d_midpoint_area(&face, &midp, &area);
			
			if(!vec3_planecmp(&linep, &midp)){
				sector = bsp->sidedefs[bsp->linedefs[bsp->segs[ss->first_seg].linedef].sidedef_l].sector;
			}
			else{
				sector = bsp->sidedefs[bsp->linedefs[bsp->segs[ss->first_seg].linedef].sidedef_r].sector;
			}
		}
		
		//Set z,u,v
		for(i = 0; i < face.num_verts; i++){
			face.verts[i].pos.z = bsp->sectors[sector].h_floor;
			
			face.verts[i].u = face.verts[i].pos.x;
			face.verts[i].v = face.verts[i].pos.y;
		}
		
		//rvface_flip(&face);
		
		struct wad_flatface ff;
		
		ff.sector = sector;
		ff.ssector = ssector_i;
		ff.type = WAD_BSPFACE_FLOOR;
		memcpy(ff.tex, bsp->sectors[sector].tex_floor, 8);
		ff.face = face;
		arr_push(&bsp->flatfaces, ff);
		
		rvface_flip(&face);
		
		for(i = 0; i < face.num_verts; i++){
			face.verts[i].pos.z = bsp->sectors[sector].h_ceil;
		}
		
		ff.sector = sector;
		ff.ssector = ssector_i;
		ff.type = WAD_BSPFACE_CEIL;
		memcpy(ff.tex, bsp->sectors[sector].tex_ceil, 8);
		ff.face = face;
		arr_push(&bsp->flatfaces, ff);
		
		return;
	}
	
	struct wad_node *node = bsp->nodes + node_i;
	
	#ifndef USE_NODEPLANES
	const vec3 linev[3] = {
		{node->x, node->y, 0},
		{node->x + node->dx, node->y + node->dy, 0},
		{node->x, node->y, 100},
	};
	vec3_verts2plane(linev, linev + 1, linev + 2, &linep);
	#else
	linep = bsp->nodeplanes[node_i];
	#endif
	
	//Sort and send face further down BSP
	int sort = rvface_sort(&linep, &face);
	assert(sort != RVFACE_COPLANAR);
	if(sort == RVFACE_SPANNING){
		rvface right, left;
		rvface_split(&linep, &face, &right, &left);
		wad_map_build_flat_faces(bsp, node->child_r, right);
		wad_map_build_flat_faces(bsp, node->child_l, left);
	}
	else if(sort == RVFACE_FRONT){
		wad_map_build_flat_faces(bsp, node->child_r, face);
	}
	else if(sort == RVFACE_BACK){
		wad_map_build_flat_faces(bsp, node->child_l, face);
	}
	
	return;
}

static void wad_map_build_wall_faces(struct wad_map_build_faces_t *bsp, int node_i, struct wad_wallface lface)
{
	mplane linep;
	if(node_i < 0){
		lface.ssector = 32768 + node_i;
		arr_push(&bsp->wallfaces, lface);
		return;
	}
	
	struct wad_node *node = bsp->nodes + node_i;
	
	vec3 fmin, fmax,
		lmin = {node->bbox_l[2], node->bbox_l[1], -FLT_MAX}, lmax = {node->bbox_l[3], node->bbox_l[0], FLT_MAX},
		rmin = {node->bbox_r[2], node->bbox_r[1], -FLT_MAX}, rmax = {node->bbox_r[3], node->bbox_r[0], FLT_MAX};
	rvface_bounds(&lface.face, &fmin, &fmax);
	if((!vec3_box2box_mm(&fmin, &fmax, &lmin, &lmax, 0)) && (!vec3_box2box_mm(&fmin, &fmax, &rmin, &rmax, 0))){
		//rdebug_rvface(&lface.face);
	}
	
	#ifndef USE_NODEPLANES
	const vec3 linev[3] = {
		{node->x, node->y, 0},
		{node->x + node->dx, node->y + node->dy, 0},
		{node->x, node->y, 100},
	};
	vec3_verts2plane(linev, linev + 1, linev + 2, &linep);
	#else
	linep = bsp->nodeplanes[node_i];
	#endif
	
	//Sort and send face further down BSP
	int sort = rvface_sort(&linep, &lface.face);
	if(sort == RVFACE_SPANNING){
		struct wad_wallface right = lface, left = lface;
		rvface_split(&linep, &lface.face, &right.face, &left.face);
		wad_map_build_wall_faces(bsp, node->child_r, right);
		wad_map_build_wall_faces(bsp, node->child_l, left);
	}
	if(sort == RVFACE_FRONT){
		wad_map_build_wall_faces(bsp, node->child_r, lface);
	}
	if(sort == RVFACE_BACK){
		wad_map_build_wall_faces(bsp, node->child_l, lface);
	}
	if(sort == RVFACE_COPLANAR){
		vec3 sqn;
		vec3_sqnormal(&lface.face.verts[0].pos, &lface.face.verts[1].pos, &lface.face.verts[2].pos, &sqn);
		const vecc_t dot = vec3_dot(&linep.normal, &sqn);
		
		if(dot > 0) wad_map_build_wall_faces(bsp, node->child_r, lface);
		else wad_map_build_wall_faces(bsp, node->child_l, lface);
	}
	return;
}

int wad_map_sector_at(struct wad_map *bsp, int x, int y)
{
	mplane linep;
	struct wad_node *node;
	vec3 cpos = {x, y, 0};
	
	int node_i = bsp->num_nodes - 1;
	while(node_i >= 0){
		node = &bsp->nodes[node_i];
		
		//Don't use nodeplanes, so the function works from wad_map rather than wad_map_build_faces_t
		const vec3 linev[3] = {
			{node->x, node->y, 0},
			{node->x + node->dx, node->y + node->dy, 0},
			{node->x, node->y, 100},
		};
		vec3_verts2plane(linev, linev + 1, linev + 2, &linep);
		
		node_i = vec3_planecmp(&linep, &cpos) ? node->child_r : node->child_l;
	}
	
	int ssector_i = 32768 + node_i;
	struct wad_ssector *ss = bsp->ssectors + ssector_i;
	
	int i, j, sector;
	
	struct wad_linedef linedef = bsp->linedefs[bsp->segs[ss->first_seg].linedef];
	struct wad_vertex *start = bsp->vertices + linedef.start;
	struct wad_vertex *end = bsp->vertices + linedef.end;
	const vec3 linev[3] = {
		{start->x, start->y, 0},
		{end->x, end->y, 0},
		{start->x, start->y, 100},
	};
	vec3_verts2plane(linev, linev + 1, linev + 2, &linep);
	
	if((linedef.sidedef_l >= 0) && (!vec3_planecmp(&linep, &cpos))){
		sector = bsp->sidedefs[linedef.sidedef_l].sector;
	}
	else{
		sector = bsp->sidedefs[linedef.sidedef_r].sector;
	}
	
	return sector;
}

//The Doom node builder creates node planes from segments of linedefs
//This causes accuracy issues, where some linedefs may be behind node planes created from themselves
//Also slime trails, etc.
//This fixes the issue by matching every node to a linedef plane
static void wad_map_build_nodeplanes(struct wad_map_build_faces_t *level)
{
	//Adjust for best results
	const int TOLERANCE_1 = 2.0;	//Distance to linedef
	const int TOLERANCE_2 = 1.0;	//Distance to linedef ends if projected outside
	const int TOLERANCE_3 = 0.99;	//Dot product to check if projected inside
	const int TOLERANCE_4 = 0.95;	//Dot product of node and linedef planes
	
	int i, j, k;
	
	level->nodeplanes = malloc(level->num_nodes * sizeof(*level->nodeplanes));
	
	for(i = 0; i < level->num_nodes; i++){
		const struct wad_node *node = level->nodes + i;
		const vec3 nodev[3] = {
			{node->x, node->y, 0},
			{node->x + node->dx, node->y + node->dy, 0},
			{node->x, node->y, 100},
		};
		const int nodev_sort = (nodev[0].x != nodev[1].x) ? (nodev[0].x > nodev[1].x ? 1 : 0) : (nodev[0].y > nodev[1].y ? 1 : 0);
		mplane nodep;
		vec3_verts2plane(nodev, nodev + 1, nodev + 2, &nodep);
		vecc_t bestscore = FLT_MAX;
		mplane bestp;
		
		for(j = 0; j < level->num_linedefs; j++){
			const struct wad_linedef *linedef = level->linedefs + j;
			const struct wad_vertex *start = level->vertices + linedef->start;
			const struct wad_vertex *end = level->vertices + linedef->end;
			const vec3 linev[3] = {
				{start->x, start->y, 0},
				{end->x, end->y, 0},
				{start->x, start->y, 100},
			};
			const int linev_sort = (linev[0].x != linev[1].x) ? (linev[0].x > linev[1].x) : (linev[0].y > linev[1].y);
			mplane linep;
			vec3_verts2plane(linev, linev + 1, linev + 2, &linep);
			
			vec3 nodev_proj[2];
			vecc_t score = 0;
			for(k = 0; k < 2; k++){
				const int idx_n = k ^ nodev_sort;
				const int idx_l = k ^ linev_sort;
				
				const vecc_t pdist = fabs(vec3_dist2plane(&linep, nodev + idx_n));
				
				if(pdist > TOLERANCE_1) break;
				mplane_closest(&linep, nodev + idx_n, nodev_proj + idx_n);
				
				const vec3 l_to_n = VEC3_SUB(nodev_proj[idx_n], linev[idx_l]);
				vec3 l_to_l = VEC3_SUB(linev[!idx_n], linev[idx_l]);
				vec3_normalize(&l_to_l);
				
				const vecc_t ltn_len = vec3_len(&l_to_n);
				const vecc_t ltn_dot = vec3_dot(&l_to_l, &l_to_n) / ltn_len;
				
				if((ltn_len > TOLERANCE_2) && (ltn_dot < TOLERANCE_3)) break;
				
				score += pdist;
			}
			if(k < 2) continue;
			
			if(score < bestscore){
				bestscore = score;
				bestp = linep;
			}
		}
		
		assert(bestscore != FLT_MAX);
		
		const vecc_t ndot = vec3_dot(&nodep.normal, &bestp.normal);
		assert(fabs(ndot) > TOLERANCE_4);
		if(ndot < 0) mplane_flip(&bestp);
		
		level->nodeplanes[i] = bestp;
	}
}

void wad_map_build_faces(struct wad_map_build_faces_t *map)
{
	#define bsp (*map)
	int i, j;
	int minx = INT_MAX, miny = INT_MAX, maxx = INT_MIN, maxy = INT_MIN;
	#ifndef MIN
	#define MIN(a,b)\
	({\
		int _a = a;\
		int _b = b;\
		(_a>_b)?_b:_a;\
	})
	#define MAX(a,b)\
	({\
		int _a = a;\
		int _b = b;\
		(_a>_b)?_a:_b;\
	})
	#endif
	
	for(i = 0; i < bsp.num_vertices; i++){
		minx = MIN(minx, bsp.vertices[i].x);
		maxx = MAX(maxx, bsp.vertices[i].x);
		miny = MIN(miny, bsp.vertices[i].y);
		maxy = MAX(maxy, bsp.vertices[i].y);
	}
	
	arr_init(&map->wallfaces);
	arr_init(&map->flatfaces);
	
	wad_map_build_nodeplanes(map);
	
	int16_t bbox0[8] = {miny, maxy, minx, maxx};
	wad_map_build_flat_faces(&bsp, bsp.num_nodes - 1, bboxflatface(bbox0).face);
	
	for(i = 0; i < bsp.num_linedefs; i++){
		const struct wad_linedef linedef = bsp.linedefs[i];
		const int flags = linedef.flags;
		const struct wad_vertex start = bsp.vertices[linedef.start], end = bsp.vertices[linedef.end];
		const int length = sqrt(pow(start.x - end.x, 2) + pow(start.y - end.y, 2));
		struct wad_wallface right = {
			.face = {
				.verts = {
					{.pos = {end.x,		end.y,		0},	.u = length,			.v = 0},
					{.pos = {start.x,	start.y,	0},	.u = 0,					.v = 0},
					{.pos = {start.x,	start.y,	0},	.u = 0,					.v = 0},
					{.pos = {end.x,		end.y,		0},	.u = length,			.v = 0},
				},
				.num_verts = 4,
			},
			.sidedef = linedef.sidedef_r,
		};
		struct wad_wallface left = {
			.face = {
				.verts = {
					{.pos = {start.x,	start.y,	0},	.u = 0,			.v = 0},
					{.pos = {end.x,		end.y,		0},	.u = length,	.v = 0},
					{.pos = {end.x,		end.y,		0},	.u = length,	.v = 0},
					{.pos = {start.x,	start.y,	0},	.u = 0,			.v = 0},
				},
				.num_verts = 4,
			},
			.sidedef = linedef.sidedef_l,
		};
		
		const struct wad_sidedef
			*sidedef_r = bsp.sidedefs + right.sidedef,
			*sidedef_l = (left.sidedef >= 0) ? (bsp.sidedefs + left.sidedef) : 0;
		const struct wad_sector
			*sector_r = bsp.sectors + sidedef_r->sector,
			*sector_l = sidedef_l ? (bsp.sectors + sidedef_l->sector) : 0;
		
		char tex_upper_r[8], tex_upper_l[8];
		memcpy(tex_upper_r, sidedef_r->tex_upper, 8);
		memcpy(tex_upper_l, (sidedef_l ?: sidedef_r)->tex_upper, 8);
		// The so-called "sky hack"
		if(sector_l){
			if(
				(!strncmp(sector_r->tex_ceil, "F_SKY", 5)) &&
				(!strncmp(sector_l->tex_ceil, "F_SKY", 5))
			){
				strncpy(tex_upper_r, sector_r->tex_ceil + 2, 6);
				strncpy(tex_upper_l, sector_r->tex_ceil + 2, 6);
			}
		}
		
		rvert verts_bk[4];
		#define VERTS_PUSH() memcpy(verts_bk, verts, sizeof(verts_bk))
		#define VERTS_POP() memcpy(verts, verts_bk, sizeof(verts_bk))
		rvert *verts = right.face.verts;
		verts[0].u += sidedef_r->offs_u;
		verts[1].u += sidedef_r->offs_u;
		verts[2].u += sidedef_r->offs_u;
		verts[3].u += sidedef_r->offs_u;
		verts[0].v += sidedef_r->offs_v;
		verts[1].v += sidedef_r->offs_v;
		verts[2].v += sidedef_r->offs_v;
		verts[3].v += sidedef_r->offs_v;
		
		int height;
		#define VERTS_SETZ(_za,_zb)({\
			verts[0].pos.z = verts[1].pos.z = _za;\
			verts[2].pos.z = verts[3].pos.z = _zb;\
			height = verts[0].pos.z - verts[2].pos.z;\
		})
		
		/*
		If a linedef is one sided (a solid wall), the texture is "pegged" to
		the top of the wall. That is to say, the top of the texture is at the
		ceiling. The texture continues downward to the floor.
		
			If the "lower unpegged" flag is set on the linedef, the texture is 
			instead "pegged" to the bottom of the wall. That is, the bottom of 
			the texture is located at the floor. The texture is drawn upwards 
			from here. This is commonly used in the jambs in doors; because 
			Doom's engine treats each door as a "rising ceiling", a doorjamb 
			pegged to the top of the wall would "rise up" with the door.
			
			The alignment of the texture can be adjusted using the sidedef X
			and Y alignment controls. This is applied after the logic 
			controlling	pegging. 
		
		If a linedef is two sided, acting as a "bridge" between two sectors - 
		such as a window or a step - the upper texture is pegged to the lowest
		ceiling, and the lower texture is pegged to the highest floor. That 
		is, the bottom of the upper texture will be at the lowest ceiling and 
		drawn upwards, and the top of the lower texture will be at the highest 
		floor and drawn downwards. This is appropriate for textures that 
		"belong" to the surface behind them, such as lifts or platforms.
		
			If the "upper unpegged" flag is set on the linedef, the upper 
			texture will begin at the higher ceiling and be drawn downwards.
			
			If the "lower unpegged" flag is set on the linedef, the lower 
			texture will be drawn as if it began at the higher ceiling and 
			continued downwards. So if the higher ceiling is at 96 and the 
			higher floor is at 64, a lower unpegged texture will start 32 
			pixels from its top edge and draw downwards from the higher floor.
			
			Both forms of unpegging cause textures to be drawn the same way 
			as if they were on a one-sided wall — relative to the ceiling —
			but with the middle section "cut out". Unpegging is thus 
			appropriate for textures that "belong" to the surface in front 
			of them, such as windows or recessed switches.
			
			The alignment of textures can be adjusted with the sidedef X and
			Y alignment control. This is applied after the logic controlling
			pegging and affects all textures.
			
			If a middle texture is set, it is clipped by the highest floor and
			the lowest ceiling and follows the same alignment logic as for a 
			single sided linedef, but does not repeat vertically.
		*/
		
		#define VERTS_SETVTOP() ({\
			if((flags & ML_DONTPEGTOP)){\
				verts[2].v += height;\
				verts[3].v += height;\
			}\
			else{\
				verts[0].v -= height;\
				verts[1].v -= height;\
			}\
		})
		#define VERTS_SETVBOTTOM() ({\
			if((flags & ML_DONTPEGBOTTOM)){\
				int higher_ceil = sector_r->h_ceil;\
				if(sector_l && sector_l->h_ceil > higher_ceil)\
					higher_ceil = sector_r->h_ceil;\
				int height2 = higher_ceil - verts[0].pos.z;\
				\
				verts[0].v += height2;\
				verts[1].v += height2;\
				verts[2].v += height2;\
				verts[3].v += height2;\
			}\
			verts[2].v += height;\
			verts[3].v += height;\
		})
		#define VERTS_SETVMIDDLE() ({\
			verts[2].v += height;\
			verts[3].v += height;\
		})
		
		/* 0---1
		   |   |
		   3---2 */
		
		if(sidedef_l &&
			sector_l->h_ceil < sector_r->h_ceil){
			//Upper texture
			VERTS_SETZ(sector_r->h_ceil, sector_l->h_ceil);
			VERTS_PUSH();
			
			VERTS_SETVTOP();
			
			memcpy(right.tex, tex_upper_r, 8);
			right.type = WAD_BSPFACE_UPPER;
			wad_map_build_wall_faces(&bsp, bsp.num_nodes - 1, right);
			VERTS_POP();
		}
		if(sidedef_l &&
			sector_l->h_floor > sector_r->h_floor){
			//Lower texture
			VERTS_SETZ(sector_l->h_floor, sector_r->h_floor);
			VERTS_PUSH();
			
			VERTS_SETVBOTTOM();
			
			memcpy(right.tex, sidedef_r->tex_lower, 8);
			right.type = WAD_BSPFACE_LOWER;
			wad_map_build_wall_faces(&bsp, bsp.num_nodes - 1, right);
			VERTS_POP();
		}
		
		if(strncmp(sidedef_r->tex_mid, "-", 8)){
			//Middle texture
			VERTS_SETZ(sector_r->h_ceil, sector_r->h_floor);
			VERTS_PUSH();
			
			VERTS_SETVMIDDLE();
			
			memcpy(right.tex, sidedef_r->tex_mid, 8);
			right.type = WAD_BSPFACE_MIDDLE;
			wad_map_build_wall_faces(&bsp, bsp.num_nodes - 1, right);
			VERTS_POP();
		}
		
		if(!sidedef_l) continue;
		
		verts = left.face.verts;
		verts[0].u += sidedef_l->offs_u;
		verts[1].u += sidedef_l->offs_u;
		verts[2].u += sidedef_l->offs_u;
		verts[3].u += sidedef_l->offs_u;
		verts[0].v += sidedef_l->offs_v;
		verts[1].v += sidedef_l->offs_v;
		verts[2].v += sidedef_l->offs_v;
		verts[3].v += sidedef_l->offs_v;
		
		if(sector_r->h_ceil < sector_l->h_ceil){
			//Upper texture
			VERTS_SETZ(sector_l->h_ceil, sector_r->h_ceil);
			VERTS_PUSH();
			
			VERTS_SETVTOP();
			
			memcpy(left.tex, tex_upper_l, 8);
			left.type = WAD_BSPFACE_UPPER;
			wad_map_build_wall_faces(&bsp, bsp.num_nodes - 1, left);
			VERTS_POP();
		}
		if(sector_r->h_floor > sector_l->h_floor){
			//Lower texture
			VERTS_SETZ(sector_r->h_floor, sector_l->h_floor);
			VERTS_PUSH();
			
			VERTS_SETVBOTTOM();
			
			memcpy(left.tex, sidedef_l->tex_lower, 8);
			left.type = WAD_BSPFACE_LOWER;
			wad_map_build_wall_faces(&bsp, bsp.num_nodes - 1, left);
			VERTS_POP();
		}
		if(strncmp(sidedef_l->tex_mid, "-", 8)){
			//Middle texture
			VERTS_SETZ(sector_l->h_ceil, sector_l->h_floor);
			VERTS_PUSH();
			
			VERTS_SETVMIDDLE();
			
			memcpy(left.tex, sidedef_l->tex_mid, 8);
			left.type = WAD_BSPFACE_MIDDLE;
			wad_map_build_wall_faces(&bsp, bsp.num_nodes - 1, left);
			VERTS_POP();
		}
	}
	
	arr_unreserve(&map->flatfaces);
	arr_unreserve(&map->wallfaces);
	
	for(struct wad_wallface *wf = map->wallfaces.data; wf < map->wallfaces.data + map->wallfaces.len; wf++){
		for(char *c = wf->tex; *c && (c - wf->tex) < 8; c++){
			*c = toupper(*c);
		}
	}
	for(struct wad_flatface *ff = map->flatfaces.data; ff < map->flatfaces.data + map->flatfaces.len; ff++){
		for(char *c = ff->tex; *c && (c - ff->tex) < 8; c++){
			*c = toupper(*c);
		}
	}
}
