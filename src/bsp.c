//BSP generation, from mesh, functions - no visibility
#include <allegro.h>

#include <stdio.h>
#include <math.h>
#include <float.h>

#include "bsp.h"
#include "mesh.h"
#include "vector.h"
#include "compat.h"
#include "rdebug.h"

typedef struct bspfacelist
{
	bspvert verts[BSPFACE_MAX_VERTS];
	int num_verts;
	
	int material;
	int plane;
	
	int has_split;
	struct bspfacelist *next;
} bspfacelist;

typedef struct bspbuilddata
{
	mplane *planes;
	bspface *faces;
	bspnode *nodes;
	bspleaf *leafs;
	int num_planes;
	int num_faces;
	int num_nodes;
	int num_leafs;
	
	unsigned int *seen_planes;
	unsigned int seen_planes_counter;
} bspbuilddata;

static bspfacelist *alloc_facelist()
{
	bspfacelist *f = malloc(sizeof(bspfacelist));
	return f;
}

#define ALLOC_I_FUNC(name,t)\
static int alloc_ ## name ## _i(bspbuilddata *b)\
{\
	const int threshold = t;\
	if(b->num_ ## name ## s % threshold == 0){\
		b->name ## s = realloc(\
			b->name ## s,\
			(b->num_ ## name ## s + threshold) * sizeof(*b->name ## s)\
		);\
	}\
	return b->num_ ## name ## s++;\
}

ALLOC_I_FUNC(leaf,100)
ALLOC_I_FUNC(node,100)
ALLOC_I_FUNC(face,100)
ALLOC_I_FUNC(plane,100)

static void bspface_split(mplane *p, bspfacelist *face)
{
	bspfacelist front_face, back_face;
	#ifdef DBG_FACE_SPLIT
	vecc_t area0, area_new;
	int bad1, bad2 = 0;
	
	if((bad1 = rvface_isbad((rvface *) face))){
		printf("bspface_split bad face %d\n", bad1);
		rdebug_set_colour(0xFF);
		rdebug_rvface((rvface *) face);
	}
	#else
	#warning "DBG_FACE_SPLIT not set, tests won't happen"
	#endif
	
	front_face = *face;
	back_face = *face;
	
	rvface_split(p, (rvface *) face, (rvface *) &front_face, (rvface *) &back_face);
	
	#ifdef DBG_FACE_SPLIT
	area0 = rvface_area((rvface *) face);
	area_new = 
		rvface_area((rvface *) &front_face) +
		rvface_area((rvface *) &back_face);
	if(
		((bad1 = rvface_isbad((rvface *) &front_face)) ||
		(bad2 = rvface_isbad((rvface *) &back_face)))
	){
		printf("bspface_split bad split faces ");
		rdebug_set_colour(0xFF00);
		rdebug_rvface((rvface *) face);
		if(bad2){
			printf("BF %d ", bad2);
			rdebug_set_colour(0xFF);
			rdebug_rvface((rvface *) &back_face);
		}
		if(bad1){
			printf("FF %d", bad1);
			rdebug_set_colour(0xFF);
			rdebug_rvface((rvface *) &front_face);
		}
		printf("\n");
	}
	if(fabs(area_new - area0) / area0 > 0.0001){
		printf("bspface_split area changed\n");
	}
	#endif
	
	back_face.next = face->next;
	front_face.next = alloc_facelist();
	*front_face.next = back_face;
	face->next = alloc_facelist();
	*face->next = front_face;
}

#define BSP_SPLITTER_SCORE(b, s) ((b) + (s) * 8)

//As this takes O(n²) time, it may be necessary eventually to optimise
//an octtree is a good pick for such purpose
static int bsp_bestsplitter(bspbuilddata *b, bspfacelist *faces)
{
	bspfacelist *curr;
	int best = -1;
	unsigned int score = 999999999;
	
	if(!b->seen_planes){
		b->seen_planes = calloc(b->num_planes, sizeof(*b->seen_planes));
	}
	b->seen_planes_counter++;
	
	for(curr = faces; curr; curr = curr->next){
		int balance = 0;
		unsigned int splits = 0;
		unsigned int currscore = 0;
		bspfacelist *curr2 = faces;
		
		if(curr->has_split) continue;
		if(b->seen_planes[curr->plane] == b->seen_planes_counter) continue;
		
		for(curr2 = faces; curr2; curr2 = curr2->next){
			int r;
			if(curr2 == curr) continue;
			if(curr2->plane == curr->plane) continue;
			
			r = rvface_sort(&b->planes[curr->plane], (rvface *) curr2);
			if(!r) splits++;
			switch(r){
				case RVFACE_COPLANAR:
					break;
				case RVFACE_SPANNING:
					splits++;
					break;
				case RVFACE_FRONT:
					balance++;
					break;
				case RVFACE_BACK:
					balance--;
					break;
			}
		}
		
		if(balance < 0) balance *= -1;
		currscore = BSP_SPLITTER_SCORE(balance, splits);
		if(currscore < score){
			best = curr->plane;
			score = currscore;
		}
		b->seen_planes[curr->plane] = b->seen_planes_counter;
	}
	
	return best;
}

static int build_bspnode(bspbuilddata *b, bspfacelist *faces)
{
	bspfacelist *curr = faces;
	int plane_i = faces ? bsp_bestsplitter(b, faces) : -1;
	mplane *plane = plane_i >= 0 ? &b->planes[plane_i] : 0;
	bspfacelist *front_faces = 0, *back_faces = 0;
	bspfacelist *front_faces_1st = 0, *back_faces_1st = 0;
	bspfacelist *free_face = 0;
	int node_i;
	int front_child, back_child;
	
	if(!faces){
		return BSPLEAF_SOLID;
	}
	
	if(plane_i < 0){
		//No splitting left to do and we have a convex hull - create leaf
		bspfacelist *old = 0;
		int leaf_i = alloc_leaf_i(b);
		bspleaf *leaf = &b->leafs[leaf_i];
		leaf->first_face = -1;
		
		for(curr = faces; ; curr = curr->next){
			free(old);
			if(!curr) break;
			
			leaf->last_face = alloc_face_i(b);
			if(leaf->first_face < 0) leaf->first_face = leaf->last_face;
			b->faces[leaf->last_face] = *((bspface *) curr);
			
			old = curr;
		}
		
		return BSPNODE_TO_LEAF(leaf_i);
	}
	
	#define INSERT_FACE(f, t)\
	if(!t ## _faces){\
		t ## _faces_1st = t ## _faces = curr;\
	}\
	else{\
		t ## _faces->next = curr;\
		t ## _faces = curr;\
	}
	
	for(curr = faces; curr; curr = curr->next){
		if(free_face){
			free(free_face);
			free_face = 0;
		}
		
		if(curr->plane == plane_i){
			curr->has_split = 1;
			
			INSERT_FACE(curr, front)
			continue;
		}
		
		switch(rvface_sort(plane, (rvface *) curr)){
			case RVFACE_BACK:
				INSERT_FACE(curr, back)
				break;
			case RVFACE_FRONT:
				INSERT_FACE(curr, front)
				break;
			case RVFACE_COPLANAR:
				//Only coplanar faces facing opposite to plane will get here
				
				INSERT_FACE(curr, back)
				break;
			case RVFACE_SPANNING:
				bspface_split(plane, curr);
				free_face = curr;
				break;
		}
	}
	
	#undef INSERT_FACE
	
	if(front_faces) front_faces->next = 0;
	if(back_faces) back_faces->next = 0;
	
	node_i = alloc_node_i(b);
	b->nodes[node_i].plane = plane_i;
	
	//Either build_bspnode may alter b->nodes by reallocation
	//Thus, assigning directly causes unintended behaviour
	front_child = build_bspnode(b, front_faces_1st);
	back_child = build_bspnode(b, back_faces_1st);
	b->nodes[node_i].children[BSPNODE_CHILD_FRONT] = front_child;
	b->nodes[node_i].children[BSPNODE_CHILD_BACK] = back_child;
	
	return node_i;
}

static int bspplane_find(bspbuilddata *b, mplane *p)
{
	int i;
	for(i = 0; i < b->num_planes; i++){
		mplane *o = &b->planes[i];
		if(mplane_equal(p, o)) return i;
	}
	i = alloc_plane_i(b);
	b->planes[i] = *p;
	return i;
}

static bspfacelist *mesh_to_bspfacelist(bspbuilddata *b, struct mesh *me)
{
	int i, j, currtex;
	bspfacelist *currface = alloc_facelist();
	bspfacelist *facelist = currface;
	mplane pl;
	int first_face = 0, last_face = me->num_faces - 1;
	
	for(i = 0; i < me->num_subs; i++){
		if(strstr(me->sub_names[i], "level")){
			first_face = me->sub_faces[i][0];
			last_face = me->sub_faces[i][1];
			break;
		}
	}
	
	currtex = 0;
	for(i = 0; i < first_face; i++){
		if(currtex + 1 < me->num_textures){
			if(i >= me->texids[currtex + 1]) currtex++;
		}
	}
	for(i = first_face; i <= last_face; i++){
		if(currtex + 1 < me->num_textures){
			if(i >= me->texids[currtex + 1]) currtex++;
		}
		
		for(j = 0; j < me->face_numverts[i]; j++){
			currface->verts[j] = me->verts[me->faces[i][j]];
		}
		
		vec3_verts2plane(
			&currface->verts[0].pos,
			&currface->verts[1].pos,
			&currface->verts[2].pos,
			&pl
		);
		currface->plane = bspplane_find(b, &pl);
		
		currface->num_verts = me->face_numverts[i];
		currface->material = currtex;
		currface->has_split = 0;
		
		if(i + 1 <= last_face){
			currface->next = alloc_facelist();
			currface = currface->next;
		}
	}
	currface->next = 0;
	
	return facelist;
}

void bsp_calcbounds(bspdata *b, int node_i, vec3 *min, vec3 *max)
{
	if(node_i == BSPLEAF_SOLID) return;
	if(!BSPNODE_ISLEAF(node_i)){
		bspnode *n = &b->nodes[node_i];
		n->min = vec3c_max;
		n->max = vec3c_min;
		
		bsp_calcbounds(b, n->children[0], &n->min, &n->max);
		bsp_calcbounds(b, n->children[1], &n->min, &n->max);
		if(min) vec3_setmin(&n->min, min);
		if(max) vec3_setmax(&n->max, max);
	}
	else{
		int i;
		int leaf_i = BSPNODE_TO_LEAF(node_i);
		bspleaf *leaf = &b->leafs[leaf_i];
		leaf->min = vec3c_max;
		leaf->max = vec3c_min;
		
		for(i = leaf->first_face; i <= leaf->last_face; i++){
			int k;
			bspface *f = &b->faces[i];
			for(k = 0; k < f->num_verts; k++){
				vec3_setmin(&f->verts[k].pos, &leaf->min);
				vec3_setmax(&f->verts[k].pos, &leaf->max);
			}
		}
		
		if(min) vec3_setmin(&leaf->min, min);
		if(max) vec3_setmax(&leaf->max, max);
	}
}

bspdata *build_bsp(struct mesh *me)
{
	bspbuilddata _b = {};
	bspbuilddata *b = &_b;
	bspdata *bd = calloc(1, sizeof(*bd));
	bspfacelist *fs;
	
	fs = mesh_to_bspfacelist(b, me);
	build_bspnode(b, fs);
	
	#define REALLOC_DATA(d) b->d = realloc(b->d, sizeof(*b->d) * b->num_ ## d)
	REALLOC_DATA(planes);//TODO: filter out planes no longer used?
	REALLOC_DATA(faces);
	REALLOC_DATA(nodes);
	REALLOC_DATA(leafs);
	#undef REALLOC_DATA
	
	free(b->seen_planes);
	
	/*
	typedef struct bspbuilddata
	{
		mplane *planes;
		bspface *faces;
		bspnode *nodes;
		bspleaf *leafs;
		int num_planes;
		int num_faces;
		int num_nodes;
		int num_leafs;
		
		unsigned int *seen_planes;
		unsigned int seen_planes_counter;
	} bspbuilddata;
	typedef struct bspdata
	{
		mplane *planes;
		bspface *faces;
		bspnode *nodes;
		bspleaf *leafs;
		int num_planes;
		int num_faces;
		int num_nodes;
		int num_leafs;
		int num_uvs;
		int *pvs;
		int pvs_length;
		int *lm_faces;
		float (*lm_uvs)[2];
	} bspdata;
	*/
	bd->planes = b->planes;
	bd->faces = b->faces;
	bd->nodes = b->nodes;
	bd->leafs = b->leafs;
	bd->num_planes = b->num_planes;
	bd->num_faces = b->num_faces;
	bd->num_nodes = b->num_nodes;
	bd->num_leafs = b->num_leafs;
	bd->num_uvs = 0;
	bd->pvs = 0;
	bd->pvs_length = 0;
	bd->lm_faces = 0;
	bd->lm_uvs = 0;
	bd->mesh_crc32 = me->crc32;
	bd->num_mdls = 0;
	bd->mdls = 0;
	
	bsp_calcbounds(bd, 0, 0, 0);
	
	return bd;
}

void destroy_bsp(bspdata *bsp)
{
	int i;
	if(bsp->planes) free(bsp->planes);
	if(bsp->faces) free(bsp->faces);
	if(bsp->nodes) free(bsp->nodes);
	if(bsp->leafs){
		free(bsp->leafs[0].portals);
		free(bsp->leafs);
	}
	if(bsp->portals) free(bsp->portals);
	if(bsp->portal_verts) free(bsp->portal_verts);
	if(bsp->pvs) free(bsp->pvs);
	if(bsp->lm_faces) free(bsp->lm_faces);
	if(bsp->lm_uvs) free(bsp->lm_uvs);
	if(bsp->mdls){
		if(bsp->num_mdls) free(bsp->mdls[0].leafs);
		free(bsp->mdls);
	}
	if(bsp->navmesh) free(bsp->navmesh);
	free(bsp);
}

void bsp_leafmidpoint(const bspdata *b, int leaf_i, vec3 *out)
{
	int i, j, k, l;
	vec3 midp = vec3c_origin;
	vecc_t areatotal = 0;
	const bspleaf *leaf = b->leafs + leaf_i;
	int leaf_num_portals = b->portals ? leaf->num_portals : 0;
	//VLA(s)
	rvface pfaces[leaf_num_portals ?: 1];
	rvface *faces[leaf->last_face - leaf->first_face + 1 + (leaf_num_portals ?: 1)];
	
	for(i = 0, j = leaf->first_face; j <= leaf->last_face; j++, i++){
		faces[i] = (rvface *) (b->faces + j);
	}
	
	if(b->portals){
		for(j = 0; j < leaf->num_portals; j++, i++){
			bspportal2 *portal = b->portals + leaf->portals[j];
			pfaces[j].num_verts = portal->num_verts;
			for(k = 0, l = portal->first_vert; k < portal->num_verts; k++, l++){
				pfaces[j].verts[k].pos = b->portal_verts[l];
			}
			faces[i] = pfaces + j;
		}
	}
	
	for(j = 0; j < i; j++){
		rvert m2;
		vecc_t area;
		rvface_midpoint_area(faces[j], &m2, &area);
		vec3_maddto(&midp, &m2.pos, area);
		areatotal += area;
	}
	vec3_mult(&midp, 1.0/areatotal);
	*out = midp;
}
