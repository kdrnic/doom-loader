#ifndef RDEBUG_H
#define RDEBUG_H

struct rvface;
struct vec3;
struct MATRIX_f;
struct BITMAP;

void rdebug_set_rvfscl(float s);
void rdebug_set_mask(int m);
void rdebug_set_colour(int c);
void rdebug_set_solid(int s);
void rdebug_set_zb(int z);
void rdebug_set_matrix(const struct MATRIX_f *m);
void rdebug_set_scale2d(float);
void rdebug_push();
void rdebug_pop();
void rdebug_string(const struct vec3 *p, const char *s);
void rdebug_printf(const struct vec3 *p, const char *fmt, ...);
void rdebug_point(const struct vec3 *p);
void rdebug_particle(const struct vec3 *p);
void rdebug_line(const struct vec3 *p1, const struct vec3 *p2);
void rdebug_arrow(const struct vec3 *p1, const struct vec3 *p2);
#define rdebug_arrow2(p1, dir)\
({\
	vec3 _tmp = VEC3_ADD(*(p1), *(dir));\
	rdebug_arrow((p1), &_tmp);\
})
#define rdebug_arrow3(p1, dir, txt, ...)\
({\
	vec3 _tmp = VEC3_ADD(*(p1), *(dir));\
	rdebug_arrow((p1), &_tmp);\
	rdebug_printf(&_tmp, (txt), ##__VA_ARGS__);\
})
void rdebug_face(const struct vec3 *verts, int num_verts);
void rdebug_rvface(const struct rvface *f);
void rdebug_box_sc(const struct vec3 *size, const struct vec3 *centre);
#define RDEBUG_BOX_MM(min, max) ({ const vec3 VEC3_MM2SC((min), (max), siz, ctr); rdebug_box_sc(&siz, &ctr); (void) 0;})
void rdebug_ruler(const struct vec3 *pos);
void rdebug_box2d(const struct vec3 *a, const struct vec3 *b);
void rdebug_face2d(const struct rvface *f);
void rdebug_printf2d(const struct vec3 *p, const char *fmt, ...);
void rdebug_draw(struct MATRIX_f *c, struct BITMAP *bmp);
void rdebug_clear(void);
#define rdebug_obb(obb)\
({\
	rdebug_box_sc(&obb->size, &obb->pos);\
})
#define rdebug_matrix_f(mat)\
({\
	MATRIX_f *_mat = (mat);\
	vec3 _o = vec3c_origin, _x = vec3c_x, _y = vec3c_y, _z = vec3c_z;\
	vec3_apply_matrix_f(&_o, _mat);\
	vec3_apply_matrix_f(&_x, _mat); rdebug_set_colour(0xFF0000); rdebug_arrow(&_o, &_x);\
	vec3_apply_matrix_f(&_y, _mat); rdebug_set_colour(0x00FF00); rdebug_arrow(&_o, &_y);\
	vec3_apply_matrix_f(&_z, _mat); rdebug_set_colour(0x0000FF); rdebug_arrow(&_o, &_z);\
})
void rdebug_hemicube(struct MATRIX_f *hemicube_mat);

void rdebug_notransform(void);

int rdebug_get_n();

#endif
