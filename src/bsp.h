//BSP stuff

#ifndef BSP_H
#define BSP_H

#include "vector.h"

typedef rvert bspvert;

#define BSPFACE_MAX_VERTS RVFACE_MAX_VERTS

//TODO: change int to char or short wherever possible
typedef struct bspface
{
	bspvert verts[BSPFACE_MAX_VERTS];
	int num_verts;
	
	int material;
	int type;
} bspface;

#define BSPNODE_CHILD_FRONT 0
#define BSPNODE_CHILD_BACK 1

#define BSPLEAF_SOLID -1

#define BSPNODE_ISLEAF(n) ((n) < 0)
#define BSPNODE_TO_LEAF(n) (BSPLEAF_SOLID - (n) - 1)
#define BSPNODE_CALC_LEAF_COLOUR(it,l,c) \
c = 0;\
for(it = 0; it <= 10 + l; it++){\
	c = (22695477u * c + 1u);\
}

typedef struct bspnode
{
	int plane;
	int children[2];
	
	vec3 min, max;
} bspnode;

typedef struct bspleaf
{
	int first_face, last_face;
	int pvs;
	
	vec3 min, max;
	
	//TODO: make short int or even char
	int *portals;
	int num_portals;
} bspleaf;

typedef struct bspportal2
{
	int first_vert;
	int num_verts;
	int leafs[2];
	int plane;
} bspportal2;

typedef struct bspmdl
{
	//TODO: orientation - OBB will pvs more efficiently
	vec3 min, max;
	
	//-1 terminated array
	int *leafs;
} bspmdl;

//Two different purposes: first is a simple identifier, to avoid collision with Quake/GoldSrc
//or other versions of TurboBSPower 97 2.0
//Second is to be defined by the caller of build_bsp, to safeguard against changes in build process
/*
Changelog:
1.1		Eliminate "c" from rvert
1.2		Add mdls info
1.3		Add navmesh
1.4		Add num_mdl_leafs
1.5		Portals
*/
#define BSP_VER "KDRBSP1.5"
extern const char *bsp_build_ver_string;

typedef struct bspdata
{
	mplane *planes;
	bspface *faces;
	bspnode *nodes;
	bspleaf *leafs;
	bspmdl *mdls;
	bspportal2 *portals;
	vec3 *portal_verts;
	int num_planes;
	int num_faces;
	int num_nodes;
	int num_leafs;
	int num_uvs;
	int num_mdls;
	int num_mdl_leafs;
	int num_portals;
	int num_portal_verts;
	int num_portal_idx;
	int *pvs;
	int pvs_length;
	int *lm_faces;
	float (*lm_uvs)[2];
	unsigned int mesh_crc32;
	char *build_ver;
	char *navmesh;
	int navmesh_len;
	int sky_material;
} bspdata;

struct mesh;
struct MATRIX_f;
struct BITMAP;

//TODO: rename
extern unsigned int pvs_counter;

extern int draw_bsp_current_leaf;
extern int draw_bsp_node_hl;
extern void (*draw_bsp_onleaf)(
	const bspdata *bsp,
	const struct MATRIX_f *cm,
	struct BITMAP *bmp,
	struct BITMAP *litbmp,
	int leaf_i,
	void *data
);
extern void *draw_bsp_onleaf_data;

bspdata *build_bsp(struct mesh *me);
void bsp_calcbounds(bspdata *b, int node_i, vec3 *min, vec3 *max);
void bsp_leafmidpoint(const bspdata *b, int leafi, vec3 *out);
int build_bsp_vis(bspdata *bsp);
extern int draw_bsp_leafs;
void draw_bsp_init();
unsigned int *bsp_pointpvs(const bspdata *b, const vec3 *pos);
unsigned int *draw_bsp(
	const bspdata *bsp,
	const struct MATRIX_f *cm,
	struct BITMAP *bmp,
	struct BITMAP ***textures,
	const int shader,
	struct BITMAP *litbmp,
	struct BITMAP *lightmap
);
void draw_bsp_local(
	const bspdata * const b,
	const struct MATRIX_f * const cm,
	struct BITMAP *bmp,
	void * const data,
	void (*draw_bspface_func)(const bspdata *, struct BITMAP *, const struct MATRIX_f *, int, void *),
	void (*draw_leaf_func)(const bspdata *, struct BITMAP *, const struct MATRIX_f *, int, void *)
);
void save_bsp(const char *fn, bspdata *bsp);
#define LOAD_BSP_VER_ERROR 1
#define LOAD_BSP_VER_STR_ERROR 2
bspdata *load_bsp(const char *fn, int check_ver);
int bsp_set_lightmap(bspdata *bsp, const struct mesh *me);
void destroy_bsp(bspdata *bsp);

#define BSP_BOXCUT_MAXFACES 32
int bsp_boxcut(const bspdata *bsp, const struct MATRIX_f *mat, const vec3 *siz, rvface *faces, int *num_faces, int *idx_leafs);

#endif