#define _GNU_SOURCE

#include <math.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <float.h>

#include <allegro.h>

#include "compat.h"

#include "vector.h"

#ifdef VECCT_DOUBLE
#pragma message "Compiling vector lib, Double precision vecct being used"
#else
#pragma message "Compiling vector lib, Single precision vecct being used"
#endif

const vec3 vec3c_origin = {0.0, 0.0, 0.0};
//FLT_ values are used instead of double ones to fit both vecc_t
const vec3 vec3c_min = {-FLT_MAX, -FLT_MAX, -FLT_MAX};
const vec3 vec3c_max = {FLT_MAX, FLT_MAX, FLT_MAX};
const vec3 vec3c_nan = {NAN, NAN, NAN};
const vec3 vec3c_x = {1.0, 0.0, 0.0}, vec3c_y = {0.0, 1.0, 0.0}, vec3c_z = {0.0, 0.0, 1.0};
const vec3 vec3c_unit = {1.0, 1.0, 1.0};

const int idx_boxedges[12][2] = {{1,0},{2,0},{3,1},{3,2},{4,0},{5,1},{5,4},{6,2},{6,4},{7,3},{7,5},{7,6}};

float q_fisqrt(float number)
{
	//unionize!
	//the gentle scalar shall no longer suffer from the noxious greed of undefined behaviour!
	union {
		long i;
		float y;
	} foo;
	float x2;
	const float threehalfs = 1.5F;
	
	x2 = number * 0.5F;
	foo.y  = number;
	//evil floating point bit level hacking
	//now with improved constant!
	foo.i  = 0x5F375A86 - (foo.i >> 1);
	//what the fuck? 
	//1st iteration
	foo.y  = foo.y * (threehalfs - (x2 * foo.y * foo.y));
	
	return foo.y;
}

//Macros to avoid repetition and potential mistakes
#ifdef __SSE__

#define OP_ABOUT(op)\
out->x = a->x op b->x;\
out->y = a->y op b->y;\
out->z = a->z op b->z;\
out->w = a->w op b->w;

#define OP_AB(op)\
a->x op b->x;\
a->y op b->y;\
a->z op b->z;\
a->w op b->w;

#define OP_A(op)\
a->x op b;\
a->y op b;\
a->z op b;\
a->w op b;

#else

#define OP_ABOUT(op)\
out->x = a->x op b->x;\
out->y = a->y op b->y;\
out->z = a->z op b->z;

#define OP_AB(op)\
a->x op b->x;\
a->y op b->y;\
a->z op b->z;

#define OP_A(op)\
a->x op b;\
a->y op b;\
a->z op b;

#endif

void vec3_add(const vec3 *a, const vec3 *b, vec3 *out)
{
	OP_ABOUT(+)
}

void vec3_sub(const vec3 *a, const vec3 *b, vec3 *out)
{
	OP_ABOUT(-)
}

void vec3_addto(vec3 *a, const vec3 *b)
{
	OP_AB(+=)
}

void vec3_subfrom(vec3 *a, const vec3 *b)
{
	OP_AB(-=)
}

vecc_t vec3_dot(const vec3 *a, const vec3 *b)
{
	return (
		a->x * b->x +
		a->y * b->y +
		a->z * b->z
	);
}

void vec3_cross(const vec3 *a, const vec3 *b, vec3 *out)
{
	out->x = a->y * b->z - a->z * b->y;
	out->y = a->z * b->x - a->x * b->z;
	out->z = a->x * b->y - a->y * b->x;
}

vecc_t vec3_crossz(const vec3 *a, const vec3 *b)
{
	return a->x * b->y - a->y * b->x;
}

vecc_t vec3_sqlen(const vec3 *a)
{
	return (
		a->x * a->x +
		a->y * a->y +
		a->z * a->z
	);
}

vecc_t vec3_len(const vec3 *a)
{
	return sqrt(
		a->x * a->x +
		a->y * a->y +
		a->z * a->z
	);
}

vecc_t vec3_filen(const vec3 *a)
{
	return q_fisqrt(
		a->x * a->x +
		a->y * a->y +
		a->z * a->z
	);
}

void vec3_normalize(vec3 *a)
{
	vecc_t b;
	
	//Those improve not only performance, but accuracy
	//Avoiding cases of 0.99999998 etc.
	if(a->x && (!a->y) && (!a->z)){
		a->x = a->x > 0 ? 1.0 : -1.0;
		return;
	}
	if((!a->x) && a->y && (!a->z)){
		a->y = a->y > 0 ? 1.0 : -1.0;
		return;
	}
	if((!a->x) && (!a->y) && a->z){
		a->z = a->z > 0 ? 1.0 : -1.0;
		return;
	}
	
	b = vec3_len(a);
	if(!b) return;
	b = 1.0 / b;
	OP_A(*=)
}

void vec3_fnormalize(vec3 *a)
{
	float b;
	
	b = q_fisqrt(
		a->x * a->x +
		a->y * a->y +
		a->z * a->z
	);
	OP_A(*=)
}

void vec3_mult(vec3 *a, const vecc_t b)
{
	OP_A(*=)
}

void vec3_maddto(vec3 *a, const vec3 *b, const vecc_t m)
{
	a->x += b->x * m;
	a->y += b->y * m;
	a->z += b->z * m;
	#ifdef __SSE__
	a->w += b->w * m;
	#endif
}

void vec3_madd(const vec3 *a, const vec3 *b, const vecc_t m, vec3 *out)
{
	out->x = a->x + b->x * m;
	out->y = a->y + b->y * m;
	out->z = a->z + b->z * m;
	#ifdef __SSE__
	out->w = a->w + b->w * m;
	#endif
}

void vec3_normal(const vec3 *a, const vec3 *b, const vec3 *c, vec3 *out)
{
	vec3 ab = VEC3_SUB(*a, *b);
	vec3 ac = VEC3_SUB(*a, *c);
	vec3_cross(&ab, &ac, out);
	vec3_normalize(out);
}

void vec3_fnormal(const vec3 *a, const vec3 *b, const vec3 *c, vec3 *out)
{
	const vec3 ab = VEC3_SUB(*a, *b);
	const vec3 ac = VEC3_SUB(*a, *c);
	vec3_cross(&ab, &ac, out);
	vec3_fnormalize(out);
}

void vec3_sqnormal(const vec3 *a, const vec3 *b, const vec3 *c, vec3 *out)
{
	const vec3 ab = VEC3_SUB(*a, *b);
	const vec3 ac = VEC3_SUB(*a, *c);
	vec3_cross(&ab, &ac, out);
}

#define SETM(c,op) if(out->c op a->c) out->c = a->c;

void vec3_setmax(const vec3 *a, vec3 *out)
{
	SETM(x,<)
	SETM(y,<)
	SETM(z,<)
}

void vec3_setmin(const vec3 *a, vec3 *out)
{
	SETM(x,>)
	SETM(y,>)
	SETM(z,>)
}

void vec3_print(const vec3 *in)
{
	printf("%+09.6f %+09.6f %+09.6f\n", in->x, in->y, in->z);
}

void vec3_swap(vec3 *a, vec3 *b)
{
	vec3 tmp = *a;
	*a = *b;
	*b = tmp;
}

int vec3_equal(const vec3 *a, const vec3 *b)
{
	if(fabs(a->x - b->x) > VECCT_EPSILON) return 0;
	if(fabs(a->y - b->y) > VECCT_EPSILON) return 0;
	if(fabs(a->z - b->z) > VECCT_EPSILON) return 0;
	return 1;
}

void vec3_bounds(const vec3 *verts, const int nverts, vec3 *min, vec3 *max)
{
	int i;
	*min = vec3c_max;
	*max = vec3c_min;
	
	for(i = 0; i < nverts; i++){
		vec3_setmax(verts + i, max);
		vec3_setmin(verts + i, min);
	}
}

void vec3_interp(const vec3 *v1, const vec3 *v2, vecc_t val, vec3 *out)
{
	out->x = v1->x + (v2->x - v1->x) * val;
	out->y = v1->y + (v2->y - v1->y) * val;
	out->z = v1->z + (v2->z - v1->z) * val;
	#ifdef __SSE__
	out->w = v1->w + (v2->w - v1->w) * val;
	#endif
}

void vec3_slerp(const vec3 *a, const vec3 *b, vecc_t i, vec3 *out)
{
	vec3 ab = VEC3_SUB(*b, *a);
	vecc_t dot = vec3_dot(a, b);
	const vecc_t DOT_THRESHOLD = 0.9995;
    if(dot > DOT_THRESHOLD){
		// If the inputs are too close for comfort, linearly interpolate
		// and normalize the result.
		vec3_madd(a, &ab, i, out);
		vec3_normalize(out);
		return;
    }
	// Robustness: Stay within domain of acos()
	if(dot < -1) dot = -1;
	if(dot > 1) dot = 1;
	// theta_0 = angle between input vectors
    vecc_t theta_0 = acos(dot);
	// theta = angle between v0 and result
    vecc_t theta = theta_0 * i;
	
    //Quaternion v2 = v1 – v0*dot;
	vec3_madd(b, a, -dot, out);
	// { v0, v2 } is now an orthonormal basis
    vec3_normalize(out);
    //return v0*cos(theta) + v2*sin(theta);
	vec3_mult(out, sin(theta));
	vec3_maddto(out, a, cos(theta));
}

//Based on: https://zeux.io/2015/07/23/approximating-slerp/
//(archive: https://web.archive.org/web/20200509130606/https://zeux.io/2015/07/23/approximating-slerp/)
void vec3_fslerp(const vec3 *a, const vec3 *b, vecc_t t, vec3 *out)
{
	const float dot = vec3_dot(a, b);
	
	const float d = fabsf(dot);
    const float A = 1.0904f + d * (-3.2452f + d * (3.55645f - d * 1.43519f));
    const float B = 0.848013f + d * (-1.06021f + d * 0.215638f);
    const float k = A * (t - 0.5f) * (t - 0.5f) + B;
    const float ot = t + t * (t - 0.5f) * (t - 1) * k;
	const float lt = 1.0 - ot;
	const float rt = dot > 0 ? ot : -ot;
	
	out->x = a->x * lt + b->x * rt;
	out->y = a->y * lt + b->y * rt;
	out->z = a->z * lt + b->z * rt;
	#ifdef __SSE__
	out->w = a->w * lt + b->w * rt;
	#endif
	vec3_fnormalize(out);
}

void vec3_lineclosest(const vec3 *a, const vec3 *b, const vec3 *c, vec3 *d)
{
	const vec3 ab = VEC3_SUB(*b, *a);
	const vec3 ac = VEC3_SUB(*c, *a);
	// Project c onto ab, but deferring divide by Dot(ab, ab)
	vecc_t t = vec3_dot(&ac, &ab);
	if(t <= 0.0f){
		// c projects outside the [a,b] interval, on the a side; clamp to a
		*d = *a;
	}
	else{
		// Always nonnegative since denom = ||ab|| ∧ 2
		vecc_t denom = vec3_dot(&ab, &ab);
		if(t >= denom){
			// c projects outside the [a,b] interval, on the b side; clamp to b
			*d = *b;
		}
		else{
			// c projects inside the [a,b] interval; must do deferred divide now
			t = t / denom;
			vec3_madd(a, &ab, t, d);
		}
	}
}

void vec3_verts2plane(const vec3 *a, const vec3 *b, const vec3 *c, mplane *out)
{
	vec3_normal(a, b, c, &out->normal);
	out->dist = vec3_dot(a, &out->normal);
}

void vec3_point2plane(const vec3 *p, const vec3 *norm, mplane *out)
{
	out->normal = *norm;
	out->dist = vec3_dot(p, &out->normal);
}

int vec3_planecmp(const mplane *pl, const vec3 *p)
{
	//could use x87 FCOMPP
	return vec3_dot(p, &pl->normal) - pl->dist > 0;
}

vecc_t vec3_dist2plane(const mplane *pl, const vec3 *p)
{
	return vec3_dot(p, &pl->normal) - pl->dist;
}

vecc_t vec3_ray2plane(const mplane *p, const vec3 *start, const vec3 *dir, vec3 *res)
{
	vecc_t dist = vec3_dist2plane(p, start) / (-vec3_dot(&p->normal, dir));
	
	if(res){
		vec3_madd(start, dir, dist, res);
	}
	return dist;
}

int vec3_segment2plane(const mplane *p, const vec3 *p1, const vec3 *p2, vec3 *res, vecc_t *dist)
{
	vec3 dir = VEC3_SUB(*p2, *p1);
	vecc_t d;
	if(!dist) dist = &d;
	*dist = vec3_ray2plane(p, p1, &dir, res);
	
	if(*dist >= 1.0) return 1;
	if(*dist <= 0.0) return -1;
	return 0;
}

int mplane_equal(const mplane *p1, const mplane *p2)
{
	//Looser limits in the distance compare, found fundamental to avoid spurious leafs
	if(fabs(p1->dist - p2->dist) > MPLANE_TOLERANCE) return 0;
	if(fabs(vec3_dot(&p1->normal, &p2->normal) - 1.0) > VECCT_EPSILON) return 0;
	return 1;
}

void mplane_flip(mplane *pl)
{
	pl->dist *= -1;
	pl->normal.x *= -1;
	pl->normal.y *= -1;
	pl->normal.z *= -1;
	#ifdef __SSE__
	pl->normal.w *= -1;
	#endif
}

int mplane_cutbox_mm(const mplane *p, const vec3 *box_min, const vec3 *box_max, vec3 *new_box_min, vec3 *new_box_max)
{
	const vec3 box_verts[8] = VEC3_BOXVERTS_MM(*box_min, *box_max);
	vec3 new_box_verts[2][16];
	int new_box_nverts[2] = {0};
	int i;
	
	//check all 8 verts
	for(i = 0; i < 8; i++){
		//add to correct box
		const int cmp = vec3_planecmp(p, box_verts + i);
		new_box_verts[cmp][new_box_nverts[cmp]++] = box_verts[i];
	}
	
	if(!new_box_nverts[0] || !new_box_nverts[1]) return 0;
	if(!(new_box_min && new_box_max)) return 1;
	
	//test all segments
	for(i = 0; i < 12; i++){
		const vec3 *p1 = box_verts + idx_boxedges[i][0];
		const vec3 *p2 = box_verts + idx_boxedges[i][1];
		vec3 mid;
		if(!vec3_segment2plane(p, p1, p2, &mid, 0)){
				//add to both boxes
				new_box_verts[0][new_box_nverts[0]++] = mid;
				new_box_verts[1][new_box_nverts[1]++] = mid;
		}
	}
	
	vec3_bounds(new_box_verts[0], new_box_nverts[0], new_box_min + 0, new_box_max + 0);
	vec3_bounds(new_box_verts[1], new_box_nverts[1], new_box_min + 1, new_box_max + 1);
	
	return 1;
}

int mplane_cutbox_sc(const mplane *p, const vec3 *box_siz, const vec3 *box_pos, vec3 *new_box_siz, vec3 *new_box_pos)
{
	const vec3 box_verts[8] = VEC3_BOXVERTS_SC(*box_siz, *box_pos);
	vec3 new_box_verts[2][16];
	int new_box_nverts[2] = {0};
	int i;
	vec3 new_box_min[2], new_box_max[2];
	
	//check all 8 verts
	for(i = 0; i < 8; i++){
		//add to correct box
		const int cmp = vec3_planecmp(p, box_verts + i);
		new_box_verts[cmp][new_box_nverts[cmp]++] = box_verts[i];
	}
	
	if(new_box_nverts[0] == new_box_nverts[1]) return 0;
	if(!(new_box_siz && new_box_pos)) return 1;
	
	//test all segments
	for(i = 0; i < 12; i++){
		const vec3 *p1 = box_verts + idx_boxedges[i][0];
		const vec3 *p2 = box_verts + idx_boxedges[i][1];
		vec3 mid;
		if(!vec3_segment2plane(p, p1, p2, &mid, 0)){
				//add to both boxes
				new_box_verts[0][new_box_nverts[0]++] = mid;
				new_box_verts[1][new_box_nverts[1]++] = mid;
		}
	}
	
	vec3_bounds(new_box_verts[0], new_box_nverts[0], new_box_min + 0, new_box_max + 0);
	vec3_bounds(new_box_verts[1], new_box_nverts[1], new_box_min + 1, new_box_max + 1);
	
	const vec3 VEC3_MM2SC(new_box_min[0], new_box_max[0], new_box_siz0, new_box_pos0),
		VEC3_MM2SC(new_box_min[1], new_box_max[1], new_box_siz1, new_box_pos1);
	new_box_siz[0] = new_box_siz0;
	new_box_siz[1] = new_box_siz1;
	new_box_pos[0] = new_box_pos0;
	new_box_pos[1] = new_box_pos1;
	
	return 1;
}

void mplane_closest(const mplane *pl, const vec3 *in, vec3 *out)
{
	vecc_t t = vec3_dot(&pl->normal, in) - pl->dist;
	vec3_madd(in, &pl->normal, t, out);
}

// Intersect ray R(t) = p + t*d against AABB a. When intersecting,
// return intersection distance tmin and point q of intersection
int vec3_intersectrayaabb_mm(const vec3 *p, const vec3 *d, const vec3 *amin, const vec3 *amax, double *tmin, vec3 *q)
{
	*tmin = 0.0f; // set to -FLT_MAX to get first hit on line
	double tmax = FLT_MAX; // set to max distance ray can travel (for segment)
	
	const vec3u pu = {*p}, du = {*d}, aminu = {*amin}, amaxu = {*amax};
	
	// For all three slabs
	int i;
	for(i = 0; i < 3; i++){
		if(fabs(du.c[i]) < VECCT_EPSILON){
			// Ray is parallel to slab. No hit if origin not within slab
			if(pu.c[i] < aminu.c[i] || pu.c[i] > amaxu.c[i]){
				return 0;
			}
		}
		else{
			// Compute intersection t value of ray with near and far plane of slab
			double ood = 1.0f / du.c[i];
			double t1 = (aminu.c[i] - pu.c[i]) * ood;
			double t2 = (amaxu.c[i] - pu.c[i]) * ood;
			// Make t1 be intersection with near plane, t2 with far plane
			if(t1 > t2){
				double tmp = t1;
				t1 = t2;
				t2 = tmp;
			}
			// Compute the intersection of slab intersection intervals
			if(t1 > *tmin){
				*tmin = t1;
			}
			if(t2 < tmax){
				tmax = t2;
			}
			// Exit with no collision as soon as slab intersection becomes empty
			if(*tmin > tmax){
				return 0;
			}
		}
	}
	// Ray intersects all 3 slabs. Return point (q) and intersection t value (tmin)
	vec3_madd(p, d, *tmin, q);
	return 1;
}

// Test if segment specified by points p0 and p1 intersects AABB b
int vec3_testsegmentaabb_mm(const vec3 *p0, const vec3 *p1, const vec3 *bmin, const vec3 *bmax)
{
	//Point c = (bmin + bmax) * 0.5f; // Box center-point
	//Vector e = bmax - c; // Box halflength extents
	const vec3 VEC3_MM2SC(*bmin, *bmax, e, c);
	
	vec3 m = VEC3_MID(*p0, *p1); // Segment midpoint
	vec3 d = VEC3_SUB(*p1, m); // Segment halflength vector
	vec3_subfrom(&m, &c); // Translate box and segment to origin
	// Try world coordinate axes as separating axes
	double adx = fabs(d.x);
	if(fabs(m.x) > e.x + adx + MPLANE_TOLERANCE){
		//printf("%d %0.9f %0.9f\n", __LINE__, fabs(m.x), e.x + adx);
		return 0;
	}
	double ady = fabs(d.y);
	if(fabs(m.y) > e.y + ady + MPLANE_TOLERANCE){
		//printf("%d %0.9f %0.9f\n", __LINE__, fabs(m.y), e.y + ady);
		return 0;
	}
	double adz = fabs(d.z);
	if(fabs(m.z) > e.z + adz + MPLANE_TOLERANCE){
		//printf("%d %0.9f %0.9f\n", __LINE__, fabs(m.z), e.z + adz);
		return 0;
	}
	// Add in an epsilon term to counteract arithmetic errors when segment is
	// (near) parallel to a coordinate axis (see text for detail)
	adx += VECCT_EPSILON; ady += VECCT_EPSILON; adz += VECCT_EPSILON;
	// Try cross products of segment direction vector with coordinate axes
	if(fabs(m.y * d.z - m.z * d.y) > e.y * adz + e.z * ady + MPLANE_TOLERANCE){
		//printf("%d %0.9f %0.9f\n", __LINE__, fabs(m.y * d.z - m.z * d.y), e.y * adz + e.z * ady);
		return 0;
	}
	if(fabs(m.z * d.x - m.x * d.z) > e.x * adz + e.z * adx + MPLANE_TOLERANCE){
		//printf("%d %0.9f %0.9f\n", __LINE__, fabs(m.z * d.x - m.x * d.z), e.x * adz + e.z * adx);
		return 0;
	}
	if(fabs(m.x * d.y - m.y * d.x) > e.x * ady + e.y * adx + MPLANE_TOLERANCE){
		//printf("%d %0.9f %0.9f\n", __LINE__, fabs(m.x * d.y - m.y * d.x), e.x * ady + e.y * adx);
		return 0;
	}
	// No separating axis found; segment must be overlapping AABB
	return 1;
}

vecc_t vec3_ray2box_s(const vec3 *start, const vec3 *dir, const vec3 *box_s)
{
	double tmin;
	vec3 q;
	const vec3 VEC3_SC2MM(*box_s, vec3c_origin, amin, amax);
	if(!vec3_intersectrayaabb_mm(start, dir, &amin, &amax, &tmin, &q)){
		tmin = FLT_MAX;
	}
	return tmin;
}

/*
int vec3_seg2box_mm(const vec3 *start, const vec3 *end, const vec3 *box_min, const vec3 *box_max, vec3 *new_start, vec3 *new_end)
{
}
*/

int vec3_box2box_sc(const vec3 *siza, const vec3 *posa, const vec3 *sizb, const vec3 *posb, vec3 *push)
{
	const vec3 VEC3_SC2MM(*siza, *posa, mina, maxa), VEC3_SC2MM(*sizb, *posb, minb, maxb);
	if(
		(maxa.x < minb.x) &&
		(maxa.y < minb.y) &&
		(maxa.z < minb.z)
	){
		return 0;
	}
	if(
		(maxb.x < mina.x) &&
		(maxb.y < mina.y) &&
		(maxb.z < mina.z)
	){
		return 0;
	}
	assert(!push);
	return 1;
}

int vec3_box2box_mm(const vec3 *mina, const vec3 *maxa, const vec3 *minb, const vec3 *maxb, vec3 *push)
{
	if(
		(maxa->x < minb->x) ||
		(maxa->y < minb->y) ||
		(maxa->z < minb->z)
	){
		return 0;
	}
	if(
		(maxb->x < mina->x) ||
		(maxb->y < mina->y) ||
		(maxb->z < mina->z)
	){
		return 0;
	}
	assert(!push);
	return 1;
}

void rvert_interp(const rvert *v1, const rvert *v2, rvert *out, const vecc_t val)
{
	out->u = v1->u + (v2->u - v1->u) * val;
	out->v = v1->v + (v2->v - v1->v) * val;
}

void rvface_flip(rvface *face)
{
	rvert tmp;
	for(int i = 0; i < face->num_verts / 2; i++){
		tmp = face->verts[i];
		face->verts[i] = face->verts[face->num_verts - i - 1];
		face->verts[face->num_verts - i - 1] = tmp;
	}
}

void rvface_scale(rvface *face, float s)
{
	int j;
	rvert midp;
	rvface_midpoint_area(face, &midp, 0);
	
	for(j = 0; j < face->num_verts; j++){
		const vec3 to = VEC3_SUB(midp.pos, face->verts[j].pos);
		vec3_maddto(&face->verts[j].pos, &to, 1.0 - s);
	}
}

//Just a point inside, not the centre of mass
void rvface_midpoint(const rvface *face, vec3 *out)
{
	int i;
	
	out->x = 0;
	out->y = 0;
	out->z = 0;
	
	for(i = 0; i < face->num_verts; i++){
		vec3_addto(out, &face->verts[i].pos);
	}
	vec3_mult(out, 1.0 / (double) face->num_verts);
}

vecc_t rvface_area(const rvface *f)
{
	const rvert *verts = f->verts;
	const int num_verts = f->num_verts;
	
	vecc_t area = 0;
	if(num_verts > 3){
		int i;
		rvface tmp;
		tmp.verts[0] = verts[0];
		tmp.num_verts = 3;
		for(i = 1; i < num_verts - 1; i++){
			tmp.verts[1] = verts[i];
			tmp.verts[2] = verts[i + 1];
			area += rvface_area(&tmp);
		}
		return area;
	}
	else{
		vec3 a = verts[0].pos;
		vec3 b = verts[1].pos;
		vec3 c = verts[2].pos;
		vec3 ab = VEC3_SUB(a, b);
		vec3 bc = VEC3_SUB(b, c);
		vec3 ac = VEC3_SUB(a, c);
		
		if(vec3_len(&ab) == 0 || vec3_len(&bc) == 0 || vec3_len(&ac) == 0){
			return 0.0;
		}
		else{
			vecc_t base = vec3_len(&ab), height;
			vec3 normal, heightv;
			vec3_normal(&a, &b, &c, &normal);
			vec3_cross(&normal, &ab, &heightv);
			vec3_normalize(&heightv);
			height = vec3_dot(&heightv, &ac);
			//assert(height >= 0.0);
			return base * height * 0.5;
		}
	}
}
int rvface2d_zcheck(const rvface *face)
{
	int j;
	for(j = 0; j < face->num_verts; j++){
		const float vz = face->verts[j].pos.z;
		if(vz < MPLANE_TOLERANCE || vz > -MPLANE_TOLERANCE) return 0;
	}
	return 1;
}

//Directly from Graphics Gems IV
void rvface2d_midpoint_area(const rvface *f, vec3 *midp, vecc_t *area)
{
	int i, j;
	double ai, atmp = 0, xtmp = 0, ytmp = 0;
	const int n = f->num_verts;
	#define X(idx) (f->verts[idx].pos.x)
	#define Y(idx) (f->verts[idx].pos.y)
	for(i = n - 1, j = 0; j < n; i = j, j++){
		ai = X(i) * Y(j) - X(j) * Y(i);
		atmp += ai;
		xtmp += (X(j) + X(i)) * ai;
		ytmp += (Y(j) + Y(i)) * ai;
	}
	if(area){
		*area = abs(atmp * 0.5);
	}
	if(atmp && midp){
		midp->x = xtmp / (3.0 * atmp);
		midp->y = ytmp / (3.0 * atmp);
	}
	#undef X
	#undef Y
}

void rvface_midpoint_area(const rvface *face, rvert *midp, vecc_t *area)
{
	int j;
	//Calculate a 2D face from the given one
	rvface face2d = *face;
	MATRIX_f mat;
	matrix_f_fromrvface(face, &mat);
	matrix_f_invert(&mat);
	rvface_apply_matrix_f(&face2d, &mat);
	//TODO: assert
	//assert(rvface2d_zcheck(&face2d));
	
	//Calulate midpoint in 2d face
	vec3 midp2d = {0, 0, 0};
	rvface2d_midpoint_area(&face2d, &midp2d, area);
	
	//Find midpoint in 3d face from the above
	rvert midp3d;
	rvface_interp2(&face2d, face, &midp2d, &midp3d);
	
	if(midp) *midp = midp3d;
}

int rvface_isconvex(const rvface *f)
{
	const rvert *verts = f->verts;
	const int num_verts = f->num_verts;
	
	int i;
	vec3 normal;
	vec3_normal(
		&verts[0].pos,
		&verts[1].pos,
		&verts[2].pos,
		&normal
	);
	
	for(i = 0; i < num_verts + 2; i++){
		const vec3 *a = &verts[(i + 0) % num_verts].pos;
		const vec3 *b = &verts[(i + 1) % num_verts].pos;
		const vec3 *c = &verts[(i + 2) % num_verts].pos;
		vec3 ab = VEC3_SUB(*a, *b);
		vec3 ac = VEC3_SUB(*b, *c);
		vec3 cross = VEC3_CROSS(normal, ab);
		if(vec3_dot(&cross, &ac) < 0) return 0;
	}
	return 1;
}

int rvface_isplanar(const rvface *f)
{
	const rvert *verts = f->verts;
	const int num_verts = f->num_verts;
	
	int i;
	mplane p;
	vec3_verts2plane(
		&verts[0].pos,
		&verts[1].pos,
		&verts[2].pos,
		&p
	);
	
	for(i = 0; i < num_verts; i++){
		if(fabs(vec3_dist2plane(&p, &verts[i].pos)) > MPLANE_TOLERANCE){
			//printf("%d ------------------------   %0.9f\n", i, vec3_dist2plane(&p, &verts[i].pos));
			return 0;
		}
	}
	return 1;
}

int rvface_badsegs(const rvface *f)
{
	int i;
	for(i = 0; i < f->num_verts; i++){
		const int n = (i + 1) % f->num_verts;
		const vec3 ab = VEC3_SUB(f->verts[i].pos, f->verts[n].pos);
		if(vec3_sqlen(&ab) <= 0.0) return 1;
	}
	return 0;
}

int rvface_hasnan(const rvface *f)
{
	int i;
	for(i = 0; i < f->num_verts; i++){
		if(f->verts[i].pos.x != f->verts[i].pos.x) return 1;
		if(f->verts[i].pos.y != f->verts[i].pos.y) return 1;
		if(f->verts[i].pos.z != f->verts[i].pos.z) return 1;
	}
	return 0;
}

int rvface_isbad(const rvface *f)
{
	const rvert *v = f->verts;
	const int n = f->num_verts;
	
	vec3 a, b, c, ab, bc;
	
	if(rvface_hasnan(f)) return 7;
	if(n < 3) return 1;
	a = v[0].pos;
	b = v[1].pos;
	c = v[2].pos;
	vec3_sub(&b, &a, &ab);
	vec3_sub(&c, &b, &bc);
	if((!vec3_sqlen(&ab)) || (!vec3_sqlen(&bc))) return 2;
	if(!rvface_isplanar(f)) return 3;
	if(!rvface_isconvex(f)) return 4;
	if(rvface_badsegs(f)) return 6;
	if(!rvface_area(f)) return 5;
	return 0;
}

void rvface_print(const rvface *f)
{
	const rvert *verts = f->verts;
	const int num_verts = f->num_verts;
	
	int i;
	for(i = 0; i < num_verts; i++){
		printf("%0.9f %0.9f %0.9f\n", verts[i].pos.x,  verts[i].pos.y,  verts[i].pos.z);
	}
}

void rvface_split(const mplane *p, const rvface *face, rvface *front_face, rvface *back_face)
{
	int i;
	
	*front_face = *face;
	*back_face = *face;
	front_face->num_verts = 0;
	back_face->num_verts = 0;
	
	for(i = 0; i < face->num_verts; i++){
		const rvert *rva = &face->verts[i];
		const rvert *rvb = &face->verts[(i + 1) % face->num_verts];
		const vec3 *va = &rva->pos;
		const vec3 *vb = &rvb->pos;
		vecc_t da = vec3_dist2plane(p, va);
		vecc_t db = vec3_dist2plane(p, vb);
		rvert newrv;
		vecc_t ratio;
		
		switch(VECCT_SIGN(da)){
			case 1:
				assert(front_face->num_verts < RVFACE_MAX_VERTS);
				front_face->verts[front_face->num_verts++] = *rva;
				break;
			case -1:
				assert(back_face->num_verts < RVFACE_MAX_VERTS);
				back_face->verts[back_face->num_verts++] = *rva;
				break;
			case 0:
				assert(front_face->num_verts < RVFACE_MAX_VERTS);
				assert(back_face->num_verts < RVFACE_MAX_VERTS);
				front_face->verts[front_face->num_verts++] = *rva;
				back_face->verts[back_face->num_verts++] = *rva;
				break;
		}
		
		if(
			(VECCT_SIGN(da) == 0) ||
			(VECCT_SIGN(db) == 0) || 
			(VECCT_SIGN(da) == VECCT_SIGN(db))
		) continue;
		
		vec3_segment2plane(p, va, vb, &newrv.pos, &ratio);
		rvert_interp(rva, rvb, &newrv, ratio);
		
		assert(front_face->num_verts < RVFACE_MAX_VERTS);
		assert(back_face->num_verts < RVFACE_MAX_VERTS);
		
		front_face->verts[front_face->num_verts++] = newrv;
		back_face->verts[back_face->num_verts++] = newrv;
	}
}

int rvface_sort(const mplane *p, const rvface *face)
{
	int i;
	int all_front = 1;
	int all_back = 1;
	int all_planar = 1;
	
	for(i = 0; i < face->num_verts; i++){
		vecc_t d = vec3_dist2plane(p, &face->verts[i].pos);
		if(d >= MPLANE_TOLERANCE){
			all_planar = 0;
			all_back = 0;
		}
		if(d <= -MPLANE_TOLERANCE){
			all_planar = 0;
			all_front = 0;
		}
	}
	
	if(all_planar) return RVFACE_COPLANAR;
	if(all_front) return RVFACE_FRONT;
	if(all_back) return RVFACE_BACK;
	return RVFACE_SPANNING;
}

//Function can't handle all cases
void rvface_sortverts(rvface *face)
{
	vec3 midp;
	int i, kek = 0;
	
	//Assure function is not actually used
	assert((printf("Function not well implemented\n"), 0));
	
	rvface_midpoint(face, &midp);
	
	for(i = 0; i < face->num_verts + 1; i++){
		vec3 *a = &face->verts[(i + 0) % face->num_verts].pos;
		vec3 *b = &face->verts[(i + 1) % face->num_verts].pos;
		vec3 toa = VEC3_SUB(*a, midp);
		vec3 tob = VEC3_SUB(*b, midp);
		vecc_t d = vec3_crossz(&toa, &tob);
		
		//Avoid dumb fused-multiply-add caused error
		if(VECCT_SIGN(d) < 0){
			vec3 tmp = *a;
			*a = *b;
			*b = tmp;
			kek++;
			if(kek >= 9999){
				printf("KEK limit reached %d %0.18f\n", i, d);
				//vec3_print(a);
			//	vec3_print(b);
				vec3_print(&toa);
				vec3_print(&tob);
				rvface_print(face);
				
				exit(0);
			}
		}
	}
}

//Given the faces of an arbitrarily positioned, sized, oriented box, calculate the axes, size and centre of the box
int rvfaces_to_obb(rvface *faces, int num_faces, struct vec3 (*boxes)[5])
{
	int j, k, l;
	int nset = 0;
	
	do{
		vec3 *box_axes = boxes[0];
		vec3 *box_size = boxes[0] + 3;
		vec3 *box_cntr = boxes[0] + 4;
		mplane me_planes[6];
		float u[6] = {0}, v[6] = {0}, minu = FLT_MAX, minv = FLT_MAX, maxu = FLT_MIN;
		int planes_found = 0;
		int xp = 0, yp = 0, zp = 0;
		int pairs[6];
		
		//iterate over all faces
		for(j = 0; j <= num_faces; j++){
			mplane p1;
			
			//build face plane
			vec3_verts2plane(
				&faces[j].verts[0].pos,
				&faces[j].verts[1].pos,
				&faces[j].verts[2].pos,
				&p1
			);
			
			//check already built planes
			for(k = 0; k < planes_found; k++){
				if(mplane_equal(&p1, me_planes + k)){
					break;
				}
			}
			if(k == planes_found){
				//fail for submeshes with >6 face planes (not hexahedral)
				if(planes_found == 6) break;
				
				//add plane to list, plus store UVs midpoint
				for(l = 0; l < faces[j].num_verts; l++){
					u[planes_found] += faces[j].verts[l].u / ((float) faces[j].num_verts);
					v[planes_found] += faces[j].verts[l].v / ((float) faces[j].num_verts);
				}
				me_planes[planes_found] = p1;
				planes_found++;
			}
		}
		if(j <= num_faces || planes_found < 6){
			//fail for non-hexahedral submeshes
			continue;
		}
		
		//check that the 6 planes are 3 pairs of opposite facing planes
		//i.e. shape is a rhombohedron
		for(j = 0; j < 6; j++){
			int matches = 0;
			for(k = 0; k < 6; k++){
				const vec3 s = VEC3_ADD(me_planes[j].normal, me_planes[k].normal);
				if(k == j) continue;
				if(vec3_sqlen(&s) > MPLANE_TOLERANCE) continue;
				pairs[j] = k;
				matches++;
			}
			if(matches != 1) break;
		}
		if(j != 6) continue;
		
		//allocate axes, trying UVs of faces as a hint first
		for(j = 0; j < 6; j++){
			if(u[j] < minu) minu = u[j];
			if(u[j] > maxu) maxu = u[j];
			if(v[j] < minv) minv = v[j];
		}
		for(j = 0; j < 6; j++){
			if(fabs(u[j] - minu) < 0.05){
				if(fabs(v[j] - minv) < 0.05 && !xp) xp = j + 1;
			}
			else if(fabs(u[j] - maxu) < 0.05){
				if(fabs(v[j] - minv) < 0.05 && !zp) zp = j + 1;
			}
			else{
				if(fabs(v[j] - minv) < 0.05 && !yp) yp = j + 1;
			}
		}
		if(!xp || !yp || !zp || (xp--, yp--, zp--, xp == yp || zp == yp || pairs[xp] == yp || pairs[zp] == yp)){
			//reallocate axes in straightforward manner
			xp = 0;
			yp = xp + 1;
			if(yp == pairs[xp]) yp++;
			zp = yp + 1;
			while(zp == pairs[xp] || zp == pairs[yp]) zp++;
		}
		
		//check that axes are orthogonal i.e. shape is cuboid
		if((fabs(vec3_dot(&me_planes[xp].normal, &me_planes[yp].normal)) > MPLANE_TOLERANCE) ||
			(fabs(vec3_dot(&me_planes[yp].normal, &me_planes[zp].normal)) > MPLANE_TOLERANCE)){
			continue;
		}
		
		//find axes, size, and centre of box
		box_axes[0] = me_planes[xp].normal;
		box_axes[1] = me_planes[yp].normal;
		box_axes[2] = me_planes[zp].normal;
		vec3_mult(box_axes + 2, -1.0);
		box_size->x = fabs(me_planes[xp].dist + me_planes[pairs[xp]].dist) * 0.5;
		box_size->y = fabs(me_planes[yp].dist + me_planes[pairs[yp]].dist) * 0.5;
		box_size->z = fabs(me_planes[zp].dist + me_planes[pairs[zp]].dist) * 0.5;
		const mplane cntr_planes[3] = {
			{me_planes[xp].normal, (me_planes[xp].dist - me_planes[pairs[xp]].dist) * 0.5},
			{me_planes[yp].normal, (me_planes[yp].dist - me_planes[pairs[yp]].dist) * 0.5},
			{me_planes[zp].normal, (me_planes[zp].dist - me_planes[pairs[zp]].dist) * 0.5}
		};
		mplane_intersect3(cntr_planes + 0, cntr_planes + 1, cntr_planes + 2, box_cntr);
		
		nset++;
	} while(0);
	
	return nset;
}

static void vec3_triclosest(const vec3 *p, const vec3 *a, const vec3 *b, const vec3 *c, vec3 *out)
{
	// Check if P in vertex region outside A
	const vec3 ab = VEC3_SUB(*b, *a);
	const vec3 ac = VEC3_SUB(*c, *a);
	const vec3 ap = VEC3_SUB(*p, *a);
	float d1 = vec3_dot(&ab, &ap);
	float d2 = vec3_dot(&ac, &ap);
	if(d1 <= 0.0f && d2 <= 0.0f){
		// barycentric coordinates (1,0,0)
		*out = *a;
		return;
	}
	// Check if P in vertex region outside B
	const vec3 bp = VEC3_SUB(*p, *b);
	float d3 = vec3_dot(&ab, &bp);
	float d4 = vec3_dot(&ac, &bp);
	if(d3 >= 0.0f && d4 <= d3){
		// barycentric coordinates (0,1,0)
		*out = *b;
		return;
	}
	// Check if P in edge region of AB, if so return projection of P onto AB
	float vc = d1*d4 - d3*d2;
	if(vc <= 0.0f && d1 >= 0.0f && d3 <= 0.0f){
		float v = d1 / (d1 - d3);
		// barycentric coordinates (1-v,v,0)
		vec3_madd(a, &ab, v, out);
		return;
	}
	// Check if P in vertex region outside C
	const vec3 cp = VEC3_SUB(*p, *c);
	float d5 = vec3_dot(&ab, &cp);
	float d6 = vec3_dot(&ac, &cp);
	if(d6 >= 0.0f && d5 <= d6){
		// barycentric coordinates (0,0,1)
		*out = *c;
		return;
	}
	// Check if P in edge region of AC, if so return projection of P onto AC
	float vb = d5*d2 - d1*d6;
	if(vb <= 0.0f && d2 >= 0.0f && d6 <= 0.0f){
		float w = d2 / (d2 - d6);
		// barycentric coordinates (1-w,0,w)
		vec3_madd(a, &ac, w, out);
		return;
	}
	// Check if P in edge region of BC, if so return projection of P onto BC
	float va = d3*d6 - d5*d4;
	if(va <= 0.0f && (d4 - d3) >= 0.0f && (d5 - d6) >= 0.0f){
		float w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
		// barycentric coordinates (0,1-w,w)
		const vec3 cb = VEC3_SUB(*c, *b);
		vec3_madd(b, &cb, w, out);
		return; 
	}
	// P inside face region. Compute Q through its barycentric coordinates (u,v,w)
	float denom = 1.0f / (va + vb + vc);
	float v = vb * denom;
	float w = vc * denom;
	// = u*a + v*b + w*c, u = va * denom = 1.0f - v - w
	vec3_madd(a, &ab, v, out);
	vec3_maddto(out, &ac, w); 
}

void rvface_closest(const rvface *face, const vec3 *in, vec3 *out)
{
	vecc_t min_dist = FLT_MAX;
	int i;
	
	for(i = 1; i < face->num_verts - 1; i++){
		vec3 closest;
		vec3_triclosest(in, &face->verts[0].pos, &face->verts[i].pos, &face->verts[i + 1].pos, &closest);
		vecc_t dist = VEC3_SQDIST(closest, *in);
		if(dist < min_dist){
			min_dist = dist;
			*out = closest;
		}
	}
}

int mplane_intersect3(const mplane *p1, const mplane *p2, const mplane *p3, vec3 *out)
{
	const vec3 m1 = {p1->normal.x, p2->normal.x, p3->normal.x};
	const vec3 m2 = {p1->normal.y, p2->normal.y, p3->normal.y};
	const vec3 m3 = {p1->normal.z, p2->normal.z, p3->normal.z};
	const vec3 u = VEC3_CROSS(m2, m3);
	double denom = vec3_dot(&m1, &u);
	const vec3 d = {p1->dist, p2->dist, p3->dist};
	const vec3 v = VEC3_CROSS(m1, d);
	if(fabs(denom) < VECCT_EPSILON) return 0; // Planes do not intersect in a point
	double ood = 1.0f / denom;
	out->x = vec3_dot(&d, &u);
	out->y = vec3_dot(&m3, &v);
	out->z = -vec3_dot(&m2, &v);
	vec3_mult(out, ood);
	return 1;
}

/*
This function works in the following manner:
-Find any 4 planes with which splitting plane is not parallel
-Always generate a quad with those 4 planes as sides
-Split quad using rvface_split and all other planes
*/
void rvface_boxslice(const mplane *plane, const vec3 *min, const vec3 *max, rvface *out)
{
	//Planes for the box
	//Note max_planes don't point inside the box
	mplane min_planes[3] = {
		{{1, 0, 0}, min->x},
		{{0, 1, 0}, min->y},
		{{0, 0, 1}, min->z}
	};
	mplane max_planes[3] = {
		{{1, 0, 0}, max->x},
		{{0, 1, 0}, max->y},
		{{0, 0, 1}, max->z}
	};
	vec3 crosses[3] = {
		VEC3_CROSS(min_planes[0].normal, plane->normal),
		VEC3_CROSS(min_planes[1].normal, plane->normal),
		VEC3_CROSS(min_planes[2].normal, plane->normal)
	};
	vecc_t lens[3] = {
		vec3_sqlen(&crosses[0]),
		vec3_sqlen(&crosses[1]),
		vec3_sqlen(&crosses[2])
	};
	int plane_a = -1, plane_b = -1, plane_c = -1, i, findc = 1;
	vec3 tmp, origin = {0.0};
	rvface backface, frontface;
	
	out->num_verts = 0;
	
	//Pick two planes that aren't parallel with the splitter
	for(i = 0; i < 3; i++){
		if(lens[i] > VECCT_EPSILON){
			if(plane_a < 0) plane_a = i;
			else if(plane_b < 0){
				plane_b = i;
				break;
			}
		}
		else plane_c = i;
	}
	if(i == 3) return;
	if(plane_c < 0) plane_c = 2;
	
	if(!vec3_dot(&min_planes[plane_c].normal, &plane->normal)){
		if(lens[plane_c]){
			if(vec3_dot(&min_planes[plane_a].normal, &plane->normal)){
				int tmp = plane_a;
				plane_a = plane_c;
				plane_c = tmp;
			}
			else if(vec3_dot(&min_planes[plane_b].normal, &plane->normal)){
				int tmp = plane_b;
				plane_b = plane_c;
				plane_c = tmp;
			}
		}
		else{
			findc = 0;
			printf("No good plane combination found\n");
		}
	}
	
	out->num_verts = 4;
	
	/*
	mplane is such that
	out->dist = vec3_dot(a, &out->normal);
	i.e.
	Eq 1: Px * Nx + Py * Ny + Pz * Nz = D
	so suppose we don't know x
	Eq 2: Px = (D - Py * Ny + Pz * Nz) / Nx
	
	Since we picked 2 AxisAllign planes to build the points we always have 1 unknown component
	
	Suppose we call the unknown component the C
	plane_c's normal has a 1.0 C
	
	dot(min_planes[plane_c].normal, plane->normal) is the value of the C of the plane-normal,
	i.e. Nx in Eq 2
	
	Since the C of the out->verts[i] i.e. P is 0.0 we can assume that
	dot(&out->verts[0].pos, &plane->normal) is like "Py * Ny + Pz * Nz" in Eq 2
	we can multiply the left side in Eq 2 by plane_c's normal to get the C of P
	*/
	
	#define DOVERT(PLANES1, PLANES2, VERTN) \
	out->verts[VERTN].pos = origin; \
	tmp = PLANES1[plane_a].normal; \
	vec3_mult(&tmp, PLANES1[plane_a].dist); \
	vec3_addto(&out->verts[VERTN].pos, &tmp); \
	tmp = PLANES2[plane_b].normal; \
	vec3_mult(&tmp, PLANES2[plane_b].dist); \
	vec3_addto(&out->verts[VERTN].pos, &tmp); \
	tmp = min_planes[plane_c].normal; \
	if(findc){ \
		const vecc_t factor = (plane->dist - vec3_dot(&out->verts[VERTN].pos, &plane->normal)); \
		const vecc_t dot = vec3_dot(&tmp, &plane->normal); \
		vec3_maddto(&out->verts[VERTN].pos, &tmp, factor / dot);\
	}
	
	DOVERT(min_planes, min_planes, 0)
	DOVERT(min_planes, max_planes, 1)
	DOVERT(max_planes, max_planes, 2)
	DOVERT(max_planes, min_planes, 3)
	#undef DOVERT
	
	//Make max_planes point inside the box
	for(i = 0; i < 3; i++){
		vec3_mult(&max_planes[i].normal, -1.0);
		max_planes[i].dist *= -1.0;
	}
	
	//Clip by the 2 remaining planes, the spanning check is important
	if(rvface_sort(&max_planes[plane_c], out) == RVFACE_SPANNING){
		rvface_split(&max_planes[plane_c], out, &frontface, &backface);
		*out = frontface;
	}
	if(rvface_sort(&min_planes[plane_c], out) == RVFACE_SPANNING){
		rvface_split(&min_planes[plane_c], out, &frontface, &backface);
		*out = frontface;
	}
}

void rvface_planeproj(const vec3 *normal, const rvface *f, rvface *out)
{
	int i;
	vec3 ab = VEC3_SUB(f->verts[1].pos, f->verts[0].pos);
	vec3 x = VEC3_CROSS(*normal, ab);
	vec3 y = VEC3_CROSS(*normal, x);
	
	for(i = 0; i < f->num_verts; i++){
		const vec3 *v = &f->verts[i].pos;
		
		out->verts[i].pos.z = vec3_dot(v, normal);
		out->verts[i].pos.x = vec3_dot(v, &x);
		out->verts[i].pos.y = vec3_dot(v, &y);
	}
}

void rvface_bounds(const rvface *f, vec3 *min, vec3 *max)
{
	int i;
	
	*min = vec3c_max;
	*max = vec3c_min;
	
	for(i = 0; i < f->num_verts; i++){
		vec3_setmax(&f->verts[i].pos, max);
		vec3_setmin(&f->verts[i].pos, min);
	}
}

int rvface_pick(const rvface *f, const vec3 *start, const vec3 *dir, vec3 *res, vecc_t *dist)
{
	mplane pl;
	vecc_t vecc_tmp;
	vec3 vec3_tmp;
	int j;
	
	if(!res) res = &vec3_tmp;
	if(!dist) dist = & vecc_tmp;
	
	vec3_verts2plane(
		&f->verts[0].pos,
		&f->verts[1].pos,
		&f->verts[2].pos, 
		&pl
	);
	
	*dist = vec3_ray2plane(&pl, start, dir, res);
	if(*dist < 0) return 0;
	
	for(j = 0; j < f->num_verts; j++){
		int o = (j + 1) % f->num_verts;
		//calculate one edge
		vec3 edge = VEC3_SUB(f->verts[o].pos, f->verts[j].pos);
		//cross product with normal gives a triangle pointing into the inside of the face
		vec3 edge2 = VEC3_CROSS(pl.normal, edge);
		
		vec3 to_point = VEC3_SUB(*res, f->verts[j].pos);
		
		//checks if point is inside the correct hemiplane
		if(vec3_dot(&edge2, &to_point) < 0) return 0;
	}
	
	return 1;
}

void rvface_planecircle(const mplane *pl, rvface *f, vecc_t radius, int segs)
{
	vec3 x = {}, y = {}, c = pl->normal;
	vec3 v = {0.577, 0.578, 0.579};
	int j;
	vecc_t invers = 1.0 / ((double) segs);
	
	vec3_cross(&pl->normal, &v, &x);
	vec3_cross(&pl->normal, &x, &y);
	f->num_verts = 0;
	
	vec3_mult(&c, pl->dist);
	
	for(j = 0; j < segs; j++){
		vec3 *tv = &f->verts[f->num_verts++].pos;
		*tv = c;
		vec3_maddto(tv, &y, sin(2.0 * M_PI * ((double) j) * invers) * radius);
		vec3_maddto(tv, &x, cos(2.0 * M_PI * ((double) j) * invers) * radius);
	}
}

void rvface_tri_baryc(const rvface *tri, const vec3 *in, vec3 *out)
{
	//Vector v0 = b - a, v1 = c - a, v2 = p - a;
	vec3 v0 = VEC3_SUB(tri->verts[1].pos, tri->verts[0].pos),
		v1 = VEC3_SUB(tri->verts[2].pos, tri->verts[0].pos),
		v2 = VEC3_SUB(*in, tri->verts[0].pos);
	double d00 = vec3_dot(&v0, &v0);
	double d01 = vec3_dot(&v0, &v1);
	double d11 = vec3_dot(&v1, &v1);
	double d20 = vec3_dot(&v2, &v0);
	double d21 = vec3_dot(&v2, &v1);
	double denom = d00 * d11 - d01 * d01;
	double u, v, w;
	if(!denom) return;
	v = (d11 * d20 - d01 * d21) / denom;
	w = (d00 * d21 - d01 * d20) / denom;
	u = 1.0f - v - w;
	out->x = u;
	out->y = v;
	out->z = w;
}

int rvface_interp(const rvface *f, const vec3 *pos, rvert *res)
{
	vec3 baryc;
	double u, v, w;
	int j = 1;
	int good;
	
	if(f->num_verts > 3){
		good = 0;
		for(; j < f->num_verts - 1; j++){
			const rvface tri = {
				{f->verts[0], f->verts[j], f->verts[j + 1]},
				3
			};
			rvface_tri_baryc(&tri, pos, &baryc);
			if(VEC3_BARYC_INSIDE(baryc)){
				good = 1;
				break;
			}
		}
	}
	else{
		rvface_tri_baryc(f, pos, &baryc);
		good = VEC3_BARYC_INSIDE(baryc);
	}
	
	if(!res) return good;
	
	u = baryc.x;
	v = baryc.y;
	w = baryc.z;
	
	res->pos = *pos;
	res->u = u * f->verts[0].u + v * f->verts[j].u + w * f->verts[j + 1].u;
	res->v = u * f->verts[0].v + v * f->verts[j].v + w * f->verts[j + 1].v;
	
	return good;
}

int rvface_interp2(const rvface *f, const rvface *f2, const vec3 *pos, rvert *res)
{
	vec3 baryc;
	double u, v, w;
	int j = 1;
	int good;
	
	if(f->num_verts > 3){
		good = 0;
		for(; j < f->num_verts - 1; j++){
			const rvface tri = {
				{f->verts[0], f->verts[j], f->verts[j + 1]},
				3
			};
			rvface_tri_baryc(&tri, pos, &baryc);
			if(VEC3_BARYC_INSIDE(baryc)){
				good = 1;
				break;
			}
		}
	}
	else{
		rvface_tri_baryc(f, pos, &baryc);
		good = VEC3_BARYC_INSIDE(baryc);
	}
	
	if(!res) return good;
	
	u = baryc.x;
	v = baryc.y;
	w = baryc.z;
	
	res->pos.x =	u * f2->verts[0].pos.x + v * f2->verts[j].pos.x + w * f2->verts[j + 1].pos.x;
	res->pos.y =	u * f2->verts[0].pos.y + v * f2->verts[j].pos.y + w * f2->verts[j + 1].pos.y;
	res->pos.z =	u * f2->verts[0].pos.z + v * f2->verts[j].pos.z + w * f2->verts[j + 1].pos.z;
	res->u =		u * f2->verts[0].u + v * f2->verts[j].u + w * f2->verts[j + 1].u;
	res->v =		u * f2->verts[0].v + v * f2->verts[j].v + w * f2->verts[j + 1].v;
	
	return good;
}

void rvface_fitminrect(const rvface *face, rvface *minface2d, float *w, float *h, struct MATRIX_f *bestmat)
{
	int i, j;
	vec3 n;
	float minarea = FLT_MAX;
	
	for(int i = 0; i < face->num_verts; i++){
		float minx = FLT_MAX, maxx = -FLT_MAX, maxy = -FLT_MAX, miny = FLT_MAX;
		MATRIX_f mat;
		vec3 x;
		rvface face2d = {
			.num_verts = face->num_verts,
		};
		const vec3
			a = face->verts[i].pos,
			b = face->verts[(i + 1) % face->num_verts].pos,
			c = face->verts[(i + 2) % face->num_verts].pos,
			ac = VEC3_SUB(c, a);
		vec3
			ab = VEC3_SUB(b, a);
		
		vec3_normalize(&ab);
		if(!i){
			vec3_cross(&ab, &ac, &n);
			vec3_normalize(&n);
		}
		vec3_cross(&ab, &n, &x);
		
		mat = identity_matrix_f;
		
		#define MAT(c,r) mat.v[c][r]
		#define CP_V2M(r,v) MAT(0,r) = v.x; MAT(1,r) = v.y; MAT(2,r) = v.z;
		CP_V2M(0, ab)
		CP_V2M(1, x)
		CP_V2M(2, n)
		#undef MAT
		#undef CP_V2M
		
		mat.t[0] = face->verts[i].pos.x;
		mat.t[1] = face->verts[i].pos.y;
		mat.t[2] = face->verts[i].pos.z;
		
		matrix_f_invert(&mat);
		
		for(j = 0; j < face->num_verts; j++){
			float vx, vy, vz;
			apply_matrix_f(
				&mat,
				face->verts[j].pos.x,
				face->verts[j].pos.y,
				face->verts[j].pos.z,
				&vx,
				&vy,
				&vz
			);
			assert(vz < MPLANE_TOLERANCE && vz > -MPLANE_TOLERANCE);
			face2d.verts[j].pos.x = vx;
			face2d.verts[j].pos.y = vy;
			face2d.verts[j].pos.z = 0;
			
			if(vx < minx) minx = vx;
			if(vx > maxx) maxx = vx;
			if(vy > maxy) maxy = vy;
			if(vy < miny) miny = vy;
		}
		
		const float area = (maxx - minx) * maxy;
		if(area < minarea){
			minarea = area;
			if(w) *w = maxx - minx;
			if(h) *h = maxy - miny;
			for(j = 0; j < face->num_verts; j++){
				face2d.verts[j].pos.x -= minx;
				face2d.verts[j].pos.y -= miny;
			}
			if(minface2d) *minface2d = face2d;
			if(bestmat) *bestmat = mat;
		}
	}
}

void vec3_apply_matrix_f(vec3 *a, const MATRIX_f *m)
{
	#ifdef VECCT_DOUBLE
	float x, y, z;
	apply_matrix_f(m, a->x, a->y, a->z, &x, &y, &z);
	a->x = x;
	a->y = y;
	a->z = z;
	#else
	apply_matrix_f(m, a->x, a->y, a->z, &a->x, &a->y, &a->z);
	#endif
}

void rvface_apply_matrix_f(rvface *f, const struct MATRIX_f *m)
{
	int i;
	for(i = 0; i < f->num_verts; i++){
		float x = f->verts[i].pos.x;
		float y = f->verts[i].pos.y;
		float z = f->verts[i].pos.z;
		apply_matrix_f(m, x, y, z, &x, &y, &z);
		f->verts[i].pos.x = x;
		f->verts[i].pos.y = y;
		f->verts[i].pos.z = z;
	}
}

/*
Not actual Euler: Tait-Bryan angles with ZYX order
Same as
	({
		MATRIX_f tmp;
		matrix_f_trs(&vec3c_origin, euler, &vec3c_unit, &tmp);
		vec3_apply_matrix_f(a, &tmp);
	});
	*/
void vec3_apply_euler(vec3 *a, const vec3 *euler)
{
	double sin_x, cos_x;
	double sin_y, cos_y;
	double sin_z, cos_z;
	
	DBL_SINCOS(euler->x, sin_x, cos_x);
	DBL_SINCOS(euler->y, sin_y, cos_y);
	DBL_SINCOS(euler->z, sin_z, cos_z);
	
	const double sinx_siny = sin_x * sin_y;
	const double cosx_siny = cos_x * sin_y;
	
	const vec3 v = *a;
#define M00	(cos_y * cos_z)
#define M01	(cos_y * sin_z)
#define M02	(-sin_y)
#define M10	((sinx_siny * cos_z) - (cos_x * sin_z))
#define M11	((sinx_siny * sin_z) + (cos_x * cos_z))
#define M12	(sin_x * cos_y)
#define M20	((cosx_siny * cos_z) + (sin_x * sin_z))
#define M21	((cosx_siny * sin_z) - (sin_x * cos_z))
#define M22	(cos_x * cos_y)
#define CALC_ROW(n) (v.x * M ## n ## 0 + v.y * M ## n ## 1 + v.z * M ## n ## 2)
	a->x = CALC_ROW(0);
	a->y = CALC_ROW(1);
	a->z = CALC_ROW(2);
#undef CALC_ROW
#undef M00
#undef M01
#undef M02
#undef M10
#undef M11
#undef M12
#undef M20
#undef M21
#undef M22
}

/*
Not actual Euler: Tait-Bryan angles with ZYX order
Same as
	({
		vec3 tmp = vec3c_z;
		vec3_apply_euler(&tmp, euler);
		tmp;
	});
	*/
vec3 vec3_euler_dir(const vec3 *euler)
{
	double sin_x, cos_x;
	double sin_y, cos_y;
	
	DBL_SINCOS(euler->x, sin_x, cos_x);
	DBL_SINCOS(euler->y, sin_y, cos_y);
	
	vec3 res = {(-sin_y), (sin_x * cos_y), (cos_x * cos_y)};
	vec3_normalize(&res);
	return res;
}

/*
Assumes:
	dir->x = sin(a->yaw) * cos(a->pitch);
	dir->y = sin(a->pitch);
	dir->z = cos(a->yaw) * cos(a->pitch);
*/
void vec3_dir2pitchyaw(const vec3 dir, float *pitch, float *yaw)
{
	double p, y;
	y = atan2(dir.x, dir.z);	//[-pi,+pi]
	p = asin(dir.y);			//[-pi/2,+pi/2]
	*pitch = p;
	*yaw = y;
}

/*
WARNING: writes garbage if v->z < near plane z
*/
void vec3_persp_project_f(vec3 *v)
{
	//assert(v->z >= 0.1);
	if(v->z < 0.1) return;
	#ifdef VECCT_DOUBLE
	float x, y;
	persp_project_f(v->x, v->y, v->z, &x, &y);
	v->x = x;
	v->y = y;
	#else
	persp_project_f(v->x, v->y, v->z, &v->x, &v->y);
	#endif
}

int matrix_f_invert(MATRIX_f *m)
{
	#define m00 m->v[0][0]
	#define m01 m->v[0][1]
	#define m02 m->v[0][2]
	#define m03 m->t[0]
	
	#define m10 m->v[1][0]
	#define m11 m->v[1][1]
	#define m12 m->v[1][2]
	#define m13 m->t[1]
	
	#define m20 m->v[2][0]
	#define m21 m->v[2][1]
	#define m22 m->v[2][2]
	#define m23 m->t[2]
	
	#define m30 0.0
	#define m31 0.0
	#define m32 0.0
	#define m33 1.0
	
	float A2323 = m22 * m33 - m23 * m32;
	float A1323 = m21 * m33 - m23 * m31;
	float A1223 = m21 * m32 - m22 * m31;
	float A0323 = m20 * m33 - m23 * m30;
	float A0223 = m20 * m32 - m22 * m30;
	float A0123 = m20 * m31 - m21 * m30;
	float A2313 = m12 * m33 - m13 * m32;
	float A1313 = m11 * m33 - m13 * m31;
	float A1213 = m11 * m32 - m12 * m31;
	float A2312 = m12 * m23 - m13 * m22;
	float A1312 = m11 * m23 - m13 * m21;
	float A1212 = m11 * m22 - m12 * m21;
	float A0313 = m10 * m33 - m13 * m30;
	float A0213 = m10 * m32 - m12 * m30;
	float A0312 = m10 * m23 - m13 * m20;
	float A0212 = m10 * m22 - m12 * m20;
	float A0113 = m10 * m31 - m11 * m30;
	float A0112 = m10 * m21 - m11 * m20;

	float det = m00 * ( m11 * A2323 - m12 * A1323 + m13 * A1223 ) 
		      - m01 * ( m10 * A2323 - m12 * A0323 + m13 * A0223 ) 
		      + m02 * ( m10 * A1323 - m11 * A0323 + m13 * A0123 ) 
		      - m03 * ( m10 * A1223 - m11 * A0223 + m12 * A0123 ) ;
	
	float t00, t10, t20, //t30,
	      t01, t11, t21, //t31,
		  t02, t12, t22, //t32,
		  t03, t13, t23  //t33
		  ;
	
	if(!det) return 0;
	det = 1 / det;
	
	t00 = det *   ( m11 * A2323 - m12 * A1323 + m13 * A1223 );
	t01 = det * - ( m01 * A2323 - m02 * A1323 + m03 * A1223 );
	t02 = det *   ( m01 * A2313 - m02 * A1313 + m03 * A1213 );
	t03 = det * - ( m01 * A2312 - m02 * A1312 + m03 * A1212 );
	t10 = det * - ( m10 * A2323 - m12 * A0323 + m13 * A0223 );
	t11 = det *   ( m00 * A2323 - m02 * A0323 + m03 * A0223 );
	t12 = det * - ( m00 * A2313 - m02 * A0313 + m03 * A0213 );
	t13 = det *   ( m00 * A2312 - m02 * A0312 + m03 * A0212 );
	t20 = det *   ( m10 * A1323 - m11 * A0323 + m13 * A0123 );
	t21 = det * - ( m00 * A1323 - m01 * A0323 + m03 * A0123 );
	t22 = det *   ( m00 * A1313 - m01 * A0313 + m03 * A0113 );
	t23 = det * - ( m00 * A1312 - m01 * A0312 + m03 * A0112 );
	//t30 = det * - ( m10 * A1223 - m11 * A0223 + m12 * A0123 );
	//t31 = det *   ( m00 * A1223 - m01 * A0223 + m02 * A0123 );
	//t32 = det * - ( m00 * A1213 - m01 * A0213 + m02 * A0113 );
	//t33 = det *   ( m00 * A1212 - m01 * A0212 + m02 * A0112 );
	
	m00 = t00;
	m01 = t01;
	m02 = t02;
	m03 = t03;
	m10 = t10;
	m11 = t11;
	m12 = t12;
	m13 = t13;
	m20 = t20;
	m21 = t21;
	m22 = t22;
	m23 = t23;
	//m30 = t30;
	//m31 = t31;
	//m32 = t32;
	//m33 = t33;
	return 1;
	
	#undef m00
	#undef m01
	#undef m02
	#undef m03
	#undef m10
	#undef m11
	#undef m12
	#undef m13
	#undef m20
	#undef m21
	#undef m22
	#undef m23
	#undef m30
	#undef m31
	#undef m32
	#undef m33
}

/*
Like matrix_f_invert, but for normal transforms (assumes last col/row are all 0)
*/
int matrix_f_invert2(MATRIX_f *m)
{
	#define m00 m->v[0][0]
	#define m01 m->v[0][1]
	#define m02 m->v[0][2]
	#define m03 0
	
	#define m10 m->v[1][0]
	#define m11 m->v[1][1]
	#define m12 m->v[1][2]
	#define m13 0
	
	#define m20 m->v[2][0]
	#define m21 m->v[2][1]
	#define m22 m->v[2][2]
	#define m23 0
	
	#define m30 0.0
	#define m31 0.0
	#define m32 0.0
	#define m33 0.0
	
	memset(m->t, 0, sizeof(m->t));
	
	float A2323 = m22 * m33 - m23 * m32;
	float A1323 = m21 * m33 - m23 * m31;
	float A1223 = m21 * m32 - m22 * m31;
	float A0323 = m20 * m33 - m23 * m30;
	float A0223 = m20 * m32 - m22 * m30;
	float A0123 = m20 * m31 - m21 * m30;
	float A2313 = m12 * m33 - m13 * m32;
	float A1313 = m11 * m33 - m13 * m31;
	float A1213 = m11 * m32 - m12 * m31;
	float A2312 = m12 * m23 - m13 * m22;
	float A1312 = m11 * m23 - m13 * m21;
	float A1212 = m11 * m22 - m12 * m21;
	float A0313 = m10 * m33 - m13 * m30;
	float A0213 = m10 * m32 - m12 * m30;
	float A0312 = m10 * m23 - m13 * m20;
	float A0212 = m10 * m22 - m12 * m20;
	float A0113 = m10 * m31 - m11 * m30;
	float A0112 = m10 * m21 - m11 * m20;

	float det = m00 * ( m11 * A2323 - m12 * A1323 + m13 * A1223 ) 
		      - m01 * ( m10 * A2323 - m12 * A0323 + m13 * A0223 ) 
		      + m02 * ( m10 * A1323 - m11 * A0323 + m13 * A0123 ) 
		      - m03 * ( m10 * A1223 - m11 * A0223 + m12 * A0123 ) ;
	
	float t00, t10, t20, //t30,
	      t01, t11, t21, //t31,
		  t02, t12, t22, //t32,
		  t03, t13, t23  //t33
		  ;
	
	if(!det) return 0;
	det = 1 / det;
	
	t00 = det *   ( m11 * A2323 - m12 * A1323 + m13 * A1223 );
	t01 = det * - ( m01 * A2323 - m02 * A1323 + m03 * A1223 );
	t02 = det *   ( m01 * A2313 - m02 * A1313 + m03 * A1213 );
	t03 = det * - ( m01 * A2312 - m02 * A1312 + m03 * A1212 );
	t10 = det * - ( m10 * A2323 - m12 * A0323 + m13 * A0223 );
	t11 = det *   ( m00 * A2323 - m02 * A0323 + m03 * A0223 );
	t12 = det * - ( m00 * A2313 - m02 * A0313 + m03 * A0213 );
	t13 = det *   ( m00 * A2312 - m02 * A0312 + m03 * A0212 );
	t20 = det *   ( m10 * A1323 - m11 * A0323 + m13 * A0123 );
	t21 = det * - ( m00 * A1323 - m01 * A0323 + m03 * A0123 );
	t22 = det *   ( m00 * A1313 - m01 * A0313 + m03 * A0113 );
	t23 = det * - ( m00 * A1312 - m01 * A0312 + m03 * A0112 );
	//t30 = det * - ( m10 * A1223 - m11 * A0223 + m12 * A0123 );
	//t31 = det *   ( m00 * A1223 - m01 * A0223 + m02 * A0123 );
	//t32 = det * - ( m00 * A1213 - m01 * A0213 + m02 * A0113 );
	//t33 = det *   ( m00 * A1212 - m01 * A0212 + m02 * A0112 );
	
	m00 = t00;
	m01 = t01;
	m02 = t02;
	m10 = t10;
	m11 = t11;
	m12 = t12;
	m20 = t20;
	m21 = t21;
	m22 = t22;
	//m30 = t30;
	//m31 = t31;
	//m32 = t32;
	//m33 = t33;
	return 1;
	
	#undef m00
	#undef m01
	#undef m02
	#undef m03
	#undef m10
	#undef m11
	#undef m12
	#undef m13
	#undef m20
	#undef m21
	#undef m22
	#undef m23
	#undef m30
	#undef m31
	#undef m32
	#undef m33
}

void matrix_f_print(MATRIX_f *m)
{
	const MATRIX_f a = *m;
	printf(
		"%+0.9f %+0.9f %+0.9f %+0.9f\n"
		"%+0.9f %+0.9f %+0.9f %+0.9f\n"
		"%+0.9f %+0.9f %+0.9f %+0.9f\n"
		"%+0.9f %+0.9f %+0.9f %+0.9f\n",
		a.v[0][0], a.v[0][1], a.v[0][2], a.t[0],
		a.v[1][0], a.v[1][1], a.v[1][2], a.t[1],
		a.v[2][0], a.v[2][1], a.v[2][2], a.t[2],
		0.0, 0.0, 0.0, 1.0
	);
}

int matrix_f_frustum2box_mm(const MATRIX_f *m, const vec3 *min_, const vec3 *max_)
{
	struct {
		float x, y, z;
	} min, max;
	apply_matrix_f(m, min_->x, min_->y, min_->z, &min.x, &min.y, &min.z);
	apply_matrix_f(m, max_->x, max_->y, max_->z, &max.x, &max.y, &max.z);
	
	#define DOSWAP(a,b) \
	if((a) > (b)){		\
		float tmp = (a);\
		(a) = (b);		\
		(b) = tmp;		\
	}
	DOSWAP(min.x, max.x)
	DOSWAP(min.y, max.y)
	DOSWAP(min.z, max.z)
	
	if(max.z < 0) return 0;
	if(max.y < -max.z) return 0;
	if(min.y > max.z) return 0;
	if(max.x < -max.z) return 0;
	if(min.x > max.z) return 0;
	
	#undef DOSWAP
	
	return 1;
}

void matrix_f_toeuler(const MATRIX_f *m, vec3 *euler)
{
	#define MAT(c,r) m->v[r][c]
	
	double sy = sqrt(MAT(0,0) * MAT(0,0) +  MAT(1,0) * MAT(1,0) );
	int singular = sy < 1e-6;
	
	if(!singular){
		euler->x = atan2(MAT(2,1), MAT(2,2));
		euler->y = atan2(-MAT(2,0), sy);
		euler->z = atan2(MAT(1,0), MAT(0,0));
	}
	else{
		euler->x = atan2(-MAT(1,2), MAT(1,1));
		euler->y = atan2(-MAT(2,0), sy);
		euler->z = 0;
	}
	
	#undef MAT
}

int matrix_f_equal(const struct MATRIX_f *m1, const struct MATRIX_f *m2)
{
	int i, j;
	for(i = 0; i < 3; i++){
		for(j = 0; j < 3; j++){
			if(fabs(m1->v[i][j] - m2->v[i][j]) > 0.001) return 0;
		}
		if(fabs(m1->t[i] - m2->t[i]) > 0.001) return 0;
	}
	return 1;
}

void matrix_f_transp(struct MATRIX_f *m)
{
	int c, r;
	for(c = 0; c < 3; c++){
		for(r = 0; r < 3; r++){
			const float tmp = m->v[r][c];
			m->v[r][c] = m->v[c][r];
			m->v[c][r] = tmp;
		}
		m->t[c] = 0.0;
	}
}

void matrix_f_cpos(const struct MATRIX_f *m1, vec3 *pos)
{
	MATRIX_f m2 = *m1;
	matrix_f_invert(&m2);
	pos->x = m2.t[0];
	pos->y = m2.t[1];
	pos->z = m2.t[2];
}

void matrix_f_trs(const vec3 *pos, const vec3 *rot, const vec3 *scl, MATRIX_f *res)
{
	const vecc_t factor = RAD_TO_BIN(1.0);
	MATRIX_f trans, scale, rotat;
	get_translation_matrix_f(&trans, pos->x, pos->y, pos->z);
	get_scaling_matrix_f(&scale, scl->x, scl->y, scl->z);
	get_rotation_matrix_f(&rotat, rot->x * factor, rot->y * factor, rot->z * factor);
	matrix_mul_f(&rotat, &scale, res);
	matrix_mul_f(res, &trans, res);
}

void matrix_f_to_trs(vec3 *pos, vec3 *rot, vec3 *scl, const MATRIX_f *src_)
{
	MATRIX_f src = *src_;
	
	if(pos){
		pos->x = src.t[0];
		pos->y = src.t[1];
		pos->z = src.t[2];
	}
	
	src.t[0] = 0;
	src.t[1] = 0;
	src.t[2] = 0;
	
	if(!(scl || rot)) return;
	if(!scl) scl = __builtin_alloca(sizeof(scl));
	
	#define VIDX(b,a) [a][b]
	
	scl->x = sqrt(
		src.v VIDX(0,0) * src.v VIDX(0,0) +
		src.v VIDX(1,0) * src.v VIDX(1,0) +
		src.v VIDX(2,0) * src.v VIDX(2,0));
	scl->y = sqrt(
		src.v VIDX(0,1) * src.v VIDX(0,1) +
		src.v VIDX(1,1) * src.v VIDX(1,1) +
		src.v VIDX(2,1) * src.v VIDX(2,1));
	scl->z = sqrt(
		src.v VIDX(0,2) * src.v VIDX(0,2) +
		src.v VIDX(1,2) * src.v VIDX(1,2) +
		src.v VIDX(2,2) * src.v VIDX(2,2));
	
	if(scl->x < 0) scl->x *= -1;
	if(scl->y < 0) scl->y *= -1;
	if(scl->z < 0) scl->z *= -1;
	
	const double
		sx1 = 1.0/scl->x,
		sy1 = 1.0/scl->y,
		sz1 = 1.0/scl->z;
	
	src.v VIDX(0,0) *= sx1;
	src.v VIDX(0,1) *= sy1;
	src.v VIDX(0,2) *= sz1;
	src.v VIDX(1,0) *= sx1;
	src.v VIDX(1,1) *= sy1;
	src.v VIDX(1,2) *= sz1;
	src.v VIDX(2,0) *= sx1;
	src.v VIDX(2,1) *= sy1;
	src.v VIDX(2,2) *= sz1;
	
	#undef VIDX
	
	if(!rot) return;
	
	matrix_f_toeuler(&src, rot);
	
	return;
}

void matrix_f_from3points(const vec3 *a, const vec3 *b, const vec3 *c, struct MATRIX_f *out)
{
	vec3 n, x, ab = VEC3_SUB(*a, *b), m = VEC3_MULT(ab, 0.5);
	
	vec3_addto(&m, b);
	vec3_normalize(&ab);
	vec3_normal(a, b, c, &n);
	vec3_cross(&n, &ab, &x);
	
	*out = identity_matrix_f;
	
	#define MAT(c,r) out->v[r][c]
	#define CP_V2M(r,v) MAT(0,r) = v.x; MAT(1,r) = v.y; MAT(2,r) = v.z;
	CP_V2M(0, ab)
	CP_V2M(1, x)
	CP_V2M(2, n)
	#undef MAT
	#undef CP_V2M
	
	MATRIX_f tmp;
	get_translation_matrix_f(&tmp, m.x, m.y, m.z);
	//matrix_mul_f(&tmp, out, out);
	matrix_mul_f(out, &tmp, out);
}

void matrix_f_fromrvface(const rvface *face, struct MATRIX_f *out)
{
	vec3 n, x,
		ab = VEC3_SUB(face->verts[1].pos, face->verts[0].pos),
		ac = VEC3_SUB(face->verts[2].pos, face->verts[0].pos);
	
	vec3_normalize(&ab);
	vec3_cross(&ab, &ac, &n);
	vec3_normalize(&n);
	vec3_cross(&ab, &n, &x);
	
	//rdebug_arrow3(&m, &ab, "ab");
	//rdebug_arrow3(&m, &ac, "ac");
	//rdebug_arrow3(&m, &n, "n");
	//rdebug_arrow3(&m, &x, "x");
	
	*out = identity_matrix_f;
	
	#define MAT(c,r) out->v[c][r]
	#define CP_V2M(r,v) MAT(0,r) = v.x; MAT(1,r) = v.y; MAT(2,r) = v.z;
	CP_V2M(0, ab)
	CP_V2M(1, x)
	CP_V2M(2, n)
	#undef MAT
	#undef CP_V2M
	
	out->t[0] = face->verts[0].pos.x;
	out->t[1] = face->verts[0].pos.y;
	out->t[2] = face->verts[0].pos.z;
}

void matrix_f_interp(const struct MATRIX_f *a, const struct MATRIX_f *b, double i, struct MATRIX_f *out)
{
	vec3 ta, ra, sa;
	vec3 tb, rb, sb;
	vec3 ti, si;
	QUAT qa, qb, qi;
	matrix_f_to_trs(&ta, &ra, &sa, a);
	matrix_f_to_trs(&tb, &rb, &sb, b);
	
	vec3_interp(&ta, &tb, i, &ti);
	vec3_interp(&sa, &sb, i, &si);
	
	const vecc_t factor = RAD_TO_BIN(1.0);
	get_rotation_quat(&qa, ra.x * factor, ra.y * factor, ra.z * factor);
	get_rotation_quat(&qb, rb.x * factor, rb.y * factor, rb.z * factor);
	
	quat_interpolate(&qa, &qb, i, &qi);
	
	MATRIX_f trans, scale, rotat;
	get_translation_matrix_f(&trans, ti.x, ti.y, ti.z);
	get_scaling_matrix_f(&scale, si.x, si.y, si.z);
	quat_to_matrix(&qi, &rotat);
	matrix_mul_f(&rotat, &scale, out);
	matrix_mul_f(out, &trans, out);
}

int vec3_clipline(vec3 *a, vec3 *b)
{
	return 1;
	if(a->x > a->z || a->x < -a->z || a->y > a->z || a->y < -a->z){
		vec3 *tmp = b;
		b = a;
		a = tmp;
	}
	
	const vec3 d = VEC3_SUB(*b, *a);
	
	//if(a->x > a->z || a->x < -a->z || a->y > a->z || a->y < -a->z)
		//return 0;
	
	/*
	x = ax+dx*t
	y = ay+dy*y
	z = az+dz*t
	x = z
	ax+dx*t=az+dz*t
	ax-az=(dz-dx)*t
	t=(ax-az)/(dz-dx)
	
	x = ax+dx*t
	y = ay+dy*y
	z = az+dz*t
	x = -z
	ax+dx*t=-az-dz*t
	ax+az=(-dz-dx)*t
	*/
	
	const double dzx = d.z - d.x;
	const double d_zx = -d.z - d.x;
	const double dzy = d.z - d.y;
	const double d_zy = -d.z - d.y;
	const double axz = a->x - a->z;
	const double ax_z = a->x + a->z;
	const double ayz = a->y - a->z;
	const double ay_z = a->y + a->z;
	
	double tzx = 1.0, t_zx = 1.0, tzy = 1.0, t_zy = 1.0;
	
	if(dzx && (axz / dzx > 0.0)) tzx = axz / dzx;
	if(d_zx && (ax_z / d_zx > 0.0)) t_zx = ax_z / d_zx;
	if(dzy && (ayz / dzy > 0.0)) tzy = ayz / dzy;
	if(d_zy && (ay_z / d_zy > 0.0)) t_zy = ay_z / d_zy;
	
	double tmin = 1.0;//fmin(1.0, fmin(tzx, fmin(t_zx, fmin(tzy, t_zy))));
	#define SETTMIN(v) if(v < tmin) tmin = v;
	SETTMIN(tzx)
	SETTMIN(t_zx)
	SETTMIN(tzy)
	SETTMIN(t_zy)
	#undef SETTMIN
	
	b->x = a->x + d.x * tmin;
	b->y = a->y + d.y * tmin;
	b->z = a->z + d.z * tmin;
	
	return 1;
}

void matrix_f_box_sc(const MATRIX_f *m, vec3 *siz, vec3 *pos)
{
	vec3_apply_matrix_f(pos, m);
	const vec3 new_siz = {
		.x = siz->x * fabs(m->v[0][0]) + siz->y * fabs(m->v[0][1]) + siz->z * fabs(m->v[0][2]),
		.y = siz->x * fabs(m->v[1][0]) + siz->y * fabs(m->v[1][1]) + siz->z * fabs(m->v[1][2]),
		.z = siz->x * fabs(m->v[2][0]) + siz->y * fabs(m->v[2][1]) + siz->z * fabs(m->v[2][2]),
	};
	*siz = new_siz;
}

void matrix_f_hemicube(const struct MATRIX_f *patch_mat, const vec3 *pos, struct MATRIX_f *hemicube_mat)
{
	MATRIX_f imat = *patch_mat;
	matrix_f_invert(&imat);
	imat.t[0] = pos->x;
	imat.t[1] = pos->y;
	imat.t[2] = pos->z;
	
	vec3 o = vec3c_origin, x_ = vec3c_x, y_ = vec3c_y, z_ = vec3c_z;
	vec3_apply_matrix_f(&o,  &imat);
	vec3_apply_matrix_f(&x_, &imat);
	vec3_apply_matrix_f(&y_, &imat);
	vec3_apply_matrix_f(&z_, &imat);
	const vec3 oy = VEC3_SUB(y_, o);
	const vec3 oz = VEC3_SUB(z_, o);
	const vec3 ox = VEC3_SUB(x_, o);
	
	vec3_maddto(&o, &oz, 0.001);
	
	get_camera_matrix_f(
		&hemicube_mat[0],
		o.x, o.y, o.z,
		oz.x, oz.y, oz.z,
		oy.x, oy.y, oy.z,
		90.0f * (256.0f / 360.0f),
		1.0
	);
	get_camera_matrix_f(
		&hemicube_mat[1],
		o.x, o.y, o.z,
		ox.x, ox.y, ox.z,
		oz.x, oz.y, oz.z,
		90.0f * (256.0f / 360.0f),
		1.0
	);
	get_camera_matrix_f(
		&hemicube_mat[2],
		o.x, o.y, o.z,
		oy.x, oy.y, oy.z,
		oz.x, oz.y, oz.z,
		90.0f * (256.0f / 360.0f),
		1.0
	);
	get_camera_matrix_f(
		&hemicube_mat[3],
		o.x, o.y, o.z,
		-ox.x, -ox.y, -ox.z,
		oz.x, oz.y, oz.z,
		90.0f * (256.0f / 360.0f),
		1.0
	);
	get_camera_matrix_f(
		&hemicube_mat[4],
		o.x, o.y, o.z,
		-oy.x, -oy.y, -oy.z,
		oz.x, oz.y, oz.z,
		90.0f * (256.0f / 360.0f),
		1.0
	);
}
