/*
A collection of useful routines
*/

#ifndef KDRLIB_H
#define KDRLIB_H

int kdr_strlen_aligned(const char *c);
char *kdr_strdup(const char *c);
char *kdr_strdup_aligned(const char *c);
int kdr_strcmp_aligned(const char *a_, const char *b_);

#endif
