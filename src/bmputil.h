#ifndef BMPUTIL_H
#define BMPUTIL_H

#define create_bitmap32(w,h) create_bitmap_ex(32, (w), (h))
#define create_bitmap8(w,h) create_bitmap_ex(8, (w), (h))

struct RLE_SPRITE *delta_bitmap(struct BITMAP *a, struct BITMAP *b);
struct BITMAP *bitmap_to32bpp(struct BITMAP *src, const RGB *pal);
struct BITMAP *bitmap_to8bpp(struct BITMAP *src, const RGB *pal);
#define bitmap_to_curr_bpp(src,pal) (get_color_depth() == 32 ? bitmap_to32bpp((src), (pal)) : bitmap_to8bpp((src), (pal)))
struct BITMAP *kdr_load_bitmap_ex(int col_depth, const char *fn);
struct BITMAP *kdr_load_bitmap(const char *fn);
struct BITMAP *kdr_load_bitmap32(const char *fn);
struct BITMAP *copy_sub_bitmap(struct BITMAP *src, int x, int y, int w, int h);
struct BITMAP *copy_bitmap(struct BITMAP *src);
struct BITMAP *trim_bitmap(struct BITMAP *src, int *off_x_, int *off_y_);
void alpha_to_transp(struct BITMAP *bmp, int limit);
void get_hemicube_bmps(int hemicube_res, struct BITMAP **hemicube_bmp, struct BITMAP *(*hemicube_subbmp)[5]);
void draw_hemicube_map(struct BITMAP *cubemap, struct BITMAP **hemicube_subbmp);
void get_bitmap_palette(struct BITMAP *src, RGB *pal);

#endif
