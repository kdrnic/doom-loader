#ifndef PRIME_H
#define PRIME_H

typedef long long int modpow_t;
modpow_t modular_pow(modpow_t base, modpow_t exponent, modpow_t modulus);
int miller_rabin_test(int n);
int get_prime_gt(int a);

#endif
