#include <allegro.h>
#include <assert.h>

#include "bmputil.h"

RLE_SPRITE *delta_bitmap(BITMAP *a, BITMAP *b)
{
	int x, y;
	BITMAP *c = create_bitmap(a->w, a->h);
	if(!c) return 0;
	const int mask = bitmap_mask_color(c);
	RLE_SPRITE *res;
	
	for(x = 0; x < a->w; x++){
		for(y = 0; y < a->h; y++){
			const int p = getpixel(b, x, y);
			putpixel(c, x, y, getpixel(a, x, y) == p ? mask : p);
		}
	}
	
	res = get_rle_sprite(c);
	destroy_bitmap(c);
	
	return res;
}

BITMAP *bitmap_to32bpp(BITMAP *src, const RGB *pal)
{
	BITMAP *bmp = create_bitmap32(src->w, src->h);
	int x, y;
	PALETTE pal_;
	if(!bmp) return 0;
	
	if(bitmap_color_depth(src) != 8){
		blit(src, bmp, 0, 0, 0, 0, src->w, src->h);
		return bmp;
	}
	
	if(!pal){
		pal = pal_;
		get_palette(pal_);
	}
	
	for(x = 0; x < src->w; x++){
		for(y = 0; y < src->h; y++){
			const int p = _getpixel(src, x, y);
			const RGB *c = pal + p;
			_putpixel32(bmp, x, y, makecol32(c->r << 2, c->g << 2, c->b << 2));
		}
	}
	return bmp;
}

BITMAP *bitmap_to8bpp(BITMAP *src, const RGB *pal)
{
	BITMAP *bmp;
	int x, y;
	
	if(bitmap_color_depth(src) != 32 && bitmap_color_depth(src) != 8){
		BITMAP *tmp = bitmap_to32bpp(src, 0);
		if(!tmp) return 0;
		bmp = bitmap_to8bpp(tmp, pal);
		destroy_bitmap(tmp);
		return bmp;
	}
	
	bmp = create_bitmap8(src->w, src->h);
	if(!bmp) return 0;
	
	if(bitmap_color_depth(src) == 8){
		blit(src, bmp, 0, 0, 0, 0, src->w, src->h);
		return bmp;
	}
	
	for(x = 0; x < src->w; x++){
		for(y = 0; y < src->h; y++){
			const int p = _getpixel32(src, x, y);
			//TODO: use kdtree
			int c = pal ?
				bestfit_color(pal, (p >> 16) & 0xff, (p >> 8) & 0xff, p & 0xff) :
				makecol8((p >> 16) & 0xff, (p >> 8) & 0xff, p & 0xff);
			if((p & 0xFFFFFF) == MASK_COLOR_32){
				c = 0;
			}
			_putpixel(bmp, x, y, c);
		}
	}
	return bmp;
}

#include <stdio.h>

BITMAP *kdr_load_bitmap_ex(int col_depth, const char *fn)
{
	int cc = get_color_conversion();
	set_color_conversion(COLORCONV_KEEP_TRANS);
	PALETTE pal_;
	RGB *pal = pal_;
	BITMAP *bmp = load_bitmap(fn, pal_);
	
	if(!bmp){
		set_color_conversion(cc);
		return 0;
	}
	begin:
	if(col_depth == 8 && bitmap_color_depth(bmp) != 8){
		BITMAP *tmp = bmp;
		bmp = bitmap_to8bpp(bmp, 0);
		destroy_bitmap(tmp);
	}
	else if(col_depth == 32 && bitmap_color_depth(bmp) != 32){
		BITMAP *tmp = bmp;
		bmp = bitmap_to32bpp(bmp, pal);
		if(!bmp) return 0;
		destroy_bitmap(tmp);
	}
	else if(col_depth == 8 && bitmap_color_depth(bmp) == 8){
		BITMAP *tmp = bmp;
		bmp = bitmap_to32bpp(bmp, pal);
		if(!bmp) return 0;
		pal = 0;
		destroy_bitmap(tmp);
		goto begin;
	}
	set_color_conversion(cc);
	return bmp;
}

BITMAP *copy_sub_bitmap(BITMAP *src, int x, int y, int w, int h)
{
	BITMAP *res = create_bitmap(w, h);
	if(!res) return 0;
	blit(src, res, x, y, 0, 0, w, h);
	return res;
}

struct BITMAP *copy_bitmap(struct BITMAP *src)
{
	BITMAP *res = create_bitmap(src->w, src->h);
	if(!res) return 0;
	blit(src, res, 0, 0, 0, 0, src->w, src->h);
	return res;
}

struct BITMAP *kdr_load_bitmap(const char *fn)
{
	return kdr_load_bitmap_ex(get_color_depth(), fn);
}

struct BITMAP *kdr_load_bitmap32(const char *fn)
{
	return kdr_load_bitmap_ex(32, fn);
}

struct BITMAP *trim_bitmap(struct BITMAP *src, int *off_x_, int *off_y_)
{
	int col_depth = bitmap_color_depth(src);
	int c = MASK_COLOR_32;
	if(col_depth == 8) c = MASK_COLOR_8;
	
	int x, y;
	int off_x = 0, off_y = 0, w = src->w, h = src->h;
	for(x = src->w - 1; x >= 0; x--){
		for(y = 0; y < src->h; y++)	if((getpixel(src, x, y) & 0xFFFFFF) != c) break;
		if(y != src->h) break;
	}
	w = x + 1;
	
	for(x = 0; x < w; x++){
		for(y = 0; y < src->h; y++)	if((getpixel(src, x, y) & 0xFFFFFF) != c) break;
		if(y != src->h) break;
	}
	off_x = x;
	w -= x;
	
	for(y = src->h - 1; y >= 0; y--){
		for(x = off_x; x < off_x + w; x++) if((getpixel(src, x, y) & 0xFFFFFF) != c) break;
		if(x != off_x + w) break;
	}
	h = y + 1;
	
	for(y = 0; y < h; y++){
		for(x = off_x; x < off_x + w; x++) if((getpixel(src, x, y) & 0xFFFFFF) != c) break;
		if(x != off_x + w) break;
	}
	off_y = y;
	h -= y;
	
	if(off_x_) *off_x_ = off_x;
	if(off_y_) *off_y_ = off_y;
	
	BITMAP *sub = create_sub_bitmap(src, off_x, off_y, w, h);
	
	//clear(screen);
	//draw_sprite(screen, src, 0, 0);
	//rest(200);
	//clear(screen);
	//clear_to_color(sub, 0xFF0000);
	//draw_sprite(screen, src, 0, 0);
	//rest(200);
	
	return sub;
}

void alpha_to_transp(struct BITMAP *src, int limit)
{
	int col_depth = bitmap_color_depth(src);
	assert(col_depth == 32);
	int mask = MASK_COLOR_32;
	
	int x, y;
	for(y = 0; y < src->h; y++){
		for(x = 0; x < src->w; x++){
			int c = getpixel(src, x, y);
			if(geta32(c) < limit){
				putpixel(src, x, y, mask);
			}
		}
	}
}

void get_hemicube_bmps(int hemicube_res, struct BITMAP **hemicube_bmp, struct BITMAP *(*hemicube_subbmp)[5])
{
	*hemicube_bmp = create_bitmap_ex(32, hemicube_res, hemicube_res * 3);
	(*hemicube_subbmp)[0] = create_sub_bitmap(*hemicube_bmp, 0,	0,								hemicube_res, hemicube_res);
	(*hemicube_subbmp)[1] = create_sub_bitmap(*hemicube_bmp, 0,	(hemicube_res * (2 + 0)) / 2,	hemicube_res, hemicube_res / 2);
	(*hemicube_subbmp)[2] = create_sub_bitmap(*hemicube_bmp, 0,	(hemicube_res * (2 + 1)) / 2,	hemicube_res, hemicube_res / 2);
	(*hemicube_subbmp)[3] = create_sub_bitmap(*hemicube_bmp, 0,	(hemicube_res * (2 + 2)) / 2,	hemicube_res, hemicube_res / 2);
	(*hemicube_subbmp)[4] = create_sub_bitmap(*hemicube_bmp, 0,	(hemicube_res * (2 + 3)) / 2,	hemicube_res, hemicube_res / 2);
}

void draw_hemicube_map(struct BITMAP *cubemap, struct BITMAP **hemicube_subbmp)
{
	const int hemicube_res = hemicube_subbmp[0]->w;
	clear_to_color(cubemap, 0);
	draw_sprite_vh_flip(cubemap, hemicube_subbmp[2], hemicube_res / 2, 0);
	draw_sprite(cubemap, hemicube_subbmp[0], hemicube_res / 2, 0 + (1 * hemicube_res) / 2);
	draw_sprite(cubemap, hemicube_subbmp[4], hemicube_res / 2, 0 + (3 * hemicube_res) / 2);
	pivot_sprite(cubemap, hemicube_subbmp[3], (hemicube_res * 1) / 2, 0 + (1 * hemicube_res) / 2, 0, 0, itofix(64));
	pivot_sprite(cubemap, hemicube_subbmp[1], (hemicube_res * 3) / 2, 0 + (3 * hemicube_res) / 2, 0, 0, -itofix(64));
}

void get_bitmap_palette(struct BITMAP *src, RGB *pal)
{
	int palsize = 1, idx = 0;
	assert(bitmap_color_depth(src) == 32);
	
	pal[0].r = pal[0].g = pal[0].b = pal[0].filler = 0;
	
	for(int y = 0; y < src->h; y++){
		for(int x = 0; x < src->w; x++){
			int c32 = _getpixel32(src, x, y);
			RGB rgb = {
				.r = getr32(c32) / 4,
				.g = getg32(c32) / 4,
				.b = getb32(c32) / 4,
				.filler = 0,
			};
			for(idx = 1; idx < palsize; idx++){
				if(!memcmp(&pal[idx], &rgb, sizeof(rgb))) break;
			}
			if(idx < palsize) continue;
			assert(palsize < 256);
			pal[palsize++] = rgb;
		}
	}
}
