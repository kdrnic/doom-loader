#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <math.h>
#include <locale.h>
#include <errno.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <assert.h>

#include "arr.h"
#include "vector.h"
#include "wad.h"

//Exclusively for replace_extension
#include <allegro.h>

static int flatface_comp(const void *a, const void *b)
{
	return strncmp(((struct wad_flatface *) a)->tex, ((struct wad_flatface *) b)->tex, 8);
}

static int wallface_comp(const void *a, const void *b)
{
	return strncmp(((struct wad_wallface *) a)->tex, ((struct wad_wallface *) b)->tex, 8);
}

void wad_map_export_obj(const char *filename, struct wad_map_build_faces_t *bsp, struct wad_texture **textures, int num_textures, const float objscale)
{
	char mtl[256];
	replace_extension(mtl, filename, "mtl", 256);
	
	struct wad_wallface *lfit, *linfaces  = bsp->wallfaces.data, *lfptr = bsp->wallfaces.data + bsp->wallfaces.len;
	struct wad_flatface *ffit, *flatfaces = bsp->flatfaces.data, *ffptr = bsp->flatfaces.data + bsp->flatfaces.len;
	
	qsort(linfaces, lfptr - linfaces, sizeof(*linfaces), wallface_comp);
	qsort(flatfaces, ffptr - flatfaces, sizeof(*flatfaces), flatface_comp);
	
	char tex[8] = {0};
	
	int i;
	FILE *file_obj = fopen(filename, "w");
	fprintf(file_obj, "mtllib %.256s\n", mtl);
	fputs("# Flat UVs""\n", file_obj);
	for(ffit = flatfaces; ffit < ffptr; ffit++){
		for(i = 0; i < ffit->face.num_verts; i++)
			fprintf(file_obj, "v %f %f %f\n",
				ffit->face.verts[i].pos.x * objscale,
				ffit->face.verts[i].pos.z * objscale,
				ffit->face.verts[i].pos.y * objscale
			);
	}
	for(lfit = linfaces, fputs("# Wall verts""\n", file_obj); lfit < lfptr; lfit++){
		rvface_flip(&lfit->face);
		for(i = 0; i < lfit->face.num_verts; i++)
			fprintf(file_obj, "v %f %f %f\n",
				lfit->face.verts[i].pos.x * objscale,
				lfit->face.verts[i].pos.z * objscale,
				lfit->face.verts[i].pos.y * objscale
			);
	}
	fputs("# Flat UVs""\n", file_obj);
	for(ffit = flatfaces; ffit < ffptr; ffit++){
		struct wad_texture *wt = 0;
		for(i = 0; i < num_textures; i++){
			if(!strncmp(ffit->tex, textures[i]->name, 8)){
				wt = textures[i];
				break;
			}
		}
		if(!wt) wt = textures[0];
		
		for(i = 0; i < ffit->face.num_verts; i++)
			fprintf(file_obj, "vt %f %f\n",
				ffit->face.verts[i].u / ((float) wt->width),
				ffit->face.verts[i].v / ((float) wt->height)
			);
	}
	for(lfit = linfaces, fputs("# Wall uvs""\n", file_obj); lfit < lfptr; lfit++){
		struct wad_texture *wt = 0;
		for(i = 0; i < num_textures; i++){
			if(!strncmp(lfit->tex, textures[i]->name, 8)){
				wt = textures[i];
				break;
			}
		}
		if(!wt) wt = textures[0];
		
		for(i = 0; i < lfit->face.num_verts; i++)
			fprintf(file_obj, "vt %f %f\n",
				lfit->face.verts[i].u / ((float) wt->width),
				lfit->face.verts[i].v / ((float) wt->height)
			);
	}
	int num_verts = 0;
	fputs("# Flat faces""\n", file_obj);
	for(ffit = flatfaces; ffit < ffptr; ffit++){
		if(strncmp(tex, ffit->tex, 8)){
			memcpy(tex, ffit->tex, 8);
			fprintf(file_obj, "usemtl %.8s\n", tex);
		}
		fprintf(file_obj, "f");
		for(i = 0; i < ffit->face.num_verts; i++, num_verts++)
			fprintf(file_obj, " %d/%d",
				num_verts + 1,
				num_verts + 1
			);
		fprintf(file_obj, "\n");
	}
	for(lfit = linfaces, fputs("# Wall faces""\n", file_obj); lfit < lfptr; lfit++){
		if(strncmp(tex, lfit->tex, 8)){
			memcpy(tex, lfit->tex, 8);
			fprintf(file_obj, "usemtl %.8s\n", tex);
		}
		fprintf(file_obj, "f");
		for(i = 0; i < lfit->face.num_verts; i++, num_verts++)
			fprintf(file_obj, " %d/%d",
				num_verts + 1,
				num_verts + 1
			);
		fprintf(file_obj, "\n");
	}
	fclose(file_obj);
}