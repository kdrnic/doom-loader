#include <stdio.h>
#include <errno.h>

#include <allegro.h>

#include "rectpack.h"
#include "arr.h"
#include "sprsheet.h"
#include "bmputil.h"
#include "readfile.h"
#include "jsmn.h"

struct sprsheet *load_sprsheet_json(const char *fn)
{
	struct sprsheet *s = malloc(sizeof(*s));
	if(load_sprsheet_json_ex(s, fn)){
		free(s);
		return 0;
	}
	return s;
}

static int jsoneq(const char *json, jsmntok_t *tok, const char *s)
{
	if(tok->type == JSMN_STRING && (int)strlen(s) == tok->end - tok->start &&
		strncmp(json + tok->start, s, tok->end - tok->start) == 0){
		return 1;
	}
	return 0;
}

static int jsonfalse(const char *json, jsmntok_t *tok)
{
	if(tok->type == JSMN_PRIMITIVE && strncmp(json + tok->start, "false", tok->end - tok->start) == 0){
		return 1;
	}
	return 0;
}

static int jsontrue(const char *json, jsmntok_t *tok)
{
	if(tok->type == JSMN_PRIMITIVE && strncmp(json + tok->start, "true", tok->end - tok->start) == 0){
		return 1;
	}
	return 0;
}

int load_sprsheet_json_ex(struct sprsheet *sheet, const char *fn_)
{
	jsmn_parser jp;
	jsmntok_t *jtoks = 0;
	
	char fn[256];
	canonicalize_filename(fn, fn_, sizeof(fn));
	
	#define ERROR(val) ({ errno = (val); goto end; })
	
	if(!exists(fn)) replace_extension(fn, fn, "jso", 256);
	if(!exists(fn)){
		ERROR(LOAD_SPRSHEET_JSON_ERR_JSONNOTFOUND);
	}
	
	char *json = 0;
	long jlen = read_file(fn, &json, 0);
	if((jlen <= 0) || !json){
		ERROR(LOAD_SPRSHEET_JSON_ERR_READFILE);
	}
	
	replace_extension(fn, fn, "png", 256);
	if(!exists(fn)) replace_extension(fn, fn, "bmp", 256);
	if(!exists(fn)) replace_extension(fn, fn, "pcx", 256);
	if(!exists(fn)){
		ERROR(LOAD_SPRSHEET_JSON_ERR_BMPNOTFOUND);
	}
	
	sheet->src = kdr_load_bitmap32(fn);
	if(!sheet->src){
		ERROR(LOAD_SPRSHEET_JSON_ERR_LOADBITMAP);
	}
	alpha_to_transp(sheet->src, 128);
	if(get_color_depth() == 8){
		PALETTE pal;
		get_palette(pal);
		BITMAP *tmp = bitmap_to8bpp(sheet->src, pal);
		destroy_bitmap(sheet->src);
		sheet->src = tmp;
	}
	
	jsmn_init(&jp);
	
	int ntoks, maxtoks = 500;
	do{
		maxtoks *= 2;
		jtoks = realloc(jtoks, maxtoks * sizeof(*jtoks));
		ntoks = jsmn_parse(&jp, json, jlen, jtoks, maxtoks);
	} while(ntoks == JSMN_ERROR_NOMEM);
	if(ntoks < 1){
		ERROR(LOAD_SPRSHEET_JSON_ERR_JSONPARSE);
	}
	
	if(jtoks[0].type != JSMN_OBJECT){
		ERROR(LOAD_SPRSHEET_JSON_ERR_OBJEXPECTED);
	}
	if(!jsoneq(json, jtoks + 1, "frames")){
		ERROR(LOAD_SPRSHEET_JSON_ERR_NOTFRAMES);
	}
	if(jtoks[2].type != JSMN_OBJECT){
		ERROR(LOAD_SPRSHEET_JSON_ERR_OBJEXPECTED);
	}
	
	arr_t(__typeof__ (*sheet->frames)) frames = {};
	
	sheet->w = 0;
	sheet->h = 0;
	
	int i, j, k, l;
	for(i = 3, l = jtoks[2].size; l; l--, i++){
		if(jtoks[i].type != JSMN_STRING){
			ERROR(LOAD_SPRSHEET_JSON_ERR_STREXPECTED);
		}
		#define JSON_OBJ() ({\
			i++; \
			if(jtoks[i].type != JSMN_OBJECT){ \
				ERROR(LOAD_SPRSHEET_JSON_ERR_OBJEXPECTED); \
			}\
		})
		
		JSON_OBJ();
		
		__typeof__ (*sheet->frames) frame = {};
		
		for(k = jtoks[i].size; k; k--){
			i++;
			if(jsoneq(json, jtoks + i, "frame")){
				JSON_OBJ();
				
				for(j = 0; j < 4; j++){
					i++;
					if(jsoneq(json, jtoks + i, "x"))
						frame.x = strtol(json + jtoks[++i].start, 0, 10);
					else if(jsoneq(json, jtoks + i, "y"))
						frame.y = strtol(json + jtoks[++i].start, 0, 10);
					else if(jsoneq(json, jtoks + i, "w"))
						frame.w = strtol(json + jtoks[++i].start, 0, 10);
					else if(jsoneq(json, jtoks + i, "h"))
						frame.h = strtol(json + jtoks[++i].start, 0, 10);
					else{
						ERROR(LOAD_SPRSHEET_JSON_ERR_PROPERTYFRAME);
					}
				}
			}
			else if(jsoneq(json, jtoks + i, "rotated")){
				i++;
				if(jsontrue(json, jtoks + i)){
					ERROR(LOAD_SPRSHEET_JSON_ERR_ROTATED);
				}
			}
			else if(jsoneq(json, jtoks + i, "trimmed")){
				i++;
			}
			else if(jsoneq(json, jtoks + i, "duration")){
				frame.duration = strtol(json + jtoks[++i].start, 0, 10);
			}
			else if(jsoneq(json, jtoks + i, "sourceSize")){
				JSON_OBJ();
				
				int w = 0, h = 0;
				for(j = 0; j < 2; j++){
					i++;
					if(jsoneq(json, jtoks + i, "w"))
						w = strtol(json + jtoks[++i].start, 0, 10);
					else if(jsoneq(json, jtoks + i, "h"))
						h = strtol(json + jtoks[++i].start, 0, 10);
					else{
						ERROR(LOAD_SPRSHEET_JSON_ERR_PROPERTYSRCSIZE);
					}
				}
				if(sheet->w && sheet->w != w) ERROR(LOAD_SPRSHEET_JSON_ERR_MULTIWIDTH);
				else sheet->w = w;
				if(sheet->h && sheet->h != h) ERROR(LOAD_SPRSHEET_JSON_ERR_MULTIWIDTH);
				else sheet->h = h;
			}
			else if(jsoneq(json, jtoks + i, "spriteSourceSize")){
				JSON_OBJ();
				
				int w = 0, h = 0;
				for(j = 0; j < 4; j++){
					i++;
					if(jsoneq(json, jtoks + i, "x"))
						frame.off_x = strtol(json + jtoks[++i].start, 0, 10);
					else if(jsoneq(json, jtoks + i, "y"))
						frame.off_y = strtol(json + jtoks[++i].start, 0, 10);
					else if(jsoneq(json, jtoks + i, "w"))
						w = strtol(json + jtoks[++i].start, 0, 10);
					else if(jsoneq(json, jtoks + i, "h"))
						h = strtol(json + jtoks[++i].start, 0, 10);
					else{
						ERROR(LOAD_SPRSHEET_JSON_ERR_PROPERTYSPRSRCSIZE);
					}
				}
				if(frame.w && frame.w != w) ERROR(LOAD_SPRSHEET_JSON_ERR_MULTIWIDTH);
				else frame.w = w;
				if(frame.h && frame.h != h) ERROR(LOAD_SPRSHEET_JSON_ERR_MULTIWIDTH);
				else frame.h = h;
			}
			else{
				ERROR(LOAD_SPRSHEET_JSON_ERR_PROPERTY);
			}
		}
		arr_push(&frames, frame);
	}
	
	sheet->num_frames = frames.len;
	sheet->frames = frames.data;
	
	//for(i = 0; i < sheet->num_frames; i++){
	//	clear_to_color(screen, 1);
	//	masked_blit(
	//		sheet->src, screen,
	//		sheet->frames[i].x, sheet->frames[i].y,
	//		sheet->frames[i].off_x, sheet->frames[i].off_y,
	//		sheet->frames[i].w, sheet->frames[i].h
	//	);
	//	rest(10);
	//}
	
	optimize_sprsheet(sheet);
	
	free(json);
	free(jtoks);
	return 0;
	
	end:
	if(json) free(json);
	if(jtoks) free(jtoks);
	return 1;
}

void destroy_sprsheet(struct sprsheet *sheet)
{
	destroy_bitmap(sheet->src);
	free(sheet->frames);
	free(sheet);
}

struct opt_frame_data
{
	BITMAP *bmp;
	int idx;
};

void optimize_sprsheet(struct sprsheet *sheet)
{
	struct sprsheet optimized_ = *sheet, *optimized = &optimized_;
	
	int i, total_area = 0;
	for(i = 0; i < sheet->num_frames; i++){
		total_area += sheet->frames[i].w * sheet->frames[i].h;
	}
	if((total_area * 100) / sheet->src->w * sheet->src->h < 95) return;
	
	optimized->frames = calloc(sheet->num_frames, sizeof(*optimized->frames));
	
	arr_t(struct rp_rect) frame_rects_in arr_cleanup = {};
	arr_t(struct rp_rect) frame_rects_out arr_cleanup = {};
	arr_t(struct opt_frame_data) frame_data arr_cleanup = {};
	
	arr_reserve(&frame_rects_in, sheet->num_frames);
	arr_reserve(&frame_rects_out, sheet->num_frames);
	arr_reserve(&frame_data, sheet->num_frames);
	for(i = 0; i < sheet->num_frames; i++){
		BITMAP *tmp = create_sub_bitmap(
			sheet->src,
			sheet->frames[i].x, sheet->frames[i].y,
			sheet->frames[i].w, sheet->frames[i].h
		);
		int off_x, off_y;
		BITMAP *trimmed = trim_bitmap(tmp, &off_x, &off_y);
		destroy_bitmap(tmp);
		
		optimized->frames[i].off_x = off_x;
		optimized->frames[i].off_y = off_y;
		optimized->frames[i].off_x += sheet->frames[i].off_x;
		optimized->frames[i].off_y += sheet->frames[i].off_y;
		optimized->frames[i].w = trimmed->w;
		optimized->frames[i].h = trimmed->h;
		
		struct opt_frame_data fd = {trimmed, i};
		arr_push(&frame_data, fd);
		struct rp_rect fr = {
			.data = arr_lastp(&frame_data),
			.w = trimmed->w,
			.h = trimmed->h,
		};
		arr_push(&frame_rects_in, fr);
	}
	
	int w, h;
	rectpack(frame_rects_in.data, frame_rects_out.data, frame_rects_in.len, &w, &h);
	frame_rects_out.len = frame_rects_in.len;
	optimized->src = create_bitmap(w, h);
	
	arr_foreach(&frame_rects_out, frame, idx, ({
		struct opt_frame_data *fd = frame.data;
		BITMAP *bmp = fd->bmp;
		if(optimized->src){
			blit(bmp, optimized->src, 0, 0, frame.x, frame.y, frame.w, frame.h);
		}
		destroy_bitmap(bmp);
		
		optimized->frames[fd->idx].x = frame.x;
		optimized->frames[fd->idx].y = frame.y;
	}));
	
	//for(i = 0; i < sheet->num_frames; i++){
	//	clear(screen);
	//	masked_blit(
	//		optimized->src, screen,
	//		optimized->frames[i].x, optimized->frames[i].y,
	//		optimized->frames[i].off_x, optimized->frames[i].off_y,
	//		optimized->frames[i].w, optimized->frames[i].h
	//	);
	//	rest(10);
	//}
	if(optimized->src){
		destroy_bitmap(sheet->src);
		free(sheet->frames);
		*sheet = *optimized;
	}
	else{
		free(optimized->frames);
	}
}
