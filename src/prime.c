#include <assert.h>

#include "prime.h"

//Returns (b^e)%m
modpow_t modular_pow(modpow_t base, modpow_t exponent, modpow_t modulus)
{
	modpow_t result = 1;
	
	if(modulus == 1) return 0;
	
	//assert(UINT_MAX / (modulus - 1) > (modulus - 1));
	
	base %= modulus;
	while(exponent > 0){
		if(exponent & 1){
			result = (result * base) % modulus;
		}
		exponent >>= 1;
		base = (base * base) % modulus;
	}
	return result;
}

//Primality test
//Accurate at least up to 2 billion
int miller_rabin_test(int n)
{
	const int bases[] = {2, 7, 61};
	int r = 0, d = n - 1, i, j;
	
	assert(n < 2000000000);
	
	if(n == 2 || n == 3) return 1;
	if((n & 1) == 0 || n < 2) return 0;
	
	while(!(d & 1)){
		d >>= 1;
		r++;
	}
	for(j = 0; j < sizeof(bases) / sizeof(bases[0]); j++){
		int a = bases[j];
		int x = modular_pow(a, d, n);
		if(x == 1 || x == n - 1) continue;
		for(i = 1; i < r; i++){
			x = modular_pow(x, 2, n);
			if(x == n - 1) break;
		}
		if(i == r) return 0;
	}
	return 1;
}

//Returns a prime greater than
int get_prime_gt(int a)
{
	if(a < 2) return 2;
	while(!miller_rabin_test(a)){
		a++;
	}
	return a;
}
