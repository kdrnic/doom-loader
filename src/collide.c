#include <math.h>
#include <float.h>
#include <stdio.h>

#include "vector.h"
#include "bsp.h"
#include "collide.h"
#include "rdebug.h"

#define COLLIDE_TOLERANCE			0.001
#define COLLIDE_TOLERANCE_FACTOR	0.999

static inline void poly_bounds(const rvface *tri, const vec3 *axis, double *min, double *max)
{
	int i;
	
	*min = FLT_MAX;
	*max = -FLT_MAX;
	
	for(i = 0; i < tri->num_verts; i++){
		double tmp = vec3_dot((vec3 *) &tri->verts[i], axis);
		if(tmp < *min) *min = tmp;
		if(tmp > *max) *max = tmp;
	}
}

static inline void box_bounds(const vec3 *box_s, const vec3 *axis, double *min, double *max)
{
	int i;
	vec3 v;
	double tmp;
	
	*min = FLT_MAX;
	*max = -FLT_MAX;
	
	for(i = 0; i < 8; i++){
		v.x = (i & 1) ? box_s->x : -box_s->x;
		v.y = (i & 2) ? box_s->y : -box_s->y;
		v.z = (i & 4) ? box_s->z : -box_s->z;
		
		tmp = vec3_dot(&v, axis);
		if(tmp < *min) *min = tmp;
		if(tmp > *max) *max = tmp;
	}
}

static inline void box_bounds_ex(const vec3 *box_s, const vec3 *box_pos, const vec3 *axis, double *min, double *max)
{
	int i;
	vec3 v;
	double tmp;
	
	*min = FLT_MAX;
	*max = -FLT_MAX;
	
	for(i = 0; i < 8; i++){
		v.x = box_pos->x + ((i & 1) ? box_s->x : -box_s->x);
		v.y = box_pos->y + ((i & 2) ? box_s->y : -box_s->y);
		v.z = box_pos->z + ((i & 4) ? box_s->z : -box_s->z);
		
		tmp = vec3_dot(&v, axis);
		if(tmp < *min) *min = tmp;
		if(tmp > *max) *max = tmp;
	}
}

int poly_testbox(const rvface *tri, const vec3 *box_s)
{
	int i, j;
	
	vec3 edge_axes[3 * RVFACE_MAX_VERTS];
	vec3 tri_normal;
	const vec3 *axes[4 + 3 * RVFACE_MAX_VERTS] = {
		//3 box face normals
		&vec3c_x,
		&vec3c_y,
		&vec3c_z,
		//triangle normal
		&tri_normal
	};
	int skip_axis[4 + 3 * RVFACE_MAX_VERTS] = {0};
	
	vec3_normal((vec3 *)tri->verts, (vec3 *)(tri->verts + 1), (vec3 *)(tri->verts + 2), &tri_normal);
	
	//edge to edge cross products
	for(i = 0; i < tri->num_verts; i++){
		vec3 tri_edge;
		vec3_sub((vec3 *)(tri->verts + i), (vec3 *)(tri->verts + ((i + 1) % tri->num_verts)), &tri_edge);
		
		for(j = 0; j < 3; j++){
			const int idx = 4 + i * 3 + j;
			vec3 *a = edge_axes + (idx - 4);
			vec3_cross(&tri_edge, axes[j], a);
			
			if(
				(vec3_sqlen(a) < MPLANE_TOLERANCE) ||
				(fabs(vec3_dot(axes[j], a)) > MPLANE_TOLERANCE) ||
				(fabs(vec3_dot(&tri_edge, a)) > MPLANE_TOLERANCE)
			){
				skip_axis[idx] = 1;
				continue;
			}
			
			axes[idx] = a;
		}
	}
	
	for(i = 0; i < 4 + 3 * tri->num_verts; i++){
		double box_min, box_max, tri_min, tri_max;
		
		if(skip_axis[i]) continue;
		
		box_bounds(box_s, axes[i], &box_min, &box_max);
		poly_bounds(tri, axes[i], &tri_min, &tri_max);
		
		if(tri_max < box_min) return 0;
		if(tri_min > box_max) return 0;
	}
	
	return 1;
}

double box_tracebox(const struct vec3 *other_s, const struct vec3 *other_pos, const struct vec3 *box_s, const struct vec3 *move, struct vec3 *hit_normal)
{
	int i, j;
	const vec3 *axes[3] = {
		//3 box face normals
		&vec3c_x,
		&vec3c_y,
		&vec3c_z
	};
	
	vec3 axis_time_s;
	//will hold an intersection of all contact intervals
	//float time_s_max = 0.0, time_e_min = 1.0;
	double time_s_max = -FLT_MAX, time_e_min = FLT_MAX;
	
	for(i = 0; i < 3; i++){
		double box_min, box_max, other_min, other_max;
		//contact interval: time of first and last contact
		double time_s, time_e;
		
		vec3 axis = *axes[i];
		//speed of box on axis
		double dot = vec3_dot(axes[i], move);
		if(dot < 0){
			dot *= -1;
			vec3_mult(&axis, -1);
		}
		if(dot) dot = 1.0 / dot;
		
		box_bounds(box_s, &axis, &box_min, &box_max);
		box_bounds_ex(other_s, other_pos, &axis, &other_min, &other_max);
		
		//if box is not moving it is just a straightforward intersection test
		if(dot == 0){
			if(other_max <= box_min) return 1.0;
			if(other_min >= box_max) return 1.0;
			continue;
		}
		else{
			//moving forwards and is in front of triangle
			//tolerance added keeps boxes just touching getting stuck
			if(box_min * COLLIDE_TOLERANCE_FACTOR >= other_max - COLLIDE_TOLERANCE) return 1.0;
			
			//calc times
			time_s = (other_min - box_max) * dot;
			time_e = (other_max - box_min) * dot;
		}
		
		if(time_s > time_e) return 1.0;
		
		//intersect intervals
		if(time_s > time_s_max){
			axis_time_s = axis;
			vec3_mult(&axis_time_s, -1);
			time_s_max = time_s;
		}
		if(time_e < time_e_min) time_e_min = time_e;
	}
	
	//interval is empty?
	if(time_e_min <= time_s_max) return 1.0;
	
	if(hit_normal){
		*hit_normal = axis_time_s;
	}
	
	if(time_s_max == -FLT_MAX){
		time_s_max = 0;
	}
	
	return time_s_max;
}

double poly_tracebox(const rvface *tri, const vec3 *box_s, const vec3 *move, vec3 *hit_normal)
{
	int i, j;
	
	vec3 edge_axes[3 * RVFACE_MAX_VERTS];
	vec3 tri_normal;
	vec3 axes[4 + 3 * RVFACE_MAX_VERTS] = {
		//3 box face normals
		vec3c_x,
		vec3c_y,
		vec3c_z,
		//triangle normal
		//tri_normal
	};
	
	//hit_normal candidate
	vec3 axis_time_s;
	//const vec3 *just_touching = 0;
	//will hold an intersection of all contact intervals
	//float time_s_max = 0.0, time_e_min = 1.0;
	double time_s_max = -FLT_MAX, time_e_min = FLT_MAX;
	int skip_axis[4 + 3 * RVFACE_MAX_VERTS] = {0};
	
	vec3_normal((vec3 *)tri->verts, (vec3 *)(tri->verts + 1), (vec3 *)(tri->verts + 2), &tri_normal);
	axes[3] = tri_normal;
	
	//edge to edge cross products
	for(i = 0; i < tri->num_verts; i++){
		vec3 tri_edge;
		vec3_sub((vec3 *)(tri->verts + i), (vec3 *)(tri->verts + ((i + 1) % tri->num_verts)), &tri_edge);
		
		for(j = 0; j < 3; j++){
			vec3 *a = edge_axes + (i * 3 + j);
			vec3_cross(&tri_edge, axes + j, a);
			axes[4 + i * 3 + j] = *a;
			//TODO: better check for degenerate cross products
			//This one is good enough to keep the tolerance adding from making the test fail
			if(
				(vec3_sqlen(a) < MPLANE_TOLERANCE) ||
				(fabs(vec3_dot(axes + j, a)) > MPLANE_TOLERANCE) ||
				(fabs(vec3_dot(&tri_edge, a)) > MPLANE_TOLERANCE)
			){
				skip_axis[4 + i * 3 + j] = 1;
			}
		}
	}
	
	for(i = 0; i < 4 + 3 * tri->num_verts; i++){
		double box_min, box_max, tri_min, tri_max;
		//contact interval: time of first and last contact
		double time_s, time_e;
		
		if(skip_axis[i]) continue;
		
		double dot;
		vec3 axis = axes[i];
		
		do_axis:
		//speed of box on axis
		dot = vec3_dot(&axis, move);
		if(dot) dot = 1.0 / dot;
		if(dot < 0){
			vec3_mult(&axis, -1);
			goto do_axis;
		}
		
		box_bounds(box_s, &axis, &box_min, &box_max);
		poly_bounds(tri, &axis, &tri_min, &tri_max);
		
		//if box is not moving it is just a straightforward intersection test
		if(fabs(dot) < COLLIDE_TOLERANCE){
			if(tri_max <= box_min * COLLIDE_TOLERANCE_FACTOR) return 1.0;
			if(tri_min >= box_max * COLLIDE_TOLERANCE_FACTOR) return 1.0;
			continue;
		}
		else{
			//moving forwards and is in front of triangle
			//tolerance added keeps boxes just touching getting stuck
			if(box_min * COLLIDE_TOLERANCE_FACTOR >= tri_max - COLLIDE_TOLERANCE) return 1.0;
			
			//calc times
			time_s = (tri_min - box_max) * dot;
			time_e = (tri_max - box_min) * dot;
		}
		
		if(time_s > time_e) return 1.0;
		
		//intersect intervals
		if(time_s > time_s_max){
			axis_time_s = axis;
			vec3_mult(&axis_time_s, -1);
			time_s_max = time_s;
		}
		if(time_e < time_e_min) time_e_min = time_e;
	}
	
	//interval is empty?
	if(time_e_min <= time_s_max) return 1.0;
	
	if(hit_normal){
		vec3_normalize(&axis_time_s);
		*hit_normal = axis_time_s;
	}
	
	if(time_s_max < 0) time_s_max = 0;
	
	return time_s_max;
}

int poly_pushbox(const rvface *tri, const vec3 *box_s, vec3 *push)
{
	int i, j;
	
	vec3 edge_axes[3 * RVFACE_MAX_VERTS];
	vec3 tri_normal;
	const vec3 *axes[4 + 3 * RVFACE_MAX_VERTS] = {
		//3 box face normals
		&vec3c_x,
		&vec3c_y,
		&vec3c_z,
		//triangle normal
		&tri_normal
	};
	
	double least_push = FLT_MAX;
	int least_axis = 0;
	int skip_axis[4 + 3 * RVFACE_MAX_VERTS] = {0};
	
	vec3_normal((vec3 *)tri->verts, (vec3 *)(tri->verts + 1), (vec3 *)(tri->verts + 2), &tri_normal);
	
	//edge to edge cross products
	for(i = 0; i < tri->num_verts; i++){
		vec3 tri_edge;
		vec3_sub((vec3 *)(tri->verts + i), (vec3 *)(tri->verts + ((i + 1) % tri->num_verts)), &tri_edge);
		
		for(j = 0; j < 3; j++){
			const int idx = 4 + i * 3 + j;
			vec3 *a = edge_axes + (idx - 4);
			vec3_cross(&tri_edge, axes[j], a);
			
			if(
				(vec3_sqlen(a) < MPLANE_TOLERANCE) ||
				(fabs(vec3_dot(axes[j], a)) > MPLANE_TOLERANCE) ||
				(fabs(vec3_dot(&tri_edge, a)) > MPLANE_TOLERANCE)
			){
				skip_axis[idx] = 1;
				continue;
			}
			
			vec3_normalize(a);
			axes[idx] = a;
		}
	}
	
	for(i = 0; i < 4 + 3 * tri->num_verts; i++){
		double box_min, box_max, tri_min, tri_max;
		double m1 = FLT_MAX, m2 = -FLT_MAX;
		
		if(skip_axis[i]) continue;
		
		box_bounds(box_s, axes[i], &box_min, &box_max);
		poly_bounds(tri, axes[i], &tri_min, &tri_max);
		
		if(tri_max < box_min) return 0;
		else m1 = tri_max - box_min;
		if(tri_min > box_max) return 0;
		else m2 = tri_min - box_max;
		
		if(m1 + m2 < 0){
			if(m1 < fabs(least_push)){
				least_axis = i;
				least_push = m1;
			}
		}
		else if(-m2 < fabs(least_push)){
			least_axis = i;
			least_push = m2;
		}
	}
	
	*push = *axes[least_axis];
	vec3_mult(push, least_push);
	
	return 1;
}

int bsp_testbox(const bspdata *bsp, const vec3 *box_s, const vec3 *box_pos)
{
	int i, j;
	
	for(i = 0; i < bsp->num_faces; i++){
		rvface tri = *(rvface *)(bsp->faces + i);
		for(j = 0; j < tri.num_verts; j++){
			vec3_subfrom(&(tri.verts + j)->pos, box_pos);
		}
		if(poly_testbox(&tri, box_s)) return 1;
	}
	
	return 0;
}

static double bsp_tracebox_node(bsp_tracebox_q *q, const int node_i)
{
	double min_time = 1.0;
	
	if(node_i == BSPLEAF_SOLID) return 1.0;
	if(!BSPNODE_ISLEAF(node_i)){
		const bspnode *node = q->bsp->nodes + node_i;
		const mplane *node_plane = q->bsp->planes + node->plane;
		vec3 VEC3_SC2MM(q->box_s, q->box_pos, box_min, box_max);
		const vec3 end = VEC3_ADD(q->box_pos, q->move);
		const int ps = vec3_planecmp(node_plane, &q->box_pos);
		const int pe = vec3_planecmp(node_plane, &end);
		
		//If movement starts and ends within single child node, go down it
		if((ps == pe) && (!mplane_cutbox_mm(node_plane, &box_min, &box_max, 0, 0))){
			vec3_addto(&box_min, &q->move);
			vec3_addto(&box_max, &q->move);
			if(!mplane_cutbox_mm(node_plane, &box_min, &box_max, 0, 0)){
				return bsp_tracebox_node(q, node->children[!ps]);
			}
		}
		
		//Otherwise check both nodes, closest first
		min_time = bsp_tracebox_node(q, node->children[!ps]);
		if(min_time >= 1.0) min_time = bsp_tracebox_node(q, node->children[ps]);
	}
	else{
		const int leaf_i = BSPNODE_TO_LEAF(node_i);
		const bspleaf *leaf = q->bsp->leafs + leaf_i;
		int i, j;
		double time;
		vec3 tmp;
		
		for(i = leaf->first_face; i <= leaf->last_face; i++){
			rvface tri = *(rvface *)(q->bsp->faces + i);
			for(j = 0; j < tri.num_verts; j++){
				vec3_subfrom(&(tri.verts + j)->pos, &q->box_pos);
			}
			time = poly_tracebox(&tri, &q->box_s, &q->move, &tmp);
			if(time < min_time){
				min_time = time;
				q->hit_normal = tmp;
				q->face_id = i;
			}
		}
		
		if(q->on_leaf){
			q->time = min_time;
			time = q->on_leaf(q, leaf_i);
			if(time < min_time) min_time = time;
		}
	}
	
	return min_time;
}

double bsp_tracebox(bsp_tracebox_q *q)
{
	return (q->time = bsp_tracebox_node(q, 0));
}

static int bsp_pushbox_node(const bspdata *bsp, int node_i, const vec3 *box_s, vec3 *box_pos, vec3 *push)
{
	int col = 0;
	const vec3 VEC3_SC2MM(*box_s, *box_pos, box_min, box_max);
	
	if(node_i == BSPLEAF_SOLID) return 0;
	if(!BSPNODE_ISLEAF(node_i)){
		const bspnode *node = &bsp->nodes[node_i];
		const mplane *node_plane = bsp->planes + node->plane;
		
		//If entirely within single child node
		if(!mplane_cutbox_mm(node_plane, &box_min, &box_max, 0, 0)){//new_box_min, new_box_max)){
			const int pc = vec3_planecmp(node_plane, box_pos);
			col = bsp_pushbox_node(bsp, node->children[!pc], box_s, box_pos, push);
		}
		//Otherwise send down both nodes (no cutting)
		else{
			col = bsp_pushbox_node(bsp, node->children[0], box_s, box_pos, push);
			col |= bsp_pushbox_node(bsp, node->children[1], box_s, box_pos, push);
		}
	}
	else{
		const int leaf_i = BSPNODE_TO_LEAF(node_i);
		const bspleaf *leaf = &bsp->leafs[leaf_i];
		int i, j;
		vec3 tmp;
		
		if(!vec3_box2box_mm(&box_min, &box_max, &leaf->min, &leaf->max, 0)) return 0;
		
		for(i = leaf->first_face; i <= leaf->last_face; i++){
			rvface tri = *(rvface *)(bsp->faces + i);
			for(j = 0; j < tri.num_verts; j++){
				vec3_subfrom(&(tri.verts + j)->pos, box_pos);
			}
			if(poly_pushbox(&tri, box_s, &tmp)){
				vec3_addto(push, &tmp);
				vec3_addto(box_pos, &tmp);
				col = 1;
			}
		}
	}
	
	return col;
}

int bsp_pushbox(const bspdata *bsp, const vec3 *box_s, vec3 *box_pos, vec3 *push)
{
	vec3 pos = *box_pos;
	*push = vec3c_origin;
	return bsp_pushbox_node(bsp, 0, box_s, &pos, push);
}

static int bsp_pick_node(bsp_pick_q *q, const int node_i, const vec3 *start, const vec3 *end)
{
	int col = 0;
	
	if(node_i == BSPLEAF_SOLID) return 0;
	if(!BSPNODE_ISLEAF(node_i)){
		const bspnode *node = q->bsp->nodes + node_i;
		const mplane *node_plane = q->bsp->planes + node->plane;
		const int ps = vec3_planecmp(node_plane, start);
		const int pe = vec3_planecmp(node_plane, end);
		
		//If start and end on same child node
		if(ps == pe){
			col = bsp_pick_node(q, node->children[!ps], start, end);
		}
		//Otherwise split and try nearest half first
		else{
			vec3 mid;
			vec3_segment2plane(node_plane, start, end, &mid, 0);
			
			col = 
				bsp_pick_node(q, node->children[!ps], start, &mid);
			if(!col) col = 
				bsp_pick_node(q, node->children[!pe], &mid, end);
		}
	}
	else{
		const int leaf_i = BSPNODE_TO_LEAF(node_i);
		const bspleaf *leaf = q->bsp->leafs + leaf_i;
		int i;
		vec3 v3res;
		vecc_t dist, min_dist = FLT_MAX;
		
		if(!vec3_testsegmentaabb_mm(start, end, &leaf->min, &leaf->max)){
			return 0;
		}
		
		for(i = leaf->first_face; i <= leaf->last_face; i++){
			const rvface *tri = (rvface *)(q->bsp->faces + i);
			vec3 tmin, tmax;
			
			rvface_bounds(tri, &tmin, &tmax);
			
			if(!vec3_testsegmentaabb_mm(start, end, &tmin, &tmax)){
				continue;
			}
			
			if(rvface_pick(tri, &q->start, &q->dir, &v3res, &dist)){
				col = 1;
				
				if(dist > min_dist) continue;
				min_dist = dist;
				
				q->res = v3res;
				q->face_id = i;
			}
		}
		
		if(q->on_leaf){
			if(col) q->dist = min_dist;
			q->on_leaf(q, leaf_i);
		}
	}
	
	return col;
}

int bsp_pick(bsp_pick_q *q)
{
	if(!vec3_sqlen(&q->dir)){
		q->dir = q->end;
		vec3_subfrom(&q->dir, &q->start);
		vec3_normalize(&q->dir);
	}
	
	q->dist = FLT_MAX;
	q->res = q->end;
	q->face_id = -1;
	
	return bsp_pick_node(q, 0, &q->start, &q->end);
}

static void bsp_boxleafs_node(const struct bspdata *bsp, int node_i, const struct vec3 *box_s, const struct vec3 *box_pos, int *leafs, int *nleafs)
{
	const vec3 VEC3_SC2MM(*box_s, *box_pos, box_min, box_max);
	
	if(node_i == BSPLEAF_SOLID) return;
	if(!BSPNODE_ISLEAF(node_i)){
		const bspnode *node = &bsp->nodes[node_i];
		const mplane *node_plane = bsp->planes + node->plane;
		vec3 new_box_min[2], new_box_max[2];
		
		//If entirely within single child node
		if(!mplane_cutbox_mm(node_plane, &box_min, &box_max, new_box_min, new_box_max)){
			const int pc = vec3_planecmp(node_plane, box_pos);
			bsp_boxleafs_node(bsp, node->children[!pc], box_s, box_pos, leafs, nleafs);
		}
		
		//Otherwise send each cut half into separate node
		else{
			const vec3 VEC3_MM2SC(new_box_min[0], new_box_max[0], new_box_s0, new_box_pos0);
			const vec3 VEC3_MM2SC(new_box_min[1], new_box_max[1], new_box_s1, new_box_pos1);
			
			bsp_boxleafs_node(bsp, node->children[0], &new_box_s1, &new_box_pos1, leafs, nleafs);
			bsp_boxleafs_node(bsp, node->children[1], &new_box_s0, &new_box_pos0, leafs, nleafs);
		}
	}
	else{
		const int leaf_i = BSPNODE_TO_LEAF(node_i);
		if(*nleafs < BSP_BOXLEAFS_MAX) leafs[(*nleafs)++] = leaf_i;
	}
}

int bsp_boxleafs(const struct bspdata *bsp, const struct vec3 *box_s, const struct vec3 *box_pos, int *leafs)
{
	int nleafs = 0;
	bsp_boxleafs_node(bsp, 0, box_s, box_pos, leafs, &nleafs);
	return nleafs;
}
