#include <allegro.h>
#include <stdio.h>

#include "bsp.h"
#include "collide.h"
#include "rdebug.h"

int bsp_boxcut(const bspdata *bsp, const struct MATRIX_f *mat, const vec3 *siz, rvface *faces, int *num_faces, int *idx_leafs)
{
	int i, j, k;
	mplane box_planes[6];
	vec3 normals[6] = {
		{1.0, 0.0, 0.0},
		{-1.0, 0.0, 0.0},
		{0.0, 1.0, 0.0},
		{0.0, -1.0, 0.0},
		{0.0, 0.0, 1.0},
		{0.0, 0.0, -1.0}
	};
	mplane mplanes[6];
	vec3 box_pos, box_s;
	int leafs[BSP_BOXLEAFS_MAX], n, excess_faces = 0;
	
	matrix_f_to_trs(&box_pos, 0, &box_s, mat);
	box_s.x *= siz->x * 0.5;
	box_s.y *= siz->y * 0.5;
	box_s.z *= siz->z * 0.5;
	if(box_s.x > box_s.y) box_s.y = box_s.x;
	if(box_s.y > box_s.z) box_s.z = box_s.y;
	box_s.x = box_s.z;
	box_s.y = box_s.z;
	
	MATRIX_f imat = *mat;
	matrix_f_invert2(&imat);
	matrix_f_transp(&imat);
	
	for(i = 0; i < 6; i++){
		vec3 p = {normals[i].x * siz->x * -0.5, normals[i].y * siz->y * -0.5, normals[i].z * siz->z * -0.5};
		vec3_apply_matrix_f(&p, mat);
		
		//rdebug_point(&p);
		
		vec3_apply_matrix_f(normals + i, &imat);
		vec3_normalize(normals + i);
		
		vec3_point2plane(&p, normals + i, mplanes + i);
	}
	
	*num_faces = 0;
	n = bsp_boxleafs(bsp, &box_s, &box_pos, leafs);
	//rdebug_box_sc(&box_s, &box_pos);
	//rdebug_printf(&box_pos, "n=%d", n);
	for(i = 0; i < n; i++){
		int leaf_i = leafs[i];
		const bspleaf *leaf = bsp->leafs + leaf_i;
		for(j = leaf->first_face; j <= leaf->last_face; j++){
			rvface face = *((rvface *)(bsp->faces + j));
			rvface front_face, back_face;
			
			//rdebug_set_colour(0xFF00);
			//rdebug_rvface(&face);
			
			for(k = 0; k < 6 && face.num_verts; k++){
				switch(rvface_sort(mplanes + k, &face)){
					case RVFACE_BACK:
						face.num_verts = 0;
						break;
					case RVFACE_FRONT:
						break;
					case RVFACE_COPLANAR:
						break;
					case RVFACE_SPANNING:
						rvface_split(mplanes + k, &face, &front_face, &back_face);
						
						//rdebug_set_colour(0xFF0000);
						//rdebug_rvface(&back_face);
						//rdebug_set_colour(0xFF);
						
						face = front_face;
						break;
				}
			}
			
			if(face.num_verts){
				*idx_leafs = leaf_i;
				idx_leafs++;
				
				faces[(*num_faces)++] = face;
				
				if(*num_faces == BSP_BOXCUT_MAXFACES){
					*num_faces--;
					*idx_leafs--;
					excess_faces = 1;
				}
			}
		}
	}
	
	return excess_faces;
}
