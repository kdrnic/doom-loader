//Horribly inefficient rvface drawing helpers
#ifndef RVF_DRAW_H
#define RVF_DRAW_H

#include "vector.h"

struct BITMAP;
struct MATRIX_f;

void rvface_al_clip(const rvface *in, rvface *out, int shader, float znear, float zfar);
void rvface_al_persp(rvface *f);
void rvface_al_matrix(rvface *f, const struct MATRIX_f *m);
void rvface_al_draw(struct BITMAP *bmp, int shader, struct BITMAP *tex, rvface *face);

#endif
