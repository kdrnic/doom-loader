//Function to read an entire file, in either binary or text mode

#ifndef READFILE_H
#define READFILE_H

long read_file(const char *filename, char **ptxt, char binary);

#endif
